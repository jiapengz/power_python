import socket
import struct
import time

print('Usage: UDP socket')
srvsock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
srvsock.bind( ('', 4950) )

try:
	while True:
		data, addr = srvsock.recvfrom(1024)
		print('Received', len(data), ' bytes')
		a,b, = struct.unpack('<id',data)
		print(data)
		print('Node ', a, ' receive command ', 'at ', b, ' second')
except KeyboardInterrupt:
	print('User press Ctrl+C')
	srvsock.close()
