#!/usr/bin/python
# Filename: hop2.py

import math

b = 1000000
b1 = 750000
C = 1500000
L = 4000

pkt_slot = L/b
pkt_slot1 = L/b1

on_time = 0.2
off_time = 0.3
_no = on_time/pkt_slot
td = L/C
offset = 0.002667*2

f = open('analysis1', 'w')

for i in range (1,int( (_no+1))*50):
	j = i % _no
	if j==0:
		j = _no
	arrival = ((j)-1)*pkt_slot1
	if arrival < (on_time - offset):
		queue_no = math.ceil((j-1)*pkt_slot1/pkt_slot)+(j-1)-arrival*C/L
		queuing_delay = queue_no*L/C
		delay = queuing_delay + td
		delay_list = [str(i),' ',str(delay)+'\n']
		f.write(''.join(delay_list))
	else:
		queue_no = math.ceil((on_time - offset)/pkt_slot)+(j-1)-arrival*C/L
		if queue_no < 0:
			queue_no = 0
		queuing_delay = queue_no*L/C
		delay = queuing_delay + td
		delay_list = [str(i),' ',str(delay)+'\n']
		f.write(''.join(delay_list))
f.close()
