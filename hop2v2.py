#!/usr/bin/python
# Filename: hop2v2.py

import math

b = 1000000
b1 = 750000
C = 1500000
L = 4000

pkt_slot = L/b
pkt_slot11 = L/b1
pkt_slot12 = L/C
pkt_slot13 = L/b

on_time = 0.2
off_time = 0.3
_no = on_time/pkt_slot
td = L/C
offset0 = 0.1
offset1 = 0
offset = 0.002667*2 + offset1


qmax = (on_time - offset)*(b*2-C)

if ((qmax/(C-b)<offset0) or (qmax/(C-b)==offset0)):
	remain_time2 = qmax/(C-b)
	remain_time3 = offset0-remain_time2
else:
	remain_time2 = offset0
print (remain_time2)
f = open('analysis2', 'w')
max1 = math.floor((on_time - offset0)/pkt_slot)
print (max1)
max2 = math.floor(max1+remain_time2/pkt_slot12)
for i in range (1,int( (_no+1))*50):
	j = i % _no
	if j==0:
		j = 1 
	if j<max1:
		arrival = ((j)-1)*pkt_slot11
		pkt_slot1 = pkt_slot11
	elif j<max1+remain_time2/pkt_slot12:
		arrival = max1*pkt_slot11+(j-max1-1)*pkt_slot12
		pkt_slot1 = pkt_slot12
	else:
		arrival = max1*pkt_slot11+(remain_time2)*pkt_slot12+(j-max2)*pkt_slot13
		pkt_slot1 = pkt_slot13
	if (arrival % (on_time + off_time)) < (on_time - offset):
		if j<max1:
			queue_no = math.ceil((j-1)*pkt_slot11/pkt_slot)+(j-1)-arrival*C/L
		elif j<max2:
			queue_no =  math.ceil((max1-1)*pkt_slot11/pkt_slot)+math.ceil((j-max1-1)*pkt_slot12/pkt_slot)+(j-1)-arrival*C/L

		queuing_delay = queue_no*L/C
		delay = queuing_delay + td
		delay_list = [str(i),' ',str(delay)+'\n']
		f.write(''.join(delay_list))
	else:
		queue_no = math.ceil((on_time - offset)/pkt_slot)+(j-1)-arrival*C/L
		if queue_no < 0:
			queue_no = 0
		queuing_delay = queue_no*L/C
		delay = queuing_delay + td
		delay_list = [str(i),' ',str(delay)+'\n']
		f.write(''.join(delay_list))
f.close()

