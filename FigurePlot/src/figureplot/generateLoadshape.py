'''
Created on Oct 12, 2014

@author: Zhang Jiapeng
'''
import random as rd
import math

class loadGenerate:
    
    folderName = "F:\OpenDSS_File\\"
    fileName = "newLoadshape.csv" # file to be generated
    filePath = folderName + fileName
    
    curveTime = 50000 # The time of load shape in (mili-second)
    normalTime = 60 
    dropTime = 50 # How fast the drop will be
    dropDuration = 30 
    
    lowThreshold = 0.2
    dropDegree = 1 - lowThreshold
    
    meanDropNumber = 20
    stage = "normal"
    fixDropNumber = True
    
    
    def __init__(self):
        self.currentDropNumber = 0
        self.currentLoadValue = 1.0
        self.currentTime = 0
        
        if not self.fixDropNumber:
            self.dropNumber = rd.randint(self.meanDropNumber*0.5,self.meanDropNumber*1.5)
        else:
            self.dropNumber = self.meanDropNumber
        print("The dropNumber is:",self.dropNumber)
        
        self.normalTime = int(self.curveTime/self.dropNumber - (2*self.dropTime+self.dropDuration))
        if self.normalTime <= 0:
            self.normalTime = 1
        print("The normalTime is:",self.normalTime)
    
    def openOutputFile(self):
        try:
            self.fpw = open(self.filePath,'w')
        except IOError:
            print("Cant't find the specified data.")
        else:
            print("Writing File ",self.filePath," opened successfully.")
            
    def endProcess(self):
        self.fpw.close()
        print("Done!")
        
    def genreateNormal(self):
        minEdge = math.floor(self.normalTime*0.8)
        maxEdge = math.ceil(self.normalTime*1.2)
        thisTimeDuration = rd.randint(minEdge,maxEdge)
        print("This Normal Duration =",thisTimeDuration)
        
        for i in range(thisTimeDuration):
            if self.currentTime > self.curveTime:
                break
            else:
                self.fpw.writelines("1.0"+'\n')
                self.currentTime += 1
                
        self.stage = "drop"
        
    def generateDrop(self):
        minDropTime = math.floor(self.dropTime*0.1)
        maxDropTime = math.ceil(self.dropTime*1.9)
        thisDropTime = rd.randint(minDropTime,maxDropTime)
        
        minDropDegree = math.floor(self.dropDegree*0.8)
        maxDropDegree = math.ceil(self.dropDegree*1.2)
        thisDropDegree = round(rd.uniform(minDropDegree,maxDropDegree),3)
        
        minDropDuration = math.floor(self.dropDuration*0.8)
        maxDropDuration = math.ceil(self.dropDuration*1.2)
        thisDropDuration = rd.randint(minDropDuration,maxDropDuration)
        print("Drop Time =",thisDropTime,"Drop Degree =",thisDropDegree,"Drop Duration =",thisDropDuration)
        
        loadValue = 0
        
        for i in range(thisDropTime):
            if self.currentTime > self.curveTime:
                break
            
            loadValue = self.currentLoadValue-(self.currentLoadValue-thisDropDegree)*(i+1)/thisDropTime
            loadValue = round(loadValue,3)
            
            if loadValue < 0:
                loadValue = 0
            elif loadValue > 1.0:
                loadValue = 1.0
            
            self.fpw.writelines(str(loadValue)+'\n')
            self.currentTime += 1
            
        for i in range(thisDropDuration):
            if self.currentTime > self.curveTime:
                break
            
            self.fpw.writelines(str(loadValue)+'\n')
            self.currentTime += 1
            
        for i in range(thisDropTime):
            if self.currentTime > self.curveTime:
                break
            
            loadValue = self.currentLoadValue-(self.currentLoadValue-thisDropDegree)*(thisDropTime-i)/thisDropTime
            loadValue = round(loadValue,3)
            
            if loadValue < 0:
                loadValue = 0
            elif loadValue > 1.0:
                loadValue = 1.0
            
            self.fpw.writelines(str(loadValue)+'\n')
            self.currentTime += 1
        
        self.currentDropNumber += 1
        self.stage = "normal"
        
    def generationStart(self):
        self.openOutputFile()
        
        while True:
            if self.currentTime > self.curveTime:
                break
            if self.stage == "normal":
                self.genreateNormal()
            elif self.stage == "drop":
                if self.currentDropNumber < self.dropNumber:
                    self.generateDrop()
                else:
                    self.stage = "normal"
        
        self.endProcess()

T = loadGenerate()
T.generationStart()

