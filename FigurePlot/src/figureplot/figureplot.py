'''
Created on Oct 6, 2014

@author: Zhang Jiapeng
'''
import string 
import numpy as np 
import matplotlib.pyplot as plt 

folderName = "F:\\cosim\\"
fileName = "cpptable.csv"
myPath = folderName + fileName

#print(myPath)

f = open(myPath)
# for i in range(5):
#     content = f.readline()
#     time = content.split(',')[2]
#     voltage = content.split(',')[3]
#     print(time, voltage)
startTime = 1
endTime = 2
plotTime = []
plotData = []
while True:
    content = f.readline()
    timeString = content.split(',')[2]
    time = float(timeString)
    if time >= startTime and time <= endTime:
        voltage = content.split(',')[3]
        plotData.append(float(voltage))
        plotTime.append(time)
    elif time > endTime:
        break

plt.plot(plotTime,plotData)

plt.title("Voltage vs. Time")
plt.xlabel("Time (s)")
plt.ylabel("Voltage (V)")

plt.show()
