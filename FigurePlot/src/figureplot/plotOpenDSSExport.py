'''
Created on Oct 13, 2014

@author: Zhang Jiapeng
'''
import matplotlib.pyplot as plt 
import copy

folderName = "F:\\OpenDSS_File\\13BusSolar\\"
# "cpptable_2voip.csv"
# "cpptable_nocrossing.csv"
# "cpptable_2voip1video.csv"
# "cpptable_qos10.csv"
fileNameList = ["IEEE13Nodeckt_Mon_ckt7_mon_5.csv"]
labelList = ["voltage_output","qos-3%","qos-10%"]
lineStyleList = ['-','--','-.',':']
myPathList = []

for i in range(len(fileNameList)):
    myPathList.append(folderName + fileNameList[i])
    
print("The required files are: ",myPathList)
fileHandlerList = []

# Open files
for i in range(len(myPathList)):
    try:
        f = open(myPathList[i])
    except IOError:
        print("Cant't find the specified data.")
    else:
        print("File ",myPathList[i]," opened successfully.")
        fileHandlerList.append(f)

startTime = 0.0
endTime = 75.0

plotTime = []
plotData = []
tempTimeList = []
tempDataList = []

# Collect data and store in plot
for i in range(len(fileHandlerList)):
    del tempTimeList[:]
    del tempDataList[:]
    fileHandlerList[i].readline() # The first line is title
    
    while True:
        content = fileHandlerList[i].readline()
        if not content:
            break
        timeString = content.split(',')[1]
        time = float(timeString)
        if time >= startTime and time <= endTime:
            voltage = content.split(',')[2]
            tempDataList.append(float(voltage))
            tempTimeList.append(time)
        elif time > endTime:
            break
    
    plotTime.append(copy.deepcopy(tempTimeList))
    plotData.append(copy.deepcopy(tempDataList))
    
# set label
for i in range(len(fileHandlerList)):
    length = len(lineStyleList)
    tempIndex = (i+length)%length
    plt.plot(plotTime[i],plotData[i],lineStyleList[tempIndex],label=labelList[i],linewidth=i+1)

plt.grid()
#plt.legend(loc="upper left")
plt.show()

# Close files
for i in range(len(fileHandlerList)):
    fileHandlerList[i].close()
