'''
Created on Oct 16, 2014

@author: Zhang Jiapeng
'''

folderName = "F:\OpenDSS_File\Examples\Loadshapes\\"
fileName = "PVLoadshape-1sec-2900pts.csv" # file to be generated
filePath = folderName + fileName

resultFileName = "new-" + fileName
resultFilePath = folderName + resultFileName

try:
    fpr = open(filePath)
except IOError:
    print("Cant't find the specified data.")
else:
    print("Writing File ",filePath," opened successfully.")
    
try:
    fpw = open(resultFilePath,'w')
except IOError:
    print("Cant't find the specified data.")
else:
    print("Writing File ",resultFilePath," opened successfully.")
    

startPoint = 1000
numberOfPoint = 100
counter = 0
dataList = []

while True:
    fileContent = fpr.readline()
    if not fileContent:
        break
    elif fileContent.isspace():
        continue
    counter += 1
    if counter >= startPoint:
        newData = float(fileContent) - 0.43
        newData = round(newData,6)
        dataList.append(str(newData))
    if counter > (startPoint + numberOfPoint):
        break

for i in range(len(dataList)):
    fpw.writelines(dataList[i]+'\n')
    
fpw.close()
print("Done!")