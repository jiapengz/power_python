'''
Created on Oct 6, 2014

@author: Zhang Jiapeng
'''
import matplotlib.pyplot as plt 
import copy

folderName = "F:\\cosim\\"
# "cpptable_2voip.csv"
# "cpptable_nocrossing.csv"
# "cpptable_2voip1video.csv"
# "cpptable_qos10.csv"
fileNameList = ["cpptable_20Dfx2.csv","cpptable_20Dqoss.csv","cpptable_fifo20dss.csv"]
labelList = ["E(drop_number) = 30","drop time = 10s","drop time = 20s"]
lineStyleList = ['-','--','-.',':']
myPathList = []

# Form the full path of the data files
for i in range(len(fileNameList)):
    myPathList.append(folderName + fileNameList[i])
    
print("The required files are: ",myPathList)
fileHandlerList = []

# Open files
for i in range(len(myPathList)):
    try:
        f = open(myPathList[i])
    except IOError:
        print("Cant't find the specified data.")
    else:
        print("File ",myPathList[i]," opened successfully.")
        fileHandlerList.append(f)

startTime = 0.0
endTime = 95.0

plotTime = []
plotData = []
reactTime = []
tempTimeList = []
tempDataList = []
tempReactList = []

# Collect data and store in plot
for i in range(len(fileHandlerList)):
    del tempTimeList[:]
    del tempDataList[:]
    del tempReactList[:]
    changeStartTime = 0
#     unnormalStage = 0
    currentStage = "normal"
    
    while True:
        content = fileHandlerList[i].readline()
        if not content:
            break
        timeString = content.split(',')[2]
        voltageString = content.split(',')[3]
        time = float(timeString)
        voltage = float(voltageString)
        if time >= startTime and time <= endTime:
            tempDataList.append(voltage)
            tempTimeList.append(time)
            
            if voltage < 2350:
                if currentStage == "normal":
                    changeStartTime = time
                    currentStage = "low"
                elif currentStage == "high":
                    changeStartTime = time
                    currentStage = "low"
            elif voltage >= 2350 and voltage <= 2450:
                if currentStage != "normal":
                    tempReactList.append((time-changeStartTime))
                    currentStage = "normal"
            elif voltage > 2450:
                if currentStage == "normal":
                    changeStartTime = time
                    currentStage = "high"
                elif currentStage == "low":
                    changeStartTime = time
                    currentStage = "high"  
            
        elif time > endTime:
            break
    
    plotTime.append(copy.deepcopy(tempTimeList))
    plotData.append(copy.deepcopy(tempDataList))
    reactTime.append(copy.deepcopy(tempReactList))
    

# Figure needs to be set first
#plt.figure(figsize=(12,6), frameon = False )

# set label
for i in range(len(fileHandlerList)):
    length = len(lineStyleList)
    tempIndex = (i+length)%length
    plt.plot(plotTime[i],plotData[i],lineStyleList[tempIndex],label=labelList[i],linewidth=i+1)
    
for i in range(len(reactTime)):
    print(labelList[i],": ")
    for j in range(len(reactTime[i])):
        print(reactTime[i][j],", ")

plt.grid()
plt.legend(loc="upper left")
plt.title("Voltage vs. Time")
plt.xlabel("Time (s)")
plt.ylabel("Voltage (V)")
plt.show()

# Close files
for i in range(len(fileHandlerList)):
    fileHandlerList[i].close()
