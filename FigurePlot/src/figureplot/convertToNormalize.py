'''
Created on Oct 9, 2014

@author: Zhang Jiapeng
'''

folderName = "F:\\OpenDSS_File\\13BusSolar\\"
fileName = "1-day-1-s-Solar-1.csv"
filePath = folderName + fileName

resultFileName = "Normalized-" + fileName
resultFilePath = folderName + resultFileName

try:
    fpr = open(filePath)
except IOError:
    print("Cant't find the specified data.")
else:
    print("File ",filePath," opened successfully.")

try:
    fpw = open(resultFilePath,'w')
except IOError:
    print("Cant't find the specified data.")
else:
    print("Writing File ",resultFilePath," opened successfully.")
    
maxNumber = 0
minNumber = 0
contentList = []
    
while True:
    fileContent = fpr.readline()
    if not fileContent:
        break
    elif fileContent.isspace():
        continue
    contentList.append(fileContent)
    num = float(fileContent)
    maxNumber = maxNumber if maxNumber > num else num
    minNumber = minNumber if minNumber < num else num 

print("Max: ",maxNumber,"; min: ",minNumber)
print("Start writing to new files.")
for i in range(len(contentList)):
    temp = (float(contentList[i]) - minNumber) / (maxNumber - minNumber)
    temp = round(temp,3)
    fpw.writelines(str(temp)+'\n')

fpr.close()
fpw.close()     
print("Done!")
