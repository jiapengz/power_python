'''
Created on Mar 25, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
from SPT import calSPT
from SPT import calSPT2
from copy import deepcopy
import SPT.IEEE39Bus_Setting as IS_39
import setuptools
import random

def testFunction(graph):
    global bus_ip, line_ip, relay_ip 
    for es in graph.es:
        print("Line "+str(es.index)+':',"name is",es["name"]+',',"line failure P:",es["line_s_pf"])
        for relay in es["prRelaySet"]:
            print("relay ID:",str(relay.relayIdx)+',','averaged line pf:',relay.avg_line_pf_req,\
                  'line_s_pf:',relay.pr_line_s_pf,'    ',end='')
        print()
    
    print(bus_ip)
    print(set(bus_ip))
    
    
def list_trivial_relay(graph):
    result = []
    for vs in graph.vs:
        for relay in vs["relaySet"]:
            if relay.relayIdx not in relay_ip:
                result.append(relay.relayIdx)
    print(result)



# Set up the failure probability and requirement for bus/line/relay
def set_system_probability(graph):
    global line_s_pf_dict
    graph["s_req"] = 0.000001
    graph["pf_hf"] = 1
    for keyIdx in line_s_pf_dict:
        graph.es[keyIdx-1]["line_s_pf"] = line_s_pf_dict[keyIdx]
    
    for es in graph.es:
        if es["prRelaySet"] != []:
            for relay in es["prRelaySet"]:
                relay.pr_line_s_pf = es["line_s_pf"]
                if relay.pr_line_s_pf != 0:
#                 effective_relay_no = graph["total_relay_no"]
                    effective_relay_no = 30
                    relay.avg_line_pf_req = round(graph["s_req"]/effective_relay_no,12)


# General initialization, find the MA in the graph and calculate the delay requirement for each relay.
# The division of the delay would be performed later in other function.
def graph_initialization(graph):
    graph.es["weight"] = 1
    calSPT.setRelayNo(graph,34)
    set_system_probability(graph)
#     IS_39.set_link_reliability(graph)
#     IS_39.set_link_reliability_random(graph)
    
        
    calSPT.findBackupRelayGraph(graph,34)
    calSPT.find_Zone3_relay_Same_bus(graph)
    calSPT.update_min_relay_req_at_a_bus(graph)
    
#     testFunction(graph)
    print()



def set_primary_path(graph,primary_type='normal'):
    SPDistance = graph.shortest_paths_dijkstra(weights=graph.es["weight"])
    optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance) #This is the best MA location
    
    idealPos = 0
    print("Optimal locations:")
    for index in optimalNodeIndex:
        idealPos = index
        print("Bus",graph.vs[index]["name"])
    
    if primary_type == 'normal':
        calSPT.findPrimaryPath_All_Nodes(graph,optimalNodeIndex,'lw_equal',plotEN=False)
    elif primary_type == 'max_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    elif primary_type == 'equal_div_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    elif primary_type == 'sp_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    
    calSPT.calDelayReq_PR_Graph(graph,34)



def staticRSV_and_backup_path(graph,maxEdgeIdx,pktSize,rtnList,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                                 prList=[],bpList=[]):
    print("Use delayType",'\"'+delayType+'\"')
    calSPT.do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
#     calSPT.showRSV_PR(graph)
    print()
    
    calSPT.find_backup_path_from_power(graph, "16", True, delayType, dstSft, powft)
    #Delay division on each hop for backup path
    calSPT.do_Perhop_DelayReq_Simple_BP(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT2.staticRSV_BP_New(graph, pktSize)
#     calSPT.showRSV_BP_Overall(graph)
    print()
    calSPT.showRSV_Overall(graph)



def loop_test():
    delay_type_choice = ['equal','distance','load']
    for dt in delay_type_choice:
        print(dt)
        copyGraph = deepcopy(myGraph)
        staticRSV_and_backup_path(copyGraph, 34, 80, [], 'dist', dt, 0, 1, [], [])



def staticRSV_and_backup_path_v2(graph,maxEdgeIdx,pktSize,rtnList,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                                 prList=[],bpList=[]):
    print("Use delayType",'\"'+delayType+'\"')
    graph.es[18]["capacity"] = 550000 # Test to decrease the CA of a link
#     graph.es[19]["capacity"] = 450000 # Test to decrease the CA of a link
#     graph.es[20]["capacity"] = 700000 # Test to decrease the CA of a link
#     graph.es[24]["capacity"] = 550000 # Test to decrease the CA of a link
    calSPT.do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    calSPT.showRSV_PR(graph)
    print()
    
    calSPT.find_backup_path_from_power(graph, "16", True, delayType, dstSft, powft)
    #Delay division on each hop for backup path
    calSPT.do_Perhop_DelayReq_Simple_BP(graph, maxEdgeIdx, delayType, dstSft, powft)
    
    
#     calSPT2.staticRSV_BP_New_order(graph, pktSize, relay_ip)
#     calSPT2.staticRSV_BP_New_order(graph, pktSize, reverse_relay_ip)
#     calSPT2.staticRSV_BP_New_order(graph, pktSize, [])
    random.shuffle(global_temp_ip)
    print("Shuffled list:",global_temp_ip)
    calSPT2.staticRSV_BP_New_order(graph, pktSize, global_temp_ip)
#     calSPT.showRSV_BP_Overall(graph)
    print()
    calSPT.showRSV_Overall(graph)
    result = calSPT.print_rsv_overall_in_list(graph,True)
    rtnList.append(result)
    calSPT2.check_not_rsv_relay(graph)
    pf = calSPT.compute_system_pf_no_backup_ca(graph)
    pf_only_pr = calSPT.compute_system_pf_only_pr(graph)
    pf_with_bp = calSPT.compute_system_pf_with_bp(graph)
    print("Failure probability due to insufficient CA:",pf)
    print("Failure probability due to primary path:",pf_only_pr)
    print("Failure probability due with backup path:",pf_with_bp)



def staticRSV_bp_for_each_relay(graph,maxEdgeIdx,pktSize,rtnList,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                                 prList=[],bpList=[]):
    print("Use delayType",'\"'+delayType+'\"')
    calSPT.do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    calSPT.showRSV_PR(graph)
    print()
    
#     graph.es[18]["capacity"] = 550000 # Test to decrease the CA of a link
    calSPT.find_backup_dv_rsv_each_relay(graph, "16", maxEdgeIdx, relay_ip, delayType, dstSft, powft)
    print()
    calSPT.showRSV_Overall(graph)
    calSPT2.check_not_rsv_relay(graph)
    pf_with_bp = calSPT2.compute_system_pf_with_bp_each_relay(graph)
    print("Failure probability due with backup path:",pf_with_bp)



def staticRSV_and_backup_path_single_line(graph,maxEdgeIdx,pktSize,rtnList,bpType='dist',delayType='equal',\
                                          dstSft=0,powft=1,prList=[],bpList=[]):
    print("Use delayType",'\"'+delayType+'\"')
    calSPT.do_Perhop_DelayReq_PR_Single_Line_Fluctuation(graph, delayType, dstSft, powft)
    calSPT.staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    calSPT.showRSV_PR(graph)
    print()
    
    calSPT.find_backup_path_from_power(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    #Delay division on each hop for backup path
    calSPT.do_Perhop_DelayReq_BP_Single_Line_Fluctuation(graph, delayType, dstSft, powft)
#     graph.es[18]["capacity"] = 550000 # Test to decrease the CA of a link
    calSPT2.staticRSV_BP_New_order(graph, pktSize, relay_ip)
#     calSPT2.staticRSV_BP_New_order(graph, pktSize, reverse_relay_ip)
#     calSPT.showRSV_BP_Overall(graph)
    print()
    calSPT.showRSV_Overall(graph)
    calSPT.print_rsv_overall_in_list(graph,True)
    calSPT2.check_not_rsv_relay(graph)
    pf = calSPT.compute_system_pf_no_backup_ca(graph)
    pf_only_pr = calSPT.compute_system_pf_only_pr(graph)
    pf_with_bp = calSPT.compute_system_pf_with_bp(graph)
    print("Failure probability due to insufficient CA:",pf)
    print("Failure probability due to primary path:",pf_only_pr)
    print("Failure probability due with backup path:",pf_with_bp)
    print()



def single_evaluation(graph):
    graph_initialization(graph)
    set_primary_path(myGraph, 'normal')
#     staticRSV_and_backup_path(graph, 34, 80, [], 'dist', 'equal', 0, 1, [], [])
#     loop_test()
#     staticRSV_and_backup_path_v2(graph, 34, 80, [], 'dist', 'equal', 0, 1, [], [])
#     list_trivial_relay(graph)
#     staticRSV_bp_for_each_relay(graph, 34, 80, [], 'dist', 'equal', 0, 1, [], [])
    staticRSV_and_backup_path_single_line(graph, 34, 80, [], 'dist', 'equal', 0, 1, [], [])



# Compare different scheme of primary path
def compare_primary_scheme(graph):
    resultList = []
    graph_initialization(graph)
    copyGraph = deepcopy(graph)
    copyGraph2 = deepcopy(graph)
    set_primary_path(graph, 'normal')
    staticRSV_and_backup_path_v2(graph, 34, 80, resultList, 'dist', 'equal', 0, 1, [], [])
    
    print("Copy Graph Test 1")
    set_primary_path(copyGraph, 'max_reliability')
    staticRSV_and_backup_path_v2(copyGraph, 34, 80, resultList, 'dist', 'equal', 0, 1, [], [])
    
    print("Copy Graph Test 2")
    set_primary_path(copyGraph2, 'sp_reliability')
    staticRSV_and_backup_path_v2(copyGraph2, 34, 80, resultList, 'dist', 'equal', 0, 1, [], [])
    
    calSPT.plotLinkUtil_From_List(resultList, [1,2,3], '1', '2', '3', ymax=1000, ymin=0)
    
    
def test_rsv_order(graph):
    resultList = []
    graph_initialization(graph)
    set_primary_path(graph, 'normal')
    staticRSV_and_backup_path_v2(graph, 34, 80, resultList, 'dist', 'equal', 0, 1, [], [])
    


# Start the initialization of the graph
myGraph = ig.Graph()
myGraph.add_vertices(39)

for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"

myGraph.add_edge("1", "2")
myGraph.add_edge("1", "39")
myGraph.add_edge("2", "3")
myGraph.add_edge("2", "25")
myGraph.add_edge("3", "4")
myGraph.add_edge("3", "18")
myGraph.add_edge("4", "5")
myGraph.add_edge("4", "14")
myGraph.add_edge("5", "6")
myGraph.add_edge("5", "8")
myGraph.add_edge("6", "7")
myGraph.add_edge("6", "11")
myGraph.add_edge("7", "8")
myGraph.add_edge("8", "9")
myGraph.add_edge("9", "39")
myGraph.add_edge("10", "11")
myGraph.add_edge("10", "13")
myGraph.add_edge("13", "14")
myGraph.add_edge("14", "15")
myGraph.add_edge("15", "16")
myGraph.add_edge("16", "17")
myGraph.add_edge("16", "19")
myGraph.add_edge("16", "21")
myGraph.add_edge("16", "24")
myGraph.add_edge("17", "18")
myGraph.add_edge("17", "27")
myGraph.add_edge("21", "22")
myGraph.add_edge("22", "23")
myGraph.add_edge("23", "24")
myGraph.add_edge("25", "26")
myGraph.add_edge("26", "27")
myGraph.add_edge("26", "28")
myGraph.add_edge("26", "29")
myGraph.add_edge("28", "29")

#non-transmission line
myGraph.add_edge("6", "31")
myGraph.add_edge("10", "32")
myGraph.add_edge("11", "12")
myGraph.add_edge("12", "13")
myGraph.add_edge("19", "20")
myGraph.add_edge("19", "33")
myGraph.add_edge("20", "34")
myGraph.add_edge("23", "36")
myGraph.add_edge("29", "38")
myGraph.add_edge("30", "2")
myGraph.add_edge("35", "22")
myGraph.add_edge("37", "25")

#Added backup link
myGraph.add_edge("19","21")

for es in myGraph.es:
    es["name"] = str(es.index + 1) #here the name for a line is of type "string"

# Priority order for bus/line/relay
# Caution: the index in graph is "real index - 1" (start from 0)
# Here we use the "real index"
line_ip = [22,27,3,31,14,15,21,5,6,7,9,12,19,23,34]
bus_ip = [19,16,21,22,2,3,26,27,8,9,39,17,4,18,5,6,11,14,15,28,29]
relay_ip = [44,43,54,53,6,5,62,61,28,27,30,29,42,41,10,9,12,11,14,13,18,17,24,23,38,37,46,45,68,67]
#line_s_pf_dict = {22: 0.042, 27: 0.0115, 3: 0.0115, 31: 0.007, 14: 0.0024, 15: 0.0024, 21: 0.0016, \
#                  5: 0.0013, 6: 0.0013, 7: 0.0013, 9: 0.0013, 12: 0.0013, 19: 0.0013, 23: 0.0013, 34: 0.0013}
line_s_pf_dict = {22: 0.042, 27: 0.0115, 3: 0.0115, 31: 0.007, 14: 0.0017, 15: 0.0017, 21: 0.0017, \
                  5: 0.0008, 6: 0.0008, 7: 0.0008, 9: 0.0008, 12: 0.0008, 19: 0.0008, 23: 0.0008, 34: 0.0008}
reverse_relay_ip = [1, 3, 2, 7, 15, 19, 21, 22, 25, 20, 26, 31, 33, 32, 34, 35, 16, 36, 39, 40, 47, 49, 51, \
                    50, 55, 56, 57, 48, 58, 8, 59, 60, 63, 65, 52, 64, 66, 4, 67, 68, 45, 46, 37, 38, 23, 24, 17, \
                    18, 13, 14, 11, 12, 9, 10, 41, 42, 29, 30, 27, 28, 61, 62, 5, 6, 53, 54, 43, 44]

global_temp_ip = [1, 3, 2, 7, 15, 19, 21, 22, 25, 20, 26, 31, 33, 32, 34, 35, 16, 36, 39, 40, 47, 49, 51, \
                    50, 55, 56, 57, 48, 58, 8, 59, 60, 63, 65, 52, 64, 66, 4, 67, 68, 45, 46, 37, 38, 23, 24, 17, \
                    18, 13, 14, 11, 12, 9, 10, 41, 42, 29, 30, 27, 28, 61, 62, 5, 6, 53, 54, 43, 44]



# Here we start the evaluation
# single_evaluation(myGraph)

test_rsv_order(myGraph)
# compare_primary_scheme(myGraph)