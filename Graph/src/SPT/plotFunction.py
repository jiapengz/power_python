'''
Created on Feb 18, 2015

@author: Zhang Jiapeng
'''

import matplotlib.pyplot as plt
import numpy as np

marker = ['D', #diamond marker 
's', #square marker 
'^', #triangle_up marker 
'p', #pentagon marker 
'*', #star marker 
'h', #hexagon1 marker 
'o', #circle marker 
'v', #triangle_down marker  
'<', #triangle_left marker 
'>', #triangle_right marker 
'H', #hexagon2 marker
'.', #point marker 
'+', #plus marker 
'x', #x marker 
'd', #thin_diamond marker 
'|', #vline marker 
'_', #hline marker 
'1', #tri_down marker 
'2', #tri_up marker 
'3', #tri_left marker 
'4', #tri_right marker 
',', #pixel marker 
]

color = ['b', #blue 
'g', #green 
'r', #red 
'c', #cyan 
'm', #magenta 
'y', #yellow 
'k', #black 
#'w', #white 
]

lineStyle = ['-', #solid 
'--', #dashed 
'-.', #dash_dot 
':', #dotted 
]

def myPlotFig(xData,yData,title,xLabel,yLabel,lty='bs-',figLabel='',annot=False,yMax='none',yMin='none'):
    if xData != []:
        plt.plot(xData,yData,lty,label=figLabel,markersize=5)
    else:
        plt.plot(yData,lty,label=figLabel,markersize=5)
#     minNum = min(yData)
    if annot == True:
        for i in range(len(xData)): # Add annotation
            xyStr = '%.1e' % (round(yData[i],8))
            plt.annotate(xyStr, xy = (xData[i], yData[i]), xytext = (xData[i], yData[i]))
    plt.grid()
    if figLabel != '':
        plt.legend(loc="best")
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    if yMin != "none":
        plt.ylim(ymin=yMin)
    if yMax != "none":
        plt.ylim(ymax=yMax)
    plt.show()


def myPlotFig_List(utilList,labelNameList,graphTitle,x_label,y_label,enableX=False,\
                           enableSci=False,ymax="none",ymin="none",titleSuffix=''):
    global marker
    global color
    
    markerLen = len(marker)
    colorLen = len(color)
    lsLen = len(lineStyle)
    markerSize = 10
    
#     if enableX == True: # we set X-axis to start from 1
#         max_size = 0
#         max_idx = 0
#         for i in range(len(utilList)):
#             if len(utilList[i]) > max_size:
#                 max_idx = i
#         for i in range(len(utilList[max_idx])):
#             x_axis.append(i+1)
    
    for idx in range(len(utilList)):
        x_axis = []
        ls = marker[idx % markerLen] + color[idx % colorLen] + lineStyle[idx % lsLen]
        if enableX == True:
            for i in range(len(utilList[idx])):
                x_axis.append(i+1)
            plt.plot(x_axis,utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
        else:
            plt.plot(utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
    
    if titleSuffix != '':
        myGraphTitle = titleSuffix + ': '+ graphTitle
    else:
        myGraphTitle = graphTitle
    
    plt.grid()
    plt.legend(loc="best")
    plt.title(myGraphTitle)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    
    if ymin != "none":
        plt.ylim(ymin=ymin)
    if ymax != "none":
        plt.ylim(ymax=ymax)
    
    if enableSci == True:
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.show()
    

# In this function the X-Axis is coming from the input variable list
def myPlotFig_List_WithX(xList,utilList,graphTitle,x_label,y_label,labelNameList,enableX=False,\
                           enableSci=False,ymax="none",ymin="none",titleSuffix='',legend_p="none"):
    global marker
    global color
    
    markerLen = len(marker)
    colorLen = len(color)
    lsLen = len(lineStyle)
    markerSize = 10
    
    
    for idx in range(len(utilList)):
        x_axis = []
        ls = marker[idx % markerLen] + color[idx % colorLen] + lineStyle[idx % lsLen]
        if enableX == True:
            for i in range(len(utilList[idx])):
                x_axis.append(i+1)
            plt.plot(x_axis,utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
        else:
            plt.plot(xList,utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
    
    if titleSuffix != '':
        myGraphTitle = titleSuffix + ': '+ graphTitle
    else:
        myGraphTitle = graphTitle
    
    plt.grid()
    plt.title(myGraphTitle)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    
    if ymin != "none":
        plt.ylim(ymin=ymin)
    if ymax != "none":
        plt.ylim(ymax=ymax)
    if legend_p == "outside":
        ax = plt.gca()
        ax.set_position([0.1, 0.1, 0.75, 0.75])
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5), prop={'size':10})
    else:
        plt.legend(loc="best")
    
    if enableSci == True:
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.show()


# In this function the X-Axis is not "number", but text
def myPlotFig2(xData,yData,title,xLabel,yLabel,lty='bs-',figLabel='',annot=False,yMax='',yMin=''):
    
    xLen = len(xData)
    barIndex=np.arange(xLen)
    bar_width = 0.35
    
    if annot == True:
        for i in range(len(xData)): # Add annotation
            xyStr = '%.1e' % (round(yData[i],8))
            plt.annotate(xyStr, xy = (xData[i], yData[i]), xytext = (xData[i], yData[i]))
    
    plt.plot(yData,lty,markersize=10)
    plt.grid()
    if figLabel != '':
        plt.legend(loc="best")
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.xticks(barIndex, xData) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width)
    plt.tight_layout()
    if yMax != '' and yMin != '':
        plt.ylim(yMin,yMax)
    plt.show()


def myPlotFig2_List(xData,yDataList,title,xLabel,yLabel,figLabelList='',yMax='',yMin=''):
    
    xLen = len(xData)
    barIndex=np.arange(xLen)
    bar_width = 0.35
    
    markerLen = len(marker)
    colorLen = len(color)
    lsLen = len(lineStyle)
    
    for idx in range(len(yDataList)):
        lty = marker[idx % markerLen] + color[idx % colorLen] + lineStyle[idx % lsLen]
        plt.plot(yDataList[idx],lty,label=figLabelList[idx],markersize=10)
        
    plt.grid()
    if figLabelList != '':
        plt.legend(loc="best")
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
    plt.xticks(barIndex, xData) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width)
    plt.tight_layout()
    if yMax != '' and yMin != '':
        plt.ylim(yMin,yMax)
    plt.show()

