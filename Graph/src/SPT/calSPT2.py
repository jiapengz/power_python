'''
Created on Mar 16, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
import numpy as np
from copy import deepcopy
import itertools as itls


def weight_function_load_new(graph,relay,epathList,vpathList,relayDR_Table,powft,pathType):
    tempSum = 0 # for temporary use
    tempSum2 = 0 # for temporary use
    tempWeightList = [] # for temporary use
    
    for linkIdx in epathList: # compute the total free_weight
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
#         print(linkIdx,"Link",linkIdx+1,name1)
        
        if graph.es[linkIdx]["RSV_temp"][name1] >= graph.es[linkIdx]["capacity"]:
            print('weight_function_load_new: Capacity not enough! '+'for link '+str(linkIdx+1)+' of relay',relay.relayIdx)
            if pathType == "PRIMARY":
                relay.no_rsv_pr = 1
            elif pathType == "BACKUP":
                relay.no_rsv_bp = 1
            else:
                raise NameError("Unknown path type.")
            return -1
        
        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
        tempSum += pow(free_weight,powft)
        
    # Next assign link weight based on free_weight and compute total link weight
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        
        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
        current_link_weight = tempSum/pow(free_weight,powft)
        tempWeightList.append(current_link_weight)
        tempSum2 += current_link_weight # total sum weight
        
#                     print(tempWeightList,tempSum2)
    # compute delay division on each hop/link and update the "temporary RSV"
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        if linkIdx not in relayDR_Table:
            relayDR_Table[linkIdx] = {}
        
        current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
        relayDR_Table[linkIdx][name1] = current_hop_DR
        relayDR_Table[linkIdx][name2] = current_hop_DR
        relayDR_Table[linkIdx]["nonDirection"] = current_hop_DR
        
        # Check the link utilization for all links of a path
        # If one link is "full", return without "pre-reservation"
        # Since currently the reservation is Bi-directional, we can check just one element of the RSV
        ca_req_nd = 80*8/relayDR_Table[linkIdx][name1]
        if graph.es[linkIdx]["RSV_temp_ND"] + ca_req_nd > graph.es[linkIdx]["capacity"]:
            print('weight_function_load_new: Capacity not enough! '+'for link '+str(linkIdx+1)+' of relay',relay.relayIdx)
            if pathType == "PRIMARY":
                relay.no_rsv_pr = 1
            elif pathType == "BACKUP":
                relay.no_rsv_bp = 1
            else:
                raise NameError("Unknown path type.")
            return -1
        
    # Update the link utilization
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relayDR_Table[linkIdx][name1]
        graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relayDR_Table[linkIdx][name2]
        graph.es[linkIdx]["RSV_temp_ND"] += 80*8/relayDR_Table[linkIdx][name1]
#         print("Comparison:",graph.es[linkIdx]["RSV_temp"][name1],graph.es[linkIdx]["RSV_temp"][name2],\
#               graph.es[linkIdx]["RSV_temp_ND"])
#     print("completed")



# In this function, for each relay in the system we assume it has only one backup path.
# We calculate the required reservation on each link of the path.     
# If a backup path has overlapping links with the primary path, we only reserve once.
def staticRSV_BP_New(graph,pktSize):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.no_rsv_bp != 0:
                continue
            
            for linkIdx in vs_i["BP_RVS_ES_path"]:
                idx = vs_i["BP_RVS_ES_path"].index(linkIdx)
                
                src = vs_i["BP_RVS_VS_path"][idx+1]
                dst = vs_i["BP_RVS_VS_path"][idx]
                
                name1 = str(src)+'_'+str(dst)
                name2 = str(dst)+'_'+str(src)
                
                # Check if the link is also in the primary path of the relay
#                 if linkIdx in vs_i["PR_RVS_ES_path"]:
#                     print("Overlapping path!")
#                 else:
#                     print("Non-overlapping path!")
                
#                 graph.es[linkIdx]["RSV_BP"][name1] += pktSize*8/relay.delay_per_link_BP
#                 graph.es[linkIdx]["RSV_BP"][name2] += pktSize*8/relay.delay_per_link_BP
                ca_req_1 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name1]
                ca_req_2 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name2]
                ca_req_nd = pktSize*8/relay.per_hop_dr_BP[linkIdx]["nonDirection"]
                
                if linkIdx not in vs_i["PR_RVS_ES_path"]:
                    if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
                    if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
                    if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                        graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
                        relay.used_link_RSV_BP[linkIdx] = ca_req_nd
                    else:
                        relay.no_rsv_bp = 1
                        print("Relay",relay.relayIdx,"-> insufficient BACKUP capacity")
                else:
#                     print("Overlapped link "+str(linkIdx+1)," for relay",relay.relayIdx)
                    pr_link_rsv = pktSize*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
                    if ca_req_nd > pr_link_rsv:
                        ca_req_1 = ca_req_1 - pr_link_rsv
                        ca_req_2 = ca_req_2 - pr_link_rsv
                        ca_req_nd = ca_req_nd - pr_link_rsv
                        if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                            graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
                        if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                            graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
                        if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                            graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                            graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
                            relay.used_link_RSV_BP[linkIdx] = ca_req_nd
                        else:
                            relay.no_rsv_bp = 1
                            print("Relay",relay.relayIdx,"-> insufficient BACKUP capacity")



# Select a path with max reliability. This function is used for selecting
# in the primary part.
def select_path_max_reliability(link_path_list,vs,graph):
    min_path_pf = 1
    result = -1
    for i in range(len(link_path_list)):
        path_pf = 0
        for linkIdx in link_path_list[i]:
            path_pf += graph.es[linkIdx]["pf"]
        
        if path_pf < min_path_pf:
            min_path_pf = path_pf
            result = i
            
    return result


# Select a path from the shortest path, but also look at reliability. This function is used for selecting
# in the primary part. (We might not select the Shortest or Most Reliable path, but consider both factors.)
# The input parameter list is already sorted in increasing order of hop count.
def select_path_SP_reliability(link_path_list,vs,graph):
    result = 0
    current_pf_imp = 0
    base_path = link_path_list[0]
    path_f_1 = 0
    path_len_1 = len(base_path)
    for linkIdx in base_path:
        path_f_1 += graph.es[linkIdx]["pf"]
    
    for i in range(len(link_path_list)):
        path_f_2 = 0
        for linkIdx in link_path_list[i]:
            path_f_2 += graph.es[linkIdx]["pf"]
        
        path_len_2 = len(link_path_list[i])
        delta_path_len = path_len_2 - path_len_1
        pf_improve = (path_f_1 - path_f_2)/path_f_1
        pf_improve = max(0,pf_improve)
        
        if delta_path_len <= 3 and pf_improve >= 0.5 and pf_improve > current_pf_imp:
            print("Bus",vs["name"],"chooses a path rather than SP,","index is",i," -> SP path:",base_path)
            result = i
            current_pf_imp = pf_improve
        
    return result

# Only count the probability issue to select a backup path
def select_path_based_on_power(link_path_list,vs,exampt_path,graph):
    max_line_s_pf = 0
    avg_line_pf_req = 0
    min_path_pf = 1
    min_index = -1
    result = -1
    for relay in vs["relaySet"]:
        if relay.pr_line_s_pf > max_line_s_pf:
            max_line_s_pf = relay.pr_line_s_pf
            avg_line_pf_req = relay.avg_line_pf_req
    
    if max_line_s_pf == 0 or avg_line_pf_req == 0:
#         print("Bus_"+vs["name"]+" does not have failure history.")
        return 0
    
    p_hf = graph["pf_hf"]
    min_path_pf_req = avg_line_pf_req/(p_hf * max_line_s_pf)/2
    
    for i in range(len(link_path_list)):
        overlap_pf = 0
        for linkIdx in link_path_list[i]:
            if linkIdx in exampt_path:
                overlap_pf += graph.es[linkIdx]["pf"]
        if min_path_pf > overlap_pf:
            min_path_pf = overlap_pf
            min_index = i
        
        if overlap_pf <= min_path_pf_req:
            result = i
            break
    
    if result != -1:
#         print("Required path pf of bus "+vs["name"]+":",round(min_path_pf_req,8),"  ","the selected one:",overlap_pf)
        return result
    else:
        print("No available path for bus "+vs["name"])
        if min_index == -1:
            min_index = 0
        return min_index


# Select a path from all possible paths, which is not the primary path. This function is used if Non-OL
# path cannot be found for a bus. Here we know there is "ONE unreliable link", thus we can select others
# if the "possible shortest path" contains the "unreliable link". 
def index_other_than_pr_path(graph,vs,link_path_list):
    pr_path = vs["PR_RVS_ES_path"]
    result = -1
    temp_result = -1
    current_max = 0
    for i in range(len(link_path_list)):
        counter = 0
        max_link_pf = 0
        for linkIdx in link_path_list[i]:
            if linkIdx in pr_path:
                counter += 1
            max_link_pf = max(max_link_pf,graph.es[linkIdx]["pf"])
        print("max_link_pf is",max_link_pf)
        if counter == len(pr_path):
            continue
        elif max_link_pf >= 0.0001:
            if current_max == 0 or max_link_pf < current_max:
                temp_result = i
                current_max = max_link_pf
            continue
        else:
            result = i
            break
    
    if result == -1:
        result = temp_result

    return result


def find_relay_with_index(graph,relayIdx):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.relayIdx == relayIdx:
                return relay
    
    return -1


def check_not_rsv_relay(graph):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.no_rsv_bp != 0:
                print("Relay",relay.relayIdx,"not enough resources.")


def find_relay_with_min_req(graph,vs_i):
    min_req = 10000
    result = 0
    for relay in vs_i["relaySet"]:
        if relay.delay_rtt < min_req:
            min_req = relay.delay_rtt
            result = relay
    
    return result


# In this function, for each relay in the system we assume it has only one backup path.
# We calculate the required reservation on each link of the path. We reserve in certain order
# to meet the power requirement     
def staticRSV_BP_New_order(graph,pktSize,ordered_list):
    temp_ordered_list = deepcopy(ordered_list)
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.relayIdx not in temp_ordered_list:
                temp_ordered_list.append(relay.relayIdx)
                
    for relayIdx in temp_ordered_list:
        relay = find_relay_with_index(graph, relayIdx)
        if relay == -1:
            raise NameError('Relay not found!')
        if relay.no_rsv_pr != 0 or relay.no_rsv_bp != 0 or relay.rsv_bp_looked != 0 or relay.single_line != 0:
            continue
        
        vs_i = graph.vs[relay.Located_Node_Idx]
        for linkIdx in vs_i["BP_RVS_ES_path"]:
            idx = vs_i["BP_RVS_ES_path"].index(linkIdx)
            
            src = vs_i["BP_RVS_VS_path"][idx+1]
            dst = vs_i["BP_RVS_VS_path"][idx]
            
            name1 = str(src)+'_'+str(dst)
            name2 = str(dst)+'_'+str(src)
            
            # Check if the link is also in the primary path of the relay
#                 if linkIdx in vs_i["PR_RVS_ES_path"]:
#                     print("Overlapping path!")
#                 else:
#                     print("Non-overlapping path!")
            
#                 graph.es[linkIdx]["RSV_BP"][name1] += pktSize*8/relay.delay_per_link_BP
#                 graph.es[linkIdx]["RSV_BP"][name2] += pktSize*8/relay.delay_per_link_BP
            ca_req_1 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name1]
            ca_req_2 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name2]
            ca_req_nd = pktSize*8/relay.per_hop_dr_BP[linkIdx]["nonDirection"]
            
            if linkIdx not in vs_i["PR_RVS_ES_path"]:
                if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
                if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
                if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                    graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
                    relay.used_link_RSV_BP[linkIdx] = ca_req_nd
                else:
                    relay.no_rsv_bp = 1
                    print("Relay",relay.relayIdx,"-> insufficient BACKUP capacity")
            else:
#                     print("Overlapped link "+str(linkIdx+1)," for relay",relay.relayIdx)
                pr_link_rsv = pktSize*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
                if ca_req_nd > pr_link_rsv:
                    ca_req_1 = ca_req_1 - pr_link_rsv
                    ca_req_2 = ca_req_2 - pr_link_rsv
                    ca_req_nd = ca_req_nd - pr_link_rsv
                    if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
                    if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
                    if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                        graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                        graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
                        relay.used_link_RSV_BP[linkIdx] = ca_req_nd
                    else:
                        relay.no_rsv_bp = 1
                        print("Relay",relay.relayIdx,"-> insufficient BACKUP capacity")
        relay.rsv_bp_looked = 1



def weight_function_bp_load_and_rsv(graph,relay,epathList,vpathList,vs,relayDR_Table,powft=1):
    tempSum = 0 # for temporary use
    tempSum2 = 0 # for temporary use
    tempWeightList = [] # for temporary use
    
#     print(epathList)
#     print(vpathList)
    for linkIdx in epathList: # compute the total free_weight
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
#         print(linkIdx,"Link",linkIdx+1,name1)
        
        if graph.es[linkIdx]["RSV_Overall"] >= graph.es[linkIdx]["capacity"]:
            raise NameError('1. Capacity not enough! '+'for link '+str(linkIdx+1))
        free_weight = 1-(graph.es[linkIdx]["RSV_Overall"])/(graph.es[linkIdx]["capacity"])
#                         tempSum += free_weight
        tempSum += pow(free_weight,powft)
        
    # Next assign link weight based on free_weight and compute total link weight
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        
        free_weight = 1-(graph.es[linkIdx]["RSV_Overall"])/(graph.es[linkIdx]["capacity"])
#                         current_link_weight = tempSum/free_weight
        current_link_weight = tempSum/pow(free_weight,powft)
        tempWeightList.append(current_link_weight)
        tempSum2 += current_link_weight # total sum weight
        
#                     print(tempWeightList,tempSum2)
    # compute delay division on each hop/link and check the potential required capacity is enough
    temp_relayDR_Table = {}
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        if linkIdx not in temp_relayDR_Table:
            temp_relayDR_Table[linkIdx] = {}
        
        current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
        temp_relayDR_Table[linkIdx][name1] = current_hop_DR
        temp_relayDR_Table[linkIdx][name2] = current_hop_DR
        temp_relayDR_Table[linkIdx]["nonDirection"] = current_hop_DR
        
        if linkIdx not in vs["PR_RVS_ES_path"]:
            to_be_rsved = 80*8/temp_relayDR_Table[linkIdx]["nonDirection"]
        else:
            bp_needed_ca = 80*8/temp_relayDR_Table[linkIdx]["nonDirection"]
            pr_link_rsv = 80*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
            to_be_rsved = max(0,bp_needed_ca - pr_link_rsv)
        
        if graph.es[linkIdx]["RSV_Overall"] + to_be_rsved > graph.es[linkIdx]["capacity"]:
            return -1
        
    # Update the link utilization
    relayDR_Table = deepcopy(temp_relayDR_Table)
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        ca_req_1 = 80*8/relayDR_Table[linkIdx][name1]
        ca_req_2 = 80*8/relayDR_Table[linkIdx][name2]
        ca_req_nd = 80*8/relayDR_Table[linkIdx]["nonDirection"]
        
        if linkIdx not in vs["PR_RVS_ES_path"]:
            graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
            graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
            graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
            graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
        else:
            pr_link_rsv = 80*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
            graph.es[linkIdx]["RSV_BP"][name1] += max(0, ca_req_1 - pr_link_rsv)
            graph.es[linkIdx]["RSV_BP"][name2] += max(0, ca_req_2 - pr_link_rsv)
            graph.es[linkIdx]["RSV_Overall"] += max(0, ca_req_nd - pr_link_rsv)
            graph.es[linkIdx]["RSV_BP_Overall"] += max(0, ca_req_nd - pr_link_rsv)
    
    return 0



def select_backup_path_dv_rsv_on_power(link_path_list,vs_path_list,vs,relay,graph):
    min_path_pf = 0
    max_line_s_pf = relay.pr_line_s_pf
    avg_line_pf_req = relay.avg_line_pf_req
    p_hf = graph["pf_hf"]
    success = -1
    result = 0
    
    if max_line_s_pf != 0 and avg_line_pf_req != 0:
        min_path_pf = avg_line_pf_req/(p_hf * max_line_s_pf)/2
    else:
        min_path_pf = 1
    
    for i in range(len(link_path_list)):
        overlap_pf = 0
        for linkIdx in link_path_list[i]:
            if linkIdx in vs["PR_RVS_ES_path"]:
                overlap_pf += graph.es[linkIdx]["pf"]
        
        if overlap_pf <= min_path_pf:
            print("Required path pf of Relay "+str(relay.relayIdx)+' at bus '+vs["name"]+":",round(min_path_pf,8),\
                  "  ","the selected one:",overlap_pf)
            success = weight_function_bp_load_and_rsv(graph, relay, link_path_list[i], vs_path_list[i], vs, \
                                                      relay.per_hop_dr_BP)
        
        if success == 0:
            result = i
            break
        else:
#             print("Relay",relay.relayIdx,"needs to find another path due to insufficient CA.")
            None
    
    if result >= len(link_path_list):
        relay.no_rsv_bp = 1
        result = 0
    
    return result



def compute_system_pf_with_bp_each_relay(graph):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.no_rsv_bp != 0:
                path_pf = len(vs_i["PR_RVS_ES_path"])*graph["link_pf"]
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            else:
                path_ol_no = 0
                for linkIdx in relay.BP_RVS_ES_path:
                    if linkIdx in vs_i["PR_RVS_ES_path"]:
                        path_ol_no += 1
                path_pf = path_ol_no*graph["link_pf"]
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
    return total_result