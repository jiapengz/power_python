'''
Created on Dec 23, 2014

@author: Zhang Jiapeng
'''

from SPT import calSPT
import igraph as ig
from SPT.calSPT import setRelayNo, findBackupRelayLine,\
    showRelaySet_v2, findBackupRelayGraph, calDelayReq_PR_Graph, showRSV_PR, staticRSV_PR,\
    smartRSV_PR,showAvgBPRelay,showSubtreeEdge_PR,smartRSV_2RDF_PR,find_Zone3_relay_Same_bus

myGraph = ig.Graph()
myGraph.add_vertices(57)

for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"
    
myGraph.add_edge("1", "2")
myGraph.add_edge("1", "15")
myGraph.add_edge("1", "16")
myGraph.add_edge("1", "17")
myGraph.add_edge("2", "3")
myGraph.add_edge("3", "4")
myGraph.add_edge("3", "15")
myGraph.add_edge("4", "5")
myGraph.add_edge("4", "6")
myGraph.add_edge("4", "18")
myGraph.add_edge("5", "6")
myGraph.add_edge("6", "7")
myGraph.add_edge("6", "8")
myGraph.add_edge("7", "8")
myGraph.add_edge("8", "9")
myGraph.add_edge("9", "10")
myGraph.add_edge("9", "11")
myGraph.add_edge("9", "12")
myGraph.add_edge("9", "13")
myGraph.add_edge("10", "12")
myGraph.add_edge("11", "13")
myGraph.add_edge("12", "13")
myGraph.add_edge("12", "16")
myGraph.add_edge("12", "17")
myGraph.add_edge("13", "14")
myGraph.add_edge("13", "15")
myGraph.add_edge("14", "15")
myGraph.add_edge("18", "19")
myGraph.add_edge("19", "20")
myGraph.add_edge("21", "22")
myGraph.add_edge("22", "23")
myGraph.add_edge("22", "38")
myGraph.add_edge("23", "24")
myGraph.add_edge("25", "30")
myGraph.add_edge("26", "27")
myGraph.add_edge("27", "28")
myGraph.add_edge("28", "29")
myGraph.add_edge("29", "52")
myGraph.add_edge("30", "31")
myGraph.add_edge("31", "32")
myGraph.add_edge("32", "33")
myGraph.add_edge("34", "35")
myGraph.add_edge("35", "36")
myGraph.add_edge("36", "37")
myGraph.add_edge("36", "40")
myGraph.add_edge("37", "38")
myGraph.add_edge("37", "39")
myGraph.add_edge("38", "44")
myGraph.add_edge("38", "48")
myGraph.add_edge("38", "49")
myGraph.add_edge("41", "42")
myGraph.add_edge("41", "43")
myGraph.add_edge("41", "56")
myGraph.add_edge("42", "56")
myGraph.add_edge("44", "45")
myGraph.add_edge("46", "47")
myGraph.add_edge("47", "48")
myGraph.add_edge("48", "49")
myGraph.add_edge("49", "50")
myGraph.add_edge("50", "51")
myGraph.add_edge("52", "53")
myGraph.add_edge("53", "54")
myGraph.add_edge("54", "55")
myGraph.add_edge("56", "57")

# Non-transmission line
myGraph.add_edge("7", "29")
myGraph.add_edge("9", "55")
myGraph.add_edge("10", "51")
myGraph.add_edge("11", "41")
myGraph.add_edge("11", "43")
myGraph.add_edge("13", "49")
myGraph.add_edge("14", "46")
myGraph.add_edge("15", "45")
myGraph.add_edge("20", "21")
myGraph.add_edge("24", "25")
myGraph.add_edge("24", "26")
myGraph.add_edge("32", "34")
myGraph.add_edge("39", "57")
myGraph.add_edge("40", "56")


myGraph.es["weight"] = 1
SPDistance = myGraph.shortest_paths_dijkstra(weights=myGraph.es["weight"])
setRelayNo(myGraph,64)
 
optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance) #This is the best MA location
 
print("Optimal locations:")
for index in optimalNodeIndex:
    print("Bus",myGraph.vs[index]["name"])
    
calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex,plotEN=False)

findBackupRelayGraph(myGraph,64)
find_Zone3_relay_Same_bus(myGraph)
calDelayReq_PR_Graph(myGraph,64)
showRelaySet_v2(myGraph)

# staticRSV(myGraph,80)
# smartRSV(myGraph,80,64)
smartRSV_2RDF_PR(myGraph, 64, 80)
showRSV_PR(myGraph)
showAvgBPRelay(myGraph,64)
# showSubtreeEdge_PR(myGraph)
