'''
Created on Nov 5, 2014

@author: Zhang Jiapeng
'''

from SPT import calSPT
import igraph as ig
from igraph import summary
import heapq
import random

def list_duplicates_of(seq,item):
    start_at = -1
    locs = []
    while True:
        try:
            loc = seq.index(item,start_at+1)
        except ValueError:
            break
        else:
            locs.append(loc)
            start_at = loc
    return locs


def test_multi_parameter(enablePrint, *args):
    if enablePrint == True:
        for i in range(len(args)):
            if i == len(args)-1:
                print(args[i])
            else:
                print(args[i],"",end='')


class testClass:
    def __init__(self,name):
        self.s = name
        self.list = []

# myGraph = ig.Graph().Full(4, directed=False, loops=False)
myGraph = ig.Graph(4,directed=False)
myGraph.vs["name"] = ["1","2","3","4"]

myGraph.es["weight"] = 1

myGraph.add_edge("1", "2")
# myGraph.add_edge("2", "1")
myGraph.add_edge("1", "3")
myGraph.add_edge("1", "4")
myGraph.add_edge("2", "3")
myGraph.add_edge("3", "4")
# SPDistance = myGraph.shortest_paths_dijkstra(weights=myGraph.es["weight"])
# optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance)
# calSPT.plotSPT(myGraph, optimalNodeIndex)
# 
# We also plot the complete graph
# calSPT.plotGraph(myGraph)


# summary(myGraph)
for edge_i in myGraph.es:
    print(edge_i.index,edge_i.source,edge_i.target)
    srcIdx = edge_i.source
    targetIdx = edge_i.target
    str1 = str(srcIdx)+'_'+str(targetIdx)+'_rsv'
    str2 = str(targetIdx)+'_'+str(srcIdx)+'_rsv'
    
    edge_i[str1] = 0
    edge_i[str2] = 0
    
myGraph["layer"] = {}
for i in range(5):
    myGraph["layer"][i] = [i]

print(myGraph)
print("Layer",myGraph["layer"])

# a = testClass('A')
# b = testClass('B')
# 
# a.list.append(0)
# a.list.append(1)
# b.list.append(2)
# b.list.append(3)
# 
# print(a,a.s,a.list)
# print(b,b.s,b.list)
# 
# a.list = ['str']
# b.list = ['ptr']
# 
# print(a,a.s,a.list)
# print(b,b.s,b.list)

nb = myGraph.vs[3].neighbors()
for vs_i in nb:
    print(vs_i.index)
    
for i in range(len(nb)):
    print("New Method:",nb[i].index)
    
myList = []
for i in range(5):
    myList.append(i)
print(myList)
myList.pop()
print(myList,myList[-1])
print((0 in myList),(4 in myList))
print(myGraph.get_eid(2,3))

myList = []
myList.append(20)
myList.append(20)
myList.append(5)
myList.append(18)
myList.append(100)
print("myList:",myList)
i = 5
res = heapq.nlargest(i, myList)
res2 = heapq.nsmallest(i, myList)
print(res,"The "+str(i)+"th largest number is:",res[i-1])
print(res2,"The "+str(i)+"th smallest number is:",res2[i-1])
print("The index of the "+str(i)+"th largest number in the list is:",myList.index(res[i-1]))
print("myList is now:",myList)
print("Duplicated index:",list_duplicates_of(myList, res[i-1]))
print(int("20")-1,int(5/2))

my_test_list = [[1,2,3,4,5,6],[5,6,7,8],[2,3,4,5],[1,3,5,7,9,10,11],[3,4,5,6,7]]
my_test_list2 = [[2,6],[3,5],[6,1],[5,0],[1,3],[4,3],[7,2]]
new_test_list = sorted(my_test_list, key=lambda path_list: len(path_list))
print(new_test_list)
new_test_list2 = sorted(my_test_list2, key=lambda myset: myset[1])
print(new_test_list2)
temp_par = new_test_list2.pop(0)
print(new_test_list2,temp_par)
new_test_list2.append([7,8])
print(new_test_list2)
a = max(1,2,3,4,5)
print("a =",a)
testDict = {1:2, 3:4, 5:6, 7:8, 9:10}
print("THe length of testDict is:",len(testDict))
print("Key:",testDict.keys(),"   Key number:",len(testDict))
max_key = max(testDict.keys(), key=int)
print("Max key:",max_key,"   Compare result:",max_key>=9)
myList.remove(20)
myList.remove(20)
myList.remove(100)
print(myList)

temp1 = [1,2,3,4,5,6,7,8,9]
random.shuffle(temp1)
print(temp1)
random.shuffle(temp1)
print(temp1)

test_multi_parameter(True,"This","a","test",1,2,3,4,5,"parameters")