'''
Created on Apr 2, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
from random import randint
from nt import link

class_no = 7
base_p = 0.00001
spc_lv = 0.000001
first_lv = base_p/class_no*1
second_lv = base_p/class_no*1
third_lv = base_p/class_no*1
forth_lv = base_p/class_no*1
fifth_lv = base_p/class_no*1
sixth_lv = base_p/class_no*7
sevth_lv = base_p/class_no*7

def set_link_reliability(graph):
    graph.es[19]["pf"] = first_lv
    graph.es[20]["pf"] = first_lv
    graph.es[21]["pf"] = first_lv
    graph.es[22]["pf"] = first_lv
    graph.es[23]["pf"] = first_lv
    
    graph.es[26]["pf"] = first_lv
    
    graph.es[8]["pf"] = third_lv
    graph.es[10]["pf"] = third_lv
    
    graph.es[2]["pf"] = forth_lv
    graph.es[28]["pf"] = forth_lv
    graph.es[15]["pf"] = forth_lv
    graph.es[33]["pf"] = forth_lv
    graph.es[11]["pf"] = forth_lv
    graph.es[9]["pf"] = forth_lv
    
    graph.es[16]["pf"] = fifth_lv
    graph.es[17]["pf"] = fifth_lv
    graph.es[30]["pf"] = fifth_lv
    graph.es[7]["pf"] = fifth_lv
    graph.es[3]["pf"] = fifth_lv
    
    graph.es[24]["pf"] = sixth_lv
    graph.es[32]["pf"] = sixth_lv
    graph.es[12]["pf"] = sixth_lv
    graph.es[6]["pf"] = sixth_lv
    graph.es[31]["pf"] = sixth_lv
    graph.es[1]["pf"] = sixth_lv
    graph.es[2]["pf"] = sixth_lv
    
    graph.es[29]["pf"] = sevth_lv
    graph.es[4]["pf"] = sevth_lv
    graph.es[27]["pf"] = sevth_lv
    graph.es[5]["pf"] = sevth_lv
    graph.es[18]["pf"] = sevth_lv
    graph.es[14]["pf"] = sevth_lv
    graph.es[13]["pf"] = sevth_lv
    graph.es[25]["pf"] = sevth_lv
    None


def set_link_reliability_random(graph):
    c_1 = [19,20,21,22,23]
    c_2 = [26,8,10,2,28,15,33,11,9,16,17,30,7,3]
    c_3 = [24,32,12,6,31,1,2,29,4,27,5,18,14,13,25]
    
    for linkIdx in c_1:
        graph.es[linkIdx]["pf"] = (base_p/10)*randint(1,3)
        
    for linkIdx in c_2:
        graph.es[linkIdx]["pf"] = (base_p/10)*randint(5,8)
    
    for linkIdx in c_3:
        graph.es[linkIdx]["pf"] = (base_p/10)*randint(10,15)
