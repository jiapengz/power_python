'''
Created on Dec 20, 2014

@author: Zhang Jiapeng
'''

from SPT import calSPT
import igraph as ig
from SPT.calSPT import setRelayNo, findBackupRelayLine,\
    showRelaySet_v2, findBackupRelayGraph, calDelayReq_PR_Graph, showRSV_PR, staticRSV_PR,\
    smartRSV_PR,showAvgBPRelay,showSubtreeEdge_PR,smartRSV_2RDF_PR,findBackupPath_simple,\
    showSubtreeEdge_BP,showEmptyBus,calDelayReq_BP_Graph,staticRSV_BP,showRSV_BP,\
    smartRSV_BP,do_Perhop_DelayReq_Simple_BP,do_Perhop_DelayReq_Simple_PR,find_Zone3_relay_Same_bus

myGraph = ig.Graph()
myGraph.add_vertices(24)

for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"

myGraph.add_edge("1", "2")
myGraph.add_edge("1", "3")
myGraph.add_edge("1", "5")
myGraph.add_edge("2", "4")
myGraph.add_edge("2", "6")
myGraph.add_edge("3", "9")
myGraph.add_edge("4", "9")
myGraph.add_edge("5", "10")
myGraph.add_edge("6", "10")
myGraph.add_edge("7", "8")
myGraph.add_edge("8", "9")
myGraph.add_edge("8", "10")
myGraph.add_edge("11", "13")
myGraph.add_edge("11", "14")
myGraph.add_edge("12", "13")
myGraph.add_edge("12", "23")
myGraph.add_edge("13", "23")
myGraph.add_edge("14", "16")
myGraph.add_edge("15", "16")
myGraph.add_edge("15", "21")
myGraph.add_edge("15", "24")
myGraph.add_edge("16", "17")
myGraph.add_edge("16", "19")
myGraph.add_edge("17", "18")
myGraph.add_edge("17", "22")
myGraph.add_edge("18", "21")
myGraph.add_edge("19", "20")
myGraph.add_edge("20", "23")
myGraph.add_edge("21", "22")

# Non-transmission line
myGraph.add_edge("3", "24")
myGraph.add_edge("9", "11")
myGraph.add_edge("9", "12")
myGraph.add_edge("10", "11")
myGraph.add_edge("10", "12")


myGraph.es["weight"] = 1
SPDistance = myGraph.shortest_paths_dijkstra(weights=myGraph.es["weight"])
setRelayNo(myGraph,29)
 
optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance) #This is the best MA location
 
print("Optimal locations:")
for index in optimalNodeIndex:
    print("Bus",myGraph.vs[index]["name"])
     
# fileName = "IEEE24bus.png"
# calSPT.plotSPT(myGraph, optimalNodeIndex)
# calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex,fileName=fileName)
calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex)

findBackupRelayGraph(myGraph,29)
find_Zone3_relay_Same_bus(myGraph)
calDelayReq_PR_Graph(myGraph,29)
do_Perhop_DelayReq_Simple_PR(myGraph,29, 'normal', 0 ,1)
showRelaySet_v2(myGraph)

staticRSV_PR(myGraph,80)
# smartRSV(myGraph,80,29)
# smartRSV_2RDF_PR(myGraph, 29, 80)
showRSV_PR(myGraph)
showAvgBPRelay(myGraph,29)
# showSubtreeEdge_PR(myGraph)
