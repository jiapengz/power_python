'''
Created on Sep 1, 2016

@author: Zhang Jiapeng
'''

# This is the network data for IEEE 39-bus power system

import igraph as ig
from numpy import mean

# Start the initialization of the graph
myGraph_13 = ig.Graph()
def setGraph(myGraph):
    
    myGraph.add_vertices(13)
    
    for v in myGraph.vs:
        v["name"] = str(v.index + 1) #here the name is type "string"
        
    myGraph.add_edge("1", "2")
    myGraph.add_edge("2", "6")
    myGraph.add_edge("3", "6")
    myGraph.add_edge("4", "6")
    myGraph.add_edge("6", "9")
    myGraph.add_edge("7", "8")
    myGraph.add_edge("8", "9")
    myGraph.add_edge("9", "10")
    myGraph.add_edge("10", "11")
    myGraph.add_edge("8", "12")
    myGraph.add_edge("9", "13")
    
    # Non-transmission Line
    myGraph.add_edge("4", "5")
    
    for es in myGraph.es:
        es["name"] = str(es.index + 1) #here the name for a line is of type "string"


