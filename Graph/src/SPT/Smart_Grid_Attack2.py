'''
Created on Sep 25, 2016

@author: Zhang Jiapeng
'''

from SPT import calSPT
from SPT import calSPT2
from copy import deepcopy
import SPT.Smart_Grid_Attack as sga
import math
import random
import igraph as ig
import matplotlib.pyplot as plt
import numpy as np
import SPT.plotFunction as plotf
import sys

sys.setrecursionlimit(50000)

def plotGraph(graph,fileName=None,graphLayout=None):
    if graphLayout != None:
        myLayout = ig.Layout(graphLayout)
    else:
        myLayout = graph.layout_kamada_kawai()
    graph.vs["label"] = graph.vs["name"]
    ig.plot(graph, layout = myLayout, bbox = (800,800), margin = 50)
    if fileName != None:
        ig.plot(graph, fileName, layout = myLayout, bbox = (800,800), margin = 50)



def cal_avg_degree(graph):
    m_deg = ig.mean(graph.degree())
    var_deg = ig.statistics.var(graph.degree())
    print("Average degree =", m_deg, "    Variance of degree =", var_deg)



def show_graph_basic(graph, plot_en=False):
    print("Number of nodes =",len(graph.vs))
    print("Number of edges =",len(graph.es))
    cal_avg_degree(graph)
    
    print(graph.degree())
    
    area_line_list = show_area_line_num(graph)
    print("Area line variance:", ig.statistics.var(area_line_list))
    
    if plot_en == True:
        #plotf.myPlotFig_List([graph.degree()], ["var(deg)=1.7"], "Graph Degree", "Line index", "degree", ymax=12, ymin=0)
        plotf.myPlotFig_List([area_line_list], ["var(deg)=1.7"], "Area line number", \
                             "Line index", "Line number", ymax=20, ymin=0)



def show_area_line_num(graph):
    result_list = []
    for es_i in graph.es:
        src_index = es_i.source
        dst_index = es_i.target
        
        line_num = graph.vs[src_index].degree() + graph.vs[dst_index].degree() - 1
        result_list.append(line_num)
    
    print("Area line number:", result_list)
    
    return result_list



def show_attack_before_after(graph):
    print("Before area attack:")
    sga.total_attack_trip_info(graph)
    for es_i in graph.es:
        sga.attack_agent_protection_pure_relay(graph, es_i.index)
    print("After area attack:")
    sga.total_attack_trip_info(graph)



def show_cascading_sequence(graph):
    assert len(graph["cascading_initial"]) == len(graph["cascading_sequence"])
    for i in range(len(graph["cascading_initial"])):
        print("Line set", graph["cascading_initial"][i], "produces sequence length", \
              len(graph["cascading_sequence"][i]),":")
        print(graph["cascading_sequence"][i])



def initialize_node_name(graph):
    for v in graph.vs:
        v["name"] = str(v.index + 1) #here the name is type "string"


def initialize_line_name(graph):
    for es in graph.es:
        es["name"] = str(es.index + 1) #here the name is type "string"


def graph_initialization(graph, max_edge_id=-1):
    graph["total_line"] = len(graph.es)
    graph["remain_line"] = len(graph.es)
    graph["cascading_initial"] = []
    graph["cascading_sequence"] = []
    graph["cascading_result"] = []
    graph.es["weight"] = 1
    
    if max_edge_id <= 0:
        edge_num = len(graph.es)
    else:
        edge_num = max_edge_id
    
    calSPT.setRelayNo(graph,edge_num)
    calSPT.findBackupRelayGraph(graph,edge_num)
#     calSPT.find_Zone3_relay_Same_bus(graph)
    calSPT.update_min_relay_req_at_a_bus(graph)



# select the index of initial cascading lines in a system graph
# an initial line set have N lines, with 1 <= N <= n
# create s sets of initial lines
def select_initial_cascading_line(graph, i_ratio, n, s=1, cascade_type="fixed"):
    result = []
    
    if cascade_type == "fixed":
        line_candidate = [i for i in range(len(graph.es))]
        
        for i in range(s):
            temp_line_set = []
            temp_candidate = deepcopy(line_candidate)
            
            #cnt = min(n, len(graph.es))
            #cnt = random.randint(1, cnt)
            #cnt = 4
            cnt = int(i_ratio * len(graph.es))
            
            while(True):
                if len(temp_line_set)>=cnt or len(temp_candidate)==0:
                    break
                
                selected_line_idx = random.choice(temp_candidate)
                
                assert selected_line_idx not in temp_line_set
                temp_line_set.append(selected_line_idx)
                temp_candidate.remove(selected_line_idx)
            
            result.append(temp_line_set)
        
    return result



# select the index of initial cascading lines in a system graph
# each line is not adjusted with any other line
# an initial line set have N lines, with 1 <= N <= n
# create s sets of initial lines
def select_initial_cascading_special(graph, i_ratio, n, s=1, cascade_type="fixed"):
    result = []
    
    if cascade_type == "fixed":
        line_candidate = [i for i in range(len(graph.es))]
        
        for i in range(s):
            vs_idx_list = []
            temp_line_set = []
            temp_candidate = deepcopy(line_candidate)
            cnt = int(i_ratio * len(graph.es))
            
            while(True):
                if len(temp_line_set)>=cnt or len(temp_candidate)==0:
                    break
                
                selected_line_idx = random.choice(temp_candidate)
                src_idx = graph.es[selected_line_idx].source
                dst_idx = graph.es[selected_line_idx].target
                
                if (src_idx not in vs_idx_list) and (dst_idx not in vs_idx_list):
                    vs_idx_list.append(src_idx)
                    vs_idx_list.append(dst_idx)
                    
                    assert selected_line_idx not in temp_line_set
                    temp_line_set.append(selected_line_idx)
                    temp_candidate.remove(selected_line_idx)
            
            result.append(temp_line_set)
    
    print("Special initial set constructed")
    return result


# select the index of initial cascading lines in a system graph
# each line is not adjusted with any other line
# any two initial lines cannot be tripped by the same neighbor line
def select_initial_cascading_special_v2(graph, i_ratio, n, s=1, cascade_type="fixed"):
    result = []
    avg_area_degree = 7
    print("Average area degree threshold:",avg_area_degree)
    
    if cascade_type == "fixed":
        line_candidate = [i for i in range(len(graph.es))]
        
        for i in range(s):
            temp_line_set = []
            temp_candidate = deepcopy(line_candidate)
            cnt = int(i_ratio * len(graph.es))
            
            while(True):
                if len(temp_line_set) < cnt and len(temp_candidate) == 0:
                    raise NameError("Cannot found satisfied initial line set")
                
                if len(temp_line_set)>=cnt or len(temp_candidate)==0:
                    break
                
                selected_line_idx = random.choice(temp_candidate)
                area_deg = check_area_degree(graph, selected_line_idx)
                #print("Area",selected_line_idx,"   degree:",area_deg)
                
                #if area_deg < avg_area_degree:
                #    temp_candidate.remove(selected_line_idx)
                #    continue
                
                exampt_two_hop_neighbors(graph, selected_line_idx, temp_candidate)
                
                assert selected_line_idx not in temp_line_set
                temp_line_set.append(selected_line_idx)
                temp_candidate.remove(selected_line_idx)
            
            result.append(temp_line_set)
        
    print("Special initial set constructed")
    return result


def check_area_degree(graph, target_line_idx):
    src_idx = graph.es[target_line_idx].source
    dst_idx = graph.es[target_line_idx].target
    
    vs_src = graph.vs[src_idx]
    vs_dst = graph.vs[dst_idx]
    
    area_deg = vs_src.degree() + vs_dst.degree() - 1
    
    return area_deg


def exampt_two_hop_neighbors(graph, target_line_idx, candidate_list=[]):
    src_idx = graph.es[target_line_idx].source
    dst_idx = graph.es[target_line_idx].target
    
    vs_src = graph.vs[src_idx]
    vs_dst = graph.vs[dst_idx]
    
    src_nb_vs_idx = [vs_i.index for vs_i in vs_src.neighbors()]
    dst_nb_vs_idx = [vs_j.index for vs_j in vs_dst.neighbors()]
    
    total_vs_idx = []
    for idx_i in src_nb_vs_idx:
        if idx_i not in total_vs_idx:
            total_vs_idx.append(idx_i)
    
    for idx_j in dst_nb_vs_idx:
        if idx_j not in total_vs_idx:
            total_vs_idx.append(idx_j)
    
    for es_i in graph.es:
        if es_i.index == target_line_idx:
            continue
        
        if es_i.source in total_vs_idx or es_i.target in total_vs_idx:
            if es_i.index in candidate_list:
                candidate_list.remove(es_i.index)


def create_cascading_sequence_pure_random(graph, initial_list, extra_ratio):
    edge_list = []
    for es_i in graph.es:
        edge_list.append(es_i.index)
    
    np.random.seed(1260)
    # for each initial line, create a cascading sequence
    for line_set in initial_list:
        copy_edge_list = deepcopy(edge_list)
        cascade_sequence = []
        
        cascade_num = min(int(np.random.poisson(len(graph.es)/4)), len(graph.es))
        cascade_num = max(cascade_num, len(line_set)+1)
        #cascade_num = 30
        cascade_num = int(extra_ratio * len(graph.es))
        
        # the initial line set is excluded from the extra line set
        for line_idx in line_set:
            #cascade_sequence.append(line_idx)
            copy_edge_list.remove(line_idx)
            #cascade_num -= 1
        
        for i in range(cascade_num):
            selected_line_idx = random.choice(copy_edge_list)
            cascade_sequence.append(selected_line_idx)
            copy_edge_list.remove(selected_line_idx)
        
        graph["cascading_sequence"].append(cascade_sequence) # a sequence is generated
        temp_mark = sga.CascadeMark()
        graph["cascading_result"].append(temp_mark) # record whether a cascading is triggered
#         print("Line set", line_set, "produces sequence length", len(cascade_sequence),":",cascade_sequence)



def effect_of_random_relay_selection_MCM(graph, total_resource=0, K=1, test_repeat_num=1, \
                                         enable_area=False, enable_cas=False):
    relay_idx_list = []
    total_num_list = []
    total_trip_relay_num = []
    
    # setup the list of all list index
    for i in range(graph["total_relay_no"]):
        relay_idx_list.append(i+1)
    
    for i in range(test_repeat_num):
        available_rsc = total_resource
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        copy_relay_idx_list = deepcopy(relay_idx_list)
        compromise_list = []
        atk_relay_num = 0
        
        # start attack, randomly select relays
        while available_rsc > 0:
            atk_relay_idx = random.choice(copy_relay_idx_list)
            
            assert atk_relay_idx not in compromise_list
            
            # compromise a relay with certain probability
            available_rsc, completed = sga.attack_relay(copyGraph, available_rsc, atk_relay_idx, \
                                                        K, en_prt=False)
            
            # if the attack is successful
            if completed == True:
                atk_relay_num += 1
                trip_line_idx = sga.trip_line_with_relay(copyGraph, atk_relay_idx)
                
                if enable_cas == True:
                    cascade_check_list = sga.cascading_initial_line_check(copyGraph, trip_line_idx)
                    
                    if cascade_check_list != []: # some cascading conditions are fulfilled
                        for idx in cascade_check_list:
                            if copyGraph["cascading_result"][idx].triggered != True:
                                copyGraph["cascading_result"][idx].triggered = True
                                sga.trigger_cascading(copyGraph, idx, False)
                
                compromise_list.append(atk_relay_idx)
                copy_relay_idx_list.remove(atk_relay_idx)

        
        #sga.effect_of_compromised_device(copyGraph, True)
        if enable_area == True:
            for es_i in graph.es:
                sga.attack_agent_protection_pure_relay(copyGraph, es_i.index)
        
        total_num = sga.total_tripped_line(copyGraph)
        total_num_list.append(total_num)
        total_trip_relay_num.append(atk_relay_num)
        
        reset_graph_simu(copyGraph)
    
    avg_trip_line_num = calSPT.mean(total_num_list)
    print("Relay combination number:",len(total_num_list),"    average tripped lines:",avg_trip_line_num,\
          "    average compromised relays:",calSPT.mean(total_trip_relay_num),\
          "    total attack resources:",total_resource)
    
    return avg_trip_line_num



def get_max_line_protection_area_index(graph):
    max_num = 0
    result = []
    
    for es_i in graph.es:
        if len(es_i["area_line"]) > max_num:
            max_num = len(es_i["area_line"])
    
    if max_num > 0:
        for es_i in graph.es:
            if len(es_i["area_line"]) == max_num:
                result.append(es_i.index)
    
    return result



# there can be different methods to select the target line
# the simplest one is to assume all relays have the same cost, and select the first satisfied line in the list
# the core of a protection area is the primary line
# area_id == line_id
def select_target_relay_idx(graph, area_idx_list):
    max_efficiency = 0
    target_relay_idx = -1
    target_line_idx = -1
    attack_effect = 0
    total_attack_effect = 0
    
    for line_idx in area_idx_list:
        for relay_i in graph.es[line_idx]["prRelaySet"]:
            
            if relay_i.compromised == False:
                
                # the "potential trip" of a relay is computed, however, this is an "iteration" selection
                # the relay selection can be simplified in "one-round", to select the 1st un-compromised relay
                attack_efficiency, total_efficiency = sga.compute_potential_of_relay(graph, relay_i)
                
                if attack_efficiency > max_efficiency:
                    max_efficiency = attack_efficiency
                    target_relay_idx = relay_i.relayIdx
                    target_line_idx = line_idx
                    attack_effect = attack_efficiency * relay_i.attack_cost
                    total_attack_effect = total_efficiency * relay_i.attack_cost
    
    return target_relay_idx, target_line_idx, attack_effect, total_attack_effect 


def cascading_knowledge_attack(graph, total_rsc, K1, atk_mode, K2=0, keep_atk=False, enable_area=False):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    rsc_all_used = False
    #print(total_rsc)
    
    if keep_atk == True:
        assert K1 == 1
    
    # the attacker first attacks the initial lines
    for i in range(len(graph["cascading_initial"])):
        temp_initial_list = deepcopy(graph["cascading_initial"][i])
        
        # when an attack to a relay is not successful, the attack to the area may be considered again
        # this is different from the K-repeat attack to a relay
        while remain_rsc > 0:
            if len(temp_initial_list) == 0:
                break
            
            line_idx = temp_initial_list[0]
            target_relay = sga.simple_cascading_relay_select(graph, line_idx)
            target_relay_idx = target_relay.relayIdx
            if atk_mode != "normal":
                #relay_i = calSPT.find_relay_by_index(graph, target_relay_idx)
                rsc_to_be -= (target_relay.attack_cost * K1)
                if rsc_to_be < 0:
                    rsc_all_used = True
                    break
            
            remain_rsc, completed = sga.attack_relay(graph, remain_rsc, target_relay_idx, K1, False)
            
            if atk_mode != "normal":
                remain_rsc = max(rsc_to_be, 0)
            #print("Cascading attack:    ","K =",K1,"Remaining rsc:", remain_rsc, "rsc to be:", rsc_to_be)
            if completed == True: # attack is successful, trip the line
                trip_line_idx = sga.trip_line_with_relay(graph, target_relay_idx)
                cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
                temp_initial_list.remove(line_idx)
            else:
                if keep_atk == False: # not repeat unsuccessful attack, continue to the next target relay
                    #print("Current list:",temp_initial_list)
                    temp_initial_list.remove(line_idx)
                    #print("New list:",temp_initial_list)
                else:
                    #print("Repeat attack, new list:",temp_initial_list)
                    None
                
            if remain_rsc <= 0:
                #print("Resources are all used, stop attack")
                rsc_all_used = True
                break
        
        if rsc_all_used == True:
            break
    
    #tripped_line_num = sga.total_tripped_line(graph)
    #print("after cascading:",tripped_line_num)
    
    # then the attacker uses the topology knowledge attack
    K = K2 if K2 > 0 else K1
    
    if enable_area == True:
        #area_it_attack_simu_K(graph, remain_rsc, K, atk_mode)
        area_onetime_attack_simu_K(graph, remain_rsc, K, atk_mode)
    
    tripped_line_num = sga.total_tripped_line(graph)
    #print("final:",tripped_line_num)
    
    return tripped_line_num



def cascading_attack_reputation(graph, total_rsc, K1, atk_mode, K2=0, keep_atk=False, enable_area=False):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    end_attack = False
    expected_list = []
    neighbor_atk_list = []
    
    if keep_atk == True:
        assert K1 == 1 and K2 == 1
    
    for i in range(len(graph["cascading_initial"])):
        temp_initial_list = deepcopy(graph["cascading_initial"][i])
        
        while remain_rsc > 0:
            if len(temp_initial_list) == 0:
                break
            
            line_idx = temp_initial_list[0]
            target_relay = graph.es[line_idx]["prRelaySet"][0]
            if len(target_relay.ZONE3_EdgeIdx) == 0:
                target_relay = graph.es[line_idx]["prRelaySet"][1]
                
            assert len(target_relay.ZONE3_EdgeIdx) != 0
            if target_relay.relayIdx not in expected_list:
                expected_list.append(target_relay.relayIdx)
            
            neighbor_line = sga.find_cascading_reputation_line(graph, target_relay)
            
            relay_0 = graph.es[neighbor_line.index]["prRelaySet"][0]
            relay_1 = graph.es[neighbor_line.index]["prRelaySet"][1]
            #router_0 = graph.vs[relay_0.Located_Node_Idx]
            #router_1 = graph.vs[relay_1.Located_Node_Idx]
            target_router = graph.vs[target_relay.Located_Node_Idx]
            
            #print("Target line:",line_idx," ->  target relay:",target_relay.relayIdx,"with",target_relay.ZONE3_EdgeIdx)
            #print("Attacked neighbor line:",neighbor_line.index,"   with K =",K1)
            
            remain_rsc, rsc_to_be, end_attack = attack_single_area_reputation(graph, total_rsc, K1, \
                                neighbor_line.index, remain_rsc, rsc_to_be, atk_mode, enable_cas=True,\
                                cas_special=True, cas_router_idx=target_router.index, K_R=K2)
            
            if neighbor_line["tripped"] == True:
                neighbor_line["tripped"] = False
            
            neighbor_atk_list.append(neighbor_line)
            
            if end_attack == True:
                break
            
#             print("RSC:",total_rsc,"  remain_rsc:",remain_rsc,"  K =",K1,"  relay[0]:",relay_0.relayIdx,\
#                   relay_0.compromised,"  relay[1]:",relay_1.relayIdx,relay_1.compromised,"  node",\
#                   target_router.index,target_router["compromised"])
            # primary relays compromised and nearby routers compromised
            #if (relay_0.compromised == True) and (relay_1.compromised == True) and \
                #(target_router["compromised"] == True):
#             if (relay_0.compromised == True) and (relay_1.compromised == True):

            #print("Relay_0:",relay_0.relayIdx,"   Relay_1:",relay_1.relayIdx,"   Router:",target_router.index)
            if relay_0.compromised == True and relay_1.compromised == True and target_router["compromised"] == True:
                temp_initial_list.remove(line_idx)
                #print("All attacks succeed")
            else:
                if keep_atk != True:
                    temp_initial_list.remove(line_idx)
#                     if relay_0.compromised == False:
#                         print("Relay_0 at",relay_0.relayIdx,"not compromised")
#                     if relay_1.compromised == False:
#                         print("Relay_1 at",relay_1.relayIdx,"not compromised")
#                     if target_router["compromised"] == False:
#                         print("Router at",target_router.index,"not compromised")
#                     print("Some devices not compromised, continue to the next area")
                else:
#                     if relay_0.compromised == False:
#                         print("Relay_0 at",relay_0.relayIdx,"not compromised")
#                     if relay_1.compromised == False:
#                         print("Relay_1 at",relay_1.relayIdx,"not compromised")
#                     if target_router["compromised"] == False:
#                         print("Router at",target_router.index,"not compromised")
#                     print("Not all attacks succeed, repeat attack")
                    None
        
        if end_attack == True:
            break
    
    
    for es_i in graph.es:
        sga.attack_agent_protection_area_reputation(graph, es_i.index, True, True, expected_list)
    
    if enable_area == True:
        for es_i in graph.es:
            sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, True, prt_en=False)
    
#     for line_i in neighbor_atk_list:
#         if line_i["tripped"] == True:
#             line_i["tripped"] = False
    #print(graph["cascading_initial"][0])
    #print(graph["cascading_sequence"])
    for es_i in graph.es:
        if es_i.index not in graph["cascading_initial"][0] and es_i.index not in graph["cascading_sequence"][0]:
            if es_i["tripped"] == True:
                es_i["tripped"] = False
    
    tripped_line_num = sga.total_tripped_line(graph)
    
    return tripped_line_num



def cascading_attack_area(graph, total_rsc, K1, atk_mode, K2=0, enable_area=False):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    end_attack = False
    special_cas_list = []
    relay_atk_list = []
    #print("total_rsc:",total_rsc,"K1:",K1)
    
    for i in range(len(graph["cascading_initial"])):
        temp_initial_list = deepcopy(graph["cascading_initial"][i])
        
        while remain_rsc > 0:
            if len(temp_initial_list) == 0:
                break
        
            line_idx = temp_initial_list[0]
            target_relay = graph.es[line_idx]["prRelaySet"][0]
            if len(target_relay.ZONE3_EdgeIdx) == 0:
                target_relay = graph.es[line_idx]["prRelaySet"][1]
            
            neighbor_line = sga.find_cascading_area_line(graph, target_relay)
            num_relay = sga.count_total_correspond_relay(graph, neighbor_line.index)
            #print("neighbor line index:",neighbor_line.index,"   has relay number:",num_relay)
            
            remain_rsc, rsc_to_be, end_attack = sga.special_area_trip_cascading(graph, total_rsc, K1, neighbor_line.index,\
                                                        remain_rsc, rsc_to_be, target_relay, atk_mode, relay_atk_list)
            
            special_cas_list.append(neighbor_line)
            if end_attack == True:
                break
            
            temp_initial_list.remove(line_idx)
    
    if enable_area == True:
        for es_i in graph.es:
            sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, True, prt_en=False)
    else:
        # account the total number as (initial set + extra set)
        for es_i in graph.es:
            assert es_i["tripped"] != True
        
        for es_i in special_cas_list:
            sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, True, cas_special=True, prt_en=False)
        
        for es_i in graph.es:
            if es_i.index not in graph["cascading_initial"][0] and es_i.index not in graph["cascading_sequence"][0]:
                if es_i["tripped"] == True:
                    es_i["tripped"] = False
    
    tripped_line_num = sga.total_tripped_line(graph)
    
    return tripped_line_num



def area_onetime_attack_model_K(graph, total_rsc, max_try):
    available_rsc = total_rsc
    
    e_total_trip = sga.select_areas_effective_trip_model(graph, available_rsc, max_try, "area_or")
    
    return e_total_trip



def area_it_attack_model_K(graph, total_rsc, max_try):
    available_rsc = total_rsc
    
    e_total_trip = sga.select_areas_effective_trip_model(graph, available_rsc, max_try, "area_it")
    
    return e_total_trip



def cascading_attack_model_K(graph, total_rsc, max_try):
    available_rsc = total_rsc
    
    e_total_trip = sga.select_areas_effective_trip_model(graph, available_rsc, max_try, "cascading")
    
    return e_total_trip



def cascading_attack_model_2K(graph, total_rsc, K1, K2):
    """Used for cascading analysis modeling with different ``K`` in different stages.
    This model is ``special`` because for each relay, we assign exactly K1 or K2 resources.
    The amount of resource is can be more than the requirement for cascading attack.
    The unused resources will not be used by the other relays. This attack model is a lower
    bound of the normal K-attack.
        
    Args:
        graph: the graph class variable
        total_rsc (int): total available attack resources
        K1 (int): the value for max allowed number of attack to a relay in cascading stage
        K2 (int): the value for max allowed number of attack to a relay in area-attack stage
    """
    available_rsc = total_rsc
    
    e_total_trip = sga.select_areas_effective_trip_model(graph, available_rsc, K1, "cascading", K2)
    
    return e_total_trip



def simple_pure_cascading_part_model(n_k, rsc, max_try, pf):
    p_trigger = 1
    for i in range(n_k):
        p_trigger *= (1 - pf**max_try)
    
    return p_trigger



def simple_pure_cascading_expect_trip_model(n_k, n_m, K, pf):
    result = n_k*(1-pf**K) + n_m*(1-pf**K)**n_k
    
    return result



def simple_pure_cascading_result_model(mtd, n_k, rsc, K, p_f, n_m, plot_en=False):
    """Used for cascading analysis modeling
    This model is ``special`` because for each relay, we assign exactly K resources.
    The amount of resource is ``just`` sufficient to attack the initial cascading line set, e.g., n_k lines.
    The unused resources will not be used by the other relays. This attack model is a lower
    bound of the normal K-attack.
    Three different comparison parameters can be tested.
    
    Args:
        mtd (string): attack testing parameter, can be ``n_k``, ``p_f``, or ``retry``
        n_k (int): the size of the initial line set
        K (int): the value for max allowed number of attack to a relay
        p_f (double): defense strength of a relay
        plot_en (bool): whether to enable plot of the result
    """
    e_trip_list = []
    k_list = []
    efficient_list = []
    
    if mtd == "n_k":
        for i in range(1, n_k+1):
            p_trigger = simple_pure_cascading_part_model(i, i*K, K, p_f)
            e_trip_list.append(round(p_trigger,2))
            k_list.append(i)
            x_title = "Length of initial set"
    elif mtd == "p_f":
        for i in range(1, 10):
            pf = round(i/10, 1)
            p_trigger = simple_pure_cascading_part_model(n_k, n_k*K, K, pf)
            e_trip_list.append(round(p_trigger,2))
            k_list.append(i)
            x_title = "$P_f$ of line"
    elif mtd == "K_retry":
        for i in range(1, K+1):
            p_trigger = simple_pure_cascading_part_model(n_k, n_k*i, i, p_f)
            e_trip_list.append(round(p_trigger,2))
            k_list.append(i)
            x_title = "Value of K"
    elif mtd == "K_eff":
        for i in range(1, K+1):
            e_trip = simple_pure_cascading_expect_trip_model(n_k, n_m, i, p_f)
            e_trip_list.append(e_trip)
            k_list.append(i)
            efficient_list.append(round(e_trip/(n_k*i), 2))
            x_title = "Value of K"
    
    print("From analysis model", e_trip_list)
    print("From analysis model x axis", k_list)
    print("From analysis model efficiency", efficient_list)
    
    if plot_en == True:
        if mtd != "K_eff":
            ymx = 1
            plotf.myPlotFig(k_list, e_trip_list, "Probability of triggering cascading", \
                            x_title, "P(cascading)", yMax=ymx, yMin=0)
        else:
            ymx = n_k+n_m
            plotf.myPlotFig(k_list, e_trip_list, "Probability of triggering cascading", \
                            x_title, "P(cascading)", yMax=ymx, yMin=0)
            
            ymx = max(efficient_list)
            plotf.myPlotFig(k_list, efficient_list, "Efficiency of cascading attack", \
                            x_title, "Efficiency", yMax=ymx, yMin=0)



def attack_result_model_K(graph, atk_rsc, K, mtd, plot_en=False):
    e_trip_list = []
    k_list = []
    
    for k in range(1, K+1):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        
        if mtd == "random":
            avg_trip = effect_of_random_relay_selection_MCM(copyGraph, atk_rsc, k, 100)
        elif mtd == "area_or":
            avg_trip = area_onetime_attack_model_K(copyGraph, atk_rsc, k)
        elif mtd == "area_it":
            avg_trip = area_it_attack_model_K(copyGraph, atk_rsc, k)
        elif mtd == "cascading":
            avg_trip = cascading_attack_model_K(copyGraph, atk_rsc, k)
        else:
            raise ValueError("Attack type is not defined")
        
        e_trip_list.append(round(avg_trip,2))
        k_list.append(k)
        
        reset_graph_model(copyGraph)
    
    print("From analysis model", e_trip_list)
    print("From analysis model x axis", k_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from analysis model", \
                        "Value of K", "Expected tripped line", yMax=ymx, yMin=0)



def attack_result_model_for_cascading_2K(graph, atk_rsc, K1, K2, plot_en=False):
    """Used for cascading analysis modeling
    This model is ``special`` because for each relay, we assign exactly K1 or K2 resources.
    The unused resources will not be used by the other relays.
    The effect of different K2 will be tested in this function
    
    Args:
        graph: the graph class variable
        atk_rsc (int): total available attack resources
        K1 (int): the value for max allowed number of attack to a relay in cascading stage
        K2 (int): the value for max allowed number of attack to a relay in area-attack stage
        plot_en (bool): whether to enable plot of the result
    """
    e_trip_list = []
    k_list = []
    
    for k in range(1, K2+1):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        avg_trip = cascading_attack_model_2K(copyGraph, atk_rsc, K1, k)
        
        e_trip_list.append(round(avg_trip,2))
        k_list.append(k)
        
        reset_graph_model(copyGraph)
    
    print("From analysis model of K2", e_trip_list)
    print("From analysis model x axis", k_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from analysis model", \
                        "Value of K2", "Expected tripped line", yMax=ymx, yMin=0)



def attack_result_model_RSC(graph, atk_rsc, K, mtd, plot_en=False):
    e_trip_list = []
    k_list = []
    
    for rsc in range(2, atk_rsc+1, 2):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        
        if mtd == "random":
            avg_trip = effect_of_random_relay_selection_MCM(copyGraph, rsc, K, 100)
        elif mtd == "area_or":
            avg_trip = area_onetime_attack_model_K(copyGraph, atk_rsc, K)
        elif mtd == "area_it":
            avg_trip = area_it_attack_model_K(copyGraph, rsc, K)
        elif mtd == "cascading":
            avg_trip = cascading_attack_model_K(copyGraph, rsc, K)
        else:
            raise ValueError("Attack type is not defined")
        
        e_trip_list.append(round(avg_trip,2))
        k_list.append(round(rsc/(graph["total_relay_no"])*100, 2))
        
        reset_graph_model(copyGraph)
    
    print("From analysis model", e_trip_list)
    print("From analysis model x axis", k_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from analysis model", \
                        "Value of resources", "Expected tripped line", yMax=ymx, yMin=0)



def attack_special_cascading_model_K_RSC(graph, K, mtd, test, plot_en=False):
    """Used for cascading analysis modeling
    This model is ``special`` because for each relay, we assign exactly K resources.
    The amount of resource is ``just`` sufficient to attack the initial cascading line set, e.g., n_k lines.
    The unused resources will not be used by the other relays. This attack model is a lower
    bound of the normal K-attack.
    
    As comparison, the resources may be used to for other attack strategies, like area attack. The results can
    show whether pure cascading attack has sufficient benefit to be considered.
    
    Args:
        myGraph: the graph class variable
        K (int): the value for max allowed number of attack to a relay
        mtd (string): the attack strategy, include random, area_onetime, area_it, and cascading
        test (string): the parameter to test, include ``K_retry`` and ``P_f``
        plot_en (bool): whether to enable plot of the result
    """
    e_trip_list = []
    k_list = []
    efficient_list = []
    
    if test == "K_retry":
        for k in range(1, K+1):
            #copyGraph = deepcopy(graph)
            copyGraph = graph
            
            atk_rsc = max([len(ini_set) for ini_set in graph["cascading_initial"]]) * k
            pf = graph.es[0]["prRelaySet"][0].defense_strength
            #print("atk_rsc =", atk_rsc)
            
            if mtd == "random":
                avg_trip = effect_of_random_relay_selection_MCM(copyGraph, atk_rsc, k, 100)
            elif mtd == "area_or":
                avg_trip = area_onetime_attack_model_K(copyGraph, atk_rsc, k)
            elif mtd == "area_it":
                avg_trip = area_it_attack_model_K(copyGraph, atk_rsc, k)
            elif mtd == "cascading":
                avg_trip = cascading_attack_model_K(copyGraph, atk_rsc, k)
            else:
                raise ValueError("Attack type is not defined")
            
            e_trip_list.append(round(avg_trip,2))
            k_list.append(k)
            efficient_list.append(round(avg_trip/atk_rsc, 2))
            
            reset_graph_model(copyGraph)
    elif test == "P_f":
        for i in range(1, 10):
            #copyGraph = deepcopy(graph)
            copyGraph = graph
            p_f = round(i/10, 1)
            sga.set_relay_defense_strength_all(copyGraph, p_f)
            
            atk_rsc = max([len(ini_set) for ini_set in graph["cascading_initial"]]) * K
            retry = K
            #print("atk_rsc =", atk_rsc)
            
            if mtd == "random":
                avg_trip = effect_of_random_relay_selection_MCM(copyGraph, atk_rsc, retry, 100)
            elif mtd == "area_or":
                avg_trip = area_onetime_attack_model_K(copyGraph, atk_rsc, retry)
            elif mtd == "area_it":
                avg_trip = area_it_attack_model_K(copyGraph, atk_rsc, retry)
            elif mtd == "cascading":
                avg_trip = cascading_attack_model_K(copyGraph, atk_rsc, retry)
            else:
                raise ValueError("Attack type is not defined")
            
            e_trip_list.append(round(avg_trip,2))
            k_list.append(p_f)
            efficient_list.append(round(avg_trip/atk_rsc, 2))
            
            reset_graph_model(copyGraph)
    
    print("From K_RSC special analysis model", e_trip_list)
    print("From analysis model x axis", k_list)
    print("Attack RSC efficiency model",efficient_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from analysis model", \
                        "Value of K", "Expected tripped line", yMax=ymx, yMin=0)
        
        ymx = max(efficient_list)
        plotf.myPlotFig(k_list, efficient_list, "Efficiency from analysis model", \
                        "Value of K", "Efficiency", yMax=ymx, yMin=0)



def reset_graph_model(graph):
    for es_i in graph.es:
        es_i["p_line_already_tripped"] = 0
        for relay_i in es_i["prRelaySet"]:
            relay_i.compromised = False
            relay_i.p_cmp = 0
            relay_i.attacked = False
    
    for cascade_mark_i in graph["cascading_result"]:
        cascade_mark_i.total_value = 0
        cascade_mark_i.triggered = False



def reset_graph_simu(graph):
    for es_i in graph.es:
        es_i["tripped"] = False
        es_i["p_line_already_tripped"] = 0
        for relay_i in es_i["prRelaySet"]:
            relay_i.compromised = False
            relay_i.p_cmp = 0
            relay_i.attacked = False
    
    for vs_i in graph.vs:
        vs_i["compromised"] = False
    
    for cascade_mark_i in graph["cascading_result"]:
        cascade_mark_i.total_value = 0
        cascade_mark_i.triggered = False


global_cnt = 0
def area_onetime_attack_simu_K(graph, total_rsc, max_try, atk_mode="normal", enable_cas=True, myrtn={}):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    area_attacked = []
    test_list = {}
    test_list2 = {}
    nb_trip_two_pr = {}
    nb_trip_one_pr = {}
    
    trip_num_dict = sga.select_area_one_time_simu(graph)
    while remain_rsc > 0:
        end_attack = False
        if len(area_attacked) == len(graph.es):
            break
        
        new_trip_list = sga.shuffle_area_one_time_simu(trip_num_dict)
        #selected_area_idx = max(trip_num_dict, key=lambda i: trip_num_dict[i])
        
        # from the randomly organized list of pairs, select the pair with max affected line numbers
        # the pair is (idx, affected_number)
        selected_area_idx = max(new_trip_list,key=lambda item:item[1])[0]
        
        if selected_area_idx in area_attacked: # the area is already attacked successfully
            trip_num_dict.pop(selected_area_idx)
            continue
        
        assert graph.es[selected_area_idx]["prRelaySet"] != []
        relay_0 = graph.es[selected_area_idx]["prRelaySet"][0]
        relay_1 = graph.es[selected_area_idx]["prRelaySet"][1]
        
        # in the cascading area-attack part, if a relay is compromised in the cascading-attack, it does not
        # need to be attacked again
        # For simplicity, we can assume that in the area-attack part, two relays will be attacked, and 
        # "K" resources will be assigned to each relay
        relay_atk_num = 0
        for relay_i in graph.es[selected_area_idx]["prRelaySet"]:
            # if the relay is already compromised in cascading, then it does not need to be attacked again
            if relay_i.compromised == True:
                continue
            
            if atk_mode != "normal":
                if remain_rsc < (relay_i.attack_cost * max_try):
                    end_attack = True
                    break
            
            rsc_to_be -= (relay_i.attack_cost * max_try)
            remain_rsc_old = remain_rsc
            remain_rsc, completed = sga.attack_relay(graph, remain_rsc, relay_i.relayIdx, max_try, False)
            
            if remain_rsc_old == remain_rsc:
                print("Total RSC:",total_rsc,"   remain_rsc =",remain_rsc,"rsc_to_be =",rsc_to_be)
            # in the simple model, assume each relay is assigned K resources
            # the unused resources are not used for other relays
            if atk_mode != "normal":
                remain_rsc = max(rsc_to_be, 0)
            
            relay_atk_num += 1
            assert relay_i.attacked == True
            
            if completed == True:
                trip_line_idx = sga.trip_line_with_relay(graph, relay_i.relayIdx)
                
                if enable_cas == True:
                    cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                    sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
                 
            if remain_rsc <= 0:
                break
        
        if end_attack == True:
            sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
            break
        
        #print("Area-or attack:    ","K =",max_try,"Remaining rsc:", remain_rsc, "rsc to be:", rsc_to_be)
        sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
        
        # if both primary relays compromised, then the area can be excluded from the options
        #if (relay_0.compromised == True) and (relay_1.compromised == True):
        
        # in one-round selection, a selected area will not repeat the attack even not all relays are compromised
        area_attacked.append(selected_area_idx)
        trip_num_dict.pop(selected_area_idx)
        
        
    print("Selected original area:",area_attacked)
    # attack the backup relays using agent-protection mechanism
    if "total_local_trip" not in myrtn:
        myrtn["total_local_trip"] = []
    extra_trip = 0
    local_trip = 0
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_pure_relay(graph, es_i.index, enable_cas, True, test_list, \
                                            test_list2, nb_trip_two_pr, nb_trip_one_pr)
#         extra_trip += sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, enable_cas, True, test_list,\
#                                                                     test_list2)
        if es_i["prRelaySet"] != []:
            if es_i["prRelaySet"][0].compromised == True or es_i["prRelaySet"][1].compromised == True:
                local_trip += 1
    
    print("Number of lines can be tripped by reputation and with local relays compromised:")
    print("Length",len(test_list),"  ->",test_list)
    print("Number of extra relays:",sum([len(test_list[key]) for key in test_list]))
    print("Area trip condition:",test_list2,"\n")
    total_neighbor_line_trip = sum([len(test_list2[key]) for key in test_list2]) if test_list2!={} else 0
    
    if "total_neighbor_trip" not in myrtn:
        myrtn["total_neighbor_trip"] = []
    assert "total_neighbor_trip" in myrtn
    myrtn["total_neighbor_trip"].append(total_neighbor_line_trip)
    myrtn["total_local_trip"].append(local_trip)
    
    # record the number of effective neighbor trips for two-pr-relay or one-pr-relay cases
    neighbor_trip_two_pr = sum(nb_trip_two_pr.values()) if nb_trip_two_pr!={} else 0
    neighbor_trip_one_pr = sum(nb_trip_one_pr.values()) if nb_trip_one_pr!={} else 0
    
    if "neighbor_trip_two_pr_relay" not in myrtn:
        myrtn["neighbor_trip_two_pr_relay"] = []
    if "neighbor_trip_single_pr_relay" not in myrtn:
        myrtn["neighbor_trip_single_pr_relay"] = []
    
    myrtn["neighbor_trip_two_pr_relay"].append(neighbor_trip_two_pr)
    myrtn["neighbor_trip_single_pr_relay"].append(neighbor_trip_one_pr)
    
    #print("Total extra trip:",extra_trip,"K =",max_try)
    tripped_line_num = sga.total_tripped_line(graph)
    return tripped_line_num


# Area attack with one-round selection and majority rule
# We may repeat attack an area if attack to any of the selected devices is not successful
# We may also attack an area only once, which is easier for analysis
def area_onetime_attack_majority_simu_K(graph, total_rsc, max_try, atk_mode="normal", enable_cas=True, myrtn={}):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    area_attacked = []
    relay_compromised = 0
    attack_try = 0
    all_selected_area = []
    test_list = {}
    test_list2 = {}
    
    trip_num_dict = sga.select_area_one_time_simu(graph)
    while remain_rsc > 0:
        end_attack = False
        if (len(area_attacked) == len(graph.es)) or (len(trip_num_dict) == 0):
            break
        
        new_trip_list = sga.shuffle_area_one_time_simu(trip_num_dict)
        selected_area_idx = max(new_trip_list,key=lambda item:item[1])[0]
        if selected_area_idx in area_attacked: # the area is already attacked
            trip_num_dict.pop(selected_area_idx)
            continue
        
        all_selected_area.append(selected_area_idx)
        relay_atk_num = 0
        total_correspond_relay = sga.count_total_correspond_relay(graph, selected_area_idx)
        total_compromised_relay = sga.count_area_compromised_relay(graph, selected_area_idx)
        
        if total_compromised_relay < math.ceil(total_correspond_relay/2+0.5): # majority not achieved
            assert math.ceil(total_correspond_relay/2) >= (total_correspond_relay/2)
            relay_to_atk = math.ceil(total_correspond_relay/2+0.5) - total_compromised_relay
            
            if (atk_mode == "normal") and (relay_to_atk > remain_rsc): # resource not enough
                trip_num_dict.pop(selected_area_idx)
                continue
            elif (atk_mode != "normal") and (relay_to_atk * max_try > remain_rsc): # rsc not enough for K-each
                trip_num_dict.pop(selected_area_idx)
                continue
        else: # majority achieved, check if primary relays are attacked
            relay_to_atk = 0
        
        print("Attacking area",selected_area_idx,"with",total_correspond_relay,"relays and",total_compromised_relay,"cp")
        relay_rm_list = []
        assert graph.es[selected_area_idx]["prRelaySet"] != []
        relay_0 = graph.es[selected_area_idx]["prRelaySet"][0]
        relay_1 = graph.es[selected_area_idx]["prRelaySet"][1]
        
        relay_rm_list.append(relay_0)
        relay_rm_list.append(relay_1)
        
        for bp_relay_i in relay_0.bp_relay:
            relay_rm_list.append(bp_relay_i)
        for bp_relay_j in relay_1.bp_relay:
            relay_rm_list.append(bp_relay_j)
            
        # the primary relays will always be checked and attacked
        # even if relay_to_atk <= 0, if the primary relays are not compromised, attack will be performed
        for relay_i in relay_rm_list:
            if relay_i.compromised == True:
                continue
            
            if (relay_i != relay_0) and (relay_i != relay_1) and (relay_to_atk <= 0):
                break
            
            if atk_mode != "normal":
                if remain_rsc < (relay_i.attack_cost * max_try):
                    end_attack = True
                    break
            
            rsc_to_be -= (relay_i.attack_cost * max_try)
            remain_rsc_old = remain_rsc
            remain_rsc, completed = sga.attack_relay(graph, remain_rsc, relay_i.relayIdx, max_try, False)
            attack_try += 1
            
            if remain_rsc_old == remain_rsc:
                print("Total RSC:",total_rsc,"   remain_rsc =",remain_rsc,"rsc_to_be =",rsc_to_be)
            
            relay_atk_num += 1
        
            if completed == True:
                trip_line_idx = sga.trip_line_with_relay(graph, relay_i.relayIdx)
                relay_to_atk -= 1
                relay_compromised += 1
                
                if enable_cas == True:
                    cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                    sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
            
            if atk_mode != "normal":
                remain_rsc = max(rsc_to_be, 0)
                if remain_rsc <= 0:
                    break
                    
        if end_attack == True:
            sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
            break
        
        sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
        
        # primary relays compromised, and majority achieved
        if (relay_0.compromised == True) and (relay_1.compromised == True) and (relay_to_atk <= 0):
            print("Area",selected_area_idx,"successfully compromised!")
        else:
            print("Area",selected_area_idx,"not all completed!")
            
        # in one-round selection, a selected area will not repeat the attack even not all relays are compromised
        area_attacked.append(selected_area_idx)
        trip_num_dict.pop(selected_area_idx)
    
    #print("Total compromised relays for rsc:",total_rsc,"is:",relay_compromised,"   attack try:",attack_try)
    print("total_rsc:",total_rsc,"remain_rsc:",remain_rsc,"Selected reputation area:",area_attacked,\
          "All selected area:",all_selected_area,len(all_selected_area))
    if "total_local_trip" not in myrtn:
        myrtn["total_local_trip"] = []
    extra_trip = 0
    local_trip = 0
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, enable_cas, True, test_list,\
                                                                      test_list2)
        if es_i["prRelaySet"] != []:
            if es_i["prRelaySet"][0].compromised == True or es_i["prRelaySet"][1].compromised == True:
                local_trip += 1
    
    print("Number of lines can be tripped by reputation and with local relays compromised:")
    print("Length",len(test_list),"  ->",test_list)
    print("Number of extra relays:",sum([len(test_list[key]) for key in test_list]))
    print("Area trip condition:",test_list2,"\n")
    total_neighbor_line_trip = sum([len(test_list2[key]) for key in test_list2]) if test_list2!={} else 0
    
    if "total_neighbor_trip" not in myrtn:
        myrtn["total_neighbor_trip"] = []
    assert "total_neighbor_trip" in myrtn
    myrtn["total_neighbor_trip"].append(total_neighbor_line_trip)
    myrtn["total_local_trip"].append(local_trip)
        
    tripped_line_num = sga.total_tripped_line(graph)
    return tripped_line_num


# Area attack with one-round selection and reputation scheme
# for each selected area, attack the two primary relays and the two nearby routers (vertex)
def area_onetime_attack_reputation_simu_K(graph, total_rsc, max_try, atk_mode="normal", enable_cas=True):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    area_attacked = []
    
    trip_num_dict = sga.select_area_one_time_simu(graph)
    while remain_rsc > 0:
        end_attack = False
        if (len(area_attacked) == len(graph.es)) or (len(trip_num_dict) == 0):
            break
        
        new_trip_list = sga.shuffle_area_one_time_simu(trip_num_dict)
        selected_area_idx = max(new_trip_list,key=lambda item:item[1])[0]
        if selected_area_idx in area_attacked: # the area is already attacked
            trip_num_dict.pop(selected_area_idx)
            continue
        
        relay_0 = graph.es[selected_area_idx]["prRelaySet"][0]
        relay_1 = graph.es[selected_area_idx]["prRelaySet"][1]
        router_0 = graph.vs[relay_0.Located_Node_Idx]
        router_1 = graph.vs[relay_1.Located_Node_Idx]
        
        remain_rsc, rsc_to_be, end_attack = attack_single_area_reputation(graph, total_rsc, max_try, \
                                        selected_area_idx, remain_rsc, rsc_to_be, atk_mode, enable_cas)
            
        if end_attack == True:
            break
        
        #sga.debug_area_relay_reputation_situation(graph, selected_area_idx, total_rsc)
        #print()
        # primary relays compromised and nearby routers compromised
        if (relay_0.compromised == True) and (relay_1.compromised == True) and \
            (router_0["compromised"] == True) and (router_1["compromised"] == True):
            #print("Area",selected_area_idx,"with: relay",relay_0.relayIdx,"   relay",relay_1.relayIdx,end='')
            #print("      node",router_0.index,"   node",router_1.index)
            area_attacked.append(selected_area_idx)
            trip_num_dict.pop(selected_area_idx)
    
    extra_trip = 0
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_area_reputation(graph, es_i.index, enable_cas, False)
    
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, enable_cas, False)
    
    tripped_line_num = sga.total_tripped_line(graph)
    return tripped_line_num



def attack_single_area_reputation(graph, total_rsc, max_try, area_idx, remain_rsc, rsc_to_be,\
                                  atk_mode="normal", enable_cas=True, cas_special=False, cas_router_idx=-1, K_R=-1):
    relay_0 = graph.es[area_idx]["prRelaySet"][0]
    relay_1 = graph.es[area_idx]["prRelaySet"][1]
    router_0 = graph.vs[relay_0.Located_Node_Idx]
    router_1 = graph.vs[relay_1.Located_Node_Idx]
    router_list = []
    end_attack = False
    
    if cas_special == False:
        router_list.append(router_0)
        router_list.append(router_1)
    else:
        assert cas_router_idx >= 0
        cas_router = graph.vs[cas_router_idx]
        router_list.append(cas_router)
    
    
    for relay_i in graph.es[area_idx]["prRelaySet"]:
        # if the relay is already compromised in cascading attack, then it does not need to be attacked again
        if relay_i.compromised == True:
            continue
        
        if atk_mode != "normal":
            if remain_rsc < (relay_i.attack_cost * max_try):
                end_attack = True
                break
        
        rsc_to_be -= (relay_i.attack_cost * max_try)
        remain_rsc, completed = sga.attack_relay(graph, remain_rsc, relay_i.relayIdx, max_try, False)
        
        # in the simple model, assume each relay is assigned K resources
        # the unused resources are not used for other relays
        if atk_mode != "normal":
            remain_rsc = max(rsc_to_be, 0)
        
        
        if completed == True:
            trip_line_idx = sga.trip_line_with_relay(graph, relay_i.relayIdx)
            
            if enable_cas == True:
                cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
        
        if remain_rsc <= 0:
            break
    
    for router_i in router_list: # attack the routers
        if cas_special != True:
            KR = max(1, K_R)
        else:
            KR = K_R
        if atk_mode != "normal":
            if remain_rsc < (router_i["cost"] * KR):
                end_attack = True
                break
        
        # if the router is already compromised then it does not need to be attacked again
        if router_i["compromised"] == True:
            continue
        rsc_to_be -= (router_i["cost"] * KR)
        remain_rsc, completed = sga.attack_router(graph, remain_rsc, router_i.index, KR, False)
        if atk_mode != "normal":
            remain_rsc = max(rsc_to_be, 0)
        
        if remain_rsc <= 0:
            break
    
    return remain_rsc, rsc_to_be, end_attack



def area_it_attack_simu_K(graph, total_rsc, max_try, atk_mode="normal", enable_cas=True, myrtn={}):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    area_attacked = []
    area_already_attacked = []
    continue_last_atk = False
    test_list = {}
    test_list2 = {}
    nb_trip_two_pr = {}
    nb_trip_one_pr = {}
    local_trip_v2 = 0
    local_trip_v3 = 0
    
    if "total_local_trip" not in myrtn:
        myrtn["total_local_trip"] = []
    if "total_neighbor_trip" not in myrtn:
        myrtn["total_neighbor_trip"] = []
    
    # with enough resources, select and attack areas/relays
    while remain_rsc > 0:
        if len(area_attacked) == len(graph.es):
            break
        
        end_attack = False
        
        # if the last targeted area is not completely attacked,then keep attacking
        if continue_last_atk != True:
            selected_area_idx = sga.select_area_trip_num_simu(graph, area_attacked)
        
        if selected_area_idx in area_attacked: # the area is already attacked
            continue
        
        assert graph.es[selected_area_idx]["prRelaySet"] != []
        relay_0 = graph.es[selected_area_idx]["prRelaySet"][0]
        relay_1 = graph.es[selected_area_idx]["prRelaySet"][1]
        
        to_be_counted_as_tripped = False
        if graph.es[selected_area_idx]["tripped"] != True:
            to_be_counted_as_tripped = True
        
        relay_atk_num = 0
        # select the two primary relays of the area, and attack
        for relay_i in graph.es[selected_area_idx]["prRelaySet"]:
            if relay_i.compromised == True:
                continue
            
            if atk_mode != "normal":
                if remain_rsc < (relay_i.attack_cost * max_try):
                    end_attack = True
                    break
            
            rsc_to_be -= (relay_i.attack_cost * max_try)
            remain_rsc, completed = sga.attack_relay(graph, remain_rsc, relay_i.relayIdx, max_try, False)
            
            #print("Relay",relay_i.relayIdx,"in area",selected_area_idx,"   compromised is",completed)
            # in the simple model, assume each relay is assigned K resources
            # the unused resources are not used for other relays
            if atk_mode != "normal":
                remain_rsc = max(rsc_to_be, 0)
            
            relay_atk_num += 1
            
            if completed == True:
                trip_line_idx = sga.trip_line_with_relay(graph, relay_i.relayIdx)
                
                if enable_cas == True:
                    cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                    sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
                 
            if remain_rsc <= 0:
                break
        
        #if relay_atk_num > 0:
        # if both primary relays compromised, then the area can be excluded from the options
        if (relay_0.compromised == True) and (relay_1.compromised == True):
            if selected_area_idx not in area_already_attacked:
                area_already_attacked.append(selected_area_idx)
            area_attacked.append(selected_area_idx)
            continue_last_atk = False
            
            # attack the backup relays using agent-protection mechanism
            # so the "available trip" in the area will be updated
            sga.attack_agent_protection_pure_relay(graph, selected_area_idx, enable_cas, True, test_list, \
                                                test_list2, nb_trip_two_pr, nb_trip_one_pr)
#             sga.attack_agent_protection_pure_relay_majority(graph, selected_area_idx, enable_cas, True, test_list,\
#                                                 test_list2, nb_trip_two_pr, nb_trip_one_pr)
        else:
            if selected_area_idx not in area_already_attacked:
                area_already_attacked.append(selected_area_idx)
            continue_last_atk = True
        
        if graph.es[selected_area_idx]["tripped"] == True and to_be_counted_as_tripped == True:
            local_trip_v2 += 1
        
        if end_attack == True:
            break
        
    
    extra_trip = 0
    local_trip = 0
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_pure_relay(graph, es_i.index, enable_cas, True, test_list, \
                                                test_list2, nb_trip_two_pr, nb_trip_one_pr)
#         extra_trip += sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, enable_cas, True, test_list,\
#                                                 test_list2, nb_trip_two_pr, nb_trip_one_pr)
        if es_i["prRelaySet"] != []:
            if es_i["prRelaySet"][0].compromised == True or es_i["prRelaySet"][1].compromised == True:
                local_trip += 1
    
    total_neighbor_line_trip = sum([len(test_list2[key]) for key in test_list2]) if test_list2!={} else 0
    
    #myrtn["total_neighbor_trip"].append(total_neighbor_line_trip)
    #myrtn["total_local_trip"].append(local_trip)
    #myrtn["total_local_trip"].append(local_trip_v2)
    
    # record the number of effective neighbor trips for two-pr-relay or one-pr-relay cases
    neighbor_trip_two_pr = sum(nb_trip_two_pr.values()) if nb_trip_two_pr!={} else 0
    neighbor_trip_one_pr = sum(nb_trip_one_pr.values()) if nb_trip_one_pr!={} else 0
    
    if "neighbor_trip_two_pr_relay" not in myrtn:
        myrtn["neighbor_trip_two_pr_relay"] = []
    if "neighbor_trip_single_pr_relay" not in myrtn:
        myrtn["neighbor_trip_single_pr_relay"] = []
    
    myrtn["neighbor_trip_two_pr_relay"].append(neighbor_trip_two_pr)
    myrtn["neighbor_trip_single_pr_relay"].append(neighbor_trip_one_pr)
    
    # if a line has at least one relay compromised, it is counted as "local trip"
    for line_idx in area_already_attacked:
        relay_i0 = graph.es[line_idx]["prRelaySet"][0]
        relay_i1 = graph.es[line_idx]["prRelaySet"][1]
        
        if relay_i0.compromised == True or relay_i1.compromised == True:
            local_trip_v3 += 1
    
    tripped_line_num = sga.total_tripped_line(graph)
    #print("select",len(area_attacked),"areas:",area_attacked)
    #sga.total_attack_trip_info(graph)
    
    myrtn["total_neighbor_trip"].append(tripped_line_num-local_trip_v3)
    myrtn["total_local_trip"].append(local_trip_v3)
    
    return tripped_line_num



def area_it_attack_majority_simu_K(graph, total_rsc, max_try, atk_mode="normal", enable_cas=True, myrtn={}):
    remain_rsc = total_rsc
    rsc_to_be = total_rsc
    area_attacked = []
    relay_compromised = 0
    attack_try = 0
    all_selected_area = []
    continue_last_atk = False
    selected_area_idx = -1
    test_list = {}
    test_list2 = {}
    
    if "total_local_trip" not in myrtn:
        myrtn["total_local_trip"] = []
    if "total_neighbor_trip" not in myrtn:
        myrtn["total_neighbor_trip"] = []
    
    while remain_rsc > 0:
        end_attack = False
        if len(area_attacked) == len(graph.es):
            break
        
        # if the last targeted area is not completely attacked,then keep attacking
        if continue_last_atk != True:
            selected_area_idx = sga.select_area_trip_num_simu(graph, area_attacked)
            last_idx = selected_area_idx
        else:
            assert last_idx == selected_area_idx
        
        if selected_area_idx in area_attacked: # the area is already attacked
            continue
        
        all_selected_area.append(selected_area_idx)
        total_correspond_relay = sga.count_total_correspond_relay(graph, selected_area_idx)
        total_compromised_relay = sga.count_area_compromised_relay(graph, selected_area_idx)
        
        if total_compromised_relay < math.ceil(total_correspond_relay/2+0.5): # majority not achieved
            assert math.ceil(total_correspond_relay/2) >= (total_correspond_relay/2)
            relay_to_atk = math.ceil(total_correspond_relay/2+0.5) - total_compromised_relay
            
            if (atk_mode == "normal") and (relay_to_atk > remain_rsc): # resource not enough
                area_attacked.append(selected_area_idx) # consider the area "attacked"
                continue_last_atk = False
                continue
            elif (atk_mode != "normal") and (relay_to_atk * max_try > remain_rsc): # rsc not enough for K-each
                area_attacked.append(selected_area_idx) # consider the area "attacked"
                continue_last_atk = False
                continue
        else: # majority achieved, check if primary relays are attacked
            relay_to_atk = 0
        
        #print("Attacking area",selected_area_idx,"with",total_correspond_relay,"relays and",total_compromised_relay,"cp")
        relay_rm_list = []
        assert graph.es[selected_area_idx]["prRelaySet"] != []
        relay_0 = graph.es[selected_area_idx]["prRelaySet"][0]
        relay_1 = graph.es[selected_area_idx]["prRelaySet"][1]
        
        relay_rm_list.append(relay_0)
        relay_rm_list.append(relay_1)
        
        for bp_relay_i in relay_0.bp_relay:
            relay_rm_list.append(bp_relay_i)
        for bp_relay_j in relay_1.bp_relay:
            relay_rm_list.append(bp_relay_j)
        
        # the primary relays will always be checked and attacked
        # even if relay_to_atk <= 0, if the primary relays are not compromised, attack will be performed
        for relay_i in relay_rm_list:
            if relay_i.compromised == True:
                continue
            
            if (relay_i != relay_0) and (relay_i != relay_1) and (relay_to_atk <= 0):
                break
            
            if atk_mode != "normal":
                if remain_rsc < (relay_i.attack_cost * max_try):
                    end_attack = True
                    break
            
            rsc_to_be -= (relay_i.attack_cost * max_try)
            remain_rsc, completed = sga.attack_relay(graph, remain_rsc, relay_i.relayIdx, max_try, False)
            attack_try += 1
        
            if completed == True:
                trip_line_idx = sga.trip_line_with_relay(graph, relay_i.relayIdx)
                relay_to_atk -= 1
                relay_compromised += 1
                
                if enable_cas == True:
                    cascade_check_list = sga.cascading_initial_line_check(graph, trip_line_idx)
                    sga.trip_cascading_sequence_from_list(graph, cascade_check_list)
            
            if atk_mode != "normal":
                remain_rsc = max(rsc_to_be, 0)
                if remain_rsc <= 0:
                    break
        
        if (relay_0.compromised == True) and (relay_1.compromised == True) and (relay_to_atk <= 0):
            print("Area",selected_area_idx,"successfully compromised!")
            area_attacked.append(selected_area_idx)
            continue_last_atk = False
            sga.attack_agent_protection_pure_relay_majority(graph, selected_area_idx, enable_cas, True, test_list,\
                                                                      test_list2)
        else:
            continue_last_atk = True
        
        if end_attack == True:
            sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
            break
        
        sga.debug_area_relay_majority_situation(graph, selected_area_idx, total_rsc, remain_rsc)
    
    print("total_rsc:",total_rsc,"remain_rsc:",remain_rsc,"Selected reputation area:",area_attacked,\
        "All selected area:",all_selected_area,len(all_selected_area))
    extra_trip = 0
    local_trip = 0
    for es_i in graph.es:
        extra_trip += sga.attack_agent_protection_pure_relay_majority(graph, es_i.index, enable_cas, True, test_list,\
                                                                      test_list2)
        if es_i["prRelaySet"] != []:
            if es_i["prRelaySet"][0].compromised == True or es_i["prRelaySet"][1].compromised == True:
                local_trip += 1
    
    total_neighbor_line_trip = sum([len(test_list2[key]) for key in test_list2]) if test_list2!={} else 0
    myrtn["total_neighbor_trip"].append(total_neighbor_line_trip)
    myrtn["total_local_trip"].append(local_trip)
    print("Number of lines can be tripped by reputation and with local relays compromised:")
    print("Length",len(test_list),"  ->",test_list)
    print("Number of extra relays:",sum([len(test_list[key]) for key in test_list]))
    print("Area trip condition:",test_list2,"\n")
    
    tripped_line_num = sga.total_tripped_line(graph)
    return tripped_line_num


def repeat_area_based_attack_it_simu_K(graph, total_rsc, test_repeat_num=1, K=1, atk_mode="normal", \
                                       enable_cas=False, rtn_data={}):
    """In the simulation, the area-attack is performed for multiple times to obtain the averaged performance
    
    Args:
        graph: the graph class variable
        total_rsc (int): the total resource used for attack
        test_repeat_num (int): the number of repeating the test
        K (int): the maximum number of attack to a relay
        atk_mode (string): "K_Each" means assign K resources to a relay, 
                            unused resources are not assigned to other relays 
    """
    trip_num_list = []
    rtn_result = {}
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        e_total_trip = area_it_attack_simu_K(copyGraph, total_rsc, K, atk_mode, enable_cas, rtn_result)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(copyGraph)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    if "total_neighbor_trip" not in rtn_data:
        rtn_data["total_neighbor_trip"] = []
    if "total_local_trip" not in rtn_data:
        rtn_data["total_local_trip"] = []
    if "neighbor_trip_two_pr_relay" not in rtn_data:
        rtn_data["neighbor_trip_two_pr_relay"] = []
    if "neighbor_trip_single_pr_relay" not in rtn_data:
        rtn_data["neighbor_trip_single_pr_relay"] = []
    
    rtn_data["total_neighbor_trip"].append(calSPT.mean(rtn_result["total_neighbor_trip"]))
    rtn_data["total_local_trip"].append(calSPT.mean(rtn_result["total_local_trip"]))
    rtn_data["neighbor_trip_two_pr_relay"].append(calSPT.mean(rtn_result["neighbor_trip_two_pr_relay"]))
    rtn_data["neighbor_trip_single_pr_relay"].append(calSPT.mean(rtn_result["neighbor_trip_single_pr_relay"]))
    
    return avg_trip_line_num


def repeat_area_based_attack_it_majority_simu_K(graph, total_rsc, test_repeat_num=1, K=1, atk_mode="normal", \
                                       enable_cas=False, rtn_data={}):
    """In the simulation, the area-attack is performed for multiple times to obtain the averaged performance
    
    Args:
        graph: the graph class variable
        total_rsc (int): the total resource used for attack
        test_repeat_num (int): the number of repeating the test
        K (int): the maximum number of attack to a relay
        atk_mode (string): "K_Each" means assign K resources to a relay, 
                            unused resources are not assigned to other relays 
    """
    trip_num_list = []
    rtn_result = {}
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        e_total_trip = area_it_attack_majority_simu_K(copyGraph, total_rsc, K, atk_mode, enable_cas, rtn_result)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(copyGraph)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    if "total_neighbor_trip" not in rtn_data:
        rtn_data["total_neighbor_trip"] = []
    if "total_local_trip" not in rtn_data:
        rtn_data["total_local_trip"] = []
    rtn_data["total_neighbor_trip"].append(calSPT.mean(rtn_result["total_neighbor_trip"]))
    rtn_data["total_local_trip"].append(calSPT.mean(rtn_result["total_local_trip"]))
    
    return avg_trip_line_num


def repeat_area_based_attack_onetime_simu_K(graph, total_rsc, test_repeat_num=1, \
                                            K=1, atk_mode="normal", enable_cas=False, rtn_data={}):
    """In the simulation, the area-attack is performed for multiple times to obtain the averaged performance
    
    Args:
        graph: the graph class variable
        total_rsc (int): the total resource used for attack
        test_repeat_num (int): the number of repeating the test
        K (int): the maximum number of attack to a relay
        atk_mode (string): "K_Each" means assign K resources to a relay, 
                            unused resources are not assigned to other relays 
    """
    trip_num_list = []
    rtn_result = {}
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        e_total_trip = area_onetime_attack_simu_K(copyGraph, total_rsc, K, atk_mode, enable_cas, rtn_result)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(copyGraph)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    
    if "total_neighbor_trip" not in rtn_data:
        rtn_data["total_neighbor_trip"] = []
    if "total_local_trip" not in rtn_data:
        rtn_data["total_local_trip"] = []
    if "neighbor_trip_two_pr_relay" not in rtn_data:
        rtn_data["neighbor_trip_two_pr_relay"] = []
    if "neighbor_trip_single_pr_relay" not in rtn_data:
        rtn_data["neighbor_trip_single_pr_relay"] = []
    
    rtn_data["total_neighbor_trip"].append(calSPT.mean(rtn_result["total_neighbor_trip"]))
    rtn_data["total_local_trip"].append(calSPT.mean(rtn_result["total_local_trip"]))
    rtn_data["neighbor_trip_two_pr_relay"].append(calSPT.mean(rtn_result["neighbor_trip_two_pr_relay"]))
    rtn_data["neighbor_trip_single_pr_relay"].append(calSPT.mean(rtn_result["neighbor_trip_single_pr_relay"]))
    
    return avg_trip_line_num


def repeat_area_based_attack_onetime_majority_simu_K(graph, total_rsc, test_repeat_num=1, \
                                                     K=1, atk_mode="normal", enable_cas=False, rtn_data={}):
    """In the simulation, the area-attack is performed for multiple times to obtain the averaged performance
    
    Args:
        graph: the graph class variable
        total_rsc (int): the total resource used for attack
        test_repeat_num (int): the number of repeating the test
        K (int): the maximum number of attack to a relay
        atk_mode (string): "K_Each" means assign K resources to a relay, 
                            unused resources are not assigned to other relays 
    """
    trip_num_list = []
    rtn_result = {}
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        e_total_trip = area_onetime_attack_majority_simu_K(copyGraph, total_rsc, K, atk_mode, enable_cas, rtn_result)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(copyGraph)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    if "total_neighbor_trip" not in rtn_data:
        rtn_data["total_neighbor_trip"] = []
    if "total_local_trip" not in rtn_data:
        rtn_data["total_local_trip"] = []
    rtn_data["total_neighbor_trip"].append(calSPT.mean(rtn_result["total_neighbor_trip"]))
    rtn_data["total_local_trip"].append(calSPT.mean(rtn_result["total_local_trip"]))
    
    return avg_trip_line_num


def repeat_area_based_attack_onetime_reputation_simu_K(graph, total_rsc, test_repeat_num=1, \
                                                     K=1, atk_mode="normal", enable_cas=False):
    trip_num_list = []
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        e_total_trip = area_onetime_attack_reputation_simu_K(copyGraph, total_rsc, K, atk_mode, enable_cas)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(copyGraph)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    
    return avg_trip_line_num


def repeat_cascading_attack_simu_K(graph, total_rsc, test_repeat_num=1, K1=1, K2=0, atk_mode="normal", c_mode="direct"):
    trip_num_list = []
    
    for i in range(test_repeat_num):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        if c_mode == "direct":
            e_total_trip = cascading_knowledge_attack(copyGraph, total_rsc, K1, atk_mode, K2, False)
        elif c_mode == "direct_persist":
            e_total_trip = cascading_knowledge_attack(copyGraph, total_rsc, K1, atk_mode, K2, True)
        elif c_mode == "reputation":
            e_total_trip = cascading_attack_reputation(copyGraph, total_rsc, K1, atk_mode, K2, False)
        elif c_mode == "reputation_persist":
            e_total_trip = cascading_attack_reputation(copyGraph, total_rsc, K1, atk_mode, K2, True)
        elif c_mode == "area":
            e_total_trip = cascading_attack_area(copyGraph, total_rsc, K1, atk_mode, K2, False)
        
        trip_num_list.append(e_total_trip)
        reset_graph_simu(graph)
        #print("-----------------------------------------------------------------",e_total_trip)
        
    avg_trip_line_num = calSPT.mean(trip_num_list)
    
    return avg_trip_line_num



def attack_result_simu_K(graph, atk_rsc, mtd, K1, K2=0, atk_mode="normal", plot_en=False):
    e_trip_list = []
    k_list = []
    efficient_list = []
    
    if K2 <= 0:
        for k in range(1, K1+1):
            #copyGraph = deepcopy(graph)
            copyGraph = graph
            
            if mtd == "random":
                avg_trip = effect_of_random_relay_selection_MCM(copyGraph, atk_rsc, k, 1000)
            elif mtd == "area_it":
                avg_trip = repeat_area_based_attack_it_simu_K(copyGraph, atk_rsc, 100, k, atk_mode)
            elif mtd == "area_or":
                avg_trip = repeat_area_based_attack_onetime_simu_K(copyGraph, atk_rsc, 100, k, atk_mode)
            elif mtd == "cascading":
                avg_trip = repeat_cascading_attack_simu_K(copyGraph, atk_rsc, 1000, k, K2, atk_mode)
            
            #reset_graph_simu(graph)
            e_trip_list.append(round(avg_trip,2))
            k_list.append(k)
            efficient_list.append(round(avg_trip/atk_rsc, 2))
    elif K2 > 0:
        for k in range(1, K2+1):
            #copyGraph = deepcopy(graph)
            copyGraph = graph
            if mtd == "cascading":
                avg_trip = repeat_cascading_attack_simu_K(copyGraph, atk_rsc, 1000, K1, k, atk_mode)
            
            reset_graph_simu(copyGraph)
            e_trip_list.append(round(avg_trip,2))
            k_list.append(k)
            efficient_list.append(round(avg_trip/atk_rsc, 2))
    
    print("From simulation", e_trip_list)
    print("From simulation x axis:", k_list)
    print("Attack RSC efficiency simulation",efficient_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from simulation", \
                        "Value of K", "Expected tripped line", yMax=ymx, yMin=0)



def attack_result_simu_RSC(graph, atk_rsc, K, rpt, mtd, atk_mode="normal", rsc_ini=2, rsc_step=10, plot_en=False):
    e_trip_list = []
    k_list = []
    efficient_list = []
    total_data = {}
    
    if mtd == "area_or_repu":
        calSPT.find_P2P_Path_All_Relays(graph, 'lw_equal')
    
    for rsc in range(rsc_ini, atk_rsc+1, rsc_step):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        
        if mtd == "random":
            avg_trip = effect_of_random_relay_selection_MCM(copyGraph, rsc, 1, rpt, False, False)
        elif mtd == "area_it":
            avg_trip = repeat_area_based_attack_it_simu_K(copyGraph, rsc, rpt, K, atk_mode, False, total_data)
        elif mtd == "area_or":
            avg_trip = repeat_area_based_attack_onetime_simu_K(copyGraph, rsc, rpt, K, atk_mode, False, total_data)
        elif mtd == "cascading":
            avg_trip = repeat_cascading_attack_simu_K(copyGraph, rsc, rpt, K, 0, atk_mode)
        elif mtd == "area_or_m":
            avg_trip = repeat_area_based_attack_onetime_majority_simu_K(copyGraph, rsc, rpt, K, atk_mode, False, \
                                                                        total_data)
        elif mtd == "area_it_m":
            avg_trip = repeat_area_based_attack_it_majority_simu_K(copyGraph, rsc, rpt, K, atk_mode, False, \
                                                                        total_data)
        elif mtd == "area_or_repu":
            avg_trip = repeat_area_based_attack_onetime_reputation_simu_K(copyGraph, rsc, rpt, K, atk_mode, False)
            
        e_trip_list.append(round(avg_trip,2))
        k_list.append(round(rsc/(graph["total_relay_no"])*100, 2))
        efficient_list.append(round(avg_trip/atk_rsc, 2))
    
    print("From simulation", e_trip_list)
    print("From simulation x axis:", k_list)
    print("Attack RSC efficiency simulation", efficient_list)
    print("Total local trip:", total_data["total_local_trip"])
    print("Total tripped effective neighbor line:", total_data["total_neighbor_trip"])
    
    if mtd == "area_or" or mtd == "area_it":
        print("Two-pr-relay neighbor trip:", total_data["neighbor_trip_two_pr_relay"])
        print("Single-pr-relay neighbor trip:", total_data["neighbor_trip_single_pr_relay"])
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from simulation", \
                        "Value of K", "Expected tripped line", yMax=ymx, yMin=0)



def attack_special_cascading_simu_K_RSC(graph, K, rpt, atk_mode, plot_en=False):
    """Used for cascading simulation
    This simulation is ``special`` because for each relay, we assign exactly K resources.
    The amount of resource is ``just`` sufficient to attack the initial cascading line set, e.g., n_k lines.
    The unused resources will not be used by the other relays. This attack model is a lower
    bound of the normal K-attack.
    
    As comparison, the resources may be used to for other attack strategies, like area attack. The results can
    show whether pure cascading attack has sufficient benefit to be considered.
    
    Args:
        myGraph: the graph class variable
        K (int): the value for max allowed number of attack to a relay
        mtd (string): the attack strategy, include random, area_onetime, area_it, and cascading
        atk_mode (string): the attack mode, set to ``special``
        plot_en (bool): whether to enable plot of the result
    """
    e_trip_list = []
    k_list = []
    efficient_list = []
    
    for k in range(1, K+1):
        #copyGraph = deepcopy(graph)
        copyGraph = graph
        atk_rsc = max([len(ini_set) for ini_set in graph["cascading_initial"]]) * k
        
        avg_trip = repeat_cascading_attack_simu_K(copyGraph, atk_rsc, rpt, k, 0, atk_mode)
        
        reset_graph_simu(copyGraph)
        e_trip_list.append(round(avg_trip,2))
        k_list.append(round(atk_rsc/(graph["total_relay_no"])*100, 2))
        efficient_list.append(round(avg_trip/atk_rsc, 2))
    
    print("From K_RSC special simulation", e_trip_list)
    print("From simulation x axis:", k_list)
    print("Attack RSC efficiency simulation",efficient_list)
    
    if plot_en == True:
        ymx = len(graph.es)
        plotf.myPlotFig(k_list, e_trip_list, "Expected tripped line from simulation", \
                        "Value of K", "Expected tripped line", yMax=ymx, yMin=0)



def setup_cascading_in_topology(graph, n_k, n_m):
    initial_line_list = select_initial_cascading_line(graph, n_k, 3, 1)
    graph["cascading_initial"] = initial_line_list
    print("Initial line set:", len(graph["cascading_initial"][0]), graph["cascading_initial"])
    
    create_cascading_sequence_pure_random(graph, initial_line_list, n_m)
    show_cascading_sequence(graph)



def setup_cascading_in_topology_special(graph, n_k, n_m, df, s_mode="one_hop"):
    if s_mode == "one_hop":
        initial_line_list = select_initial_cascading_special(graph, n_k, 3, 1)
    elif s_mode == "two_hop":
        initial_line_list = select_initial_cascading_special_v2(graph, n_k, 3, 1)
    
    graph["cascading_initial"] = initial_line_list
    print("Initial line set:", len(graph["cascading_initial"][0]), graph["cascading_initial"])
    sga.set_relay_defense_strength_cascading(graph, df)
    
    create_cascading_sequence_pure_random(graph, initial_line_list, n_m)
    show_cascading_sequence(graph)



def cascading_effect_test():
    myGraph = ig.Graph.Watts_Strogatz(1, 30, 2, 0.9)
    temp = []
    for v in myGraph.vs:
        temp.append(v.degree())
    print(temp)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    graph_initialization(myGraph, len(myGraph.es))
    
    show_graph_basic(myGraph)
    initial_line_list = select_initial_cascading_line(myGraph, 3, 1)
    myGraph["cascading_initial"] = initial_line_list
    print("Initial line:", myGraph["cascading_initial"])
    
    create_cascading_sequence_pure_random(myGraph, initial_line_list)
    show_cascading_sequence(myGraph)
#     set_relay_defense_strength_randomly(myGraph, 0.1, 0.1)
    
#     print("Total relay No.:",myGraph["total_relay_no"])
#     for line_i in initial_line:
#         cascading_trigger_line(myGraph, line_i)
#     print(total_tripped_line(myGraph))
#     for es_i in myGraph.es:
#         for relay_i in es_i["prRelaySet"]:
#             print("Relay",relay_i.relayIdx,"local line:",relay_i.Located_EdgeIdx)



def test_poisson():
    poisson = np.random.poisson(30, 10000)
    plt.hist(poisson)
    plt.show()
    lam_1 = 30
    print(lam_1+2.58*pow(lam_1, 0.5))



def test_small_world():
    myGraph = ig.Graph.Watts_Strogatz(1, 30, 2, 0.6)
    initialize_node_name(myGraph)
    show_graph_basic(myGraph)
    temp = []
    for v in myGraph.vs:
        temp.append(v.degree())
    print(temp)
    plotGraph(myGraph)



def test_function_IEEE_topology():
    myGraph = sga.bd_13.myGraph_13
    sga.bd_13.setGraph(myGraph)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    graph_initialization(myGraph, 11)
#     sga.identify_protection_area_total_line(myGraph)
    sga.protection_area_line_update(myGraph)
    result = get_max_line_protection_area_index(myGraph)
    
    initial_line_list = select_initial_cascading_line(myGraph, 2, 1)
    myGraph["cascading_initial"] = initial_line_list
    print("Initial line:", myGraph["cascading_initial"])
    create_cascading_sequence_pure_random(myGraph, initial_line_list)
    show_cascading_sequence(myGraph)
    sga.total_attack_trip_info(myGraph)
    
    for i in range(len(myGraph["cascading_initial"])):
        for line_idx in myGraph["cascading_initial"][i]:
            relay_idx = line_idx * 2 + 1
            trip_line_idx = sga.trip_line_with_relay(myGraph, relay_idx)
            print("Line index:",line_idx,"trip_line_idx:",trip_line_idx)
            cascade_check_list = sga.cascading_initial_line_check(myGraph, trip_line_idx)
            
            if cascade_check_list != []: # some cascading conditions are fulfilled
                for idx in cascade_check_list:
                    myGraph["cascading_result"][idx].triggered = True
                    sga.trigger_cascading(myGraph, idx)
    
    sga.total_attack_trip_info(myGraph)
    
#     sga.set_relay_defense_strength_all(myGraph, 0.9)
#     available_rsc = 10
#     relay_idx = 20
#     available_rsc, completed = sga.attack_relay(myGraph, available_rsc, relay_idx, "retry", 6)
#     print("remaining rsc:", available_rsc)
#     
#     if completed == True:
#         sga.trip_line_with_relay(myGraph, relay_idx)
#     sga.total_attack_trip_info(myGraph)
    
#     for es_i in sga.bd_13.myGraph_13.es:
#         for relay_i in es_i["prRelaySet"]:
#             print("Relay",relay_i.relayIdx,"has cost of",relay_i.attack_cost)
#     for line_idx in result:
#         print("Area",line_idx,"has the max potential lines:",myGraph.es[line_idx]["area_line"])
    for es_i in sga.bd_13.myGraph_13.es:
        print("Line "+es_i["name"]+" protection area lines:",es_i["area_line"])
    
    result = get_max_line_protection_area_index(myGraph)
    target_area_idx = select_target_relay_idx(myGraph, result)
    print("Largest area:", target_area_idx)
        
    sga.protection_area_line_update(myGraph)
    for es_i in sga.bd_13.myGraph_13.es:
        print("Line "+es_i["name"]+" protection area lines:",es_i["area_line"])



def test_IEEE_13_topology():
    myGraph = sga.bd_13.myGraph_13
    sga.bd_13.setGraph(myGraph)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    graph_initialization(myGraph, 11)
    sga.find_corresponding_bp_relay_for_pr_relay(myGraph)
    sga.protection_area_line_update(myGraph)
    
#     for es_i in myGraph.es:
#         for relay_i in es_i["prRelaySet"]:
#             relay_i.showBackupRelay()
    
#     myGraph["cascading_initial"] = [[3,4],[1,2]]
#     myGraph["cascading_sequence"] = [[3,4,5,6],[1,2,9]]
#     for i in range(len(myGraph["cascading_initial"])):
#         temp_struct = sga.CascadeMark()
#         myGraph["cascading_result"].append(temp_struct)
    
#     effect_of_random_relay_selection_MCM(myGraph, 2, 100)
#     topology_knowledge_attack(myGraph, 5)
#     cascading_knowledge_attack(myGraph, 6)
#     set_relay_defense_strength(myGraph, 9, 0.1)
#     set_relay_defense_strength(myGraph, 10, 0.1)
    sga.set_relay_defense_strength_all(myGraph, 0.5)
    rsc = 10
    attack_result_model_K(myGraph, atk_rsc=rsc, mtd="area", plot_en=False)
    attack_result_simu_K(myGraph, rsc, mtd="area", plot_en=False)
    
    #show_attack_before_after(myGraph)



def test_generic_network():
    random.seed(12345)
    myGraph = ig.Graph.Watts_Strogatz(1, 60, 2, 0.1)
    show_graph_basic(myGraph)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    
    graph_initialization(myGraph)
    sga.find_corresponding_bp_relay_for_pr_relay(myGraph)
    sga.set_relay_defense_strength_all(myGraph, 0.8)
    setup_cascading_in_topology(myGraph, 0.1, 0.5)
    #setup_cascading_in_topology_special(myGraph, 0.1, 0.5, 0)
    
    K1 = 1
    K2 = 5
    rsc = int(2 * len(myGraph.es))
    #rsc = 230
    rptn = 100
    r_start = 12
    r_step = 12
#     attack_result_model_K(myGraph, atk_rsc=rsc, K=retry, mtd="cascading", plot_en=False)
#     attack_result_model_RSC(myGraph, rsc, K=retry, mtd="cascading", plot_en=False)
#     attack_special_cascading_model_K_RSC(myGraph, K1, "cascading", "K_retry", plot_en=False)
#     simple_pure_cascading_result_model("K_eff", n_k=12, rsc=20, K=10, p_f=0.5, n_m=30, plot_en=True)
    
#     attack_result_simu_K(myGraph, rsc, mtd="cascading", K1=K1, K2=K2, atk_mode="special", plot_en=False)
    attack_result_simu_RSC(myGraph, atk_rsc=rsc, K=K1, rpt=rptn, mtd="random", atk_mode="special", \
                           rsc_ini=r_start, rsc_step=r_step, plot_en=False)
#     attack_special_cascading_simu_K_RSC(myGraph, K=K1, rpt=rptn, atk_mode="special", plot_en=False)
#     cascading_knowledge_attack(myGraph, rsc, 1, "normal")



def compare_cascading_attack():
    base = 2
    random.seed(12345)
    myGraph = ig.Graph.Watts_Strogatz(1, int(60*base), 2, 0.05)
    show_graph_basic(myGraph)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    
    graph_initialization(myGraph)
    sga.find_corresponding_bp_relay_for_pr_relay(myGraph)
    sga.set_relay_defense_strength_all(myGraph, 0.2)
    sga.set_node_defense_strength_all(myGraph, 0.2)
    setup_cascading_in_topology_special(myGraph, 0.1/base, 0.5/base, 0.9, "two_hop")
    
    rsc = int(0.42/base * len(myGraph.es))
    #rsc = 30
    rsc_test_mode = 170
    atk_mode = "special"
    cas_mode = "reputation_persist"
    test_mode = "initial"
    rsc_ini = 5
    K2_ini = 1
    K2 = 2
    
    if cas_mode == "reputation" or cas_mode == "reputation_persist":
        calSPT.find_P2P_Path_All_Relays(myGraph, 'lw_equal')
    
    ini_line = min(10, len(myGraph["cascading_initial"][0]))
    e_trip_list = []
    k_list = []
    efficient_list = []
    initial_list = deepcopy(myGraph["cascading_initial"][0])
    
    if test_mode == "initial":
        for n_k in range(1, ini_line+1):
            temp_initial_list = []
            for i in range(n_k):
                temp_initial_list.append(initial_list[i])
            myGraph["cascading_initial"][0] = temp_initial_list
            print("New cascading initial:",myGraph["cascading_initial"][0])
            copyGraph = myGraph
            
            if cas_mode == "direct":
                max_try = int(rsc/n_k)
            elif cas_mode == "direct_persist":
                max_try = 1
            elif cas_mode == "reputation":
                #max_try = max(int(rsc/n_k/2)-1, 1)
                K2_ini = max(int(rsc/n_k/3), 1)
                max_try = max(int((rsc/n_k-K2_ini)/2), 1)
            elif cas_mode == "reputation_persist":
                K2_ini = 1
                max_try = 1
            elif cas_mode == "area":
                max_try = max(int(rsc/n_k/5), 1)
            
            print("Rsc:",rsc,"   max_try:",max_try,"   K2_ini:",K2_ini)
            
            if n_k != 15:
                avg_trip = repeat_cascading_attack_simu_K(copyGraph, rsc, 1000, max_try, K2_ini, atk_mode, cas_mode)
            else:
                avg_trip = 0
            
            e_trip_list.append(round(avg_trip,2))
            k_list.append(round(rsc/(copyGraph["total_relay_no"])*100, 2))
            efficient_list.append(round(avg_trip/rsc, 2))
            #print()
    elif test_mode == "rsc":
        temp_initial_list = []
        for i in range(rsc_ini):
            temp_initial_list.append(initial_list[i])
        myGraph["cascading_initial"][0] = temp_initial_list
        print("New cascading initial for mode rsc:",myGraph["cascading_initial"][0])
        for rsc_i in range(10, rsc_test_mode+2, 10):
            copyGraph = myGraph
            
            if cas_mode == "direct":
                max_try = int(rsc_i/rsc_ini)
            elif cas_mode == "reputation":
                #max_try = max(int((rsc_i/rsc_ini-K2)/2), 1)
                #max_try = max(int((rsc_i/rsc_ini)/3), 1)
                #K2 = max(int(rsc_i/rsc_ini - 2*max_try),0)
                K2 = max(int((rsc_i/rsc_ini)/3), 0)
                max_try = max(int((rsc_i/rsc_ini - K2)/2),1)
            elif cas_mode == "area":
                max_try = max(int(rsc_i/rsc_ini/5), 1)
            print("rsc =",rsc_i,"max_try =",max_try,"K2 =",K2)
            
            avg_trip = repeat_cascading_attack_simu_K(copyGraph, rsc_i, 1000, max_try, K2, atk_mode, cas_mode)
            
            e_trip_list.append(round(avg_trip,2))
            k_list.append(round(rsc/(copyGraph["total_relay_no"])*100, 2))
            efficient_list.append(round(avg_trip/rsc, 2))
    
    print("From simulation", e_trip_list)
    print("From simulation x axis:", k_list)
    print("Attack RSC efficiency simulation",efficient_list)



def small_network_property(node_num, pr):
    std_graph = ig.Graph.Watts_Strogatz(1, node_num, 2, 0)
    std_L0 = std_graph.average_path_length()
    std_C0 = std_graph.transitivity_undirected()
    random.seed(12345)
    myGraph = ig.Graph.Watts_Strogatz(1, node_num, 2, pr)
    myGraph_L0 = myGraph.average_path_length()
    myGraph_C0 = myGraph.transitivity_undirected()
    
    test_graph = ig.Graph.Watts_Strogatz(1, node_num, 2, 1)
    test_graph_L0 = test_graph.average_path_length()
    test_graph_C0 = test_graph.transitivity_undirected()
    
    L0_list = []
    C0_list = []
    w_list = []
    for i in range(100):
        graph_v = ig.Graph.Watts_Strogatz(1, node_num, 2, pr)
        graph_v_L0 = graph_v.average_path_length()
        graph_v_C0 = graph_v.transitivity_undirected()
        
        L0_list.append(round(graph_v_L0/std_L0,3))
        C0_list.append(round(graph_v_C0/std_C0,3))
        
        w_list.append(round(test_graph_L0/graph_v_L0 - graph_v_C0/std_C0, 3))
    
    print("CC ratio:",round(myGraph_C0/std_C0,3),"L0 ratio:",round(myGraph_L0/std_L0,3))
    print("Average CC ratio:",round(np.mean(C0_list),3),"Average L0 ratio:",round(np.mean(L0_list),3))
    print("Average w value:",round(np.mean(w_list),3))
    


def other_test():
    a = [0,1,2]
    dict_1 = [[1,2,3], [0,2,3], [0,1,2]]
    dict_2 = [[4,5,6], [4,6,7], [5,6,7]]
    
    for i in range(len(dict_1)):
        if dict_1[i] == a:
            print("Index",a,"has element",dict_2[i])
    
    b = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
    c = random.randrange(0, 10)
    d = random.choice(b)
    
    print(c, d)
    
    rlt = []
    for j in range(1000):
        num = 0
        for i in range(10):
            temp = random.uniform(0,1)
            if temp > 0.5:
                num += 1
        
        rlt.append(num)
    
    print(np.mean(rlt))



def test_probability():
    result_list = []
    xlist = []
    legend_name = []
    
    for pf_i in range(1,10):
        temp_list = []
        for k in range(1,10):
            pf = pf_i/10
            ps = 1 - pf**k
            ps_two = ps**2
            
            temp_list.append(ps_two)
        
        result_list.append(temp_list)
    
    for k in range(1,10):
        xlist.append(k)
    
    for pf_i in range(1,10):
        i = round(pf_i/10, 2)
        legend_name.append("$P_f=$"+str(i))
    
    plotf.myPlotFig_List_WithX(xlist, result_list, "Test", "Value of K", "P(Success)", legend_name, ymax=1, ymin=0)



def test_graph_copy():
    g = ig.Graph.Full(3)

    g['foo'] = []
    g.vs[0]['foo'] = []
    g.es[0]['foo'] = []
    
    h = g.copy()
    #h = deepcopy(g)
    
    assert(h['foo'] == [])
    assert(h.vs[0]['foo'] == [])
    assert(h.es[0]['foo'] == [])
    
    
    g['foo'].append('bar')
    g.vs[0]['foo'].append('bar')
    g.es[0]['foo'].append('bar')
    
    print(g['foo'], h['foo'])
    print(g.vs[0]['foo'], h.vs[0]['foo'])
    print(g.es[0]['foo'], h.es[0]['foo'])
    
    assert(h['foo'] == [])
    assert(h.vs[0]['foo'] == [])
    assert(h.es[0]['foo'] == [])


def test_graph_copy2():
    myGraph = sga.bd_13.myGraph_13
    
    copyGraph = myGraph.copy()
    
    sga.bd_13.setGraph(myGraph)
    initialize_node_name(myGraph)
    initialize_line_name(myGraph)
    
    for es_i in copyGraph.es:
        print("Copygraph:", es_i["name"])
        
    for es_i in myGraph.es:
        print("Original graph", es_i["name"])


def test_graph():
    random.seed(12345)
    myGraph = ig.Graph.Watts_Strogatz(1, 60, 2, 0.0)
    
    for es_i in myGraph.es:
        print("Index:",es_i.index,"   src:",es_i.source,"   dst:",es_i.target)
        
    for vs_i in myGraph.vs:
        print("Node",vs_i.index,"neighbor:",[vs_nb.index for vs_nb in vs_i.neighbors()])


# cascading_effect_test()


# test_poisson()
# test_small_world()
# test_function_IEEE_topology()
# other_test()
# test_IEEE_13_topology()

# test_generic_network()
compare_cascading_attack()
# small_network_property(1000, 0.1)

# test_probability()
# test_graph_copy()
# test_graph_copy2()
# test_graph()