'''
Created on Nov 4, 2014

@author: Zhang Jiapeng
'''

import igraph as ig
import numpy as np
from copy import deepcopy
import itertools as itls
from matplotlib._cntr import Cntr
import matplotlib.pyplot as plt
import heapq
from SPT import calSPT2
from statistics import mean



marker = ['.', #point marker 
'o', #circle marker 
'v', #triangle_down marker 
'^', #triangle_up marker 
'<', #triangle_left marker 
'>', #triangle_right marker 
'1', #tri_down marker 
'2', #tri_up marker 
'3', #tri_left marker 
'4', #tri_right marker 
's', #square marker 
'p', #pentagon marker 
'*', #star marker 
'h', #hexagon1 marker 
'H', #hexagon2 marker 
'+', #plus marker 
'x', #x marker 
'D', #diamond marker 
'd', #thin_diamond marker 
'|', #vline marker 
'_', #hline marker 
',', #pixel marker 
]

color = ['b', #blue 
'g', #green 
'r', #red 
'c', #cyan 
'm', #magenta 
'y', #yellow 
'k', #black 
'w', #white 
]

lineStyle = ['-', #solid 
'--', #dashed 
'-.', #dash_dot 
':', #dotted 
]

class relayStruct:
    def __init__(self,LN=-1,rIdx=-1,LEIdx=-1,Z3Idx=-1,RN=-1,DRTT=-1,DPL_P=-1,DPL_B=-1):
        self.Located_Node_Idx = LN
        self.relayIdx = rIdx
        self.Located_EdgeIdx = LEIdx
        self.ZONE3_EdgeIdx = Z3Idx
        self.Remote_Node = RN
        self.delay_rtt = DRTT
        self.delay_per_link_PR = DPL_P # Simple equal division
        self.delay_per_link_BP = DPL_B # may not be used since we may have multiple backup paths
        self.no_rsv_pr = 0 # indicate if the relay cannot reserve enough capacity on primary path
        self.no_rsv_bp = 0 # indicate if the relay cannot reserve enough capacity on backup path
        self.rsv_pr_looked = 0 # indicate if the relay has been "scanned" in the PR_RSV process, no matter result
        self.rsv_bp_looked = 0 # indicate if the relay has been "scanned" in the BP_RSV process, no matter result
        self.link_s_pf = 0 # The failure probability of the line this relay is as "Primary Relay"
        self.avg_line_pf_req = 0 # The "averaged" requirement for the failure probability of the line
                                 # that this relay is on
        self.pr_line_s_pf = 0 # The probability of system failure due to the trip of the primary line of the relay
        self.single_line = 0 # If it is not 0, we are assuming only 1 line having fluctuation each time 
        
        self.per_hop_dr_PR = {} # {link : DR} or {link_direction : DR} or {link : {link_direction : DR}}?
                                # Currently we use the third one with "double dictionaries"
                                # If direction is not needed, the key is "nonDirection"
                                
        self.per_hop_dr_BP = {} # may not be used since we may have multiple backup paths
                                # but if we use only one non-overlapped backup path, it is useful
                                # like the PR, we use "double dictionaries"
                                
        self.per_hop_dr_BP_OFE = {} # Assume each link of primary path has a backup path (may have same backup)
                                    # Currently we use the structure as:
                                    # {failLink : {link : {link_direction : DR}}}
                                    # If direction is not needed, the key is "nonDirection"
        
        self.used_link_RSV_PR = {} # {link : RSV} or {link_direction : RSV} or {link : {link_direction : RSV}}?
                                   # Currently we use the first one
        self.used_link_RSV_BP = {}
        self.BP_RVS_ES_path = []
        self.BP_RVS_VS_path = []
        self.p2p_per_hop_dr_PR = {} # {peer_relay_idx : {link : {link_direction : DR}}}
        self.p2p_rsv_record = {} # {relayIdx : T/F} to indicate whether the path to another relay has been reserved
        self.relay_hf_pf = -1
        self.p2p_per_hop_dr_BP = {} # {peer_relay_idx : {link : {link_direction : DR}}}
        self.p2p_rsv_record_bp = {} # {relayIdx : T/F}: whether the backup path to another relay has been reserved
        self.line_relay_bp_peers = {} # {line_# : peer_list[]}, used in p2p backup to see a relay talks to which relays
        self.contacted_bp_relay = [] # Indicate in the backup stage, which relay is contacted by the inquiry relay
        
        self.compromised = False # Indicate whether the relay is compromised
        self.defense_strength = 0 # The defense strength of the relay (in [0,1] to indicate probability of failure attack)
        self.attack_cost = 1 # Each attack needs 1 unit of resource
        self.bp_relay = [] # corresponding backup relays of a primary relay
        self.p_cmp = 0 # P(relay already compromised)
        self.attacked = False # the relay has been attacked
    
    def showBackupRelay(self):
        idx_list = []
        for relay_i in self.bp_relay:
            idx_list.append(relay_i.relayIdx)
        
        print("Relay", self.relayIdx, "has backup relays:", idx_list)
        
        

def find_relay_by_index(graph, relay_idx):
    for es_i in graph.es:
        if es_i["prRelaySet"] != []:
            for relay_i in es_i["prRelaySet"]:
                if relay_i.relayIdx == relay_idx:
                    return relay_i



def bus(busName,graph):
    temp = graph.vs.find(name=busName)
    return temp


def translatePathName(shortestPathSet,graph):
    temp = []
    for path in shortestPathSet:
        for nodeID in path:
            temp.append(graph.vs[nodeID]["name"])
        print(temp)
        temp.clear()


def plotGraph(graph,fileName=None,graphLayout=None):
    if graphLayout != None:
        myLayout = ig.Layout(graphLayout)
    else:
        myLayout = graph.layout_kamada_kawai()
    graph.vs["label"] = graph.vs["name"]
    ig.plot(graph, layout = myLayout, bbox = (800,800), margin = 50)
    if fileName != None:
        ig.plot(graph, fileName, layout = myLayout, bbox = (800,800), margin = 50)
    

# For example, if graph has 24 nodes, then maxLengthSet stores 24 numbers, each is the max-length path for
# a specified MA. In this way, we know the "minimal max-length" and the corresponding MA, e.g. the 1st value
# is for node-1 2nd for node-2 etc. maxIndexResult will store the "index" of the "optimal max" and the last
# step is to find the index with the "minimal average length" and return the index.
def optimalMaxPathLength(graphSPSet):#graphSPSet is of type "list", stores the distance of each path
    maxLengthSet = []
    avgLengthSet = []
    optimalMax = 1000000
    for subSet in graphSPSet:
        if optimalMax > max(subSet):
            optimalMax = max(subSet)
        maxLengthSet.append(max(subSet)) #append the max path length of each sub-list in the list to a new list
        avgLengthSet.append(np.average(subSet))
#     print(optimalMax,maxLengthSet)
#     print(averageSet)
    
    maxIndexResult = []
    avgResult = []
    for value in range(len(maxLengthSet)): #value is the index of each element in the list
        if maxLengthSet[value] == optimalMax:
            maxIndexResult.append(value)
            avgResult.append(avgLengthSet[value])
    print(maxIndexResult)
    print(avgResult)
    
    minAvg = np.min(avgResult)
    finalResult = []
    for tempIndex in maxIndexResult:
        if avgLengthSet[tempIndex] == minAvg:
            finalResult.append(tempIndex)
    
    return finalResult


def addEdgeFromNodePairList(graph,nodePairEdgeList):
    for nodePair in nodePairEdgeList:
        graph.add_edge(nodePair[0],nodePair[1])


def newSPT_NodePair(graph,nodePairList):
    copyGraph = graph.copy()
    copyGraph.delete_edges(None)
    addEdgeFromNodePairList(copyGraph, nodePairList)
    
    return copyGraph


def setMAVertex(graph,masterIndex):
    graph.vs["label"] = graph.vs["name"]
    graph.vs["state"] = "slave"
    graph.vs[masterIndex]["state"] = "master"
    graph.vs[masterIndex]["shape"] = "rectangle"
    graph.vs[masterIndex]["size"] = 40
    color_dict = {"master": "blue", "slave": "red"}
    graph.vs["color"] = [color_dict[state] for state in graph.vs["state"]]
    graph.vs["label_size"] = 36
    graph.vs["label_dist"] = 1
#     graph.vs["label_angle"] = 0


def plotSPT(graph,nodeIndexSet,fileName=None):
    remainingNodePair = [] # This store the node pairs that form edges
    temp2 = []
    
    for index in nodeIndexSet:
        remainingNodePair.clear()
        SPSet = graph.vs[index].get_shortest_paths()
        for SP in SPSet:
            for i in range(len(SP)-1):
                nodePair = [SP[i],SP[i+1]]
                try:
                    remainingNodePair.index(nodePair)
                except ValueError:
                    remainingNodePair.append(nodePair)
#         print(remainingNodePair)
#         print(SPSet)
        temp2.append(deepcopy(remainingNodePair))
        copyGraph = newSPT_NodePair(graph, remainingNodePair)
        setMAVertex(copyGraph, index)
        plotGraph(copyGraph,fileName)
        

def setEdgeParameters(graph,edgeList,edgeColor="black",edgeWidth=1):
    for es_idx in edgeList:
        graph.es[es_idx]["color"] = edgeColor
        graph.es[es_idx]["width"] = edgeWidth
    
    
def newSPT_EdgeList(graph,edgeList):
    print(len(edgeList),edgeList)
    copyGraph = graph.copy()
    deleteList = []
#     print(len(copyGraph.es))
#     copyGraph.delete_edges(45)
    for eachEdge in copyGraph.es:
        if not eachEdge.index in edgeList:
            deleteList.append(eachEdge.index)
        else:
#             print(eachEdge,eachEdge.index)
            None
#             copyGraph.delete_edges(eachEdge.index)
#         templist.append(eachEdge.index)
#         i+=1
#         src = eachEdge.source
#         dst = eachEdge.target
#         print(copyGraph.vs[src]["name"],copyGraph.vs[dst]["name"])
#         if eachEdge.index in edgeList:
#             copyGraph.delete_edges(eachEdge.index)
#     print(templist,i)
#     print(copyGraph.es[40])
    print("Deleted edges:",deleteList)
#     copyGraph.delete_edges(deleteList)
    setEdgeParameters(copyGraph, edgeList, "green", 10)
    return copyGraph


# This function considers the number of relays using a link when we are selecting a primary path.
# After selecting a primary path for a bus, we will update the data.
def findPrimaryPath_overlap(graph,vs_i,dstIdx,esSetList,vsSetList):
    simple_link_weight_update_v2(graph)
    esPathList = graph.get_shortest_paths(v=dstIdx,to=vs_i,weights=graph.es["weight"],output = "epath")
    vsPathList = graph.get_shortest_paths(v=dstIdx,to=vs_i,weights=graph.es["weight"],output = "vpath")
    
    for edgeIdx in esPathList[0]:
        graph.es[edgeIdx]["usedNo"]["nonDirection"] += len(vs_i["relaySet"])
#     if 9 in vsPathList[0]:
#         print(vsPathList[0])
        
    esSetList.append(deepcopy(esPathList[0]))
    vsSetList.append(deepcopy(vsPathList[0]))
    

# This function considers load on different links in selecting primary paths.
# Currently the algorithm used is "greedy-based" shortest path algorithm
def findPrimaryPath_load(graph,vs_i,dstIdx,esSetList,vsSetList):
#     print(graph.es["weight"])
    simple_link_weight_update_v3(graph)
    esPathList = graph.get_shortest_paths(v=dstIdx,to=vs_i,weights=graph.es["weight"],output = "epath")
    vsPathList = graph.get_shortest_paths(v=dstIdx,to=vs_i,weights=graph.es["weight"],output = "vpath")
    
    for edgeIdx in esPathList[0]:
        assumed_PH_Delay = 0.5/len(esPathList[0])/2
#         print(assumed_PH_Delay)
        graph.es[edgeIdx]["RSV_temp_ND"] += (80*8/assumed_PH_Delay)*len(vs_i["relaySet"])
        if graph.es[edgeIdx]["RSV_temp_ND"] > graph.es[edgeIdx]["capacity"]:
            raise NameError('Primary Non-Direction: Capacity not enough! '+'for link '+str(edgeIdx+1))
    
    esSetList.append(deepcopy(esPathList[0]))
    vsSetList.append(deepcopy(vsPathList[0]))


# The function finds primary paths for all nodes in the graph, with consideration of "equal weight",
# "overlap number" or "load" as criteria
def findPrimaryPath_All_Nodes(graph,nodeIndexSet,pathMode='lw_equal',graphLayout=None,fileName=None,plotEN=False):
    remainingEdgeList = [] # This stores the SPT edges
    
    for index in nodeIndexSet:
        remainingEdgeList.clear()
        edgeSetList = []
        nodeSetList = []
#         for edge_i in graph.es:
#             print("PR check: Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"])
        
        if pathMode == 'lw_equal':
            edgeSetList = graph.vs[index].get_shortest_paths(output="epath")
            nodeSetList = graph.vs[index].get_shortest_paths(output="vpath")
        elif pathMode == 'lw_overlap':
            for vs_i in graph.vs:
                findPrimaryPath_overlap(graph, vs_i, index, edgeSetList, nodeSetList)
        elif pathMode == 'lw_load':
            for vs_i in graph.vs:
                findPrimaryPath_load(graph, vs_i, index, edgeSetList, nodeSetList)
            copyPR_RSV_to_Temp(graph) # Clear the temporary variable for later use
        
        print("Path of edges:",edgeSetList)
        print("Path of nodes:",nodeSetList)
#         newESL = graph.get_shortest_paths(v="1",to="16",weights=graph.es["weight"],output = "epath"\
#                                           ,mode=ig.OUT)
#         print(newESL)
        # edgeSetList stores all the paths to the target node, the path is from the first node in graph.vs
        # actually the function set the target node as the "source node", the edge id orde in the "path"
        # starts from the edge that is adjacent to this node
        
        for idx in range(len(graph.vs)):
            graph.vs[idx]["PR_hop_no"] = len(edgeSetList[idx])
            graph.vs[idx]["PR_RVS_ES_path"] = (deepcopy(edgeSetList[idx]))
            graph.vs[idx]["PR_RVS_VS_path"] = (deepcopy(nodeSetList[idx]))
            findSubtreeEdge_PR(graph,edgeSetList[idx],nodeSetList[idx])
            if pathMode != 'overlap':
                for edgeIdx in edgeSetList[idx]:
                    graph.es[edgeIdx]["usedNo"]["nonDirection"] += len(graph.vs[idx]["relaySet"])

        # Store the edges that used in building the spanning tree. We may count how many times a link is used.
        # We should decide to count the number of nodes using the link or the number of relay using the link.
        for edgeList in edgeSetList:
            for edgeIdx in edgeList:
                try:
                    remainingEdgeList.index(edgeIdx)
                except ValueError:
                    remainingEdgeList.append(edgeIdx)
#                 graph.es[edgeIdx]["usedNo"]["nonDirection"] += 1
        
#         print(len(remainingEdgeList),remainingEdgeList)
        
        if plotEN == True:
            copyGraph = newSPT_EdgeList(graph, remainingEdgeList)
            setMAVertex(copyGraph, index)
            plotGraph(copyGraph,fileName,graphLayout)
        

# This function choose a primary path with the minimum path failure probability
def findPrimaryPath_All_Nodes_Max_Reliability(graph,MA_idx,primary_type):
    print("Use",primary_type,"method.")
    srcIdx = MA_idx
    for vs_i in graph.vs:
        
        dstIdx = vs_i.index

        esSetList = []
        vsSetList = []
        delaySetList = []
#         print("Bus",vs_i.index+1)
        find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, [],\
                        vs_i["min_relay_req"], enableOL=True, debugPrint=False)
        vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
        esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        if vs_i.index != srcIdx:
            if primary_type == 'max_reliability':
                selectedIdx = calSPT2.select_path_max_reliability(esSetList,vs_i,graph)
            elif primary_type == 'equal_div_reliability':
                selectedIdx = calSPT2.select_path_based_on_power(esSetList, vs_i, [], graph)
            elif primary_type == 'sp_reliability':
                selectedIdx = calSPT2.select_path_SP_reliability(esSetList, vs_i, graph)
        else:
            selectedIdx = 0
        print("Bus_"+vs_i["name"]+" chooses no.",selectedIdx,"path",esSetList[selectedIdx])
        vs_i["PR_hop_no"] = len(esSetList[selectedIdx])
        vs_i["PR_RVS_ES_path"] = deepcopy(esSetList[selectedIdx])
        vs_i["PR_RVS_VS_path"] = deepcopy(vsSetList[selectedIdx])



# This function test if the path fulfill the delay requirement
def path_delay_constrain_test(graph,linkList,delay_req,pr_path):
    result = -1
    overlapping = 0
    min_path_delay = 0
    for edgeIdx in linkList:
        curEdge = graph.es[edgeIdx]
        min_path_delay += 80*8/(curEdge["capacity"]-curEdge["RSV_temp_ND"])
        if edgeIdx in pr_path:
            overlapping += 1
#         print((curEdge["RSV_temp_ND"]),'  ',end='')
#     print()
#     print("min path delay:",min_path_delay)
    
    if delay_req <= 0:
        result = min_path_delay
    else:
        if min_path_delay < delay_req:
            result = min_path_delay
        else:
            print("Min path delay",min_path_delay,"is larger than req",delay_req)
            None
#     print("PR:",pr_path,"new:",linkList,overlapping,len(pr_path))
    # Decide if the new path is the same as the primary path. For destination, its primary path is "[]",
    # thus we still use this "[]" as the backup path so that the backup path hop count is "0"; meanwhile, in
    # later calculations this "[]" would help us to make the code easier
    if overlapping == len(pr_path) and len(pr_path) != 0:
#         print(linkList,"is the same as primary path.")
        result = -1
    
    return result
        
# This function finds all possible paths from the "source" to "destination"
# The found paths are in "reverse order", e.g. from the MA to a given source vertex
# NOTICE: when using this function, usually "dstIdx" is the "real source" since we store the path in
# "reverse order".
def find_all_paths(graph,srcIdx,dstIdx,vsPathList=[],esPathList=[],minDelayList=[],\
                   exemptPath=[],delay_req=-1,enableOL=False,debugPrint=False,mode_use="MA"):
    myStack = []
    myStack.append(srcIdx) # First push the source into the stack
#     print(exemptPath)
#     print(graph.vs[dstIdx]["PR_RVS_ES_path"])
    while len(myStack) != 0:
        # If the top node in the stack is the destination, we find a path and store it
        if myStack[-1] == dstIdx:
            tempEdgeList = get_edge_path_from_node_path(graph,myStack)
            if debugPrint == True:
                print("Find a path:",myStack)
                print("Corresponding edge path:",tempEdgeList)
            min_path_delay = path_delay_constrain_test(graph, tempEdgeList, delay_req, exemptPath)
            if min_path_delay != -1:
                vsPathList.append(deepcopy(myStack))
                esPathList.append(tempEdgeList)
                minDelayList.append(min_path_delay)
            myStack.pop()
        else:
            # If the top node is not destination, we check whether all of its neighbors have been visited
            curIdx = myStack[-1]
            curVS = graph.vs[curIdx]
            cur_NB_List = curVS.neighbors()
            nbSize = len(cur_NB_List)
            
            # We visit the neighbors in certain order (as the index in the neighbor list of a node)
            # If all neighbors have been visited, we should return to the previous node, thus "pop"
            # the current node
            if curVS["visited_NB"] == (nbSize):
                curVS["visited_NB"] = 0
                myStack.pop()
            else:
                if mode_use == "MA":
                    path_link_table = graph.vs[dstIdx]["PR_RVS_ES_path"]
                elif mode_use == "p2p":
                    path_link_table = graph.vs[dstIdx]["P2P_RVS_ES_path"][srcIdx]
                # The neighbor we are looking at has not been visited, we check whether it is already in the
                # stack or it is the source node to prevent loop
                idx_to_visit = curVS["visited_NB"]
                curVS["visited_NB"] += 1
                nxtVS = cur_NB_List[idx_to_visit]
                edge_to_visit = graph.get_eid(curIdx,nxtVS.index)
                if (nxtVS.index == srcIdx) or (nxtVS.index in myStack):
                    continue
                elif edge_to_visit in path_link_table and enableOL == False:
                    continue # This is to find a non-overlapped path
                else: # It is a "satisfied" node, push into the stack
                    myStack.append(nxtVS.index)
    
    if debugPrint == True:      
        print("All", len(vsPathList),"vertex paths:",vsPathList)
        print("All",len(esPathList),"edge paths:",esPathList)
        print("Minimum path delay:",minDelayList)


# The function finds P2P paths for all relays in the graph, with consideration of "equal weight",
# "overlap number" or "load" as criteria
def find_P2P_Path_All_Relays(graph,pathMode='lw_equal',graphLayout=None,fileName=None,plotEN=False):
    for edge_i in graph.es:
        relaySet = []
        busSet = [] # Store the bus index where the relays reside
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
            busSet.append(relay.Located_Node_Idx)
            
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
            busSet.append(relay.Located_Node_Idx)
        
        # Use the shortest-distance path
        for vs_src_idx in busSet:
            vs_src = graph.vs[vs_src_idx] # The current source node
            
            for vs_dst_idx in busSet:
                vs_dst = graph.vs[vs_dst_idx]
                if vs_src_idx == vs_dst_idx:
                    vs_src["P2P_RVS_ES_path"][vs_dst_idx] = deepcopy([])
                    vs_src["P2P_RVS_VS_path"][vs_dst_idx] = deepcopy([vs_dst_idx])
                    
                    vs_dst["P2P_RVS_ES_path"][vs_src_idx] = deepcopy([])
                    vs_dst["P2P_RVS_VS_path"][vs_src_idx] = deepcopy([vs_src_idx])
                    continue
                elif vs_dst_idx in vs_src["P2P_RVS_ES_path"]:
                    continue
                else:
                    esSetList = graph.get_shortest_paths(v=vs_dst_idx,to=vs_src_idx,weights=graph.es["weight"],output = "epath")
                    vsSetList = graph.get_shortest_paths(v=vs_dst_idx,to=vs_src_idx,weights=graph.es["weight"],output = "vpath")
                    vs_src["P2P_RVS_ES_path"][vs_dst_idx] = deepcopy(esSetList[0])
                    vs_src["P2P_RVS_VS_path"][vs_dst_idx] = deepcopy(vsSetList[0])
                    
                    # Reverse the path, so it can be assigned to the other peer as the path
                    esSetList[0].reverse()
                    vsSetList[0].reverse()
                    
                    # For the destination node, it also has a path to the source (the pair of paths)
                    vs_dst["P2P_RVS_ES_path"][vs_src_idx] = deepcopy(esSetList[0])
                    vs_dst["P2P_RVS_VS_path"][vs_src_idx] = deepcopy(vsSetList[0])



# We get the "edge path" from the "vertex path"
# If the vertex is the destination it self, the returning edge is [] (empty)
def get_edge_path_from_node_path(graph,vsPathList):
    tempList = []
    for i in range(len(vsPathList)-1):
        esIdx = graph.get_eid(vsPathList[i],vsPathList[i+1])
        tempList.append(esIdx)
    
    return tempList
    
        
def find_ALL_SPT_Links(graph,dstName):
    remainingEdgeList = [] # This stores the SPT edges
    dst = graph.vs.find(dstName)
    dstIdx = dst.index
    
    edgeSetList = graph.vs[dstIdx].get_shortest_paths(output = "epath")
    for edgeList in edgeSetList:
        for edge_index in edgeList:
            try:
                remainingEdgeList.index(edge_index)
            except ValueError:
                remainingEdgeList.append(edge_index)
    
    # Here we set the unused links' weight to be a little larger than others
    # so that in the case of only one failure, the used links are more preferred
    # than the unused ones
    for eachEdge in graph.es:
        if not eachEdge.index in remainingEdgeList:
            graph.es[eachEdge.index]["weight"] = 5
        
    

def showBackupCase(graph,srcName,dstName,linkIdx,graphLayout=None,fileName=None):
    find_ALL_SPT_Links(graph, dstName)
    graph.es[linkIdx]["weight"] = 100
#     print(graph.es["weight"])
    src = graph.vs.find(srcName)
    srcIdx = src.index
    dst = graph.vs.find(dstName)
    dstIdx = dst.index
    pathEdgeList = graph.vs[srcIdx].get_shortest_paths(to=dstName,weights=graph.es["weight"],output="epath")
    
    #Plot the graph
    setMAVertex(graph,dstIdx)
    graph.es[linkIdx]["color"] = "red"
    graph.es[linkIdx]["width"] = 4
    setEdgeParameters(graph,pathEdgeList,"yellow",10)
    plotGraph(graph, fileName, graphLayout)
    
    

# Initialize the vertex for RSV
def rsvInitialize(graph):
    graph["total_relay_no"] = 0
    graph["s_req"] = 0
    graph["pf_hf"] = 0
    graph["link_pf"] = 0.00001
    
    for vs_i in graph.vs: # initialize the node's relay set to null
        vs_i["relaySet"] = []
        vs_i["PR_hop_no"] = 0
        vs_i["BP_hop_no"] = 0
        vs_i["PR_RVS_ES_path"] = [] # There is only one PR path, thus we can use "list []"
        vs_i["PR_RVS_VS_path"] = []
        vs_i["BP_RVS_ES_path"] = []
        vs_i["BP_RVS_VS_path"] = []
        vs_i["BP_RVS_ES_Each"] = {}
        vs_i["BP_RVS_VS_Each"] = {}
        vs_i["visited_NB"] = 0
        vs_i["min_relay_req"] = 0
        vs_i["relay_for_same_bus"] = 0
        vs_i["P2P_RVS_ES_path"] = {}
        vs_i["P2P_RVS_VS_path"] = {}
        vs_i["P2P_BP_RVS_ES_path"] = {}
        vs_i["P2P_BP_RVS_VS_path"] = {}
        vs_i["compromised"] = False
        vs_i["cost"] = 1
        vs_i["df"] = 0

    for edge_i in graph.es:
        edge_i["prRelaySet"] = []
        edge_i["bpRelaySet"] = []
        edge_i["RSV_PR"] = {}
        edge_i["RSV_BP"] = {}
        edge_i["RSV_P2P"] = {}
        edge_i["RSV_P2P_BP"] = {}
        edge_i["RSV_temp"] = {} # a temporary parameter used in "load scheme"
        edge_i["RSV_final"] = {}
        edge_i["RSV_Overall"] = 0 # "non-direction capacity RSV"
        edge_i["RSV_temp_ND"] = 0 # stands for "nonDirection"
        edge_i["RSV_BP_Overall"] = 0 # used for non-overlapping path to store the RSV on backup path
        edge_i["capacity"] = 1500000
        edge_i["relay_num_PR"] = 0
        edge_i["relay_num_BP"] = 0
        edge_i["pf"] = graph["link_pf"]
        edge_i["line_s_pf"] = 0
        edge_i["tripped"] = False
        edge_i["area_line"] = []
        edge_i["p_line_already_tripped"] = 0
        
        # Store many times a link is used. Here we use the dictionary in case of potential
        # need of link direction. "nonDirection" is used if no such requirement.
        edge_i["usedNo"] = {}
        edge_i["usedNo"]["nonDirection"] = 0
        
        e_src = edge_i.source
        e_target = edge_i.target
        
        name1 = str(e_src)+'_'+str(e_target)
        name2 = str(e_target)+'_'+str(e_src)
        
        edge_i["RSV_PR"][name1] = 0
        edge_i["RSV_PR"][name2] = 0
        edge_i["RSV_BP"][name1] = 0
        edge_i["RSV_BP"][name2] = 0
        edge_i["RSV_temp"] = deepcopy(edge_i["RSV_PR"])
        edge_i["RSV_P2P"][name1] = 0
        edge_i["RSV_P2P"][name2] = 0
        edge_i["RSV_P2P_BP"][name1] = 0
        edge_i["RSV_P2P_BP"][name2] = 0
        
        
        edge_i["pe"] = 0 # relay failure probability
        edge_i["lineHF"] = 0 # line hidden failure probability due to relay HF
#         edge_i["SBT_ES_PR"] = []
        # Primary subtree links
        edge_i["SBT_ES_PR"] = {}
        edge_i["SBT_ES_PR"][name1] = []
        edge_i["SBT_ES_PR"][name2] = []
        
        #Backup subtree links
        edge_i["SBT_ES_BP"] = {}
        edge_i["SBT_ES_BP"][name1] = []
        edge_i["SBT_ES_BP"][name2] = []
        
#         print(name1+'_'+'SBT_ES_PR',edge_i.index,edge_i[name1+'_'+'SBT_ES_PR'])
        edge_i["pSubTwoHF_PR"] = {}
        edge_i["pSubTwoHF_PR"][name1] = 0
        edge_i["pSubTwoHF_PR"][name2] = 0
        
        
def copyPR_RSV_to_Temp(graph):
    for edge_i in graph.es:
        edge_i["RSV_temp"].clear()
        edge_i["RSV_temp"] = deepcopy(edge_i["RSV_PR"])
        edge_i["RSV_temp_ND"] = edge_i["RSV_Overall"]
        

# This function counts the number of relays using a same link        
def num_of_relay_on_link(graph):
#     temp = 0
    for vs_i in graph.vs:
        for linkIdx_PR in vs_i["PR_RVS_ES_path"]:
            graph.es[linkIdx_PR]["relay_num_PR"] += len(vs_i["relaySet"])
            
        for linkIdx_BP in vs_i["BP_RVS_ES_path"]:
            graph.es[linkIdx_BP]["relay_num_BP"] += len(vs_i["relaySet"])
            
#         print("Bus",vs_i.index+1,'-->',len(vs_i["relaySet"]))
#         temp += len(vs_i["relaySet"])
#     print(temp)
            
            
def averageHopCnt(graph,prt=False):
    total_cnt_pr = 0
    total_cnt_bp = 0
    pr_num = 0
    bp_num = 0
    max_cnt_pr = 0
    max_cnt_bp = 0
    for vs_i in graph.vs:
        if vs_i["relaySet"] != []:
            total_cnt_pr += len(vs_i["PR_RVS_ES_path"])
            total_cnt_bp += len(vs_i["BP_RVS_ES_path"])
            
            if len(vs_i["PR_RVS_ES_path"]) > max_cnt_pr:
                max_cnt_pr = len(vs_i["PR_RVS_ES_path"])
            if len(vs_i["BP_RVS_ES_path"]) > max_cnt_bp:
                max_cnt_bp = len(vs_i["BP_RVS_ES_path"])
                
            pr_num += 1
            bp_num += 1
    
    if prt == True:
        print(pr_num,"buses with relays.","Max pr_cnt:",max_cnt_pr,"Max bp_cnt:",max_cnt_bp)
        print("Average PR cnt:",total_cnt_pr/pr_num,"Average BP cnt:",total_cnt_bp/bp_num)


def compute_line_pf_link_error(graph):
    result = []
    for edge_i in graph.es:
        overlapping = 0
        line_pf = 0
        
        if edge_i["prRelaySet"] == []:
            continue
        else:
            for relay in edge_i["prRelaySet"]:
                if relay.ZONE3_EdgeIdx == []:
                    continue
                else:
                    located_node_Idx = relay.Located_Node_Idx
                    located_node = graph.vs[located_node_Idx]
                    for linkIdx in located_node["BP_RVS_ES_path"]:
                        if linkIdx in located_node["PR_RVS_ES_path"]:
                            overlapping += 1
                            line_pf += (graph.es[linkIdx]["pf"])
            result.append(line_pf)
    
    return result

# Compute the failure probability if no backup path is provided for MA-based scheme, assume a single link failure
def compute_each_relay_false_trip_pf_only_pr(graph):
    result = []
    for es_i in graph.es:
        for relay in es_i["prRelaySet"]:
            path_pf = 0
            vs_i = graph.vs[relay.Located_Node_Idx]
            for linkIdx in vs_i["PR_RVS_ES_path"]:
                path_pf += (graph.es[linkIdx]["pf"])
            hf_pf = graph["pf_hf"]
            relay_hf_pf = path_pf * hf_pf
            
            result.append(relay_hf_pf)
    return result


# Compute the failure probability if no backup path is provided for MA-based scheme, assume a single link failure
# Here we not only consider the failure of the inquiry relay, but also the possibility that a single link failure would
# make all other peer relays unable to communicate with the MA (e.g. the failed link is shared by all peer relays'
# primary path).
def compute_each_relay_false_trip_pf_only_pr_complex(graph):
    result = []
    for es_i in graph.es:
        for relay in es_i["prRelaySet"]:
            relaySet = []
            for rp in es_i["prRelaySet"]:
                if rp != relay:
                    relaySet.append(rp)
            for rb in es_i["bpRelaySet"]:
                relaySet.append(rb)
            # "relaySet" includes all peer relays but not the "inquiry relay" itself
            
            path_pf = 0
            vs_i = graph.vs[relay.Located_Node_Idx] # The current bus we are looking at
            # The false trip due to not sending or receiving decision from MA by the inquiry relay
            for linkIdx in vs_i["PR_RVS_ES_path"]:
                path_pf += (graph.es[linkIdx]["pf"])
            
            tempDict = {}
            for peer_relay in relaySet:
                vs_peer = graph.vs[peer_relay.Located_Node_Idx]
                for peer_linkIdx in vs_peer["PR_RVS_ES_path"]:
                    if peer_linkIdx not in tempDict:
                        tempDict[peer_linkIdx] = 1
                    else:
                        tempDict[peer_linkIdx] += 1
            
            for peer_linkIdx in tempDict:
                if tempDict[peer_linkIdx] >= len(relaySet):
                    # The affected link is no counted in the "inquiry relay's path"
                    if peer_linkIdx not in vs_i["PR_RVS_ES_path"]:
                        path_pf += (graph.es[peer_linkIdx]["pf"])
            
            hf_pf = graph["pf_hf"]
            relay_hf_pf = path_pf * hf_pf
            
            result.append(relay_hf_pf)
            relay.relay_hf_pf = relay_hf_pf
            
    return result


# Compute the failure probability if backup path is provided for MA-based scheme, assume a single link failure
def compute_each_relay_false_trip_pf_with_bp(graph):
    result = []
    for es_i in graph.es:
        for relay in es_i["prRelaySet"]:
            path_pf = 0
            vs_i = graph.vs[relay.Located_Node_Idx]
            if relay.no_rsv_pr != 0:
                path_pf = 1
                hf_pf = graph["pf_hf"]
                relay_hf_pf = path_pf * hf_pf
            elif relay.no_rsv_bp != 0:
                for linkIdx in vs_i["PR_RVS_ES_path"]:
                    path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                relay_hf_pf = path_pf * hf_pf
            else:
                for linkIdx in vs_i["BP_RVS_ES_path"]:
                    if linkIdx in vs_i["PR_RVS_ES_path"]:
                        path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                relay_hf_pf = path_pf * hf_pf
            
            result.append(relay_hf_pf)
    return result


# Compute the false-trip probability for each relay in the p2p-scheme, assume a single link failure
def compute_each_relay_false_trip_pf_p2p(graph):
    resultList = []
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            pf_result = 0
            for lineIdx in relay.ZONE3_EdgeIdx: # For each remote line it is protecting, we consider the false trip
                target_line = graph.es[lineIdx]
                relaySet = []
                
                for rp in target_line["prRelaySet"]:
                    relaySet.append(rp)
                for rb in target_line["bpRelaySet"]:
                    relaySet.append(rb)
                tempDict = {}
                
                for peer in relaySet: # For each peer of the relay, we look at the path
                    if peer.relayIdx == relay.relayIdx:
                        continue
                    elif peer.Located_Node_Idx == relay.Located_Node_Idx:
                        continue
                    else:
                        for link_Idx in relay.p2p_per_hop_dr_PR[peer.relayIdx]:
                            if link_Idx not in tempDict:
                                tempDict[link_Idx] = 1
                            else:
                                tempDict[link_Idx] += 1
                
                for link_index in tempDict:
                    # Whether it is len(relaySet)-1 or len(relaySet)-2 depends on how we make the decision
                    # If a single reply is enough, then len(relaySet)-1 is used, which means the link exists
                    # in paths to all other peers.
                    if tempDict[link_index] >= len(relaySet)-1:
                        pf_result += graph.es[link_index]["pf"]*(1/len(relay.ZONE3_EdgeIdx))
                        
            resultList.append(pf_result)
            relay.relay_hf_pf = pf_result
            
    return resultList


# Compute the false-trip probability for each relay in the p2p-scheme, assume a single link failure
def compute_each_relay_false_trip_pf_p2p_full_bp(graph):
    resultList = []
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            pf_result = 0
            for lineIdx in relay.ZONE3_EdgeIdx: # For each remote line it is protecting, we consider the false trip
                target_line = graph.es[lineIdx]
                relaySet = []
                
                for rp in target_line["prRelaySet"]:
                    relaySet.append(rp)
                for rb in target_line["bpRelaySet"]:
                    relaySet.append(rb)
                tempDict = {}
                
                for peer in relaySet: # For each peer of the relay, we look at the path
                    if peer.relayIdx == relay.relayIdx:
                        continue
                    elif peer.Located_Node_Idx == relay.Located_Node_Idx:
                        continue
                    else:
                        for link_Idx in relay.p2p_per_hop_dr_PR[peer.relayIdx]:
                            if link_Idx not in tempDict:
                                tempDict[link_Idx] = []
                                tempDict[link_Idx].append(peer)
                            else:
                                tempDict[link_Idx].append(peer)
                
                for link_index in tempDict:
                    cnt = 0
                    affected_path_no = len(tempDict[link_index])
                    if affected_path_no >= len(relaySet)-2:
                        for peer in tempDict[link_index]: # For each peer of the relay, we look at the path
                            if peer.relayIdx == relay.relayIdx:
                                continue
                            elif peer.Located_Node_Idx == relay.Located_Node_Idx:
                                continue
                            else:
                                if link_index in relay.p2p_per_hop_dr_BP[peer.relayIdx]:
                                    cnt += 1
                        if cnt >= len(relaySet)-2:
                            pf_result += graph.es[link_index]["pf"]*(1/len(relay.ZONE3_EdgeIdx))
            
            resultList.append(pf_result)
            relay.relay_hf_pf = pf_result
            
    return resultList



# Compute the false-trip probability for each relay in the p2p-scheme, with partial non-overlap backup paths 
# Assume a single link failure
def compute_each_relay_false_trip_pf_p2p_partial_bp(graph,req_reply_no):
    resultList = []
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            pf_result = 0
            for lineIdx in relay.ZONE3_EdgeIdx: # For each remote line it is protecting, we consider the false trip
                target_line = graph.es[lineIdx]
                relaySet = []
                
                for rp in target_line["prRelaySet"]:
                    relaySet.append(rp)
                for rb in target_line["bpRelaySet"]:
                    relaySet.append(rb)
                tempDict = {}
                
                for peer in relaySet: # For each peer of the relay, we look at the path
                    if peer.relayIdx == relay.relayIdx:
                        continue
                    elif peer.Located_Node_Idx == relay.Located_Node_Idx:
                        continue
                    else:
                        for link_Idx in relay.p2p_per_hop_dr_PR[peer.relayIdx]:
                            if link_Idx not in tempDict:
                                tempDict[link_Idx] = []
                                tempDict[link_Idx].append(peer)
                            else:
                                tempDict[link_Idx].append(peer)
                
                for link_index in tempDict:
                    cnt = 0
                    affected_path_no = len(tempDict[link_index])
                    if affected_path_no >= len(relaySet)-req_reply_no:
                        for peer in tempDict[link_index]: # For each peer of the relay, we look at the path
                            if peer.relayIdx == relay.relayIdx:
                                continue
                            elif peer.Located_Node_Idx == relay.Located_Node_Idx:
                                continue
                            elif peer.relayIdx not in relay.p2p_per_hop_dr_BP:
                                cnt += 1
                            elif relay.p2p_rsv_record_bp == 0:
                                cnt += 1
                            else:
                                if link_index in relay.p2p_per_hop_dr_BP[peer.relayIdx]:
                                    cnt += 1
                        if cnt >= len(relaySet)-req_reply_no:
                            pf_result += graph.es[link_index]["pf"]*(1/len(relay.ZONE3_EdgeIdx))
            
            resultList.append(pf_result)
            relay.relay_hf_pf = pf_result
            
    return resultList



# Compute the system failure probability if backup path does not have enough capacity for MA-based scheme
def compute_system_pf_no_backup_ca(graph):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            path_pf = 0
            if relay.no_rsv_bp != 0:
                for linkIdx in vs_i["PR_RVS_ES_path"]:
                    path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
    return total_result


# Compute the system failure probability if no backup path is provided for MA-based scheme
def compute_system_pf_only_pr(graph):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            path_pf = 0 
            for linkIdx in vs_i["PR_RVS_ES_path"]:
                path_pf += (graph.es[linkIdx]["pf"])
            hf_pf = graph["pf_hf"]
            total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
    return total_result


# Compute the system failure probability if backup path is provided for MA-based scheme
def compute_system_pf_with_bp(graph):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            path_pf = 0
            if relay.no_rsv_pr != 0:
                path_pf = 1
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            elif relay.no_rsv_bp != 0:
                for linkIdx in vs_i["PR_RVS_ES_path"]:
                    path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            else:
                for linkIdx in vs_i["BP_RVS_ES_path"]:
                    if linkIdx in vs_i["PR_RVS_ES_path"]:
                        path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
    return total_result


# Compute the system failure probability for p2p scheme
def compute_system_pf_p2p(graph):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            hf_pf = graph["pf_hf"]
            total_result += (relay.pr_line_s_pf * relay.relay_hf_pf * hf_pf)
    return total_result


def compute_system_pf_double_lf(graph, linkIdx1, linkIdx2):
    total_result = 0
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            path_pf = 0
            if relay.no_rsv_pr != 0:
                path_pf = 1
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            elif (linkIdx1 in vs_i["PR_RVS_ES_path"] and linkIdx2 in vs_i["BP_RVS_ES_path"]) or \
            (linkIdx2 in vs_i["PR_RVS_ES_path"] and linkIdx1 in vs_i["BP_RVS_ES_path"]):
                path_pf = 1
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            elif relay.no_rsv_bp != 0:
                for linkIdx in vs_i["PR_RVS_ES_path"]:
                    path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
            else:
                for linkIdx in vs_i["BP_RVS_ES_path"]:
                    if linkIdx in vs_i["PR_RVS_ES_path"]:
                        path_pf += (graph.es[linkIdx]["pf"])
                hf_pf = graph["pf_hf"]
                total_result += (relay.pr_line_s_pf * path_pf * hf_pf)
    
    return total_result


# Compute the max/min/avg hop count from a bus to the MA. This can help look at the response time.
def hop_count_info_MA(graph):
    pr_hop_cnt_list = []
    bp_hop_cnt_list = []
    for vs_i in graph.vs:
        if vs_i["relaySet"] != []:
            pr_hop_cnt_list.append(len(vs_i["PR_RVS_ES_path"]))
            bp_hop_cnt_list.append(len(vs_i["BP_RVS_ES_path"]))
    
    max_pr = max(pr_hop_cnt_list)
    min_pr = min(pr_hop_cnt_list)
    avg_pr = mean(pr_hop_cnt_list)
    
    max_bp = max(bp_hop_cnt_list)
    min_bp = min(bp_hop_cnt_list)
    avg_bp = mean(bp_hop_cnt_list)
    
    print("Primary path:","max =",max_pr,"min =",min_pr,"avg =",avg_pr)
    print("Backup path:","max =",max_bp,"min =",min_bp,"avg =",avg_bp)


# Compute the max/min/avg hop count between each p2p peer relays. This can help look at the response time.
def hop_count_info_P2P(graph):
    pr_hop_cnt_list = []
    bp_hop_cnt_list = []
    for vs_i in graph.vs:
        if vs_i["relaySet"] != []:
            for peer_bus_idx_pr in vs_i["P2P_RVS_ES_path"]:
                pr_hop_cnt_list.append(len(vs_i["P2P_RVS_ES_path"][peer_bus_idx_pr]))
            
            for peer_bus_idx_bp in vs_i["P2P_BP_RVS_ES_path"]:
                bp_hop_cnt_list.append(len(vs_i["P2P_BP_RVS_ES_path"][peer_bus_idx_bp]))
            
    max_pr = max(pr_hop_cnt_list)
    min_pr = min(pr_hop_cnt_list)
    avg_pr = mean(pr_hop_cnt_list)
    
    max_bp = max(bp_hop_cnt_list)
    min_bp = min(bp_hop_cnt_list)
    avg_bp = mean(bp_hop_cnt_list)
    
    print("P2P Primary path:","max =",max_pr,"min =",min_pr,"avg =",avg_pr)
    print("P2P Backup path:","max =",max_bp,"min =",min_bp,"avg =",avg_bp)
    
    
def show_num_of_relay_on_link(graph):
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+':', "PR->",edge_i["relay_num_PR"],\
              "BP->",edge_i["relay_num_BP"])
        
        
def showRelaySet(graph):
    for edge_i in graph.es:
        print(edge_i.index,edge_i["prRelaySet"])
    
    for vs_i in graph.vs:
        print(vs_i["name"],vs_i["relaySet"])
        
    for vs_i in graph.vs:
        print(vs_i["name"],"Hop Count:",vs_i["PR_hop_no"],"Path to MA:",vs_i["MA_path"])
        

# Print out some information of the graph property
def showRelaySet_v2(graph):
    for edge_i in graph.es:
        print("Line",edge_i.index+1)
        for relay in edge_i["prRelaySet"]:
            print("Primary: Idx =",relay.relayIdx,"LN =",relay.Located_Node_Idx,"RN =",relay.Remote_Node,\
                  "LE =",relay.Located_EdgeIdx,"Z3E =",relay.ZONE3_EdgeIdx,"RTT = ",relay.delay_rtt)
        for relay in edge_i["bpRelaySet"]:
            print("Backup: Idx =",relay.relayIdx,"LN =",relay.Located_Node_Idx,"RN =",relay.Remote_Node,\
                  "LE =",relay.Located_EdgeIdx,"Z3E =",relay.ZONE3_EdgeIdx,"RTT = ",relay.delay_rtt)
    
    print()
    
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            print("Bus",vs_i["name"]+':',"Idx =",relay.relayIdx,"LN =",relay.Located_Node_Idx,"RN =",relay.Remote_Node,\
                  "LE =",relay.Located_EdgeIdx,"Z3E =",relay.ZONE3_EdgeIdx)
            
    print()
    
    for vs_i in graph.vs:
        print("Bus",vs_i["name"]+':',"Hop Count:",vs_i["PR_hop_no"],"Path to MA:",vs_i["PR_RVS_ES_path"])
        
        
def showLineErrorPro(graph):
    for edge_i in graph.es:
        print("Line "+str(edge_i.index+1)+" failure:",'%.3e'%(edge_i["pe"]))
        print("Line "+str(edge_i.index+1)+" HF:",'%.3e'%(edge_i["lineHF"]))


# For each transmission line we set two relays. The "maxEdgeIdx" is used if some lines in the system
# are not transmission lines. We also initialize the parameters of links in this function.
def setRelayNo(graph,maxEdgeIdx):
    rsvInitialize(graph)
    graph["total_relay_no"] = maxEdgeIdx * 2
    
    for edge_i in graph.es: # assign relay index and find primary relay for each line, store relays for vertex
        bigEnd = max(edge_i.source,edge_i.target)
        littleEnd = min(edge_i.source,edge_i.target)
                
        if edge_i.index < maxEdgeIdx:
            bigRelayIdx = (edge_i.index+1)*2
            smallRelayIdx = (edge_i.index+1)*2-1
            
#             edge_i["prRelaySet"].append([bigEnd,bigRelay])
#             edge_i["prRelaySet"].append([littleEnd,smallRelay])
#             graph.vs[bigEnd]["relaySet"].append([edge_i.index,littleEnd,bigRelay])
#             graph.vs[littleEnd]["relaySet"].append([edge_i.index,bigEnd,smallRelay])

            smallRelay = relayStruct(LN=littleEnd,RN=bigEnd,rIdx=smallRelayIdx,LEIdx=edge_i.index,Z3Idx=[])
            bigRelay = relayStruct(LN=bigEnd,RN=littleEnd,rIdx=bigRelayIdx,LEIdx=edge_i.index,Z3Idx=[])
            
            edge_i["prRelaySet"].append(smallRelay)
            edge_i["prRelaySet"].append(bigRelay)
            graph.vs[bigEnd]["relaySet"].append(bigRelay)
            graph.vs[littleEnd]["relaySet"].append(smallRelay)


def findBpRelayFromNode(graph,targetIdx,exptIdx,edgeIdx):
    
    for vs_i in graph.vs[targetIdx].neighbors():
        if vs_i.index == exptIdx:
            None
        elif vs_i["relaySet"] == []:
            None
        else:
            for relay in vs_i["relaySet"]:
                if relay.Remote_Node == targetIdx:
                    relay.ZONE3_EdgeIdx.append(edgeIdx) # store the line this relay is looking at
                    
            for relay in vs_i["relaySet"]:
                if relay.Remote_Node == targetIdx:
                    graph.es[edgeIdx]["bpRelaySet"].append(relay)
                    # for the line we are looking at, a backup relay is found and stored
                    if edgeIdx not in relay.line_relay_bp_peers:
                        relay.line_relay_bp_peers[edgeIdx] = []
                    
                    setLinePro(graph.es[edgeIdx])


def findBackupRelayLine(graph,edge_i):
    srcIdx = edge_i.source
    dstIdx = edge_i.target
    
    findBpRelayFromNode(graph, srcIdx, dstIdx, edge_i.index)
    findBpRelayFromNode(graph, dstIdx, srcIdx, edge_i.index)
    

# Find all backup relays for different power lines    
def findBackupRelayGraph(graph,maxEdgeIdx):   
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            findBackupRelayLine(graph, edge_i)
        

# Find the lines that have multiple zone-3 relays at the same bus
def find_Zone3_relay_Same_bus(graph):
    total_line = 0
    for edge_i in graph.es:
        multiFlag = 0
        tempDict = {}
        relaySet = []
    
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
            
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
            
        for relay in relaySet:
            if relay.Located_Node_Idx not in tempDict:
                tempDict[relay.Located_Node_Idx] = []
                tempDict[relay.Located_Node_Idx].append(relay.relayIdx)
            else:
                tempDict[relay.Located_Node_Idx].append(relay.relayIdx)
                
        for dictKey in tempDict:
            if len(tempDict[dictKey]) > 1:
                print("Line",edge_i.index+1,' -> ',len(tempDict[dictKey]),'relays at bus',dictKey+1)
                graph.vs[dictKey]["relay_for_same_bus"] = 1
                multiFlag = 1
        if multiFlag == 1:
            total_line += 1
    print("Total repeated number:",total_line)
    

# This function finds the minimum relay one-way delay requirement at a bus
def update_min_relay_req_at_a_bus(graph):
    for vs_i in graph.vs:
        min_relay_req = 10000
        for relay in vs_i["relaySet"]:
            one_way_delay_req = relay.delay_rtt/2
            if one_way_delay_req < min_relay_req:
                min_relay_req = one_way_delay_req
                
        if vs_i["relaySet"] == []:
            min_relay_req = 0
        vs_i["min_relay_req"] = min_relay_req
        
                
# Calculate the delay requirement for relays of a line
# The calculation uses the primary path information since most of the time
# we only need to use the primary path

# Caution: now we may not use delay division part of this function since the reservation function
# is modified to co-operate with the "do_Perhop_DelayReq_Simple_PR"
def calDelayReq_PR_Line(graph,edge_i):
    relaySet = []
    
    for relay in edge_i["prRelaySet"]:
        relaySet.append(relay)
        
    for relay in edge_i["bpRelaySet"]:
        relaySet.append(relay)
        
    maxHopRelay = None
    maxHop = 0
    subMaxHop = 0
    
    for relay in relaySet:
        relayHopNo = graph.vs[relay.Located_Node_Idx]["PR_hop_no"]
        if relayHopNo > maxHop:
            maxHop = relayHopNo
            maxHopRelay = relay
            
    for relay in relaySet:
        relayHopNo = graph.vs[relay.Located_Node_Idx]["PR_hop_no"]
        if relayHopNo > subMaxHop and relay != maxHopRelay:
            subMaxHop = relayHopNo
    
    maxRTT = maxHop/(maxHop+subMaxHop)*1 # Divide the 1-sec to two parts
    subMaxRTT = 1-maxRTT
    
    for relay in relaySet:
        relayHopNo = graph.vs[relay.Located_Node_Idx]["PR_hop_no"]
        if relayHopNo == maxHop:
            if relay.delay_rtt < 0:
                relay.delay_rtt = maxRTT
                relay.delay_per_link_PR = (maxRTT/relayHopNo)/2
            elif relay.delay_rtt > maxRTT:
                relay.delay_rtt = maxRTT
                relay.delay_per_link_PR = (maxRTT/relayHopNo)/2
        elif relayHopNo == 0:
            relay.delay_rtt = -2
            relay.delay_per_link_PR = -2
            relay.delay_per_link_BP = -2
        elif relayHopNo < maxHop:
            if relay.delay_rtt < 0:
                relay.delay_rtt = subMaxRTT
                relay.delay_per_link_PR = (subMaxRTT/relayHopNo)/2
            elif relay.delay_rtt > subMaxRTT:
                relay.delay_rtt = subMaxRTT
                relay.delay_per_link_PR = (subMaxRTT/relayHopNo)/2
                

# Calculate the delay requirement for all relays on their primary paths                
def calDelayReq_PR_Graph(graph,maxEdgeIdx,myType='equal'):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            calDelayReq_PR_Line(graph, edge_i)
            

# Calculate the delay requirement for all relays in the P2P scheme
def calDelayReq_P2P(graph,maxEdgeIdx,myType='equal'):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            relaySet = []
    
            for relay in edge_i["prRelaySet"]:
                relaySet.append(relay)
                
            for relay in edge_i["bpRelaySet"]:
                relaySet.append(relay)
                
            for relay in relaySet:
                relay.delay_rtt = 1


def weight_function_equal(graph,relay,epathList,vpathList,relayDR_Table,tempUse=False):
    for linkIdx in epathList:
        relayHopNo = len(epathList)
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        if linkIdx not in relayDR_Table:
            relayDR_Table[linkIdx] = {}
        
        relayDR_Table[linkIdx][name1] = (relay.delay_rtt/relayHopNo)/2
        relayDR_Table[linkIdx][name2] = (relay.delay_rtt/relayHopNo)/2
        relayDR_Table[linkIdx]["nonDirection"] = (relay.delay_rtt/relayHopNo)/2
        
        if tempUse == True: # if true, it means we would use it in a "load-based" function 
            graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relayDR_Table[linkIdx][name1]
            graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relayDR_Table[linkIdx][name2]
            graph.es[linkIdx]["RSV_temp_ND"] += 80*8/relayDR_Table[linkIdx][name1]
            
            if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
                raise NameError('2. Capacity not enough! '+'for link '+str(linkIdx+1))
            if graph.es[linkIdx]["RSV_temp_ND"] > graph.es[linkIdx]["capacity"]:
                raise NameError('Non-Direction: Capacity not enough! '+'for link '+str(linkIdx+1))
        
        
def weight_function_distance(graph,relay,epathList,vpathList,relayDR_Table,dstSft,tempUse=False):
    tempSum = 0 # for temporary use
    tempSum2 = 0 # for temporary use
    tempWeightList = [] # for temporary use

    for esIdx in epathList:
        # the element idx can be seen as the hop count to MA across the link
        listElementIdx = (epathList.index(esIdx)+1) + dstSft
        tempSum +=  listElementIdx # total hop count
    
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)

        hop_cnt = idx + 1 + dstSft

        current_weight = tempSum/hop_cnt
        tempWeightList.append(current_weight)
        tempSum2 += (current_weight)

    # Now calculate the delay division at different hops
    # Because the edge list is in a "reverse order", e.g. the list starts from the edge closest to the
    # destination - MA, when we traverse the list, the first weight is also assigned to this "closest edge".
    # In the hop weight, the "closest edge" receive the highest weight because its hop count is the minimum.
    # We hope that the "closest edge" to be given more delay because it is shared by multiple flows.
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        if linkIdx not in relayDR_Table:
            relayDR_Table[linkIdx] = {}
        
        current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
        relayDR_Table[linkIdx][name1] = current_hop_DR
        relayDR_Table[linkIdx][name2] = current_hop_DR
        relayDR_Table[linkIdx]["nonDirection"] = current_hop_DR
        
        if tempUse == True: # if true, it means we would use it in a "load-based" function 
            graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relayDR_Table[linkIdx][name1]
            graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relayDR_Table[linkIdx][name2]
            graph.es[linkIdx]["RSV_temp_ND"] += 80*8/relayDR_Table[linkIdx][name1]
            
            if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
                raise NameError('2. Capacity not enough! '+'for link '+str(linkIdx+1))
            if graph.es[linkIdx]["RSV_temp_ND"] > graph.es[linkIdx]["capacity"]:
                raise NameError('Non-Direction: Capacity not enough! '+'for link '+str(linkIdx+1))
    
    
def weight_function_load(graph,relay,epathList,vpathList,relayDR_Table,powft):
    tempSum = 0 # for temporary use
    tempSum2 = 0 # for temporary use
    tempWeightList = [] # for temporary use
    
#     print(epathList)
#     print(vpathList)
    for linkIdx in epathList: # compute the total free_weight
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
#         print(linkIdx,"Link",linkIdx+1,name1)
        
        if graph.es[linkIdx]["RSV_temp"][name1] >= graph.es[linkIdx]["capacity"]:
            raise NameError('1. Capacity not enough! '+'for link '+str(linkIdx+1))
        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
#                         tempSum += free_weight
        tempSum += pow(free_weight,powft)
        
    # Next assign link weight based on free_weight and compute total link weight
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        
        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
#                         current_link_weight = tempSum/free_weight
        current_link_weight = tempSum/pow(free_weight,powft)
        tempWeightList.append(current_link_weight)
        tempSum2 += current_link_weight # total sum weight
        
#                     print(tempWeightList,tempSum2)
    # compute delay division on each hop/link and update the "temporary RSV"
    for linkIdx in epathList:
        idx = epathList.index(linkIdx)
        
        src = vpathList[idx+1]
        dst = vpathList[idx]
        
        name1 = str(src)+'_'+str(dst)
        name2 = str(dst)+'_'+str(src)
        
        if linkIdx not in relayDR_Table:
            relayDR_Table[linkIdx] = {}
        
        current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
        relayDR_Table[linkIdx][name1] = current_hop_DR
        relayDR_Table[linkIdx][name2] = current_hop_DR
        relayDR_Table[linkIdx]["nonDirection"] = current_hop_DR
        
        # Update the link utilization
        graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relayDR_Table[linkIdx][name1]
        graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relayDR_Table[linkIdx][name2]
        graph.es[linkIdx]["RSV_temp_ND"] += 80*8/relayDR_Table[linkIdx][name1]
#         print("Comparison:",graph.es[linkIdx]["RSV_temp"][name1],graph.es[linkIdx]["RSV_temp"][name2],\
#               graph.es[linkIdx]["RSV_temp_ND"])
        
        if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
            raise NameError('2. Capacity not enough! '+'for link '+str(linkIdx+1))
        if graph.es[linkIdx]["RSV_temp_ND"] > graph.es[linkIdx]["capacity"]:
            raise NameError('Non-Direction: Capacity not enough! '+'for link '+str(linkIdx+1))
                        
            
def do_Perhop_DelayReq_Simple_PR(graph,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            for relay in edge_i["prRelaySet"]:
                
                epathList = graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]
                vpathList = graph.vs[relay.Located_Node_Idx]["PR_RVS_VS_path"]
#                 relayPRHopNo = graph.vs[relay.Located_Node_Idx]["PR_hop_no"]
#                 print('relayPRHopNo',relayPRHopNo,len(epathList))
                
                
                if myType == 'equal':
                    weight_function_equal(graph, relay, epathList, vpathList,relay.per_hop_dr_PR)
                        
                elif myType == 'distance':
                    weight_function_distance(graph, relay, epathList, vpathList, relay.per_hop_dr_PR, dstSft)

                elif myType == 'load':
                    #weight_function_load(graph, relay, epathList, vpathList, relay.per_hop_dr_PR, powft)
                    calSPT2.weight_function_load_new(graph,relay,epathList,vpathList,relay.per_hop_dr_PR,powft,"PRIMARY")
            

# Divide the relay requirement along the path to a peer, as the per-hop delay
def do_Perhop_DelayReq_Simple_P2P(graph,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            relaySet = []
            for relay in edge_i["prRelaySet"]:
                relaySet.append(relay)
            
            for relay in edge_i["bpRelaySet"]:
                relaySet.append(relay)
            
            for r_src in edge_i["bpRelaySet"]:
                for r_peer in relaySet:
                    if r_src.relayIdx == r_peer.relayIdx:
                        continue
                    elif r_peer.relayIdx in r_src.p2p_per_hop_dr_PR:
                        continue
                    elif r_peer.Located_Node_Idx == r_src.Located_Node_Idx:
                        continue
                    else:
#                         print(r_src.relayIdx,r_src.Located_Node_Idx,r_peer.relayIdx,r_peer.Located_Node_Idx)
                        r_src.p2p_per_hop_dr_PR[r_peer.relayIdx] = {}
                        vs_src = graph.vs[r_src.Located_Node_Idx]
                        epathList = vs_src["P2P_RVS_ES_path"][r_peer.Located_Node_Idx]
                        vpathList = vs_src["P2P_RVS_VS_path"][r_peer.Located_Node_Idx]
                        peerIdx = r_peer.relayIdx
                        if myType == 'equal':
                            weight_function_equal(graph, r_src, epathList, vpathList, r_src.p2p_per_hop_dr_PR[peerIdx])


# If we assume each time only one power-line has fluctuation, then for most buses, only one
# relay on those buses would be affected if that relay does have a hidden failure. Some buses have
# multiple relays looking at the same remote-line, for them we reserve for each relay. 
def do_Perhop_DelayReq_PR_Single_Line_Fluctuation(graph,myType='equal',dstSft=0,powft=1):
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        if vs_i["relay_for_same_bus"] == 0:
            min_relay = calSPT2.find_relay_with_min_req(graph, vs_i)
            epathList = graph.vs[min_relay.Located_Node_Idx]["PR_RVS_ES_path"]
            vpathList = graph.vs[min_relay.Located_Node_Idx]["PR_RVS_VS_path"]
            
            for relay in vs_i["relaySet"]:
                if relay.relayIdx != min_relay.relayIdx:
                    relay.single_line = 1
            
            if myType == 'equal':
                    weight_function_equal(graph, min_relay, epathList, vpathList, min_relay.per_hop_dr_PR)
                        
            elif myType == 'distance':
                weight_function_distance(graph, min_relay, epathList, vpathList, min_relay.per_hop_dr_PR, dstSft)

            elif myType == 'load':
                #weight_function_load(graph, relay, epathList, vpathList, relay.per_hop_dr_PR, powft)
                calSPT2.weight_function_load_new(graph,min_relay,epathList,vpathList,min_relay.per_hop_dr_PR,powft,"PRIMARY")
        
        else:
            print("Bus "+vs_i["name"]+" reserves for multiple relays.")
            for relay in vs_i["relaySet"]:
                epathList = graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]
                vpathList = graph.vs[relay.Located_Node_Idx]["PR_RVS_VS_path"]
                if myType == 'equal':
                    weight_function_equal(graph, relay, epathList, vpathList,relay.per_hop_dr_PR)
                        
                elif myType == 'distance':
                    weight_function_distance(graph, relay, epathList, vpathList, relay.per_hop_dr_PR, dstSft)
    
                elif myType == 'load':
                    calSPT2.weight_function_load_new(graph,relay,epathList,vpathList,relay.per_hop_dr_PR,powft,"PRIMARY")
    None


# In this function we only need to compute the per-hop delay requirement of the backup
# path using the formula: delay_rtt/bp_hop_no
# The hop_no is computed by counting the number of edges in the path. If we use the path list
# in the format of "node", be careful that the source node is also included in the path so that
# we may need to subtract 1 in the computing
def calDelayReq_BP_Line(graph,edge_i):
    
    for relay in edge_i["prRelaySet"]:
        relayBPHopNo = graph.vs[relay.Located_Node_Idx]["BP_hop_no"]
        if relayBPHopNo != 0:
            relay.delay_per_link_BP = (relay.delay_rtt/relayBPHopNo)/2
            

# In this function we only need to compute the per-hop delay requirement of the backup path
# Using different weight functions we have different assignments
# This is a simple computing method since we only assume one backup path for each node
# It is useful if each node has a non-overlapped backup path
def do_Perhop_DelayReq_Simple_BP(graph,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
#     for edge_i in graph.es:
#         if edge_i.index < maxEdgeIdx:
    for vs_i in graph.vs:
            for relay in vs_i["relaySet"]:
                
                # Only used in "combination"
                rsvSum = 0
                dstSum = 0
                overallSum2 = 0
                rsvSum2 = 0
                dstSum2 = 0
                rsvWeightList = []
                dstWeightList = []
                overallWeightList = []
                
                
                epathList = graph.vs[relay.Located_Node_Idx]["BP_RVS_ES_path"]
                vpathList = graph.vs[relay.Located_Node_Idx]["BP_RVS_VS_path"]
                relayBPHopNo = graph.vs[relay.Located_Node_Idx]["BP_hop_no"]
                
                if myType == 'equal':
                    weight_function_equal(graph, relay, epathList, vpathList,relay.per_hop_dr_BP)
                        
                elif myType == 'distance':
                    weight_function_distance(graph, relay, epathList, vpathList, relay.per_hop_dr_BP, dstSft)
                    
                elif myType == 'load':
                    #weight_function_load(graph, relay, epathList, vpathList, relay.per_hop_dr_BP, powft)
                    calSPT2.weight_function_load_new(graph,relay,epathList,vpathList,relay.per_hop_dr_BP,powft,"BACKUP")
                        
                elif myType == 'combination':
                    for linkIdx in epathList:
                        idx = epathList.index(linkIdx)
                    
                        src = vpathList[idx+1]
                        dst = vpathList[idx]
                        
                        name1 = str(src)+'_'+str(dst)
                        
                        if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
                            raise NameError('1. Capacity not enough!')
                        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
                        rsvSum += free_weight
                        
                        dstSum +=  (idx + 1 + dstSft) # total hop count
                        
                    for linkIdx in epathList:
                        idx = epathList.index(linkIdx)
                        
                        src = vpathList[idx+1]
                        dst = vpathList[idx]
                        
                        name1 = str(src)+'_'+str(dst)
                        
                        free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
                        current_rsv_weight = rsvSum/free_weight
                        rsvWeightList.append(current_rsv_weight)
                        rsvSum2 += current_rsv_weight # total sum weight
                        
                        hop_cnt = idx + 1 + dstSft
                        current_dst_weight = dstSum/hop_cnt
                        dstWeightList.append(current_dst_weight)
                        dstSum2 += (current_dst_weight) # total distance-based weight
                        
                        current_overall_weight = current_dst_weight * current_rsv_weight
                        overallWeightList.append(current_overall_weight)
                        overallSum2 += current_overall_weight
                        
                    for linkIdx in epathList:
                        idx = epathList.index(linkIdx)
                        
                        src = vpathList[idx+1]
                        dst = vpathList[idx]
                        
                        name1 = str(src)+'_'+str(dst)
                        name2 = str(dst)+'_'+str(src)
                        
                        if linkIdx not in relay.per_hop_dr_BP:
                            relay.per_hop_dr_BP[linkIdx] = {}
                        
                        current_hop_DR = (relay.delay_rtt/2)*overallWeightList[idx]/overallSum2
                        relay.per_hop_dr_BP[linkIdx][name1] = current_hop_DR
                        relay.per_hop_dr_BP[linkIdx][name2] = current_hop_DR
                        relay.per_hop_dr_BP[linkIdx]["nonDirection"] = current_hop_DR
                        
                        # Update the link utilization
                        graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relay.per_hop_dr_BP[linkIdx][name1]
                        graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relay.per_hop_dr_BP[linkIdx][name2]
                        
                        if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
                            raise NameError('2. Capacity not enough!')
                None

    

def do_Perhop_DelayReq_BP_Single_Line_Fluctuation(graph,myType='equal',dstSft=0,powft=1):
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        if vs_i["relay_for_same_bus"] == 0:
            min_relay = calSPT2.find_relay_with_min_req(graph, vs_i)
            epathList = graph.vs[min_relay.Located_Node_Idx]["BP_RVS_ES_path"]
            vpathList = graph.vs[min_relay.Located_Node_Idx]["BP_RVS_VS_path"]
            
            for relay in vs_i["relaySet"]:
                if relay.relayIdx != min_relay.relayIdx:
                    relay.single_line = 1
            
            if myType == 'equal':
                weight_function_equal(graph, min_relay, epathList, vpathList, min_relay.per_hop_dr_BP)
                        
            elif myType == 'distance':
                weight_function_distance(graph, min_relay, epathList, vpathList, min_relay.per_hop_dr_BP, dstSft)

            elif myType == 'load':
                #weight_function_load(graph, relay, epathList, vpathList, relay.per_hop_dr_PR, powft)
                calSPT2.weight_function_load_new(graph,min_relay,epathList,vpathList,min_relay.per_hop_dr_BP,powft,"BACKUP")
        
        else:
            print("Bus "+vs_i["name"]+" reserves for multiple relays.")
            for relay in vs_i["relaySet"]:
                epathList = graph.vs[relay.Located_Node_Idx]["BP_RVS_ES_path"]
                vpathList = graph.vs[relay.Located_Node_Idx]["BP_RVS_VS_path"]
                if myType == 'equal':
                    weight_function_equal(graph, relay, epathList, vpathList,relay.per_hop_dr_BP)
                        
                elif myType == 'distance':
                    weight_function_distance(graph, relay, epathList, vpathList, relay.per_hop_dr_BP, dstSft)
    
                elif myType == 'load':
                    calSPT2.weight_function_load_new(graph,relay,epathList,vpathList,relay.per_hop_dr_BP,powft,"BACKUP")
    None



# Divide the relay requirement along the backup path to a peer, as the per-hop delay
def do_Perhop_DelayReq_Simple_P2P_BP(graph,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            relaySet = []
            for relay in edge_i["prRelaySet"]:
                relaySet.append(relay)
            
            for relay in edge_i["bpRelaySet"]:
                relaySet.append(relay)
            
            for r_src in edge_i["bpRelaySet"]:
                for r_peer in relaySet:
                    if r_src.relayIdx == r_peer.relayIdx:
                        continue
                    elif r_peer.relayIdx in r_src.p2p_per_hop_dr_BP:
                        continue
                    elif r_peer.Located_Node_Idx == r_src.Located_Node_Idx:
                        continue
                    else:
#                         print(r_src.relayIdx,r_src.Located_Node_Idx,r_peer.relayIdx,r_peer.Located_Node_Idx)
                        r_src.p2p_per_hop_dr_BP[r_peer.relayIdx] = {}
                        vs_src = graph.vs[r_src.Located_Node_Idx]
                        epathList = vs_src["P2P_BP_RVS_ES_path"][r_peer.Located_Node_Idx]
                        vpathList = vs_src["P2P_BP_RVS_VS_path"][r_peer.Located_Node_Idx]
                        peerIdx = r_peer.relayIdx
                        if myType == 'equal':
                            weight_function_equal(graph, r_src, epathList, vpathList, r_src.p2p_per_hop_dr_BP[peerIdx])


# Divide the relay requirement along the backup path to a peer, as the per-hop delay
# Performed for the partial backup path scheme
def do_Perhop_DelayReq_Partial_P2P_BP(graph,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            vs_src = graph.vs[relay.Located_Node_Idx]
            for lineIdx in relay.line_relay_bp_peers: # The line "relay" is protecting
                for peerIdx in relay.line_relay_bp_peers[lineIdx]: # The peer backup relay index
                    if peerIdx not in relay.p2p_per_hop_dr_BP:
#                         print("R"+str(relay.relayIdx)+" at bus", relay.Located_Node_Idx," -> peerIdx",peerIdx)
                        # If the inquiry relay and peer relay both look at multiple lines, we can assume
                        # that the peer relay send back status of multiple lines
                        relay.p2p_per_hop_dr_BP[peerIdx] = {}
                        peer_relay = calSPT2.find_relay_with_index(graph, peerIdx)
                        epathList = vs_src["P2P_BP_RVS_ES_path"][peer_relay.Located_Node_Idx]
                        vpathList = vs_src["P2P_BP_RVS_VS_path"][peer_relay.Located_Node_Idx]
#                         print(epathList)
                        
                        if myType == 'equal':
                            weight_function_equal(graph, relay, epathList, vpathList, relay.p2p_per_hop_dr_BP[peerIdx])


# Call this function after the "calDelayReq_PR_Graph" since we need the "delay_rtt"
# for each relay. Notice that this function only uses "equal division".     
def calDelayReq_BP_Graph(graph,maxEdgeIdx,myType='equal'):
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            calDelayReq_BP_Line(graph, edge_i)
            

# Static reservation. For the MA node, since its PR_path and BP_path do not have elements, the third "for"
# loop would not be executed.
# Meanwhile, each node has only one single primary path, we can easily compute the required capacity.
def staticRSV_PR(graph,pktSize):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.no_rsv_pr != 0 or relay.single_line != 0:
                continue
            
            for linkIdx in vs_i["PR_RVS_ES_path"]:
                idx = vs_i["PR_RVS_ES_path"].index(linkIdx)
                
                src = vs_i["PR_RVS_VS_path"][idx+1]
                dst = vs_i["PR_RVS_VS_path"][idx]
                
                name1 = str(src)+'_'+str(dst)
                name2 = str(dst)+'_'+str(src)
                
#                 graph.es[linkIdx]["RSV_PR"][name1] += pktSize*8/relay.delay_per_link_PR
#                 graph.es[linkIdx]["RSV_PR"][name2] += pktSize*8/relay.delay_per_link_PR
                ca_req_1 = pktSize*8/relay.per_hop_dr_PR[linkIdx][name1]
                ca_req_2 = pktSize*8/relay.per_hop_dr_PR[linkIdx][name2]
                ca_req_nd = pktSize*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
                if graph.es[linkIdx]["RSV_PR"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_PR"][name1] += ca_req_1
                if graph.es[linkIdx]["RSV_PR"][name2] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_PR"][name2] += ca_req_2
                if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                    relay.used_link_RSV_PR[linkIdx] = ca_req_nd
                else:
                    relay.no_rsv_pr = 1
                    print("Relay",relay.relayIdx,"-> insufficient PRIMARY capacity")
                

# Static reservation for p2p scheme primary path
def staticRSV_P2P_PR(graph,pktSize):
    for edge_i in graph.es:
        if edge_i["prRelaySet"] == []:
            continue
        
        relaySet = []
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
        
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
        
        for r_src in edge_i["bpRelaySet"]:
            for r_peer in relaySet:
                if r_src.relayIdx == r_peer.relayIdx:
                    continue
                elif r_peer.relayIdx in r_src.p2p_rsv_record:
                    continue
                elif r_peer.Located_Node_Idx == r_src.Located_Node_Idx:
                    continue
                else:
                    r_src.p2p_rsv_record[r_peer.relayIdx] = 1
                    r_peer.p2p_rsv_record[r_src.relayIdx] = 1
                    
                    tempTable = r_src.p2p_per_hop_dr_PR[r_peer.relayIdx]
                    for linkIdx in tempTable:
                        for dirct in tempTable[linkIdx]:
                            if dirct != "nonDirection":
                                ca_req = pktSize*8/tempTable[linkIdx][dirct]
                                if graph.es[linkIdx]["RSV_P2P"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_P2P"][dirct] += ca_req
                                else:
                                    r_src.p2p_rsv_record[r_peer.relayIdx] = 0
                                    r_peer.p2p_rsv_record[r_src.relayIdx] = 0
                            elif dirct == "nonDirection":
                                ca_req = pktSize*8/tempTable[linkIdx][dirct]
                                if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_Overall"] += ca_req
                                else:
                                    r_src.p2p_rsv_record[r_peer.relayIdx] = 0
                                    r_peer.p2p_rsv_record[r_src.relayIdx] = 0


# In this function, for each relay in the system we assume it has only one backup path.
# We calculate the required reservation on each link of the path.     
def staticRSV_BP(graph,pktSize):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.no_rsv_bp != 0 or relay.single_line != 0:
                continue
            
            for linkIdx in vs_i["BP_RVS_ES_path"]:
                idx = vs_i["BP_RVS_ES_path"].index(linkIdx)
                
                src = vs_i["BP_RVS_VS_path"][idx+1]
                dst = vs_i["BP_RVS_VS_path"][idx]
                
                name1 = str(src)+'_'+str(dst)
                name2 = str(dst)+'_'+str(src)
                
                # Check if the link is also in the primary path of the relay
#                 if linkIdx in vs_i["PR_RVS_ES_path"]:
#                     print("Overlapping path!")
#                 else:
#                     print("Non-overlapping path!")
                
#                 graph.es[linkIdx]["RSV_BP"][name1] += pktSize*8/relay.delay_per_link_BP
#                 graph.es[linkIdx]["RSV_BP"][name2] += pktSize*8/relay.delay_per_link_BP
                ca_req_1 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name1]
                ca_req_2 = pktSize*8/relay.per_hop_dr_BP[linkIdx][name2]
                ca_req_nd = pktSize*8/relay.per_hop_dr_BP[linkIdx]["nonDirection"]
                if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_1 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_BP"][name1] += ca_req_1
                if graph.es[linkIdx]["RSV_BP"][name1] + ca_req_2 <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_BP"][name2] += ca_req_2
                if graph.es[linkIdx]["RSV_Overall"] + ca_req_nd <= graph.es[linkIdx]["capacity"]:
                    graph.es[linkIdx]["RSV_Overall"] += ca_req_nd
                    graph.es[linkIdx]["RSV_BP_Overall"] += ca_req_nd
                    relay.used_link_RSV_BP[linkIdx] = ca_req_nd
                else:
                    relay.no_rsv_bp = 1
                    print("Relay",relay.relayIdx,"-> insufficient BACKUP capacity")
                

# # Static reservation for p2p scheme primary path
# def staticRSV_P2P_BP(graph,pktSize):
#     for edge_i in graph.es:
#         if edge_i["prRelaySet"] == []:
#             continue
#         
#         relaySet = []
#         for relay in edge_i["prRelaySet"]:
#             relaySet.append(relay)
#         
#         for relay in edge_i["bpRelaySet"]:
#             relaySet.append(relay)
#         
#         for r_src in edge_i["bpRelaySet"]:
#             for r_peer in relaySet:
#                 if r_src.relayIdx == r_peer.relayIdx:
#                     continue
#                 elif r_peer.relayIdx in r_src.p2p_rsv_record_bp:
# #                     print("Path already reserved for R"+str(r_src.relayIdx)+" and R"+str(r_peer.relayIdx))
#                     continue
#                 elif r_peer.Located_Node_Idx == r_src.Located_Node_Idx:
#                     continue
#                 else:
# #                     print("Reservation for R"+str(r_src.relayIdx)+" and R"+str(r_peer.relayIdx))
#                     r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 1
#                     r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 1
#                     
#                     tempTable_bp = r_src.p2p_per_hop_dr_BP[r_peer.relayIdx]
#                     tempTable_pr = r_src.p2p_per_hop_dr_PR[r_peer.relayIdx]
#                     for linkIdx in tempTable_bp:
#                         if linkIdx not in tempTable_pr:
#                             for dirct in tempTable_bp[linkIdx]:
#                                 if dirct != "nonDirection":
#                                     ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
#                                     if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
#                                         graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
#                                     else:
#                                         r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
#                                         r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
#                                 elif dirct == "nonDirection":
#                                     ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
#                                     if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
#                                         graph.es[linkIdx]["RSV_Overall"] += ca_req
#                                         graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
#                                     else:
#                                         r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
#                                         r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
#                                         print("Backup link",linkIdx,"between","R"+str(r_src.relayIdx),\
#                                               "and","R"+str(r_peer.relayIdx),\
#                                               "-> insufficient BACKUP capacity")
#                         else:
#                             for dirct in tempTable_bp[linkIdx]:
#                                 if dirct != "nonDirection":
#                                     ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
#                                     pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
#                                     ca_req = max(ca_req - pr_link_rsv, 0)
#                                     
#                                     if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
#                                         graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
#                                     else:
#                                         r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
#                                         r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
#                                 elif dirct == "nonDirection":
#                                     ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
#                                     pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
#                                     ca_req = max(ca_req - pr_link_rsv, 0)
#                                     
#                                     if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
#                                         graph.es[linkIdx]["RSV_Overall"] += ca_req
#                                         graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
#                                     else:
#                                         r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
#                                         r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
#                                         print("Backup link",linkIdx,"between","R"+str(r_src.relayIdx),\
#                                               "and","R"+str(r_peer.relayIdx),\
#                                               "-> insufficient BACKUP capacity")



# Static reservation for p2p scheme primary path
def staticRSV_P2P_BP(graph,pktSize):
    for edge_i in graph.es:
        if edge_i["prRelaySet"] == []:
            continue
        
        relaySet = []
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
        
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
        
        for r_src in edge_i["prRelaySet"]:
            for r_peer_idx in r_src.p2p_per_hop_dr_BP:
                r_peer = calSPT2.find_relay_with_index(graph, r_peer_idx)
                if r_src.relayIdx == r_peer_idx:
                    continue
                elif r_peer_idx in r_src.p2p_rsv_record_bp:
#                     print("Path already reserved for R"+str(r_src.relayIdx)+" and R"+str(r_peer.relayIdx))
                    continue
                elif r_peer.Located_Node_Idx == r_src.Located_Node_Idx:
                    continue
                else:
#                     print("Reservation for R"+str(r_src.relayIdx)+" and R"+str(r_peer.relayIdx))
                    r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 1
                    r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 1
                    
                    tempTable_bp = r_src.p2p_per_hop_dr_BP[r_peer.relayIdx]
                    tempTable_pr = r_src.p2p_per_hop_dr_PR[r_peer.relayIdx]
                    for linkIdx in tempTable_bp:
                        if linkIdx not in tempTable_pr:
                            for dirct in tempTable_bp[linkIdx]:
                                if dirct != "nonDirection":
                                    ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                    if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
                                        graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
                                    else:
                                        r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
                                        r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
                                elif dirct == "nonDirection":
                                    ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                    if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
                                        graph.es[linkIdx]["RSV_Overall"] += ca_req
                                        graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
                                    else:
                                        r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
                                        r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
                                        print("Backup link",linkIdx,"between","R"+str(r_src.relayIdx),\
                                              "and","R"+str(r_peer.relayIdx),\
                                              "-> insufficient BACKUP capacity")
                        else:
                            for dirct in tempTable_bp[linkIdx]:
                                if dirct != "nonDirection":
                                    ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                    pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
                                    ca_req = max(ca_req - pr_link_rsv, 0)
                                    
                                    if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
                                        graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
                                    else:
                                        r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
                                        r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
                                elif dirct == "nonDirection":
                                    ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                    pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
                                    ca_req = max(ca_req - pr_link_rsv, 0)
                                    
                                    if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
                                        graph.es[linkIdx]["RSV_Overall"] += ca_req
                                        graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
                                    else:
                                        r_src.p2p_rsv_record_bp[r_peer.relayIdx] = 0
                                        r_peer.p2p_rsv_record_bp[r_src.relayIdx] = 0
                                        print("Backup link",linkIdx,"between","R"+str(r_src.relayIdx),\
                                              "and","R"+str(r_peer.relayIdx),\
                                              "-> insufficient BACKUP capacity")



# Static reservation for PARTIAL p2p scheme primary path
def staticRSV_P2P_Partial_BP(graph,pktSize):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
#             vs_src = graph.vs[relay.Located_EdgeIdx]
            for peerIdx in relay.p2p_per_hop_dr_BP:
#                 print("R"+str(relay.relayIdx)+" has peer R"+str(peerIdx))
                if peerIdx in relay.p2p_rsv_record_bp:
#                     print("R"+str(relay.relayIdx)+" and R"+str(peerIdx)+" already reserved.")
                    continue
                
                peer_relay = calSPT2.find_relay_with_index(graph, peerIdx)
                relay.p2p_rsv_record_bp[peerIdx] = 1
                peer_relay.p2p_rsv_record_bp[relay.relayIdx] = 1
                
#                 print("R"+str(relay.relayIdx)+" at bus", relay.Located_Node_Idx," -> peerIdx",peerIdx)
                if relay.Located_Node_Idx == peer_relay.Located_Node_Idx:
                    print("R"+str(relay.relayIdx)+" and R"+str(peerIdx)+" are at the same bus",\
                          relay.Located_Node_Idx," --> No need to reserve.")
                    continue
                
                tempTable_bp = relay.p2p_per_hop_dr_BP[peerIdx]
                tempTable_pr = relay.p2p_per_hop_dr_PR[peerIdx]
                
                for linkIdx in tempTable_bp:
                    if linkIdx not in tempTable_pr:
                        for dirct in tempTable_bp[linkIdx]:
                            if dirct != "nonDirection":
                                ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
                                else:
                                    relay.p2p_rsv_record_bp[peerIdx] = 0
                                    peer_relay.p2p_rsv_record_bp[relay.relayIdx] = 0
                            elif dirct == "nonDirection":
                                ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_Overall"] += ca_req
                                    graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
                                else:
                                    relay.p2p_rsv_record_bp[peerIdx] = 0
                                    peer_relay.p2p_rsv_record_bp[relay.relayIdx] = 0
                                    print("Backup link",linkIdx,"between","R"+str(relay.relayIdx),\
                                          "and","R"+str(peerIdx),\
                                          "-> insufficient BACKUP capacity")
                    else:
                        for dirct in tempTable_bp[linkIdx]:
                            if dirct != "nonDirection":
                                ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
                                ca_req = max(ca_req - pr_link_rsv, 0)
                                
                                if graph.es[linkIdx]["RSV_P2P_BP"][dirct] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_P2P_BP"][dirct] += ca_req
                                else:
                                    relay.p2p_rsv_record_bp[peerIdx] = 0
                                    peer_relay.p2p_rsv_record_bp[relay.relayIdx] = 0
                            elif dirct == "nonDirection":
                                ca_req = pktSize*8/tempTable_bp[linkIdx][dirct]
                                pr_link_rsv = pktSize*8/tempTable_pr[linkIdx][dirct]
                                ca_req = max(ca_req - pr_link_rsv, 0)
                                
                                if graph.es[linkIdx]["RSV_Overall"] + ca_req <= graph.es[linkIdx]["capacity"]:
                                    graph.es[linkIdx]["RSV_Overall"] += ca_req
                                    graph.es[linkIdx]["RSV_BP_Overall"] += ca_req
                                else:
                                    relay.p2p_rsv_record_bp[peerIdx] = 0
                                    peer_relay.p2p_rsv_record_bp[relay.relayIdx] = 0
                                    print("Backup link",linkIdx,"between","R"+str(relay.relayIdx),\
                                          "and","R"+str(peerIdx),\
                                          "-> insufficient BACKUP capacity")



def staticRSV_OFE_SingleFail_BP(graph,pktSize,maxEdgeIdx,failLinkIdx=-1):
    if failLinkIdx > -1 and failLinkIdx < maxEdgeIdx:
        for vs_i in graph.vs:
            for relay in vs_i["relaySet"]:
                if failLinkIdx in relay.per_hop_dr_BP_OFE:
                    for linkIdx in vs_i["BP_RVS_ES_Each"][failLinkIdx]:
                        idx = vs_i["BP_RVS_ES_Each"][failLinkIdx].index(linkIdx)
                        
                        src = vs_i["BP_RVS_VS_Each"][failLinkIdx][idx+1]
                        dst = vs_i["BP_RVS_VS_Each"][failLinkIdx][idx]
                        
                        name1 = str(src)+'_'+str(dst)
                        name2 = str(dst)+'_'+str(src)
                        
                        BP_To_BE_RSV = pktSize*8/relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"]
                        relay.used_link_RSV_BP[linkIdx] = BP_To_BE_RSV
#                         print('relay'+str(relay.relayIdx),linkIdx,failLinkIdx,vs_i["BP_RVS_ES_Each"][failLinkIdx],\
#                               vs_i["PR_RVS_ES_path"])
                        
                        if linkIdx in vs_i["PR_RVS_ES_path"]:
                            link_RSV_PR = relay.used_link_RSV_PR[linkIdx]
                            if BP_To_BE_RSV > link_RSV_PR:
#                                 print("Link",linkIdx+1,'PR->',link_RSV_PR,'BP->',BP_To_BE_RSV)
                                # Here we add the delta RSV on BP path if it is larger than the PR path
                                graph.es[linkIdx]["RSV_BP"][name1] += (BP_To_BE_RSV - link_RSV_PR)
                                graph.es[linkIdx]["RSV_BP"][name2] += (BP_To_BE_RSV - link_RSV_PR)
                                graph.es[linkIdx]["RSV_Overall"] += (BP_To_BE_RSV - link_RSV_PR)
                        else:
                            graph.es[linkIdx]["RSV_BP"][name1] += BP_To_BE_RSV
                            graph.es[linkIdx]["RSV_BP"][name2] += BP_To_BE_RSV
                            graph.es[linkIdx]["RSV_Overall"] += BP_To_BE_RSV
    
    elif failLinkIdx < 0:
        for vs_i in graph.vs:
            for relay in vs_i["relaySet"]:
                for pr_failLink_Idx in vs_i["PR_RVS_ES_path"]: # Each primary link (pr_link_Idx) may fail
                    if pr_failLink_Idx not in vs_i["BP_RVS_ES_Each"]:
                        print('NOT FOUND')
                    for linkIdx in vs_i["BP_RVS_ES_Each"][pr_failLink_Idx]:
                        # Each link of a specified backup path for the failed primary link
                        idx = vs_i["BP_RVS_ES_Each"][pr_failLink_Idx].index(linkIdx)
                        
                        src = vs_i["BP_RVS_VS_Each"][pr_failLink_Idx][idx+1]
                        dst = vs_i["BP_RVS_VS_Each"][pr_failLink_Idx][idx]
                        
                        name1 = str(src)+'_'+str(dst)
                        name2 = str(dst)+'_'+str(src)
                        
                        link_DR = relay.per_hop_dr_BP_OFE[pr_failLink_Idx][linkIdx]["nonDirection"]
                        BP_To_BE_RSV = pktSize*8/link_DR
                        
                        if linkIdx not in relay.used_link_RSV_BP:
                            relay.used_link_RSV_BP[linkIdx] = 0
                            
                        if BP_To_BE_RSV > relay.used_link_RSV_BP[linkIdx]:
                            relay.used_link_RSV_BP[linkIdx] = BP_To_BE_RSV
                            
                        if linkIdx == 12:
                            print(relay.relayIdx,vs_i["BP_RVS_ES_Each"][pr_failLink_Idx])
                            print(relay.per_hop_dr_BP_OFE[pr_failLink_Idx][linkIdx]["nonDirection"],\
                                  BP_To_BE_RSV,relay.used_link_RSV_BP[linkIdx])
                            
                for bp_link_Idx in relay.used_link_RSV_BP:
                    if bp_link_Idx in vs_i["PR_RVS_ES_path"]:
                        link_RSV_PR = relay.used_link_RSV_PR[bp_link_Idx]
                        link_RSV_BP = relay.used_link_RSV_BP[bp_link_Idx]
                        if link_RSV_BP > link_RSV_PR:
                            graph.es[bp_link_Idx]["RSV_Overall"] += (link_RSV_BP - link_RSV_PR)
                        if bp_link_Idx == 12:
                            print(relay.relayIdx,'overlap',graph.es[bp_link_Idx]["RSV_Overall"])
                    else:
                        graph.es[bp_link_Idx]["RSV_Overall"] += relay.used_link_RSV_BP[bp_link_Idx]
                        if bp_link_Idx == 12:
                            print(relay.relayIdx,graph.es[bp_link_Idx]["RSV_Overall"])
                    
    
    
# Smart reservation for specific conditions, e.g. 1-HF or 2-HF; currently for 1-HF.          
def smartRSV_PR(graph,pktSize,maxEdgeIdx):
    tempDict = {} # Store all used links for relays of a specified power line
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            relaySet = []
        
            for relay in edge_i["prRelaySet"]:
                relaySet.append(relay)
                
            for relay in edge_i["bpRelaySet"]:
                relaySet.append(relay)
                
            for relay in relaySet:
                for linkIdx in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                    idx = graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"].index(linkIdx)
                    
                    src = graph.vs[relay.Located_Node_Idx]["PR_RVS_VS_path"][idx+1]
                    dst = graph.vs[relay.Located_Node_Idx]["PR_RVS_VS_path"][idx]
                    
                    name1 = str(src)+'_'+str(dst)
                    name2 = str(dst)+'_'+str(src)
                    
                    if linkIdx not in tempDict:
                        tempDict[linkIdx] = {}
                    if name1 not in tempDict[linkIdx]:
                        tempDict[linkIdx][name1] = 0
                    if name2 not in tempDict[linkIdx]:
                        tempDict[linkIdx][name2] = 0
                    
#                     tempDict[linkIdx][name1] += pktSize*8/relay.delay_per_link_PR
#                     tempDict[linkIdx][name2] += pktSize*8/relay.delay_per_link_PR
                    tempDict[linkIdx][name1] += pktSize*8/relay.per_hop_dr_PR[linkIdx][name1]
                    tempDict[linkIdx][name2] += pktSize*8/relay.per_hop_dr_PR[linkIdx][name2]
                    relay.used_link_RSV_PR[linkIdx] = pktSize*8/relay.per_hop_dr_PR[linkIdx]["nonDirection"]
                    
            for idx in tempDict:
                for dictKey in tempDict[idx]:
                    if tempDict[idx][dictKey] > graph.es[idx]["RSV_PR"][dictKey]:
                        graph.es[idx]["RSV_PR"][dictKey] = tempDict[idx][dictKey]
                    if tempDict[idx][dictKey] > graph.es[idx]["RSV_Overall"]:
                        graph.es[idx]["RSV_Overall"] = tempDict[idx][dictKey]
            tempDict.clear()
                

def smartRSV_BP(graph,pktSize,maxEdgeIdx):
    tempDict = {}
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            relaySet = []
        
            for relay in edge_i["prRelaySet"]:
                relaySet.append(relay)
                
            for relay in edge_i["bpRelaySet"]:
                relaySet.append(relay)
                
            for relay in relaySet:
                for linkIdx in graph.vs[relay.Located_Node_Idx]["BP_RVS_ES_path"]:
                    idx = graph.vs[relay.Located_Node_Idx]["BP_RVS_ES_path"].index(linkIdx)
                    
                    src = graph.vs[relay.Located_Node_Idx]["BP_RVS_VS_path"][idx+1]
                    dst = graph.vs[relay.Located_Node_Idx]["BP_RVS_VS_path"][idx]
                    
                    name1 = str(src)+'_'+str(dst)
                    name2 = str(dst)+'_'+str(src)
                    
                    if linkIdx not in tempDict:
                        tempDict[linkIdx] = {}
                    if name1 not in tempDict[linkIdx]:
                        tempDict[linkIdx][name1] = 0
                    if name2 not in tempDict[linkIdx]:
                        tempDict[linkIdx][name2] = 0
                    
#                     tempDict[linkIdx][name1] += pktSize*8/relay.delay_per_link_BP
#                     tempDict[linkIdx][name2] += pktSize*8/relay.delay_per_link_BP
                    tempDict[linkIdx][name1] += pktSize*8/relay.per_hop_dr_BP[linkIdx][name1]
                    tempDict[linkIdx][name2] += pktSize*8/relay.per_hop_dr_BP[linkIdx][name2]
                    relay.used_link_RSV_BP[linkIdx] = pktSize*8/relay.per_hop_dr_BP[linkIdx]["nonDirection"]
                    
            for idx in tempDict:
                for dictKey in tempDict[idx]:
                    if tempDict[idx][dictKey] > graph.es[idx]["RSV_BP"][dictKey]:
                        graph.es[idx]["RSV_BP"][dictKey] = tempDict[idx][dictKey]
                    if tempDict[idx][dictKey] > graph.es[idx]["RSV_Overall"]:
                        graph.es[idx]["RSV_Overall"] = tempDict[idx][dictKey]
            tempDict.clear()
            

def smartRSV_OFE_SingleFail_BP(graph,pktSize,maxEdgeIdx,failLinkIdx=-1):
    tempDict = {}
    
    if failLinkIdx > -1 and failLinkIdx < maxEdgeIdx:
        for edge_i in graph.es:
            if edge_i.index < maxEdgeIdx:
                relaySet = []
                
                for relay in edge_i["prRelaySet"]:
                    if failLinkIdx in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                        relaySet.append(relay)
                
                for relay in edge_i["bpRelaySet"]:
                    if failLinkIdx in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                        relaySet.append(relay)
                        
                for relay in relaySet:
                    vs_i = graph.vs[relay.Located_Node_Idx]
                    for linkIdx in vs_i["BP_RVS_ES_Each"][failLinkIdx]: # Each link of the backup path
                        
                        # Compute the required RSV on the link of the backup path
                        BP_To_BE_RSV = pktSize*8/relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"]
                        relay.used_link_RSV_BP[linkIdx] = BP_To_BE_RSV
                        
                        if linkIdx not in tempDict:
                            tempDict[linkIdx] = 0
                            
                        tempDict[linkIdx] += BP_To_BE_RSV
                
                for idx in tempDict:
                    if tempDict[idx] > graph.es[idx]["RSV_Overall"]:
                        graph.es[idx]["RSV_Overall"] = tempDict[idx]
                tempDict.clear()
    elif failLinkIdx < 0:
        for failEdge in graph.es:
            if failEdge.index < maxEdgeIdx:
                current_fail_link_idx = failEdge.index
                for edge_i in graph.es:
                    relaySet = []
                    
                    for relay in edge_i["prRelaySet"]:
                        if current_fail_link_idx in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                            relaySet.append(relay)
                    
                    for relay in edge_i["bpRelaySet"]:
                        if current_fail_link_idx in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                            relaySet.append(relay)
                            
                    for relay in relaySet:
                        vs_i = graph.vs[relay.Located_Node_Idx]
                        for linkIdx in vs_i["BP_RVS_ES_Each"][current_fail_link_idx]: # Each link of the backup path
                            
                            # Compute the required RSV on the link of the backup path
                            relay_DR = relay.per_hop_dr_BP_OFE[current_fail_link_idx][linkIdx]["nonDirection"]
                            BP_To_BE_RSV = pktSize*8/relay_DR
                            
                            if linkIdx not in relay.used_link_RSV_BP:
                                relay.used_link_RSV_BP[linkIdx] = 0
                                
                            if BP_To_BE_RSV > relay.used_link_RSV_BP[linkIdx]:
                                relay.used_link_RSV_BP[linkIdx] = BP_To_BE_RSV
                                
                            if linkIdx not in tempDict:
                                tempDict[linkIdx] = 0
                                
                            tempDict[linkIdx] += BP_To_BE_RSV
                            
                    for idx in tempDict:
                        if tempDict[idx] > graph.es[idx]["RSV_Overall"]:
                            graph.es[idx]["RSV_Overall"] = tempDict[idx]
                    tempDict.clear()
                        

# The reservation of the primary path
def showRSV_PR(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_PR"]:
            print(str(elmt)+'->',edge_i["RSV_PR"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_PR"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_PR"][elmt]
            bus_name = edge_i.index
        print()
    print("MAX PR Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')
        
 
# The reservation of the backup path
def showRSV_BP(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_BP"]:
            print(str(elmt)+'->',edge_i["RSV_BP"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_BP"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_BP"][elmt]
            bus_name = edge_i.index
        print()
    print("MAX BP Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')
    
    
def showRSV_temp(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_temp"]:
            print(str(elmt)+'->',edge_i["RSV_temp"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_temp"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_temp"][elmt]
            bus_name = edge_i.index
        print()
    print("MAX temp Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')
        

# Show total reservation
def showRSV_Total(graph):
    max_RSV = 0
    bus_name = ' '
    link_direction = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_final"]:
            print(str(elmt)+'->',edge_i["RSV_final"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_final"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_final"][elmt]
            bus_name = edge_i.index
            link_direction = elmt
        print()
    print("MAX Final Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')
    
    
def showRSV_Overall(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',edge_i["RSV_Overall"]/1000,"kbps")
        if edge_i["RSV_Overall"] > max_RSV:
            max_RSV = edge_i["RSV_Overall"]
            bus_name = edge_i.index
            
    print("MAX Final Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')


# Show the p2p reservation
def showRSV_P2P(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_P2P"]:
            print(str(elmt)+'->',edge_i["RSV_P2P"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_P2P"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_P2P"][elmt]
            bus_name = edge_i.index
        print()
    print("MAX P2P Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')


# Show the p2p reservation
def showRSV_P2P_BP(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',end='')
        for elmt in edge_i["RSV_P2P_BP"]:
            print(str(elmt)+'->',edge_i["RSV_P2P_BP"][elmt]/1000,"kbps   ",end='')
        if edge_i["RSV_P2P_BP"][elmt] > max_RSV:
            max_RSV = edge_i["RSV_P2P_BP"][elmt]
            bus_name = edge_i.index
        print()
    print("MAX P2P BP Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')


# This function print reservation of all links in a list
def print_rsv_overall_in_list(graph,sortEnable=False,num=-1):
    result = []
    temp = []
    for edge_i in graph.es:
        temp.append(round(edge_i["RSV_Overall"]/1000,3))
    
    if sortEnable == True:
        temp.sort(reverse=True)
        if num > 0:
            for i in range(0,num):
                result.append(temp[i])
        else:
            result = temp
    
    print(result,len(result))
    
    return result
    
def showRSV_BP_Overall(graph):
    max_RSV = 0
    bus_name = ' '
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+': ',edge_i["RSV_BP_Overall"]/1000,"kbps")
        if edge_i["RSV_BP_Overall"] > max_RSV:
            max_RSV = edge_i["RSV_BP_Overall"]
            bus_name = edge_i.index
            
    print("MAX BP Reserved Link",str(bus_name+1)+' ->',max_RSV/1000,'kbps')
    

# The reservation on a link, with both directions
def totalRSV(graph,myType='equal'):
    if myType == 'equal':
        for edge_i in graph.es:
            for elmt in edge_i["RSV_PR"]:
                if elmt not in edge_i["RSV_final"]:
                    edge_i["RSV_final"][elmt] = 0
                edge_i["RSV_final"][elmt] = edge_i["RSV_PR"][elmt] + edge_i["RSV_BP"][elmt]
    elif myType == 'smart':
        for edge_i in graph.es:
            for elmt in edge_i["RSV_PR"]:
                if elmt not in edge_i["RSV_final"]:
                    edge_i["RSV_final"][elmt] = 0
                edge_i["RSV_final"][elmt] = max(edge_i["RSV_PR"][elmt],edge_i["RSV_BP"][elmt])
                

# This function prints different protection areas, each with primary relays and backup relays
def print_protection_area(graph):
    for edge_i in graph.es:
        relayIdxSet = []
        busIdxSet = []
        for relay in edge_i["prRelaySet"]:
            relayIdxSet.append(relay.relayIdx)
            busIdxSet.append(relay.Located_Node_Idx+1)
            
        for relay in edge_i["bpRelaySet"]:
            relayIdxSet.append(relay.relayIdx)
            busIdxSet.append(relay.Located_Node_Idx+1)
        print("Line",edge_i.index+1,"has relays:",relayIdxSet,"    Reside at buses:",busIdxSet)


# Print out all zone-3 lines for a relay
def print_remote_lines(graph):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            print("R"+str(relay.relayIdx)+" zone-3 lines:",relay.ZONE3_EdgeIdx)

    
def showAvgBPRelay(graph,maxEdgeIdx):
    total_BP = 0
    for edge_i in graph.es:
        if edge_i.index < maxEdgeIdx:
            total_BP += len(edge_i["bpRelaySet"])
            
    print("Average Backup Relay per-line:",total_BP/maxEdgeIdx)
    
    
# Show buses without relays
def showEmptyBus(graph):
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            print('Bus-'+str(vs_i.index+1)+'  ',end='')
    print()
    
    
def showSubtreeEdge_PR(graph):
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+':')
        for dictKey in edge_i["SBT_ES_PR"]:
            print("      ",dictKey,"->",edge_i["SBT_ES_PR"][dictKey])
            

def showSubtreeEdge_BP(graph):
    for edge_i in graph.es:
        print("Line",str(edge_i.index+1)+':')
        for dictKey in edge_i["SBT_ES_BP"]:
            print("      ",dictKey,"->",edge_i["SBT_ES_BP"][dictKey])


def setLinePro(edge_i):
    edge_i["pe"] = len(edge_i["bpRelaySet"])*0.00001
    edge_i["lineHF"] = len(edge_i["bpRelaySet"])*0.002
    
    
# Find the links that have would generate data to cross the target link for the primary path
def findSubtreeEdge_PR(graph,edgeList,nodeList):
    for idx in range(len(edgeList)):
        
        src = nodeList[idx+1]
        dst = nodeList[idx]
        
        attrName = str(src)+'_'+str(dst)
        
        for idx2 in range(idx+1,len(edgeList)):
#             print(name1,graph.es[edgeList[idx]].index,graph.es[edgeList[idx]][name1])
            if edgeList[idx2] not in graph.es[edgeList[idx]]["SBT_ES_PR"][attrName]:
                if graph.es[edgeList[idx2]]["prRelaySet"] != []:
                    graph.es[edgeList[idx]]["SBT_ES_PR"][attrName].append(edgeList[idx2])
        
        # Add the edge itself to the list of subtree-edge, if the edge has no relays, discard it
        if edgeList[idx] not in graph.es[edgeList[idx]]["SBT_ES_PR"][attrName]:
            if graph.es[edgeList[idx]]["prRelaySet"] != []:
                graph.es[edgeList[idx]]["SBT_ES_PR"][attrName].append(edgeList[idx])
                
                
# Find the links that have would generate data to cross the target link for the backup path
def findSubtreeEdge_BP(graph,edgeList,nodeList):
    for idx in range(len(edgeList)):
        
        src = nodeList[idx+1]
        dst = nodeList[idx]
        
        attrName = str(src)+'_'+str(dst)
        
        for idx2 in range(idx+1,len(edgeList)):
            if edgeList[idx2] not in graph.es[edgeList[idx]]["SBT_ES_BP"][attrName]:
                if graph.es[edgeList[idx2]]["prRelaySet"] != []:
                    graph.es[edgeList[idx]]["SBT_ES_BP"][attrName].append(edgeList[idx2])
        
        # Add the edge itself to the list of subtree-edge, if the edge has no relays, discard it
        if edgeList[idx] not in graph.es[edgeList[idx]]["SBT_ES_BP"][attrName]:
            if graph.es[edgeList[idx]]["prRelaySet"] != []:
                graph.es[edgeList[idx]]["SBT_ES_BP"][attrName].append(edgeList[idx])


def twoRDF_Pro(graph,line1_idx,line2_idx):
    p_RDF = graph.es[line1_idx]["lineHF"]*graph.es[line2_idx]["lineHF"]
    
    return p_RDF
    

def RSV_For_Single_Link_PR(graph,edge_i,pktSize,direction):
    relaySet = []
    rsv = 0
        
    for relay in edge_i["prRelaySet"]:
        relaySet.append(relay)
        
    for relay in edge_i["bpRelaySet"]:
        relaySet.append(relay)
        
    for relay in relaySet:
        if edge_i.index in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
            rsv += pktSize*8/relay.delay_per_link_PR
    
    if rsv > edge_i["RSV_PR"][direction]:
        edge_i["RSV_PR"][direction] = rsv


# Currently reserve for two links
def RSV_For_Multiple_Link_PR(graph,edge_i,pktSize,direction):
    relaySet = []
    rsv = 0
    max1 = 0
    max1_link_idx = -1
    max2 = 0
    max2_link_idx = -1
    
    for edgeIdx in edge_i["SBT_ES_PR"][direction]:
        relaySet.clear()
        rsv = 0
        
        for relay in graph.es[edgeIdx]["prRelaySet"]:
            relaySet.append(relay)
        
        for relay in graph.es[edgeIdx]["bpRelaySet"]:
            relaySet.append(relay)
            
        for relay in relaySet:
            if edge_i.index in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                rsv += pktSize*8/relay.delay_per_link_PR
#                 print(edgeIdx,relay.relayIdx,relay.delay_per_link_PR)
                
        if rsv > max1:
            max1 = rsv
            max1_link_idx = edgeIdx
        elif rsv > max2 and rsv <= max1:
            max2 = rsv
            max2_link_idx = edgeIdx

    rsv = max1 + max2
    print("Two max idx:",max1_link_idx,max2_link_idx)
    
    if rsv > edge_i["RSV_PR"][direction]:
        edge_i["RSV_PR"][direction] = rsv


# Currently reserve for two links
def RSV_Less_For_Multiple_Link_PR(graph,edge_i,pktSize,direction):
    relaySet = []
    rsv = 0
    max1 = 0
    max1_link_idx = -1
    
    for edgeIdx in edge_i["SBT_ES_PR"][direction]:
        relaySet.clear()
        rsv = 0
        
        for relay in graph.es[edgeIdx]["prRelaySet"]:
            relaySet.append(relay)
        
        for relay in graph.es[edgeIdx]["bpRelaySet"]:
            relaySet.append(relay)
            
        for relay in relaySet:
            if edge_i.index in graph.vs[relay.Located_Node_Idx]["PR_RVS_ES_path"]:
                rsv += pktSize*8/relay.delay_per_link_PR
                
        if rsv > max1:
            max1 = rsv
            max1_link_idx = edgeIdx

    rsv = max1
    
    if rsv > edge_i["RSV_PR"][direction]:
        edge_i["RSV_PR"][direction] = rsv
        

def smartRSV_2RDF_PR(graph,maxEdgeIdx=0,pktSize=0):
    req = 0.00001
    fulfilledList = []
    
    for edge_i in graph.es:
        p_TwoHF = 0
#         if edge_i.index < maxEdgeIdx:
        print("Line "+str(edge_i.index+1)+" HF Probability:")
        for dictKey in edge_i["SBT_ES_PR"]:
            p_TwoHF = 0
            for i in itls.combinations(edge_i["SBT_ES_PR"][dictKey],2):
                temp = twoRDF_Pro(graph,i[0],i[1])
    #             print(i,i[0],i[1],temp)
                p_TwoHF += temp
            edge_i["pSubTwoHF_PR"][dictKey] = p_TwoHF
    #             print("Two RDF Probability =",p_TwoHF)
            if p_TwoHF <= req and p_TwoHF != 0:
                print("Fulfilled: line",edge_i.index+1)
                fulfilledList.append(edge_i.index)
                RSV_Less_For_Multiple_Link_PR(graph,edge_i,pktSize,dictKey)
            elif p_TwoHF > req and p_TwoHF != 0 and len(edge_i["SBT_ES_PR"][dictKey]) > 1:
                RSV_For_Multiple_Link_PR(graph,edge_i,pktSize,dictKey)
            elif len(edge_i["SBT_ES_PR"][dictKey]) == 1:
                RSV_For_Single_Link_PR(graph,edge_i,pktSize,dictKey)
                
    print(fulfilledList)


# The method is to set the weight of the first link of the path to infinity. Thus the shortest path algorithm
# would find another path to the destination.
def findBackupPath_simple(graph,dstName,examptList=[]):

    for vs_i in graph.vs:
        if vs_i["name"] == dstName or vs_i["relaySet"] == []:
#             print("Jump bus",vs_i.index+1)
            if vs_i["name"] not in examptList:
                continue
        
        listLastIdx = len(vs_i["PR_RVS_ES_path"]) - 1
        graph.es[vs_i["PR_RVS_ES_path"][listLastIdx]]["weight"] = 10000
        edgeSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "epath")
        vsSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "vpath")
        graph.es[vs_i["PR_RVS_ES_path"][listLastIdx]]["weight"] = 1
        
        print("Bus",str(vs_i.index+1)+':',vs_i["PR_RVS_ES_path"][listLastIdx],"  ->  ",edgeSetList)
        
        vs_i["BP_hop_no"] = len(edgeSetList[0])
        vs_i["BP_RVS_ES_path"] = deepcopy(edgeSetList[0])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[0])
        findSubtreeEdge_BP(graph, edgeSetList[0], vsSetList[0])

#         print("Bus",str(vs_i.index+1)+':',edgeSetList)
#         print("Bus",str(vs_i.index+1)+':',vsSetList)


def showRVS_ES_Each(graph):
    for vs_i in graph.vs:
        for elemt in vs_i["BP_RVS_ES_Each"]:
            print("Bus",vs_i.index+1,"Link",elemt+1,"fails, new path: ")
            print("Edge:",vs_i["BP_RVS_ES_Each"][elemt])
            print("Bus:",vs_i["BP_RVS_VS_Each"][elemt])
            None
    
    
# Use pure distance (Dijkstra's AL) to find a backup path
# For each link, we find out the affected nodes and calculate a backup path for one node
# For different link failures, a node may or may not have the same backup path, but we record all
# backup paths
def findBpPath_OneForEach_PR_Link_Dist(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    
    for edge_i in graph.es:
        affectedNodeList = []
        if edge_i.index < maxEdgeIdx:
            edge_i["weight"] = 10000
            for vs_i in graph.vs:
                if edge_i.index in vs_i["PR_RVS_ES_path"] and vs_i["relaySet"] != []:
                    affectedNodeList.append(vs_i.index)
                    esSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "epath")
                    vsSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "vpath")
                    
                    # For each failed link, a node has a backup path
                    if edge_i.index not in vs_i["BP_RVS_ES_Each"]:
                        vs_i["BP_RVS_ES_Each"][edge_i.index] = deepcopy(esSetList[0])
                    
                    if edge_i.index not in vs_i["BP_RVS_VS_Each"]:
                        vs_i["BP_RVS_VS_Each"][edge_i.index] = deepcopy(vsSetList[0])
                    
#                     if vs_i.index == 2:
#                         print("Link",edge_i.index+1,"fails, new path: ")
#                          
#                         print("Edge:",esSetList)
#                         print("Bus:",vsSetList)
                        
            edge_i["weight"] = 1
            
            # Do the delay requirement division for the affected nodes
#             print(edge_i.index)
            do_Perhop_DelayReq_SingleFailure_BP(graph,affectedNodeList,edge_i.index,myType,dstSft,powft)
            
#             print("Link",edge_i.index+1,"affect bus: ",end='')
#             for i in affectedNodeList:
#                 print(i+1,' ',end='')
#             print()
#     showRVS_ES_Each(graph)
    None
    
# Use pure distance (Dijkstra's AL) to find a backup path. Now for each node we find a totally
# non-overlapping path to the MA.
def findBpPath_PR_Path_NonOL_Dist(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
#         if vs_i.index != maxEdgeIdx:
#             continue
        # Set all links on the primary path to be very large weights but not "disconnected"
        for linkIdx in vs_i["PR_RVS_ES_path"]:
            graph.es[linkIdx]["weight"] = 10000
            
        esSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "epath")
        vsSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "vpath")
#         print("Bus",vs_i.index+1,"-> ")
#         print("Primary edge path:",vs_i["PR_RVS_ES_path"])
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        
        for linkIdx in vs_i["PR_RVS_ES_path"]:
            graph.es[linkIdx]["weight"] = 1
            
        vs_i["BP_hop_no"] = len(esSetList[0])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[0])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[0])
    

# A function to update the link weight.
# Currently only plus the number of relays using this link. It is valid if a non-overlapped path is used
def simple_link_weight_update_v1(graph):
    for edge_i in graph.es:
#         print("Link",edge_i.index+1,"Before ->",edge_i["weight"])
        edge_i["weight"] = 1 + edge_i["usedNo"]["nonDirection"]
#         print("Link",edge_i.index+1,"Now ->",edge_i["weight"])
        

# A function to update the link weight.
# Currently multiply (1+ ratio of relays using this link). It is valid if a non-overlapped path is used
def simple_link_weight_update_v2(graph):
    for edge_i in graph.es:
#         print("Link",edge_i.index+1,"Before ->",edge_i["weight"])
        edge_i["weight"] = 10*(1 + edge_i["usedNo"]["nonDirection"]/graph["total_relay_no"])
#         if edge_i.index ==9:
#             edge_i["weight"] = 1000.11111
#         print("Link",edge_i.index+1,"Now ->",edge_i["weight"])


# A function to update the link weight.
# This function considers the load utilization for the link weight.
def simple_link_weight_update_v3(graph):
    for edge_i in graph.es:
        linkUtil = edge_i["RSV_temp_ND"]/edge_i["capacity"]
        freeWeight = 1 - linkUtil
        edge_i["weight"] = 1/freeWeight
    None
    

# A function to update the link weight. This function is IDENTICAL to the above "v3"!!!
# This function considers the available capacity on a link, so that the minimum delay is L/(available CA).
def simple_link_weight_update_v4(graph):
    for edge_i in graph.es:
        edge_i["weight"] = 80*8/(edge_i["capacity"]-edge_i["RSV_temp_ND"])
    
        
        
            
# Use distance and number of relays using the link to compute link weight. Use (Dijkstra's AL) to find a backup path.
# Now for each node we find a totally non-overlapping path to the MA.
def findBpPath_PR_Path_NonOL_LinkUsedNo(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    # We first update the link weights
#     simple_link_weight_update(graph)
    
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
#         if vs_i.index != maxEdgeIdx:
#             continue
        simple_link_weight_update_v1(graph)
        tempDict = {}
        for linkIdx in vs_i["PR_RVS_ES_path"]:
            tempDict[linkIdx] = graph.es[linkIdx]["weight"] # Store the original value
            graph.es[linkIdx]["weight"] = 10000
        
        esSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "epath")
        vsSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "vpath")
#         print("Bus",vs_i.index+1,"-> ")
#         print("Primary edge path:",vs_i["PR_RVS_ES_path"])
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        
        for linkIdx in vs_i["PR_RVS_ES_path"]:
            graph.es[linkIdx]["weight"] = tempDict[linkIdx]
        for edgeIdx in esSetList[0]:
            graph.es[edgeIdx]["usedNo"]["nonDirection"] += len(vs_i["relaySet"])
            
        vs_i["BP_hop_no"] = len(esSetList[0])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[0])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[0])
    print(graph.is_weighted(),graph.es["weight"])


# Use link utilization to compute link weight. Use (Dijkstra's AL) to find a backup path.
# Now for each node we find a totally non-overlapping path to the MA.
def findBpPath_PR_Path_NonOL_LinkLoad(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        simple_link_weight_update_v4(graph) # Here we set link weight to be related to "Utilization"
#         print("Weight:",graph.es["weight"])
        for linkIdx in vs_i["PR_RVS_ES_path"]:
            graph.es[linkIdx]["weight"] = 10000
            
        esSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "epath")
        vsSetList = graph.get_shortest_paths(v=dstName,to=vs_i,weights=graph.es["weight"],output = "vpath")
#         print("Bus",vs_i.index+1,"-> ")
#         print("Primary edge path:",vs_i["PR_RVS_ES_path"])
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        vs_i["BP_hop_no"] = len(esSetList[0])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[0])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[0])
        
        # We need to update the link utilization
        for relay in vs_i["relaySet"]:
            tempDict = {}
            if myType == 'equal':
                weight_function_equal(graph, relay, esSetList[0], vsSetList[0], tempDict, True)
                        
            elif myType == 'distance':
                weight_function_distance(graph, relay, esSetList[0], vsSetList[0], tempDict, dstSft, True)
                
            elif myType == 'load':
                weight_function_load(graph, relay, esSetList[0], vsSetList[0], tempDict, powft)
    
#     for edge_i in graph.es:
#             print("In function, Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)
    if myType == 'load':
        copyPR_RSV_to_Temp(graph)
    None
    

# A simple function that returns an index for the path list
def select_from_path_set(path_delay_list):
    idx = int(len(path_delay_list)/2)
    if idx == 0:
        idx = 1
#     idx = 1
    if idx > len(path_delay_list):
        idx = len(path_delay_list)
        
    res = heapq.nsmallest(idx, path_delay_list)
    result = path_delay_list.index(res[idx-1])
    
    return result


# A function that returns the path index that "just-fit" the requirement
def select_from_path_set_v2(path_delay_list,min_req):
    selected_No = min(path_delay_list, key=lambda x:abs(x-min_req))
    idx = path_delay_list.index(selected_No)
#     print("Given List:",path_delay_list,"min req:",min_req,"choose",str(idx)+"th",path_delay_list[idx])
    
    return idx
    

# A function that returns the path index with "shortest hop count", used in finding a backup path
# either there is overlapping path or not. Here we do not consider "multiple possible paths", but
# simply choose the first satisfying path
def select_from_path_set_v3(linkPath_list):
    if linkPath_list == []:
        return -1
    
    hop_count_list = []
    for path_i in linkPath_list:
        hop_count_list.append(len(path_i))
    
    min_hc = min(hop_count_list)
    idx = hop_count_list.index(min_hc)
    print("Choose",str(idx)+"th path:",linkPath_list[idx],min_hc)
    print("All hop counts:",hop_count_list)
    return idx
    

# A function that returns the path index with "shortest hop count", used in finding a backup path
# either there is overlapping path or not. Here we consider "multiple possible paths", different
# strategies may be used to select one path.
def select_from_path_set_v4(linkPath_list,pr_path):
    hop_count_list = []
    for path_i in linkPath_list:
        hop_count_list.append(len(path_i))
    
    min_hc = min(hop_count_list)
    dupList = list_duplicates_of(hop_count_list, min_hc)
    idx = hop_count_list.index(min_hc)
#     print("Choose",str(idx)+"th path:",linkPath_list[idx],min_hc)
#     print("All hop counts:",hop_count_list)
#     print("Duplicated index:",dupList)
    if len(dupList) != 1:
        idx = select_min_ol_pr(linkPath_list, dupList, pr_path)
#         print("New: Choose",str(idx)+"th path:",linkPath_list[idx],min_hc)
    return idx


# This function selects the path with minimum overlap with the primary path
def select_min_ol_pr(linkPath_list,dupList,pr_path):
    ol_no_list = []
#     print("Primary path:",pr_path)
    for idx in dupList:
        overlapping = 0
#         print("Path:",linkPath_list[idx])
        for linkIdx in linkPath_list[idx]:
            if linkIdx in pr_path:
                overlapping += 1
        ol_no_list.append(overlapping)
    
#     print("Overlapping case:",ol_no_list)
    min_ol = min(ol_no_list) # The minimum overlapping number
    ol_idx = ol_no_list.index(min_ol) # The location of the minimum OL No in the list
    result = dupList[ol_idx] # The path index to be selected
#     print(min_ol,ol_idx,result)
    
    return result


# If a list has duplicated elements, this function finds out their positions
def list_duplicates_of(seq,item):
    start_at = -1
    locs = []
    while True:
        try:
            loc = seq.index(item,start_at+1)
        except ValueError:
            break
        else:
            locs.append(loc)
            start_at = loc
    return locs

    
# This function finds all non-overlapping backup path for a node        
def find_ALLBpPath_PR_Path_NonOL_LinkLoad(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    srcIdx = int(dstName) - 1
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        dstIdx = vs_i.index
#         simple_link_weight_update_v4(graph)
#         print("Weight:",graph.es["weight"])
            
        esSetList = []
        vsSetList = []
        delaySetList = []
#         print("Bus",vs_i.index+1)
        find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, vs_i["PR_RVS_ES_path"],\
                        vs_i["min_relay_req"], enableOL=False, debugPrint=False)
#         print("Bus",vs_i.index+1,"-> ")
#         print("Primary edge path:",vs_i["PR_RVS_ES_path"])
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        if vs_i.index != srcIdx:
#             selectedIdx = select_from_path_set(delaySetList)
            selectedIdx = select_from_path_set_v2(delaySetList, vs_i["min_relay_req"]/60)
#             print("Select",selectedIdx)
        else:
            selectedIdx = 0
#         print("Choose",selectedIdx,"path",esSetList[selectedIdx])
        vs_i["BP_hop_no"] = len(esSetList[selectedIdx])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[selectedIdx])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[selectedIdx])
        
        # We need to update the link utilization
        for relay in vs_i["relaySet"]:
            tempDict = {}
            if myType == 'equal':
                weight_function_equal(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], tempDict, True)
                        
            elif myType == 'distance':
                weight_function_distance(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], \
                                         tempDict, dstSft, True)
                
            elif myType == 'load':
                weight_function_load(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], tempDict, powft)
    
#     for edge_i in graph.es:
#             print("In function, Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)
    if myType == 'load':
        copyPR_RSV_to_Temp(graph)
    None
    
    
# This function finds all backup paths for a node
def find_ALLBpPath_PR_Path_LinkLoad(graph,dstName,maxEdgeIdx,myType='equal',dstSft=0,powft=1):
    srcIdx = int(dstName) - 1
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        dstIdx = vs_i.index
#         simple_link_weight_update_v4(graph)
#         print("Weight:",graph.es["weight"])
            
        esSetList = []
        vsSetList = []
        delaySetList = []
#         print("Bus",vs_i.index+1)
        find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, vs_i["PR_RVS_ES_path"],\
                        vs_i["min_relay_req"], enableOL=True, debugPrint=False)
#         print("Bus",vs_i.index+1,"-> ")
#         print("Primary edge path:",vs_i["PR_RVS_ES_path"])
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        if vs_i.index != srcIdx:
#             selectedIdx = select_from_path_set(delaySetList)
            selectedIdx = select_from_path_set_v4(esSetList,vs_i["PR_RVS_ES_path"])
#             print("Select",selectedIdx)
        else:
            selectedIdx = 0
#         print("Choose",selectedIdx,"path",esSetList[selectedIdx])
        vs_i["BP_hop_no"] = len(esSetList[selectedIdx])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[selectedIdx])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[selectedIdx])
        
        # We need to update the link utilization
        # Currently we choose the "shortest hop count backup path", and choose one with least overlaps with
        # the primary path if multiple paths exist. Thus at this step whether to "pre-reserve" capacity is not
        # important but if we consider from the load issues, the "pre-reservation" is required.
        for relay in vs_i["relaySet"]:
            tempDict = {}
            if myType == 'equal':
                weight_function_equal(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], tempDict, True)
                        
            elif myType == 'distance':
                weight_function_distance(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], \
                                         tempDict, dstSft, True)
                
            elif myType == 'load':
                weight_function_load(graph, relay, esSetList[selectedIdx], vsSetList[selectedIdx], tempDict, powft)
    
#     for edge_i in graph.es:
#             print("In function, Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)
    if myType == 'load':
        # Set the temporary variable to initial state since later we would divide delay requirement
        # and this variable would be used again, especially for "load"; the other types do not use it.
        copyPR_RSV_to_Temp(graph)
    None


# This function finds all backup paths for a node, considering the power issues
# We can choose either overlapping or non-overlapping path to fulfill the requirement
# Start from the shortest path, check if the path fulfill the probability requirement
def find_backup_path_from_power(graph,dstName,use_OL=True,myType='equal',dstSft=0,powft=1):
    srcIdx = int(dstName) - 1
    for vs_i in graph.vs:
        if vs_i["relaySet"] == []:
            continue
        
        dstIdx = vs_i.index
#         simple_link_weight_update_v4(graph)
#         print("Weight:",graph.es["weight"])
            
        esSetList = []
        vsSetList = []
        delaySetList = []
#         print("Bus",vs_i.index+1)
        find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, vs_i["PR_RVS_ES_path"],\
                        -1, enableOL=use_OL, debugPrint=False)
        vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
        esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        if vs_i.index != srcIdx:
#             selectedIdx = select_from_path_set(delaySetList)
            selectedIdx = calSPT2.select_path_based_on_power(esSetList,vs_i,vs_i["PR_RVS_ES_path"],graph)
#             print("Select",selectedIdx)
        else:
            selectedIdx = 0
        
        if esSetList == [] and use_OL == False:
            find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, vs_i["PR_RVS_ES_path"],\
                        -1, enableOL=True, debugPrint=False)
            vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
            esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
            if vs_i.index != srcIdx:
                selectedIdx = calSPT2.index_other_than_pr_path(graph, vs_i, esSetList)
        
        print("Bus_"+vs_i["name"]+" chooses",selectedIdx,"path",esSetList[selectedIdx],"  ","PR:",vs_i["PR_RVS_ES_path"])
        vs_i["BP_hop_no"] = len(esSetList[selectedIdx])
        vs_i["BP_RVS_ES_path"] = deepcopy(esSetList[selectedIdx])
        vs_i["BP_RVS_VS_path"] = deepcopy(vsSetList[selectedIdx])



# This function try to find a backup path for each relay, not for a bus. We also perform the
# delay division and capacity reservation in this function. We first sort the found paths using the
# probability from power data and then check whether the whole path is able to support the full
# reservation. If yes, we choose the path, else we would continue to the next path.
def find_backup_dv_rsv_each_relay(graph,dstName,maxEdgeIdx,ordered_list,myType='equal',dstSft=0,powft=1):
    srcIdx = int(dstName) - 1
    temp_ordered_list = deepcopy(ordered_list)
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            if relay.relayIdx not in temp_ordered_list:
                temp_ordered_list.append(relay.relayIdx)
                
    for relayIdx in temp_ordered_list:
        relay = calSPT2.find_relay_with_index(graph, relayIdx)
        if relay == -1:
            raise NameError('Relay not found!')
        
        vs_i = graph.vs[relay.Located_Node_Idx]
        dstIdx = vs_i.index
        esSetList = []
        vsSetList = []
        delaySetList = []
        find_all_paths(graph, srcIdx, dstIdx, vsSetList, esSetList, delaySetList, vs_i["PR_RVS_ES_path"],\
                        vs_i["min_relay_req"], enableOL=True, debugPrint=False)
        vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
        esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
#         print("Backup edge path:",esSetList)
#         print("Backup node path:",vsSetList)
        if vs_i.index != srcIdx:
#             selectedIdx = select_from_path_set(delaySetList)
            selectedIdx = calSPT2.select_backup_path_dv_rsv_on_power(esSetList, vsSetList, vs_i, relay, graph)
#             print("Select",selectedIdx)
        else:
            selectedIdx = 0
        
        relay.BP_RVS_ES_path = deepcopy(esSetList[selectedIdx])
        relay.BP_RVS_VS_path = deepcopy(vsSetList[selectedIdx])
    None



# This function finds backup path in the P2P-scheme. Shortest distance is used as the metric.
# For each path from a bus to another bus, we expect to find a Non-Overlap path for it. Then each relay
# on this bus has Non-Overlap backup path to any of its peers, whether the peers are within the same protect
# area or not
def find_backup_p2p_simple(graph,enOL=False):
#     self.p2p_per_hop_dr_BP = {} # {peer_relay_idx : {link : {link_direction : DR}}}
#     self.p2p_rsv_record_bp = {}
#     vs_i["P2P_BP_RVS_ES_path"] = {}
#     vs_i["P2P_BP_RVS_VS_path"] = {}
#     edge_i["RSV_P2P_BP"] = {}
#     edge_i["RSV_P2P_BP"][name1] = 0
#     edge_i["RSV_P2P_BP"][name2] = 0
    no_overlap_path = 0
    for vs_i in graph.vs:
#         if vs_i.index != 0:
#             continue
        srcIdx = vs_i.index
        for peerIdx in vs_i["P2P_RVS_ES_path"]:
            esSetList = []
            vsSetList = []
            delaySetList = []
            find_all_paths(graph, peerIdx, srcIdx, vsSetList, esSetList, delaySetList, vs_i["P2P_RVS_ES_path"][peerIdx],\
                        -1, enableOL=enOL, debugPrint=False, mode_use="p2p")
            
            if enOL == False and esSetList == []:
                print("No non-overlap path available from bus "+vs_i["name"]+" to bus",peerIdx+1)
                no_overlap_path += 1
            
            vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
            esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
#             if vs_i.index != srcIdx:
#             selectedIdx = select_from_path_set_v3(esSetList)
#             print("Select",selectedIdx)
#             else:
            selectedIdx = 0
            
            vs_i["P2P_BP_RVS_ES_path"][peerIdx] = deepcopy(esSetList[selectedIdx])
            vs_i["P2P_BP_RVS_VS_path"][peerIdx] = deepcopy(vsSetList[selectedIdx])
            
            # reverse the path direction to store in the peers path table
            peer_bus = graph.vs[peerIdx]
            esSetList[selectedIdx].reverse()
            vsSetList[selectedIdx].reverse()
            
            peer_bus["P2P_BP_RVS_ES_path"][srcIdx] = deepcopy(esSetList[selectedIdx])
            peer_bus["P2P_BP_RVS_VS_path"][srcIdx] = deepcopy(vsSetList[selectedIdx])
            
    print(no_overlap_path,"primary paths cannot find non-overlap path.")



# This function finds backup path in the P2P-scheme. Shortest distance is used as the metric.
# We find specified number of non-overlap backup paths rather than finding one for each "relay-pair"
def find_backup_p2p_partial(graph,req_bp_no,enOL=False,ln=0,ridx=0):
    
    for edge_i in graph.es:
        relaySet = [] # Store all relays for this line
        busSet = [] # Store the bus index where the relays reside
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
            busSet.append(relay.Located_Node_Idx)
            
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
            busSet.append(relay.Located_Node_Idx)
        
        # Use the shortest-distance path
        for relay in edge_i["bpRelaySet"]:
            vs_src_idx = relay.Located_Node_Idx
            vs_src = graph.vs[vs_src_idx] # The current source node
            
            temp_vs_list = []
            temp_es_list = []
            temp_pair_list = []
            
            no_overlap_path = 0
            
            # Calculate how many backup paths we still require, for each relay, we expect it to have at least
            # "req_bp_no" backup paths to other different relays. Be careful that if req_bp_no > the number of
            # relays in a protection set, then req_bp_no need to be adjusted
            if len(relay.line_relay_bp_peers[edge_i.index]) >= req_bp_no:
                continue
            
            if req_bp_no > len(relaySet)-1:
                req_backup_num = len(relaySet)-1
            else:
                req_backup_num = req_bp_no
            
            peerSet = []
            for r_i in relaySet:
                if r_i != relay:
                    peerSet.append(r_i.relayIdx)
            
            delete_peer_list = []
            for relayIdx in peerSet:
                if relayIdx in relay.contacted_bp_relay:
                    delete_peer_list.append(relayIdx)
                    if relayIdx not in relay.line_relay_bp_peers[edge_i.index]:
                        if len(relay.line_relay_bp_peers[edge_i.index]) < req_backup_num:
                            relay.line_relay_bp_peers[edge_i.index].append(relayIdx)
            
            if len(relay.line_relay_bp_peers[edge_i.index]) >= req_backup_num:
                continue
            
            for idx in delete_peer_list:
                peerSet.remove(idx)
            
            if edge_i.index == ln and relay.relayIdx == ridx:
                print("Peer candidates:",peerSet)
            
            # Select backup peers from the remaining elements of "peerSet"
            for relay_idx in peerSet:
                peer_relay = calSPT2.find_relay_with_index(graph, relay_idx)
                esSetList = []
                vsSetList = []
                delaySetList = []
                if relay.relayIdx == relay_idx:
                    continue
                elif relay.Located_Node_Idx == peer_relay.Located_Node_Idx:
                    vsSetList.append(peer_relay.Located_Node_Idx)
                    
                    temp_vs_list.append(deepcopy(vsSetList))
                    temp_es_list.append(deepcopy(esSetList))
                    temp_pair_list.append(deepcopy(relay_idx))
                else:
                    vs_dst_idx = peer_relay.Located_Node_Idx
                    find_all_paths(graph, vs_dst_idx, vs_src_idx, vsSetList, esSetList, delaySetList, \
                                   vs_src["P2P_RVS_ES_path"][vs_dst_idx],\
                                -1, enableOL=enOL, debugPrint=False, mode_use="p2p")
                     
                    if enOL == False and esSetList == []:
                        print("No non-overlap path available from bus "+vs_src["name"]+" to bus",vs_dst_idx+1)
                        no_overlap_path += 1
                     
                    vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
                    esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
#                     if vs_src_idx != srcIdx:
#                         selectedIdx = select_from_path_set_v3(esSetList)
        #             print("Select",selectedIdx)
#                     else:
                    selectedIdx = 0
                     
                    temp_vs_list.append(deepcopy(vsSetList[selectedIdx]))
                    temp_es_list.append(deepcopy(esSetList[selectedIdx]))
                    temp_pair_list.append(deepcopy(relay_idx))
            
            if edge_i.index == ln and relay.relayIdx == ridx:
                print("Peer candidates path:",temp_es_list)
            
            still_req_bp_no = max(0, req_backup_num - len(relay.line_relay_bp_peers[edge_i.index]))
            
            hop_cnt_list = []
            for es_path in temp_es_list:
                hop_cnt_list.append(len(es_path))
            
            if still_req_bp_no > len(hop_cnt_list):
                still_req_bp_no = len(hop_cnt_list)
            
            res = heapq.nsmallest(still_req_bp_no, hop_cnt_list)
            index_list = []
            already_used_idx = []
            for res_hc in res:
                for i in range(len(hop_cnt_list)):
                    if res_hc == hop_cnt_list[i]:
                        idx = i
                        if idx not in already_used_idx:
                            already_used_idx.append(i)
                            if idx not in index_list:
                                index_list.append(idx)
                                break
            
            if edge_i.index == ln and relay.relayIdx == ridx:
                print("still_req_bp_no =",still_req_bp_no,"req_backup_num =",req_backup_num,\
                      "already have:",len(relay.line_relay_bp_peers[edge_i.index]),"hop_cnt_list:",\
                      hop_cnt_list,"index_list:",index_list,"res:",res)
            
            for idx in index_list:
                src_relay = relay
                dst_relay = calSPT2.find_relay_with_index(graph, temp_pair_list[idx])
                srcIdx = src_relay.Located_Node_Idx
                dstIdx = dst_relay.Located_Node_Idx
                 
                vs_i = graph.vs[srcIdx]
                 
                vs_i["P2P_BP_RVS_ES_path"][dstIdx] = deepcopy(temp_es_list[idx])
                vs_i["P2P_BP_RVS_VS_path"][dstIdx] = deepcopy(temp_vs_list[idx])
                 
                dst_bus = graph.vs[dstIdx]
                temp_es_list[idx].reverse()
                temp_vs_list[idx].reverse()
                 
                dst_bus["P2P_BP_RVS_ES_path"][srcIdx] = deepcopy(temp_es_list[idx])
                dst_bus["P2P_BP_RVS_VS_path"][srcIdx] = deepcopy(temp_vs_list[idx])
                
                if dst_relay.relayIdx not in src_relay.contacted_bp_relay:
                    src_relay.contacted_bp_relay.append(dst_relay.relayIdx)
                
                if dst_relay.relayIdx not in src_relay.line_relay_bp_peers[edge_i.index]:
                    src_relay.line_relay_bp_peers[edge_i.index].append(dst_relay.relayIdx)
            # BE CAREFUL: a primary relay can be a backup peer for a relay, but the relay might not be a backup
            # peer for this primary relay because they do not look at the same remote-line

# def find_backup_p2p_partial(graph,req_bp_no,enOL=False):
#     no_overlap_path = 0
#     for edge_i in graph.es:
#         relaySet = []
#         busSet = [] # Store the bus index where the relays reside
#         for relay in edge_i["prRelaySet"]:
#             relaySet.append(relay)
#             busSet.append(relay.Located_Node_Idx)
#             
#         for relay in edge_i["bpRelaySet"]:
#             relaySet.append(relay)
#             busSet.append(relay.Located_Node_Idx)
#             
#         tempDict = {}
#         temp_vs_list = []
#         temp_es_list = []
#         temp_pair_list = []
#         for vs_idx in busSet:
#             tempDict[vs_idx] = []
#         
#         # Use the shortest-distance path
#         for vs_src_idx in busSet:
#             vs_src = graph.vs[vs_src_idx] # The current source node
#             for vs_dst_idx in busSet:
# #                 vs_dst = graph.vs[vs_dst_idx] # The destination node
#                 if vs_src_idx == vs_dst_idx:
#                     continue
#                 elif vs_dst_idx in tempDict[vs_src_idx]:
#                     continue
#                 else:
#                     tempDict[vs_src_idx].append(vs_dst_idx)
#                     tempDict[vs_dst_idx].append(vs_src_idx)
#                     
#                     esSetList = []
#                     vsSetList = []
#                     delaySetList = []
#                     find_all_paths(graph, vs_dst_idx, vs_src_idx, vsSetList, esSetList, delaySetList, \
#                                    vs_src["P2P_RVS_ES_path"][vs_dst_idx],\
#                                 -1, enableOL=enOL, debugPrint=False, mode_use="p2p")
#                     
#                     if enOL == False and esSetList == []:
#                         print("No non-overlap path available from bus "+vs_src["name"]+" to bus",vs_dst_idx+1)
#                         no_overlap_path += 1
#                     
#                     vsSetList = sorted(vsSetList, key=lambda path_list: len(path_list))
#                     esSetList = sorted(esSetList, key=lambda path_list: len(path_list))
# #                     if vs_src_idx != srcIdx:
# #                         selectedIdx = select_from_path_set_v3(esSetList)
#         #             print("Select",selectedIdx)
# #                     else:
#                     selectedIdx = 0
#                     
#                     temp_vs_list.append(deepcopy(vsSetList[selectedIdx]))
#                     temp_es_list.append(deepcopy(esSetList[selectedIdx]))
#                     temp = [vs_src_idx,vs_dst_idx]
#                     temp_pair_list.append(deepcopy(temp))
#         
#         hop_cnt_list = []
#         for es_path in temp_es_list:
#             hop_cnt_list.append(len(es_path))
#         
#         if req_bp_no > len(hop_cnt_list):
#             req_bp_no = len(hop_cnt_list)
#         
#         res = heapq.nsmallest(req_bp_no, hop_cnt_list)
#         index_list = []
#         for res_hc in res:
#             for hop_cnt in hop_cnt_list:
#                 if res_hc == hop_cnt:
#                     idx = hop_cnt_list.index(hop_cnt)
#                     if idx not in index_list:
#                         index_list.append(idx)
#                         break
#         
#         for idx in index_list:
#             srcIdx = temp_pair_list[idx][0]
#             dstIdx = temp_pair_list[idx][1]
#             
#             vs_i = graph.vs[srcIdx]
#             
#             vs_i["P2P_BP_RVS_ES_path"][dstIdx] = deepcopy(temp_es_list[idx])
#             vs_i["P2P_BP_RVS_VS_path"][dstIdx] = deepcopy(temp_vs_list[idx])
#             
#             dst_bus = graph.vs[dstIdx]
#             temp_es_list[idx].reverse()
#             temp_vs_list[idx].reverse()
#             
#             dst_bus["P2P_BP_RVS_ES_path"][srcIdx] = deepcopy(temp_es_list[idx])
#             dst_bus["P2P_BP_RVS_VS_path"][srcIdx] = deepcopy(temp_vs_list[idx])

            
                    
# This function finds the Path/Relay/Zone (PRZ) affected by the given link (edgeIdx)
def find_affected_PRZ(graph,edgeIdx):
    vs_index_record = []
    relay_index_record = []
    zone_line_index_record = []
    
    for edge_i in graph.es:
        relaySet = []
        for relay in edge_i["prRelaySet"]:
            relaySet.append(relay)
        
        for relay in edge_i["bpRelaySet"]:
            relaySet.append(relay)
            
        for relay in relaySet:
            vs_i = graph.vs[relay.Located_Node_Idx]
            if edgeIdx in vs_i["PR_RVS_ES_path"]:
                if vs_i.index not in vs_index_record:
                    vs_index_record.append(vs_i.index)
                
                if relay.relayIdx not in relay_index_record:
                    relay_index_record.append(relay.relayIdx)
                
#                 print(edge_i.index,relay.relayIdx,relay.Located_Node_Idx,vs_i["PR_RVS_ES_path"])
                if edge_i.index not in zone_line_index_record:
                    zone_line_index_record.append(edge_i.index)
                    
    print("Affected bus:",len(vs_index_record),' -> ',vs_index_record)
    print("Affected relay:",len(relay_index_record),' -> ',relay_index_record)
    print("Affected zone:",len(zone_line_index_record),' -> ',zone_line_index_record)


def check_pr_bp_ol_no(graph,dstName,x_label,y_label,enablePlot=False):
    ol_ratio_list = []
    for vs_i in graph.vs:
        overlapping = 0
        for linkIdx in vs_i["BP_RVS_ES_path"]:
            if linkIdx in vs_i["PR_RVS_ES_path"]:
                overlapping += 1
        bp_path_len = len(vs_i["BP_RVS_ES_path"])
        if vs_i["name"] == dstName:
            ol_ratio = 0
        elif bp_path_len == 0:
            ol_ratio = 0
        else:
            ol_ratio = overlapping/bp_path_len
        ol_ratio_list.append(ol_ratio)
        print("Bus",vs_i.index+1," -> ",overlapping,"overlap,","backup length",str(bp_path_len)+",","ratio =",ol_ratio)
        
    if enablePlot == True:
        tempList = []
        tempList.append(ol_ratio_list)
        plotLinkUtil_From_List(tempList, ["Overlap Ratio"], "Backup Path Overlap Ratio", \
                               x_label,y_label,True)
        
                    
def allLinkUtilization(graph,maxEdgeIdx=-1,pathType='overall'):
    dataList = []
    
    for edge_i in graph.es:
        if maxEdgeIdx >= 0:
            if edge_i.index >= maxEdgeIdx:
                continue
        
#         print("Link "+str(edge_i.index+1)+" RSV:",edge_i["RSV_Overall"],'   ','Link Capacity',edge_i["capacity"])
        if pathType == 'overall':
            linkUtil = edge_i["RSV_Overall"]/edge_i["capacity"]
        elif pathType == 'backup':
            linkUtil = edge_i["RSV_BP_Overall"]/edge_i["capacity"]
        dataList.append(linkUtil)
    
    return dataList


def plotLinkUtilization(graph,maxEdgeIdx=-1,labelName='Default',enableShow=True):
    dataList = []
    
    for edge_i in graph.es:
        if maxEdgeIdx >= 0:
            if edge_i.index >= maxEdgeIdx:
                continue
        
#         print("Link "+str(edge_i.index+1)+" RSV:",edge_i["RSV_Overall"],'   ','Link Capacity',edge_i["capacity"])
        linkUtil = edge_i["RSV_Overall"]/edge_i["capacity"]
        dataList.append(linkUtil)
#     print(len(dataList),dataList)
    markerSize = 10
    
    plt.plot(dataList,'bs-',label=labelName,markersize=markerSize)
    plt.grid()
    plt.legend(loc="best")
    plt.xlabel('Link ID')
    plt.ylabel('Utilization')
    plt.ylim(0,0.8)
    if enableShow == True:
        plt.show()
    

def plotLinkUtil_From_List(utilList,labelNameList,graphTitle,x_label,y_label,enableX=False,\
                           enableSci=False,ymax=1,ymin=0,titleSuffix=''):
    global marker
    global color
    
    markerLen = len(marker)
    colorLen = len(color)
    lsLen = len(lineStyle)
    markerSize = 10
    x_axis = []
    
    if enableX == True: # we set X-axis to start from 1
        for i in range(len(utilList[0])):
            x_axis.append(i+1)
    
    for idx in range(len(utilList)):
        ls = marker[idx % markerLen] + color[idx % colorLen] + lineStyle[idx % lsLen]
        if enableX == True:
            plt.plot(x_axis,utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
        else:
            plt.plot(utilList[idx],ls,label=labelNameList[idx],markersize=markerSize)
    
    if titleSuffix != '':
        myGraphTitle = titleSuffix + ': '+ graphTitle
    else:
        myGraphTitle = graphTitle
    
    plt.grid()
    plt.legend(loc="best")
    plt.title(myGraphTitle)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.ylim(ymin,ymax)
    if enableSci == True:
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.show()
    
            
def isRepeatedPath(listOfPath,newPath):
    repeated = 0
    number_of_same = 0
    for idx in range(len(listOfPath)):
        if len(listOfPath[idx]) != len(newPath):
            if idx == len(listOfPath) - 1:
                repeated = 0
            else:
                continue
        else:
            for i in range(len(listOfPath[idx])):
                if listOfPath[idx][i] != newPath[i]:
                    break
                else:
                    number_of_same += 1
            
            if number_of_same == len(listOfPath[idx]):
                repeated = 1
                break
                
    
    return repeated
    
    
def showBP_OFE_Relay_RSV(graph):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            for failLinkIdx in relay.per_hop_dr_BP_OFE:
                print("relay",relay.relayIdx,"failed link:",failLinkIdx,' ->   ',end='')
                for linkDirt in relay.per_hop_dr_BP_OFE[failLinkIdx]:
                    result = relay.per_hop_dr_BP_OFE[failLinkIdx][linkDirt]
                    print(linkDirt,' -> ',result,'  ',end='')
                print()
        None


# For a single link failure, a fixed set of nodes and relays are affected. In this function, we
# calculate the delay requirement for each of the relay within the set.
def do_Perhop_DelayReq_SingleFailure_BP(graph,affectedNodeList,failLinkIdx,myType='equal',dstSft=0,powft=1):
    
    # Each time before we do the "load scheme" we use the "primary RSV" because
    # we only have a single failure
    copyPR_RSV_to_Temp(graph)
    
    for vsIdx in affectedNodeList:    # scan the affected node list
        vs_i = graph.vs[vsIdx] # choose one node
        if vs_i["relaySet"] == []:
            continue
        
        epathList = vs_i["BP_RVS_ES_Each"][failLinkIdx]
        vpathList = vs_i["BP_RVS_VS_Each"][failLinkIdx]
        relayBPHopNo = len(vs_i["BP_RVS_ES_Each"][failLinkIdx])
                
        for relay in vs_i["relaySet"]:

            # Only used in "combination"
            rsvSum = 0
            dstSum = 0
            overallSum2 = 0
            rsvSum2 = 0
            dstSum2 = 0
            rsvWeightList = []
            dstWeightList = []
            overallWeightList = []
            
#             for linkIdx in epathList:
            if failLinkIdx not in relay.per_hop_dr_BP_OFE:
                    relay.per_hop_dr_BP_OFE[failLinkIdx] = {}
            
            relayDR_Table = relay.per_hop_dr_BP_OFE[failLinkIdx]
            if myType == 'equal':   
                weight_function_equal(graph, relay, epathList, vpathList, relayDR_Table)
#                 for linkIdx in epathList:
#                     idx = epathList.index(linkIdx)
# #                     print(relay.relayIdx,idx,epathList,vpathList)
#                     
#                     src = vpathList[idx+1]
#                     dst = vpathList[idx]
#                     
#                     name1 = str(src)+'_'+str(dst)
#                     name2 = str(dst)+'_'+str(src)
#                     
# #                     if failLinkIdx not in relay.per_hop_dr_BP_OFE:
# #                         relay.per_hop_dr_BP_OFE[failLinkIdx] = {}
#                     if linkIdx not in relay.per_hop_dr_BP_OFE[failLinkIdx]:
#                         relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx] = {}
#                     
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name1] = (relay.delay_rtt/relayBPHopNo)/2
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name2] = (relay.delay_rtt/relayBPHopNo)/2
#                     relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"] = (relay.delay_rtt/relayBPHopNo)/2
                    
            elif myType == 'distance':
                weight_function_distance(relay, epathList, vpathList, relayDR_Table, dstSft)
#                 for esIdx in epathList:
#                     # the element idx can be seen as the hop count to MA across the link
#                     listElementIdx = (epathList.index(esIdx)+1) + dstSft
#                     tempSum +=  listElementIdx # total hop count
#                 
#                 for linkIdx in epathList:
#                     idx = epathList.index(linkIdx)
#                     hop_cnt = idx + 1 + dstSft
# 
#                     current_weight = tempSum/hop_cnt
#                     tempWeightList.append(current_weight)
#                     tempSum2 += (current_weight)
# #                         print(hop_cnt,' ',end='')
# #                     print()
#                     
# #                     print(tempWeightList,tempSum2)
#                 # Now calculate the delay division at different hops
#                 for linkIdx in epathList:
#                     idx = epathList.index(linkIdx)
#                     
#                     src = vpathList[idx+1]
#                     dst = vpathList[idx]
#                     
#                     name1 = str(src)+'_'+str(dst)
#                     name2 = str(dst)+'_'+str(src)
#                     
# #                     if failLinkIdx not in relay.per_hop_dr_BP_OFE:
# #                         relay.per_hop_dr_BP_OFE[failLinkIdx] = {}
#                     if linkIdx not in relay.per_hop_dr_BP_OFE[failLinkIdx]:
#                         relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx] = {}
#                     
#                     current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name1] = current_hop_DR
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name2] = current_hop_DR
#                     relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"] = current_hop_DR
# #                         print(linkIdx+1,current_hop_DR)

            elif myType == 'load':
                weight_function_load(graph, relay, epathList, vpathList, relayDR_Table, powft)
#                 for linkIdx in epathList: # compute the total free_weight
#                     idx = epathList.index(linkIdx)
#                     
#                     src = vpathList[idx+1]
#                     dst = vpathList[idx]
#                     
#                     name1 = str(src)+'_'+str(dst)
#                     
#                     if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
#                         raise NameError('1. Capacity not enough!')
#                     free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
#                     tempSum += pow(free_weight,powft) # free_weight can be powered
#                     
#                 # Next assign link weight based on free_weight and compute total link weight
#                 for linkIdx in epathList:
#                     idx = epathList.index(linkIdx)
#                     
#                     src = vpathList[idx+1]
#                     dst = vpathList[idx]
#                     
#                     name1 = str(src)+'_'+str(dst)
#                     
#                     free_weight = 1-(graph.es[linkIdx]["RSV_temp"][name1])/(graph.es[linkIdx]["capacity"])
#                     current_link_weight = tempSum/pow(free_weight,powft) # free_weight can be powered
#                     tempWeightList.append(current_link_weight)
#                     tempSum2 += current_link_weight # total sum weight
#                     
#                 # compute delay division on each hop/link and update the "temporary RSV"
#                 for linkIdx in epathList:
#                     idx = epathList.index(linkIdx)
#                     
#                     src = vpathList[idx+1]
#                     dst = vpathList[idx]
#                     
#                     name1 = str(src)+'_'+str(dst)
#                     name2 = str(dst)+'_'+str(src)
#                     
# #                     if failLinkIdx not in relay.per_hop_dr_BP_OFE:
# #                         relay.per_hop_dr_BP_OFE[failLinkIdx] = {}
#                     if linkIdx not in relay.per_hop_dr_BP_OFE[failLinkIdx]:
#                         relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx] = {}
#                         
#                     current_hop_DR = (relay.delay_rtt/2)*tempWeightList[idx]/tempSum2
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name1] = current_hop_DR
# #                     relay.per_hop_dr_BP_OFE[failLinkIdx][name2] = current_hop_DR
#                     relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"] = current_hop_DR
#                     
#                     # Update the link utilization
#                     graph.es[linkIdx]["RSV_temp"][name1] += 80*8/relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"]
#                     graph.es[linkIdx]["RSV_temp"][name2] += 80*8/relay.per_hop_dr_BP_OFE[failLinkIdx][linkIdx]["nonDirection"]
#                     
#                     if graph.es[linkIdx]["RSV_temp"][name1] > graph.es[linkIdx]["capacity"]:
#                         raise NameError('2. Capacity not enough!')