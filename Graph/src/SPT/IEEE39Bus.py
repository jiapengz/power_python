'''
Created on Dec 1, 2014

@author: Zhang Jiapeng
'''
from SPT import calSPT
import igraph as ig
from copy import deepcopy
from SPT.calSPT import *


def show_all_remote_lines_for_relay(graph):
    for vs_i in graph.vs:
        for relay in vs_i["relaySet"]:
            print("Relay",relay.relayIdx,"remote lines:",relay.ZONE3_EdgeIdx)

    
def staticRSV(graph,maxEdgeNum,pktSize,myType='normal',rsvType='normal',dstSft=0):
    staticRSV_PR(graph,pktSize)
    copyPR_RSV_to_Temp(graph)
    do_Perhop_DelayReq_Simple_BP(graph, maxEdgeNum, myType,dstSft)
    staticRSV_BP(graph, pktSize)
    totalRSV(graph,rsvType)
    
    
def smartRSV(graph,maxEdgeNum,pktSize,myType='normal',rsvType='smart',dstSft=0):
    smartRSV_PR(graph,pktSize,maxEdgeNum)
    copyPR_RSV_to_Temp(graph)
    do_Perhop_DelayReq_Simple_BP(graph, maxEdgeNum, myType,dstSft)
    smartRSV_BP(graph, pktSize, maxEdgeNum)
    totalRSV(graph,rsvType)
    showRSV_Total(graph)
    
    
def staticRSV_with_NonOL_BP_Path(rtnList,graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                                 prList=[],bpList=[]):
    #Delay division on each hop for primary path
    do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    
    staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    pr_RSV = allLinkUtilization(graph, maxEdgeIdx) #Store the data for primary links' utilization
    prList.append(pr_RSV)
#     print("After Static:")
#     for edge_i in graph.es:
#         print("Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)
    
    copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    showRSV_PR(graph)
    print()
#     showRSV_Overall(graph)
    print()
    #Find backup path using different methods
    if bpType == 'dist':
        findBpPath_PR_Path_NonOL_Dist(graph,"16",maxEdgeIdx,delayType,dstSft,powft)
    elif bpType == 'usedNo':
        findBpPath_PR_Path_NonOL_LinkUsedNo(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    elif bpType == 'linkLoad':
        findBpPath_PR_Path_NonOL_LinkLoad(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    elif bpType == 'findAll':
        find_ALLBpPath_PR_Path_NonOL_LinkLoad(graph, "16", maxEdgeIdx, delayType, dstSft, powft)

#     print("After BP:")
#     for edge_i in graph.es:
#         print("Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)
    averageHopCnt(graph,prt=True)
    do_Perhop_DelayReq_Simple_BP(graph, maxEdgeIdx, delayType, dstSft, powft) #Delay division on each hop for backup path
    staticRSV_BP(graph, pktSize)
    bp_RSV = allLinkUtilization(graph, maxEdgeIdx, 'backup')
    bpList.append(bp_RSV)
#     showRSV_BP_Overall(graph)
    showRSV_Overall(graph)
    print_rsv_overall_in_list(graph,True)
    
#     plotLinkUtilization(graph,maxEdgeIdx,delayType)
    utilList = allLinkUtilization(graph, maxEdgeIdx)
    rtnList.append(utilList)
    print(delayType,utilList)


# A test function to calculate the probability of false tripping of line due to link error 
def line_pf_due_to_link_error(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1):
    do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    staticRSV_PR(graph,pktSize)
    copyPR_RSV_to_Temp(graph)
    showRSV_PR(graph)
    print()
    
    if bpType == 'dist':
        findBpPath_PR_Path_NonOL_Dist(graph,"16",maxEdgeIdx,delayType,dstSft,powft)
    elif bpType == 'usedNo':
        findBpPath_PR_Path_NonOL_LinkUsedNo(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    elif bpType == 'linkLoad':
        findBpPath_PR_Path_NonOL_LinkLoad(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    elif bpType == 'findAll':
        find_ALLBpPath_PR_Path_NonOL_LinkLoad(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    elif bpType == 'find_all_ol':
        find_ALLBpPath_PR_Path_LinkLoad(graph, "16", maxEdgeIdx, delayType, dstSft, powft)
    
    check_pr_bp_ol_no(myGraph,"16",'Bus ID','Overlapping Percentage',True)
    result = calSPT.compute_line_pf_link_error(graph)
    
    #Add a comparison line to the data
    temp = []
    for i in range(len(result)):
        temp.append(0.0001)
    calSPT.plotLinkUtil_From_List([result,temp], ['Probability','Threshold'], 'False Tripping Probability of Line', \
                                  'Power Line ID', 'P(f)', True,True,0.0002,0)
    
    
# Start the initialization of the graph
myGraph = ig.Graph()
myGraph.add_vertices(39)

for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"
#print(myGraph.vs["name"])

myGraph.add_edge("1", "2")
myGraph.add_edge("1", "39")
myGraph.add_edge("2", "3")
myGraph.add_edge("2", "25")
myGraph.add_edge("3", "4")
myGraph.add_edge("3", "18")
myGraph.add_edge("4", "5")
myGraph.add_edge("4", "14")
myGraph.add_edge("5", "6")
myGraph.add_edge("5", "8")
myGraph.add_edge("6", "7")
myGraph.add_edge("6", "11")
myGraph.add_edge("7", "8")
myGraph.add_edge("8", "9")
myGraph.add_edge("9", "39")
myGraph.add_edge("10", "11")
myGraph.add_edge("10", "13")
myGraph.add_edge("13", "14")
myGraph.add_edge("14", "15")
myGraph.add_edge("15", "16")
myGraph.add_edge("16", "17")
myGraph.add_edge("16", "19")
myGraph.add_edge("16", "21")
myGraph.add_edge("16", "24")
myGraph.add_edge("17", "18")
myGraph.add_edge("17", "27")
myGraph.add_edge("21", "22")
myGraph.add_edge("22", "23")
myGraph.add_edge("23", "24")
myGraph.add_edge("25", "26")
myGraph.add_edge("26", "27")
myGraph.add_edge("26", "28")
myGraph.add_edge("26", "29")
myGraph.add_edge("28", "29")

#non-transmission line
myGraph.add_edge("6", "31")
myGraph.add_edge("10", "32")
myGraph.add_edge("11", "12")
myGraph.add_edge("12", "13")
myGraph.add_edge("19", "20")
myGraph.add_edge("19", "33")
myGraph.add_edge("20", "34")
myGraph.add_edge("23", "36")
myGraph.add_edge("29", "38")
myGraph.add_edge("30", "2")
myGraph.add_edge("35", "22")
myGraph.add_edge("37", "25")

#Added backup link
myGraph.add_edge("19","21")
#calSPT.plotGraph(myGraph)

# Add the attribute to store the list of nodes at the same height
myGraph["nodeLayer"] = {}

myGraph.es["weight"] = 1
SPDistance = myGraph.shortest_paths_dijkstra(weights=myGraph.es["weight"])
setRelayNo(myGraph,34)

optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance) #This is the best MA location

idealPos = 0
print("Optimal locations:")
for index in optimalNodeIndex:
    idealPos = index
    print("Bus",myGraph.vs[index]["name"])
    
    
fileName = "IEEE39bus-bp.png"
myLayout = [(3,18),(5,14),(8,21),(8,28),(7,33),(12,36),(9,41),(6,46),(3,52),(18,46),(15,41),(18,36),(20,41),(20,28),\
            (21,24),(24,20),(15,20),(12,15),(27,39),(26,46),(28,30),(32,30),(36,33),(32,14),(8,9),(20,9),(21,15),\
            (27,6),(35,9),(3,8),(12,41),(20,52),(29,46),(28,52),(32,23),(36,39),(8,5),(35,16),(2,27)]
#calSPT.plotSPT(myGraph, optimalNodeIndex)
# calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex,myLayout,fileName)
# calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex,fileName)
# calSPT.showBackupCase(myGraph, "25", "16", 29, myLayout,fileName)
print()
print()
# find_all_paths(myGraph,15,0,exemptPath=[],delay_req=0,debugPrint=True)
print()
print()

calSPT.findPrimaryPath_All_Nodes(myGraph,optimalNodeIndex,'lw_equal',plotEN=False)
# findBackupPath_simple(myGraph,myGraph.vs[idealPos]["name"],["12"])
# num_of_relay_on_link(myGraph)
# show_num_of_relay_on_link(myGraph)

findBackupRelayGraph(myGraph,34)
find_Zone3_relay_Same_bus(myGraph)
# find_affected_PRZ(myGraph, 19)
calDelayReq_PR_Graph(myGraph,34)
update_min_relay_req_at_a_bus(myGraph)

# show_all_remote_lines_for_relay(myGraph)
# for vs_i in myGraph.vs:
#     print("Bus",vs_i.index+1,' -> ',vs_i["min_relay_req"])
# for vs_i in myGraph.vs:
#     for relay in vs_i["relaySet"]:
#         print("Relay",relay.relayIdx,"RTT ->",relay.delay_rtt)

delay_type_choice = ['equal','distance','load']
delayType = delay_type_choice[0]
# line_pf_due_to_link_error(myGraph, 34, 80, 'find_all_ol', 'equal', 0, 1)
 
# do_Perhop_DelayReq_Simple_PR(myGraph, 34, delayType, 0, 1)
# averageHopCnt(myGraph,True)
# showRelaySet_v2(myGraph)
#   
# staticRSV_PR(myGraph,80)
# smartRSV_PR(myGraph,80,34)
# copyPR_RSV_to_Temp(myGraph)
# showRSV_PR(myGraph)
print()
# showRSV_Overall(myGraph)
# calDelayReq_BP_Graph(myGraph, 34)
# do_Perhop_DelayReq_Simple_BP(myGraph, 34, 'load', 0, 1)
    
# staticRSV_BP(myGraph, 80)
# smartRSV_BP(myGraph, 80, 34)
# showRSV_BP(myGraph)
# totalRSV(myGraph,'normal')
print()
# showRSV_Total(myGraph)
# smartRSV(myGraph, 34, 80, 'combination', 'smart',4)

# Test the "each link" backup case
# findBpPath_OneForEach_PR_Link_Dist(myGraph, "16", 34, delayType, 0, 1)
# # showBP_OFE_Relay_RSV(myGraph)
# staticRSV_OFE_SingleFail_BP(myGraph, 80, 34, failLinkIdx=-1)
# # smartRSV_OFE_SingleFail_BP(myGraph, 80, 34, failLinkIdx=-1)
# showRSV_Overall(myGraph)


# Test the completely non-overlapping path
# findBpPath_PR_Path_NonOL_Dist(myGraph,"16",34,delayType,0,1)
# findBpPath_PR_Path_NonOL_LinkUsedNo(myGraph,"16",38,delayType,0,1)
# find_ALLBpPath_PR_Path_NonOL_LinkLoad(myGraph, "16", 34, delayType, 0, 1)
# find_ALLBpPath_PR_Path_LinkLoad(myGraph, "16", 34, delayType, 0, 1)
# check_pr_bp_ol_no(myGraph,"16",True)
# do_Perhop_DelayReq_Simple_BP(myGraph, 34, delayType, 0, 1)
# staticRSV_BP(myGraph, 80)
# showRSV_Overall(myGraph)
 
# plotLinkUtilization(myGraph,34,delayType)
# staticRSV_with_NonOL_BP_Path(myGraph, 34, 80, delayType, 0, 1)


# Plot the link utilization
pr_dataList = [] # for recording primary RSV
bp_dataList = [] # for recording backup RSV
dataList = [] # for recording
for dt in delay_type_choice:
    print(dt)
#     if dt == 'distance':
#         continue
    copyGraph = deepcopy(myGraph)
    staticRSV_with_NonOL_BP_Path(dataList,copyGraph, 34, 80, 'dist', dt, 0, 1, pr_dataList, bp_dataList)
#     print("Print the temp RSV")
#     for edge_i in copyGraph.es:
#             print("Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000)

plotTitle = "Link Weight = L/(Available Capacity)"
plotLinkUtil_From_List(pr_dataList,delay_type_choice,plotTitle,True,'Primary Path')
# plotLinkUtil_From_List(bp_dataList,delay_type_choice,plotTitle,True,'Backup Path')
# plotLinkUtil_From_List(dataList,delay_type_choice,plotTitle,True,'Overall')

# for edge_i in myGraph.es:
#     print("Link",edge_i.index+1,' -> ',edge_i["RSV_temp_ND"]/1000,edge_i["RSV_Overall"])
# print(myGraph["total_relay_no"])