'''
Created on Sep 1, 2016

@author: Zhang Jiapeng
'''

import SPT.IEEE13Bus_data as bd_13
from SPT import calSPT
from SPT import calSPT2
from copy import deepcopy
import math
import random

# random.seed(123456)

class CascadeMark:    
    def __init__(self):
        self.total_value = 0
        self.triggered = False


def graph_initialization(graph, max_edge_id=0):
    graph.es["weight"] = 1
    calSPT.setRelayNo(graph,max_edge_id)
#     sibd.set_system_probability(graph)
#     IS_39.set_link_reliability(graph)
#     IS_39.set_link_reliability_random(graph)
           
    calSPT.findBackupRelayGraph(graph,max_edge_id)
    calSPT.find_Zone3_relay_Same_bus(graph)
    calSPT.update_min_relay_req_at_a_bus(graph)
    
#     set_relay_defense_strength_all(graph, 0.5)
    for i in range(10):
#         temp_num = random.uniform(0,1)
#         if temp_num < 0.1:
        set_relay_defense_strength(graph, i+1, 0.5)
#     testFunction(graph)
    print()


def debug_print(enablePrint, *args):
    if enablePrint == True:
        for i in range(len(args)):
            if i == len(args)-1:
                print(args[i])
            else:
                print(args[i],"",end='')


def set_relay_defense_strength_all(myGraph, defense_strength=0):
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            relay_i.defense_strength = defense_strength


def set_relay_defense_strength(myGraph, relay_idx, defense_strength=0):
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.relayIdx == relay_idx:
                relay_i.defense_strength = defense_strength


# randomly select relays and set the defense strength
def set_relay_defense_strength_randomly(myGraph, ratio=0, defense_strength=0):
    relay_idx_list = []
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            relay_idx_list.append(relay_i.relayIdx)
    
    assert len(relay_idx_list) == (2 * len(myGraph.es))
    
    compromise_relay_num = int(ratio * len(relay_idx_list))
    for i in range(compromise_relay_num):
        selected_relay_idx = random.choice(relay_idx_list)
        
        set_relay_defense_strength(myGraph, selected_relay_idx, defense_strength)
        
        relay_idx_list.remove(selected_relay_idx)
        
    print(str(i)+" relays have defense")


def set_relay_defense_strength_cascading(myGraph, defense_strength=0):
    for initial_list in myGraph["cascading_initial"]:
        for line_idx in initial_list:
            target_line = myGraph.es[line_idx]
            for relay_i in target_line["prRelaySet"]:
                relay_i.defense_strength = defense_strength
                #print("relay",relay_i.relayIdx,"at line",line_idx,"set defense to",relay_i.defense_strength)


def set_node_defense_strength_all(myGraph,defense_strength=0):
    for vs_i in myGraph.vs:
        vs_i["df"] = defense_strength


def relay_info(myGraph):
    for es_i in myGraph.es:
        print("Primary relay for line",es_i["name"],"------>",end="")
        for relay_i in es_i["prRelaySet"]:
            print(relay_i.relayIdx,"  ",end="")
        print("       ",end="")
        print("Backup relay for line",es_i["name"],"------>",end="")
        for relay_i in es_i["bpRelaySet"]:
            print(relay_i.relayIdx,"  ",end="")
        print()


def total_attack_trip_info(myGraph):
    for es_i in myGraph.es:
        if es_i["tripped"] == True:
            print("Line",es_i["name"],"is tripped")


def total_tripped_line(myGraph):
    total_num = 0
    for es_i in myGraph.es:
        if es_i["tripped"] == True:
            total_num += 1
    
    return total_num


def compromise_relay(myGraph,target_relay_list=[]):
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.relayIdx in target_relay_list:
                relay_i.compromised = True


def compromise_single_relay(myGraph,target_relay_idx):
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.relayIdx == target_relay_idx:
                relay_i.compromised = True
                
                return


# the attack has a probability to succeed
def attack_relay(myGraph, total_rsc, relay_idx, retry_num = 1, en_prt = False):
    """Used for attacking a relay in the simulation
    The backup relay may be tripped by its compromised primary relay. However, it is only "tripped",
    not compromised. Even if the relay is "tripped", the attacker can still compromise this relay.
    As a result, the attacker needs to use certain resources.
    
    Args:
        myGraph: the graph class variable
        total_rsc (int): the total resource available for attack
        relay_idx (int): the index of the relay to be attacked
        retry_num (int): for K-attack, the number of attacks, at least 1
        en_prt (bool): whether to print the debug information
    """
    rsc = total_rsc
    relay_found = False
    atk_success = False
    
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.relayIdx == relay_idx:
                relay_found = True
                target_relay = relay_i
                break
        
        if relay_found == True:
            break
    
    if relay_found == False:
        raise Exception("Relay index not found!")
    
    if target_relay.compromised == True: # relay already compromised
        atk_success = True
        assert rsc == total_rsc
        return rsc, atk_success
    
    debug_print(en_prt, "Target relay index:",target_relay.relayIdx)
    # start attacking the target relay
    if rsc < target_relay.attack_cost: # if the resource is not enough
        rsc = 0 # all resources are used
        return rsc, atk_success
    else:
        target_relay.attacked = True
        while (retry_num > 0) and (rsc >= target_relay.attack_cost):
            debug_print(en_prt, "Remaining retry:",retry_num)
            retry_num -= 1
            temp_num = random.uniform(0,1)
            rsc -= target_relay.attack_cost # certain resources are used
            
            if temp_num >= target_relay.defense_strength: # the attack is successful
                target_relay.compromised = True
                atk_success = True
                debug_print(en_prt, "Attack succeeds")
                break
        
        return rsc, atk_success


def attack_router(myGraph, total_rsc, vs_idx, retry_num=1, en_prt=True):
    rsc = total_rsc
    atk_success = False
    
    if vs_idx >= len(myGraph.vs) or vs_idx < 0:
        raise Exception("Router index not found!")
    
    target_router = myGraph.vs[vs_idx]
    if target_router["compromised"] == True:
        atk_success = True
        return rsc, atk_success
    
    if rsc < target_router["cost"]: # if the resource is not enough
        rsc = 0 # all resources are used
        atk_success = False
        return rsc, atk_success
    else:
        while (retry_num > 0) and (rsc >= target_router["cost"]):
            debug_print(en_prt, "Remaining retry:",retry_num)
            retry_num -= 1
            temp_num = random.uniform(0,1)
            rsc -= target_router["cost"] # certain resources are used
            
            if temp_num >= target_router["df"]: # the attack is successful
                target_router["compromised"] = True
                atk_success = True
                debug_print(en_prt, "Attack succeeds")
                break
        
        return rsc, atk_success


# issue trip request to a relay to trip the local line
def trip_line_with_relay(myGraph, relay_idx):
    relay_found = False
    target_relay = None
    trip_line_idx = -1
    
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.relayIdx == relay_idx:
                relay_found = True
                target_relay = relay_i
                break
        
        if relay_found == True:
            line_idx = target_relay.Located_EdgeIdx
            myGraph.es[line_idx]["tripped"] = True
            trip_line_idx = line_idx
            break
    
    return trip_line_idx


# determine if the initial lines of a cascading are tripped
def cascading_initial_line_check(myGraph, line_idx):
    index_set = []
    
    for i in range(len(myGraph["cascading_initial"])):
        if line_idx in myGraph["cascading_initial"][i]:
            myGraph["cascading_result"][i].total_value += 1
            
            # one initial line set is fulfilled
            if myGraph["cascading_result"][i].total_value == len(myGraph["cascading_initial"][i]):
                index_set.append(i)
    
    return index_set


# trigger the specified cascading sequence
def trigger_cascading(myGraph, seq_list_idx, prt_en):
    debug_print(prt_en,"Trigger cascading sequence", myGraph["cascading_sequence"][seq_list_idx])
    for line_idx in myGraph["cascading_sequence"][seq_list_idx]:
        if myGraph.es[line_idx]["tripped"] != True:
            myGraph.es[line_idx]["tripped"] = True


def trip_cascading_sequence_from_list(myGraph, cascade_check_list):
    #print("Check cascading sequence!")
    cascading_triggered = False
    if cascade_check_list != []: # some cascading conditions are fulfilled
        for idx in cascade_check_list:
            if myGraph["cascading_result"][idx].triggered != True:
                myGraph["cascading_result"][idx].triggered = True
                trigger_cascading(myGraph, idx, False)
                cascading_triggered = True
    
    return cascading_triggered


def compromise_router(myGraph,target_router_list):
    for router_id in target_router_list:
        myGraph.vs[router_id]["compromised"] = True
        # router/bus id starts from 0 in the program


# the effect of compromised relays and routers on the power system
def effect_of_compromised_device(myGraph, know_topology=True):
    # first compute the lines that can be directly tripped by a relay
    for es_i in myGraph.es:
        for relay_i in es_i["prRelaySet"]:
            if relay_i.compromised == True and es_i["tripped"] == False:
                es_i["tripped"] = True
                continue
    
    # second compute the lines that can be tripped with existing scheme
    # e.g., only one additional relay required
    # if the topology is known to the attacker, he can trip relays in an area
    if know_topology == True:
        for es_i in myGraph.es:
            for relay_i in es_i["prRelaySet"]:
                if relay_i.compromised == True:
                    compromised_num = 0
                    for r_j in es_i["prRelaySet"]:
                        if r_j != relay_i and r_j.compromised == True:
                            compromised_num += 1
                    for r_j in es_i["bpRelaySet"]:
                        if r_j.compromised == True:
                            compromised_num += 1
                    if compromised_num >= 1:
                        for r_j in es_i["bpRelaySet"]:
                            if r_j.Remote_Node == relay_i.Located_Node_Idx:
                                es_idx = r_j.Located_EdgeIdx
                                if myGraph.es[es_idx]["tripped"] != True:
                                    myGraph.es[es_idx]["tripped"] = True


# identify the protection areas and the potential total number of lines to trip
def identify_protection_area_total_line(myGraph):
    for es_i in myGraph.es:
        if es_i["prRelaySet"] != []:
            es_i["area_line"].append(es_i.index)
            
            for relay_i in es_i["bpRelaySet"]:
                es_i["area_line"].append(relay_i.Located_EdgeIdx)


# update the number of "effective line" in the protection area
def protection_area_line_update(myGraph):
    for es_i in myGraph.es:
        if es_i["prRelaySet"] != []:
            if es_i.index not in es_i["area_line"]:
                if es_i["tripped"] != True:
                    es_i["area_line"].append(es_i.index)
            else:
                if es_i["tripped"] == True:
                    es_i["area_line"].remove(es_i.index)
                    
            for relay_i in es_i["bpRelaySet"]:
                local_line_idx = relay_i.Located_EdgeIdx
                local_es = myGraph.es[local_line_idx]
                
                if local_line_idx not in es_i["area_line"]:    
                    if local_es["tripped"] != True:
                        es_i["area_line"].append(local_es.index)
                else:
                    if local_es["tripped"] == True:
                        es_i["area_line"].remove(local_es.index)


# use the agent-protection mechanism to attack multiple relays in an area
# the local lines are already tripped, and will not be considered here
def attack_agent_protection_pure_relay(myGraph, area_idx, enable_cas=False, enable_test=False,\
                            testlist={}, testlist2={}, nb_trip_two_pr={}, nb_trip_one_pr={}, prt_en=False):
    extra_trip = 0
    primary_line = myGraph.es[area_idx]
    relay_0 = primary_line["prRelaySet"][0]
    relay_1 = primary_line["prRelaySet"][1]
    
    if enable_test == True:
        neighbor_trip_count = 0
        if relay_0.compromised == True and relay_1.compromised == True: # two primary relays compromised
            for relay_bk_line in primary_line["bpRelaySet"]:
                # if the neighbor line is not tripped, then it can be counted in the neighbor area trip
                # if a neighbor line is already tripped, it is either tripped by its local relay, or by area trip
                # from another line
                neighbor_line_idx = relay_bk_line.Located_EdgeIdx
                if myGraph.es[neighbor_line_idx]["tripped"] != True:
                    neighbor_trip_count += 1
            
            if area_idx not in nb_trip_two_pr:
                nb_trip_two_pr[area_idx] = neighbor_trip_count
                    
        elif relay_0.compromised == True and relay_1.compromised == False: # only relay_0 is compromised
            area_trip_allowed = False
            for relay_bk_line in primary_line["bpRelaySet"]:
                if relay_bk_line.compromised == True:
                    area_trip_allowed = True
                    break
            if area_trip_allowed == True:
                for relay_bk in relay_0.bp_relay:
                    bk_line_idx = relay_bk.Located_EdgeIdx
                    
                    # a primary relay can only trip the neighbor backup relays at the same side
                    if myGraph.es[bk_line_idx]["tripped"] != True:
                        neighbor_trip_count += 1
            
            if area_idx not in nb_trip_one_pr:
                nb_trip_one_pr[area_idx] = neighbor_trip_count
                    
        elif relay_0.compromised == False and relay_1.compromised == True: # only relay_1 is compromised
            area_trip_allowed = False
            for relay_bk_line in primary_line["bpRelaySet"]:
                if relay_bk_line.compromised == True:
                    area_trip_allowed = True
                    break
            if area_trip_allowed == True:
                for relay_bk in relay_1.bp_relay:
                    bk_line_idx = relay_bk.Located_EdgeIdx
                    
                    # a primary relay can only trip the neighbor backup relays at the same side
                    if myGraph.es[bk_line_idx]["tripped"] != True:
                        neighbor_trip_count += 1
            
            if area_idx not in nb_trip_one_pr:
                nb_trip_one_pr[area_idx] = neighbor_trip_count
    
    
    for relay_i in primary_line["prRelaySet"]:
        if relay_i.compromised == True:
            compromised_num = 0
            
            for r_j in primary_line["prRelaySet"]:
                if r_j != relay_i and r_j.compromised == True:
                    compromised_num += 1
            
            for r_j in primary_line["bpRelaySet"]:
                if r_j.compromised == True:
                    compromised_num += 1
            
            if compromised_num >= 1:
                for r_j in relay_i.bp_relay:
                    es_idx = r_j.Located_EdgeIdx
                    
                    if enable_test == True:
                        if r_j.compromised == True:
                            if es_idx not in testlist: # a relay tripped but not compromised 
                                testlist[es_idx] = []
                            if r_j.relayIdx not in testlist[es_idx]:
                                testlist[es_idx].append(r_j.relayIdx)
                    
                    if myGraph.es[es_idx]["tripped"] != True:
                        myGraph.es[es_idx]["tripped"] = True
                        extra_trip += 1
                        
                        if enable_test == True:
                            if relay_i.relayIdx not in testlist2:
                                testlist2[relay_i.relayIdx] = []
                            if es_idx not in testlist2[relay_i.relayIdx]:
                                testlist2[relay_i.relayIdx].append(es_idx)
                        
                        # check potential cascading
                        if enable_cas == True:
                            cascade_check_list = cascading_initial_line_check(myGraph, es_idx)
                            trip_cascading_sequence_from_list(myGraph, cascade_check_list)
    
    debug_print(prt_en, "Extra trip:", extra_trip)
    return extra_trip


# use the agent-protection mechanism with majority rule to attack multiple relays in an area
# the local lines are already tripped, and will not be considered here
def attack_agent_protection_pure_relay_majority(myGraph, area_idx, enable_cas=False, enable_test=False,\
                                    testlist={}, testlist2={}, nb_trip_two_pr={}, nb_trip_one_pr={},\
                                    cas_special=False, prt_en=False):
    extra_trip = 0
    primary_line = myGraph.es[area_idx]
    
    if primary_line["prRelaySet"] == []:
        return extra_trip
    
    relay_0 = primary_line["prRelaySet"][0]
    relay_1 = primary_line["prRelaySet"][1]
    
    # total number of corresponding relays in an area (primary + backup)
    total_correspond_relay = count_total_correspond_relay(myGraph, area_idx)
    
    # count total number of compromised relays
    total_compromised = count_area_compromised_relay(myGraph, area_idx)
    
    for relay_i in primary_line["prRelaySet"]:
        if relay_i.compromised == True: # primary relays can trip backup relays
            if total_compromised < 2:
                continue
            
            if total_compromised >= math.ceil(total_correspond_relay/2+0.5): # majority of the relays are compromised
                for bp_relay_k in relay_i.bp_relay: # backup relays at the same side can be tripped
                    es_idx = bp_relay_k.Located_EdgeIdx
                    
                    if enable_test == True:
                        if bp_relay_k.compromised == True:
                            if es_idx not in testlist: # a relay tripped but not compromised 
                                testlist[es_idx] = []
                            if bp_relay_k.relayIdx not in testlist[es_idx]:
                                testlist[es_idx].append(bp_relay_k.relayIdx)
                    
                    if cas_special == False:
                        if myGraph.es[es_idx]["tripped"] != True:
                            myGraph.es[es_idx]["tripped"] = True
                            extra_trip += 1
                            
                            if enable_test == True:
                                if relay_i.relayIdx not in testlist2:
                                    testlist2[relay_i.relayIdx] = []
                                if es_idx not in testlist2[relay_i.relayIdx]:
                                    testlist2[relay_i.relayIdx].append(es_idx)
                            
                            if enable_cas == True:
                                cascade_check_list = cascading_initial_line_check(myGraph, es_idx)
                                trip_cascading_sequence_from_list(myGraph, cascade_check_list)
                    else:
                        # specially for area-based cascading attack, only trip the required initial line
                        if myGraph.es[es_idx]["tripped"] != True:
                            if es_idx in myGraph["cascading_initial"][0]:
                                myGraph.es[es_idx]["tripped"] = True
                                extra_trip += 1
                                #print("Line",es_idx,"not tripped. Now trip.")
                            
                                if enable_cas == True:
                                    cascade_check_list = cascading_initial_line_check(myGraph, es_idx)
                                    trip_cascading_sequence_from_list(myGraph, cascade_check_list)
    
    return extra_trip



# use the agent-protection reputation mechanism with majority or non-majority
# rule to attack multiple relays in an area
# the local lines are already tripped, and will not be considered here
# assume the two routers near the primary relays will be attacked
def attack_agent_protection_area_reputation(myGraph, area_idx, enable_cas=False, special_trip=False, explist=[]):
    extra_trip = 0
    
    primary_line = myGraph.es[area_idx]
    compromised_vs_list = [] # compromised routers
    area_relay_list = [] # all relays related the target area
    compromised_relay_list = []
    
    if primary_line["prRelaySet"] == []:
        return extra_trip
    
    for relay_i in primary_line["prRelaySet"]:
        if myGraph.vs[relay_i.Located_Node_Idx]["compromised"] == True:
            target_vs_idx = myGraph.vs[relay_i.Located_Node_Idx].index
            if target_vs_idx not in compromised_vs_list:
                compromised_vs_list.append(target_vs_idx)
        
        area_relay_list.append(relay_i)
        if relay_i.compromised == True:
            if relay_i.relayIdx not in compromised_relay_list:
                compromised_relay_list.append(relay_i.relayIdx)
        
        for bp_relay_j in relay_i.bp_relay:
            if myGraph.vs[bp_relay_j.Located_Node_Idx]["compromised"] == True:
                target_vs_idx = myGraph.vs[bp_relay_j.Located_Node_Idx].index
                if target_vs_idx not in compromised_vs_list:
                    compromised_vs_list.append(target_vs_idx)
            if bp_relay_j not in area_relay_list:
                area_relay_list.append(bp_relay_j)
            if bp_relay_j.compromised == True:
                if bp_relay_j.relayIdx not in compromised_relay_list:
                    compromised_relay_list.append(bp_relay_j.relayIdx)
    
    total_compromised = len(compromised_relay_list)
    for relay_i in primary_line["prRelaySet"]:
        if relay_i.compromised != True: # the primary relay is not compromised, thus it cannot trip neighbor relays
            continue
        else: # besides the primary relay, need at least ONE other compromised relay
            temp_total_cp = total_compromised - 0
        
        #print("In reputation trip, line",area_idx,"relay",relay_i.relayIdx,"compromised")
        for bp_relay_k in relay_i.bp_relay: # bp_relay_k is the target backup relay
            es_idx = bp_relay_k.Located_EdgeIdx
            if myGraph.es[es_idx]["tripped"] == True: # the line is already tripped, continue to the next
                continue
            
            total_correspond_relay = 0
            src_vs_idx = bp_relay_k.Located_Node_Idx
            src_vs = myGraph.vs[src_vs_idx]
            
            # check the paths from the target relay to other neighbor relays
            # each neighbor relay may have different numbers of related relays
            for neighbor_relay in area_relay_list:
                if neighbor_relay.relayIdx == bp_relay_k.relayIdx: # skip target relay itself
                    continue
                elif neighbor_relay.compromised == True: # compromised relays will always agree
                    neighbor_es_idx = neighbor_relay.Located_EdgeIdx
                    myGraph.es[neighbor_es_idx]["tripped"] = True
                    continue
                
                dst_vs_idx = neighbor_relay.Located_Node_Idx
                if src_vs_idx == dst_vs_idx:
                    continue
                
                #print("src_id:",src_vs_idx, "dst_id:",dst_vs_idx)
                path_affected = False
                assert src_vs["P2P_RVS_VS_path"][dst_vs_idx] != []
                for cp_vs_idx in compromised_vs_list: # check if the path has compromised routers
                    if cp_vs_idx in src_vs["P2P_RVS_VS_path"][dst_vs_idx]:
                        path_affected = True
                        break
                
                #print("Src node",src_vs_idx,"to dst node",dst_vs_idx,"   ",src_vs["P2P_RVS_VS_path"][dst_vs_idx])
                # this is the number of relays not compromised and path not affected
                if path_affected == False:
                    total_correspond_relay += 1
            
            assert myGraph.es[es_idx]["tripped"] != True
            if temp_total_cp >= total_correspond_relay and temp_total_cp > 1:
                # the "trusted" relays are majority and compromised (at least ONE)
                if special_trip == True and bp_relay_k.relayIdx not in explist:
                    continue
                #print("Relay",bp_relay_k.relayIdx,"tripped by reputation from line",area_idx,"-> relay",relay_i.relayIdx)
                if es_idx not in myGraph["cascading_initial"][0]:
                    print("Relay",bp_relay_k.relayIdx,"line",es_idx,"not in initial cascading")
                    if es_idx not in myGraph["cascading_sequence"][0]:
                        print("Relay",bp_relay_k.relayIdx,"line",es_idx,"not in cascading sequence")
                myGraph.es[es_idx]["tripped"] = True
                extra_trip += 1
                        
                if enable_cas == True:
                    cascade_check_list = cascading_initial_line_check(myGraph, es_idx)
                    triggered = trip_cascading_sequence_from_list(myGraph, cascade_check_list)
                    if triggered == True:
                        #print("Cascading triggered after checking for line",primary_line.index)
                        None
    
    return extra_trip


def find_cascading_reputation_line(myGraph, target_relay):
    assert len(target_relay.ZONE3_EdgeIdx) != 0
    max_pr_relay_cmp = -1
    
    for es_idx in target_relay.ZONE3_EdgeIdx:
        es_line = myGraph.es[es_idx]
        pr_relay_compromised = 0
        
        for relay_i in es_line["prRelaySet"]:
            if relay_i.compromised == True:
                pr_relay_compromised += 1
        
        if pr_relay_compromised > max_pr_relay_cmp:
            max_pr_relay_cmp = pr_relay_compromised
            target_line = es_line
    
    assert target_line["prRelaySet"] != []
    
    return target_line


def find_cascading_area_line(myGraph, target_relay):
    assert len(target_relay.ZONE3_EdgeIdx) != 0
    max_relay_cmp_ratio = -1
    max_relay_num = 10000
    
    for es_idx in target_relay.ZONE3_EdgeIdx:
        es_line = myGraph.es[es_idx]
        relay_compromised = 0
        total_relay_number = 0
        
        for relay_i in es_line["prRelaySet"]:
            total_relay_number += 1
            if relay_i.compromised == True:
                relay_compromised += 1
        
        for bk_relay_i in es_line["bpRelaySet"]:
            total_relay_number += 1
            if bk_relay_i.compromised == True:
                relay_compromised += 1
        
        relay_0 = es_line["prRelaySet"][0]
        relay_1 = es_line["prRelaySet"][1]
        
        if relay_0.compromised == True or relay_1.compromised == True:
            continue
#         relay_cmp_ratio = relay_compromised / total_relay_number
#         if relay_cmp_ratio > max_relay_cmp_ratio:
#             max_relay_cmp_ratio = relay_cmp_ratio
#             target_line = es_line
        if total_relay_number < max_relay_num:
            max_relay_num = total_relay_number
            target_line = es_line
    
    #target_line = myGraph.es[target_relay.ZONE3_EdgeIdx[0]]
    assert target_line["prRelaySet"] != []
    assert target_line.index not in myGraph["cascading_initial"]
    
    return target_line


def special_area_trip_cascading(myGraph, total_rsc, max_try, area_id, remain_rsc, rsc_to_be, target_relay,\
                                atk_mode="normal", relay_atk=[]):
    total_correspond_relay = count_total_correspond_relay(myGraph, area_id)
    total_compromised_relay = count_area_compromised_relay(myGraph, area_id)
    end_attack = False
    attack_try = 0
    
    if total_compromised_relay < math.ceil(total_correspond_relay/2+0.5): # majority not achieved
        assert math.ceil(total_correspond_relay/2) >= (total_correspond_relay/2)
        relay_to_atk = math.ceil(total_correspond_relay/2+0.5) - total_compromised_relay
        
        if (atk_mode == "normal") and (relay_to_atk > remain_rsc): # resource not enough
            end_attack = True
            return remain_rsc, rsc_to_be, end_attack
        elif (atk_mode != "normal") and (relay_to_atk * max_try > remain_rsc): # rsc not enough for K-each
            end_attack = True
            return remain_rsc, rsc_to_be, end_attack
    else:
        relay_to_atk = 0
    
    relay_rm_list = []
    assert myGraph.es[area_id]["prRelaySet"] != []
    relay_0 = myGraph.es[area_id]["prRelaySet"][0]
    relay_1 = myGraph.es[area_id]["prRelaySet"][1]
    
    relay_rm_list.append(relay_0)
    relay_rm_list.append(relay_1)
    
    for bp_relay_i in relay_0.bp_relay:
        if bp_relay_i not in relay_rm_list:
            relay_rm_list.append(bp_relay_i)
    for bp_relay_j in relay_1.bp_relay:
        if bp_relay_j not in relay_rm_list:
            relay_rm_list.append(bp_relay_j)
    
    #print("Target relay attack list:",[rl.relayIdx for rl in relay_rm_list])
    # the primary relays will always be checked and attacked
    # even if relay_to_atk <= 0, if the primary relays are not compromised, attack will be performed
    for relay_i in relay_rm_list:
        if relay_i.relayIdx not in relay_atk:
            relay_atk.append(relay_i.relayIdx)
        else:
            print("Relay",relay_i.relayIdx,"already attacked")
        
        if relay_i.compromised == True:
            continue
        
        if relay_i.relayIdx == target_relay.relayIdx:
            #print("Target relay selected")
            continue
        
        if (relay_i != relay_0) and (relay_i != relay_1) and (relay_to_atk <= 0):
            break
        
        if atk_mode != "normal":
            if remain_rsc < (relay_i.attack_cost * max_try):
                end_attack = True
                break
        
        rsc_to_be -= (relay_i.attack_cost * max_try)
        remain_rsc, completed = attack_relay(myGraph, remain_rsc, relay_i.relayIdx, max_try, False)
        attack_try += 1
        
        if completed == True:
            #if relay_i == relay_0 or relay_i == relay_1:
            #    trip_line_idx = trip_line_with_relay(myGraph, relay_i.relayIdx)
            relay_to_atk -= 1
            None
        
        if atk_mode != "normal":
            remain_rsc = max(rsc_to_be, 0)
            if remain_rsc <= 0:
                end_attack = True
                break
    
    return remain_rsc, rsc_to_be, end_attack


def simple_cascading_relay_select(myGraph, line_idx):
    assert myGraph.es[line_idx]["prRelaySet"] != []
    
    result_relay = myGraph.es[line_idx]["prRelaySet"][0]
    
    return result_relay


def count_total_correspond_relay(myGraph, area_idx):
    primary_line = myGraph.es[area_idx]
    
    if primary_line["prRelaySet"] == []:
        return 0
    
    relay_0 = primary_line["prRelaySet"][0]
    relay_1 = primary_line["prRelaySet"][1]
    
    # total number of corresponding relays in an area (primary + backup)
    total_relay_list = []
    total_relay_list.append(relay_0.relayIdx)
    total_relay_list.append(relay_1.relayIdx)
    
    for bp_relay_i in  relay_0.bp_relay:
        if bp_relay_i.relayIdx not in total_relay_list:
            total_relay_list.append(bp_relay_i.relayIdx)
    
    for bp_relay_j in  relay_1.bp_relay:
        if bp_relay_j.relayIdx not in total_relay_list:
            total_relay_list.append(bp_relay_j.relayIdx)
    
    total_correspond_relay = len(total_relay_list)
    
    return total_correspond_relay


def count_area_compromised_relay(myGraph, area_idx):
    primary_line = myGraph.es[area_idx]
    compromised_relay_list = []
    
    for relay_i in primary_line["prRelaySet"]:
        if relay_i.compromised == True:
            compromised_relay_list.append(relay_i.relayIdx)
        
        for bp_relay_j in relay_i.bp_relay:
            if bp_relay_j.compromised == True:
                if bp_relay_j.relayIdx not in compromised_relay_list:
                    compromised_relay_list.append(bp_relay_j.relayIdx)
    
    total_compromised = len(compromised_relay_list)
    
    return total_compromised


def debug_area_relay_majority_situation(myGraph, area_idx, total_rsc, remain_rsc):
    primary_line = myGraph.es[area_idx]
    relay_0 = primary_line["prRelaySet"][0]
    relay_1 = primary_line["prRelaySet"][1]
    total_comp = 0
    
    total_relay_list = []
    total_relay_list.append(relay_0.relayIdx)
    total_relay_list.append(relay_1.relayIdx)
    for bp_relay_i in  relay_0.bp_relay:
        if bp_relay_i.relayIdx not in total_relay_list:
            total_relay_list.append(bp_relay_i.relayIdx)
    for bp_relay_j in  relay_1.bp_relay:
        if bp_relay_j.relayIdx not in total_relay_list:
            total_relay_list.append(bp_relay_j.relayIdx)
    
    total_relay = len(total_relay_list)
    #print(total_relay_list)
    
    trip = "CP" if (relay_0.compromised == True) else "NCP"
    total_comp = (total_comp+1) if (relay_0.compromised == True) else total_comp
    print("Total rsc:",total_rsc,"remain_rsc:",remain_rsc,"  {","PR relay:",relay_0.relayIdx,trip,"}   ",end='')
    trip = "CP" if (relay_1.compromised == True) else "NCP"
    total_comp = (total_comp+1) if (relay_1.compromised == True) else total_comp
    print("{","PR relay:",relay_1.relayIdx,trip,"}   ","on line",area_idx,"   Total relays:",total_relay)
    
    for bp_relay_i in  relay_0.bp_relay:
        es_idx = bp_relay_i.Located_EdgeIdx
        trip = "CP" if (bp_relay_i.compromised == True) else "NCP"
        print("{","relay:",bp_relay_i.relayIdx,trip,"(L"+str(es_idx)+")"," ->",str(relay_0.relayIdx)+"}   ",end='')
        total_comp = (total_comp+1) if (bp_relay_i.compromised == True) else total_comp
    
    for bp_relay_j in  relay_1.bp_relay:
        es_idx = bp_relay_j.Located_EdgeIdx
        trip = "CP" if (bp_relay_j.compromised == True) else "NCP"
        print("{","relay:",bp_relay_j.relayIdx,trip,"(L"+str(es_idx)+")"," ->",str(relay_1.relayIdx)+"}   ",end='')
        total_comp = (total_comp+1) if (bp_relay_j.compromised == True) else total_comp
    print("compromised relays:", total_comp)



def debug_area_relay_reputation_situation(myGraph, area_idx, total_rsc):
    debug_area_relay_majority_situation(myGraph, area_idx, total_rsc)
    
    primary_line = myGraph.es[area_idx]
    relay_0 = primary_line["prRelaySet"][0]
    relay_1 = primary_line["prRelaySet"][1]
    router_0 = myGraph.vs[relay_0.Located_Node_Idx]
    router_1 = myGraph.vs[relay_1.Located_Node_Idx]
    
    print("relay",relay_0.relayIdx,"at node",router_0.index,"with neighbor node:")
    for bp_relay_i in relay_0.bp_relay:
        print("Node",bp_relay_i.Located_Node_Idx,"   ",end='')
    print()
    print("relay",relay_1.relayIdx,"at node",router_1.index,"")
    for bp_relay_j in relay_1.bp_relay:
        print("Node",bp_relay_j.Located_Node_Idx,"   ",end='')
    print()



# compute the "potential effective line" for a relay
# a relay can issue trip request to its corresponding backup relays
def compute_potential_of_relay(myGraph, target_relay):
    effect_num = 0
    total_effect_num = 0
    local_line_idx = target_relay.Located_EdgeIdx
    additional_relay = False
    additional_num = 0
    the_other_relay = None
    
    # check if another primary relay is compromised
    for relay_i in myGraph.es[local_line_idx]["prRelaySet"]:
        if relay_i.relayIdx != target_relay.relayIdx:
            the_other_relay = relay_i
            
            if relay_i.compromised == True:
                additional_num += 1
    
    # check if any of the backup relays for the line is compromised
    for relay_j in myGraph.es[local_line_idx]["bpRelaySet"]:
        if relay_j.relayIdx != target_relay.relayIdx:
            if relay_j.compromised == True:
                additional_num += 1
    
    # if the local line is not tripped, the attacker can trip at least one line
    if myGraph.es[local_line_idx]["tripped"] != True:
        effect_num += 1
        total_effect_num += 1
    
    # if there are additional relays, the agent-protection attack can be used
    if additional_num > 0:
        additional_relay = True
    
    # compute the potential number of "effective line"
    if additional_relay == True:
        for relay_bp in myGraph.es[local_line_idx]["bpRelaySet"]:
            backup_line_idx = relay_bp.Located_EdgeIdx
            
            if relay_bp.Remote_Node == target_relay.Located_Node_Idx:
                if myGraph.es[backup_line_idx]["tripped"] != True:
                    effect_num += 1
                    total_effect_num += 1
            else:
                if the_other_relay.compromised == True:
                    if myGraph.es[backup_line_idx]["tripped"] != True:
                        total_effect_num += 1
        
    efficiency = effect_num / target_relay.attack_cost
    total_efficiency = total_effect_num / target_relay.attack_cost
    #print("Relay",target_relay.relayIdx,"has efficiency",efficiency,total_efficiency)
    
    return efficiency, total_efficiency


# find the corresponding backup relays for a primary relay
def find_corresponding_bp_relay_for_pr_relay(myGraph):
    for es_i in myGraph.es:
        if es_i["prRelaySet"] == []:
            continue
        
        relay_0 = es_i["prRelaySet"][0]
        relay_1 = es_i["prRelaySet"][1]
        
        for r_j in es_i["bpRelaySet"]:
            if r_j.Remote_Node == relay_0.Located_Node_Idx:
                relay_0.bp_relay.append(r_j)
            else:
                relay_1.bp_relay.append(r_j)


# compute the expected number of tripped line if two primary relays
# of an area are attacked, not considering "overlap"
def expected_tripped_line_num_each_area_based(myGraph):
    temp_dict = {}
    
    for es_i in myGraph.es:
        if es_i["prRelaySet"] == []:
            continue
        
        pf_0 = es_i["prRelaySet"][0].defense_strength
        pf_1 = es_i["prRelaySet"][1].defense_strength
        
        # probability that primary line is tripped (any PR relay is tripped)
        P_pr_line_trip = 1 - (pf_0 * pf_1)
        
        # probability that backup lines are tripped (both PR relays be tripped)
        P_bp_line_trip = (1 - pf_0) * (1 - pf_1) 
        
        exp_line_trip_area_i = (1 * P_pr_line_trip) + (len(es_i["bpRelaySet"]) * P_bp_line_trip)
        
        if es_i.index not in temp_dict:
            temp_dict[es_i.index] = exp_line_trip_area_i
    
    return temp_dict


def select_areas_based_on_e_line_can_trip(myGraph, rsc):
    all_dict = expected_tripped_line_num_each_area_based(myGraph)
    relay_idx_selected = []
    atk_rsc = rsc
    
    while atk_rsc > 0:
        max_value_idx = max(all_dict, key=lambda i: all_dict[i])
        temp_list = []
        
        for relay_i in myGraph.es[max_value_idx]["prRelaySet"]:
            temp_list.append(relay_i.relayIdx)
        
        for relay_idx in temp_list:
            if atk_rsc > 0:
                relay_idx_selected.append(relay_idx)
                atk_rsc -= 1
        
        all_dict.pop(max_value_idx, None)
    
    return relay_idx_selected



def select_area_trip_num_simu(myGraph, area_atk=[]):
    """In the simulation, the area-attack select the area with max potential lines to trip
    This function is used in the iterative selection. The (i+1)_th selection is performed
    after the i_th attack action is completed
    
    Args:
        myGraph: the graph class variable
        area_atk (list): the areas that have been selected
    """
    all_dict = {}
    
    for es_i in myGraph.es:
        if es_i["prRelaySet"] == []:
            continue
        
        # if the two primary relays of the area are compromised, then do not need to consider this area
        #if es_i["prRelaySet"][0].compromised == True and es_i["prRelaySet"][1].compromised == True:
            #continue
        # if the area is attacked, then skip this area
        if es_i.index in area_atk:
            continue
        
        trip_line_list = []
        # check whether primary line tripped
        if es_i["tripped"] != True:
            if es_i.index not in trip_line_list:
                trip_line_list.append(es_i.index)
        
        # check whether backup line tripped
        for relay_i in es_i["bpRelaySet"]:
            bp_line_idx = relay_i.Located_EdgeIdx
            if myGraph.es[bp_line_idx]["tripped"] != True:
                if bp_line_idx not in trip_line_list:
                    trip_line_list.append(bp_line_idx)
        
        potential_trip_num = len(trip_line_list)
        if es_i.index not in all_dict:
            all_dict[es_i.index] = potential_trip_num
    
#     while all_dict != {}:
#         selected_area = max(all_dict, key=lambda i: all_dict[i])
#         if selected_area in area_atk:
#             all_dict.pop(selected_area)
#             continue
#         else:
#             break
    a = max(all_dict, key=all_dict.get)
    m = all_dict[a]
    max_list = [i for i in all_dict if all_dict[i]==m]
    #print("max=",m,a,b)
    #selected_area = random.choice(max_list)
    
#     max_l = -1
#     selected_area = -1
#     for line_idx in max_list:
#         temp_max = count_total_correspond_relay(myGraph, line_idx)
#         if temp_max > max_l:
#             max_l = temp_max
#             selected_area = line_idx
#     assert selected_area >= 0
    
    result_list = shuffle_area_one_time_simu(all_dict)
    selected_area = max(result_list, key=lambda item:item[1])[0]
    #print(result_list)
    
    return selected_area



def select_area_one_time_simu(myGraph):
    all_dict = {}
    for es_i in myGraph.es:
        if es_i["prRelaySet"] == []:
            continue
        
        area_trip_num = 1 + len(es_i["bpRelaySet"])
        
        if es_i.index not in all_dict:
            all_dict[es_i.index] = area_trip_num
    
    return all_dict



def shuffle_area_one_time_simu(area_dict):
    keys =  list(area_dict.keys())
    random.shuffle(keys)
    
    result_list = [(key, area_dict[key]) for key in keys]
    
    return result_list



def sort_area_for_onetime_model(myGraph):
    """Used for area selection
    This model is ``one-round``, all target areas are selected before the attack.
    The metric of selection is purely based on the ``area degree``.
    
    Args:
        myGraph: the graph class variable
    """
    all_dict = {}
    for es_i in myGraph.es:
        if es_i["prRelaySet"] == []:
            continue
        
        area_trip_num = 1 + len(es_i["bpRelaySet"])
        
        if es_i.index not in all_dict:
            all_dict[es_i.index] = area_trip_num
    
    return all_dict



def expected_tripped_line_num_each_area_model(myGraph, K, relay_atk = [], area_atk = []):
    """Expected ``effective tripped lines`` when the TWO primary relays of an area are attacked (an ``if`` case)
    The result is an estimation of the line trip for an area, because the relays haven't been attacked actually
    The case when one primary relay is compromised depends on whether there is at least backup relay
    compromised. As an approximation, assume if at least one backup relay compromised, backup lines at
    the same side will be counted as ``effective tripped lines``
    
    Args:
        myGraph: the graph class variable
        K (int): the value for total attack number
        relay_atk (list): the relays that have been attacked
        area_atk (list): the areas that have been selected
    """
    temp_dict = {}
    
    for es_i in myGraph.es:
        if es_i.index in area_atk:
            continue
        
        if es_i["prRelaySet"] == []:
            continue
        
        pr_relay_0 = es_i["prRelaySet"][0]
        pr_relay_1 = es_i["prRelaySet"][1]
        
        pf_0 = pr_relay_0.defense_strength
        pf_1 = pr_relay_1.defense_strength
        
        # Case (1): probability that two primary relays are tripped
        # 1). no primary relays have been attacked, the result depends on current attack
        # 2). one primary relay has been attacked, the result depends on current and previous attacks 
        if pr_relay_0.p_cmp == 0 and pr_relay_1.p_cmp == 0:
            P_two_pr_trip = (1 - (pf_0**K)) * (1 - (pf_1**K))
            
        elif pr_relay_0.p_cmp == 0 and pr_relay_1.p_cmp != 0:
            # P(r0 atk success) * P(r1 atk not success) * (r1 already cmp) + P(both atks success)
            P_two_pr_trip = (1 - (pf_0**K)) * (pf_1**K) * pr_relay_1.p_cmp + (1 - (pf_0**K)) * (1 - (pf_1**K))
            
        elif pr_relay_0.p_cmp != 0 and pr_relay_1.p_cmp == 0:
            P_two_pr_trip = (1 - (pf_1**K)) * (pf_0**K) * pr_relay_0.p_cmp + (1 - (pf_0**K)) * (1 - (pf_1**K))
        
        exp_line_trip_area_i = 0
        # compute the E(effective trip) in Case (1)
        # first add the "effective expectation" of the primary line
        # then for each backup line in area_i, add the "effective expectation"
        exp_line_trip_area_i += (1 - es_i["p_line_already_tripped"]) * P_two_pr_trip
        
        for relay_j in es_i["bpRelaySet"]:
            es_j_idx = relay_j.Located_EdgeIdx
            exp_line_trip_area_i += (1 - myGraph.es[es_j_idx]["p_line_already_tripped"]) * P_two_pr_trip
        
        
        # Case (2): if only one PR relay of a backup line_j is tripped, the relay must have one other relay to confirm
        # the final result depends on whether there is another compromised backup relay 
        P_pr_0_trip = 1 - (pf_0**K)
        P_pr_1_trip = 1 - (pf_1**K)
        
        # for either primary relay, they look at the both sides of the backup relays
        # however, each primary relay can only trip one side
        no_bk_relay_tripped = 1
        for bp_relay_0_i in pr_relay_0.bp_relay:
            if bp_relay_0_i.relayIdx in relay_atk:
                pf_i = bp_relay_0_i.defense_strength
                no_bk_relay_tripped *= (pf_i**K) 
        
        for bp_relay_1_i in pr_relay_1.bp_relay:
            if bp_relay_1_i.relayIdx in relay_atk:
                pf_i = bp_relay_1_i.defense_strength
                no_bk_relay_tripped *= (pf_i**K) 
        
        # Similarly, first add the "effective expectation" of the primary line
        # If one primary relay is tripped, there are TWO sub-cases, either the pr_0 or pr_1 is tripped
        # need to compute for the two sub-cases 
        exp_line_trip_area_i += (1 - es_i["p_line_already_tripped"]) * P_pr_0_trip * (1 - P_pr_1_trip)
        exp_line_trip_area_i += (1 - es_i["p_line_already_tripped"]) * P_pr_1_trip * (1 - P_pr_0_trip)
        
        # Then compute the "effective expectation" of backup lines for each of the sub-cases
        # pr_0 side backup line
        for bp_relay_0_i in pr_relay_0.bp_relay:
            bp_line = bp_relay_0_i.Located_EdgeIdx
            exp_line_trip_area_i += (1 - myGraph.es[bp_line]["p_line_already_tripped"]) * \
                                (1 - no_bk_relay_tripped) * P_pr_0_trip * (1 - P_pr_1_trip)
        
        # pr_1 side backup line
        for bp_relay_1_i in pr_relay_1.bp_relay:
            bp_line = bp_relay_1_i.Located_EdgeIdx
            exp_line_trip_area_i += (1 - myGraph.es[bp_line]["p_line_already_tripped"]) * \
                                (1 - no_bk_relay_tripped) * P_pr_1_trip * (1 - P_pr_0_trip)
        
        
        # Case (1) and Case (2) are already computed, save the result
        if es_i.index not in temp_dict:
            temp_dict[es_i.index] = exp_line_trip_area_i
    
    return temp_dict



def update_p_line_already_tripped(myGraph, area_idx, K, relay_atk = [], atk_type = "two"):
    """Update the probability that a line is already tripped, after an area is selected to be attacked
    the other areas will not be affected since only primary relays are attacked
    
    Args:
        myGraph: the graph class variable
        area_idx (int): the area just selected, also the primary line index
        K (int): the value for total attack number
        relay_atk (list): the relays that have been attacked
        atk_type (str): indicate whether the attack to an area can attack two primary relays
    """
    pr_line = myGraph.es[area_idx]
    p_pr_tripped_old = pr_line["p_line_already_tripped"]
    
    pr_relay_0 = pr_line["prRelaySet"][0]
    pr_relay_1 = pr_line["prRelaySet"][1]
    
    pf_0 = pr_relay_0.defense_strength
    pf_1 = pr_relay_1.defense_strength
    
    #P_pr_0_trip = pr_relay_0.p_cmp + (1-pr_relay_0.p_cmp) * (1 - (pf_0**K))
    #P_pr_1_trip = pr_relay_1.p_cmp + (1-pr_relay_1.p_cmp) * (1 - (pf_1**K))
    P_pr_0_trip = pr_relay_0.p_cmp
    P_pr_1_trip = pr_relay_1.p_cmp
    
    # in an attack
    # Case (1): probability that two primary relays are tripped
    if atk_type == "two":
        #P_two_pr_trip = (1 - (pf_0**K)) * (1 - (pf_1**K))
        #P_no_pr_trip = (pf_0**K) * (pf_1**K)
        P_two_pr_trip = P_pr_0_trip * P_pr_1_trip
        P_no_pr_trip = (1-P_pr_0_trip) * (1-P_pr_1_trip)
    else:
        P_two_pr_trip = 0 # resource only enough for one relay attack
        # P(no pr trip) = P(pr_0 selected) * P(pr_0 not trip) + P(pr_1 selected) * P(pr_1 not trip)
        #P_no_pr_trip = ((pf_0**K) + (pf_1**K))/2
        #P_no_pr_trip = (1-P_pr_0_trip) * (1-P_pr_1_trip)
        P_no_pr_trip = ((1-P_pr_0_trip) + (1-P_pr_1_trip))/2
    
    # Case (2): if only one PR relay of a backup line_j is tripped, the relay must have one other relay to confirm
    # the final result depends on whether there is another compromised backup relay 
    #P_pr_0_trip = 1 - (pf_0**K)
    #P_pr_1_trip = 1 - (pf_1**K)
    
    # first update the probability of primary line in the area
    pr_line["p_line_already_tripped"] = p_pr_tripped_old + ((1 - p_pr_tripped_old) * (1 - P_no_pr_trip)) 
    
    # then update the probability of backup lines in the area
    # for either primary relay, they look at the both sides of the backup relays
    # however, each primary relay can only trip one side
    no_bk_relay_tripped = 1
    for bp_relay_0_i in pr_relay_0.bp_relay:
        if bp_relay_0_i.relayIdx in relay_atk:
            pf_i = bp_relay_0_i.defense_strength
            #no_bk_relay_tripped *= ((1-bp_relay_0_i.p_cmp) * (pf_i**K)) 
            no_bk_relay_tripped *= ((1-bp_relay_0_i.p_cmp)) 
    
    for bp_relay_1_i in pr_relay_1.bp_relay:
        if bp_relay_1_i.relayIdx in relay_atk:
            pf_i = bp_relay_1_i.defense_strength
            #no_bk_relay_tripped *= ((1-bp_relay_1_i.p_cmp) * (pf_i**K)) 
            no_bk_relay_tripped *= ((1-bp_relay_1_i.p_cmp)) 
    
    if atk_type == "two":
        p_pr0_select = 1
        p_pr1_select = 1
    else:
        p_pr0_select = 0.5
        p_pr1_select = 0.5
    
    # update the probability for backup lines of primary relay pr_0
    for bp_relay_0_i in pr_relay_0.bp_relay:
        bp_line_idx = bp_relay_0_i.Located_EdgeIdx
        bp_line = myGraph.es[bp_line_idx] 
        bp_line["p_line_already_tripped"] = bp_line["p_line_already_tripped"] + (1-bp_line["p_line_already_tripped"]) *\
                    (P_two_pr_trip + P_pr_0_trip * (1-P_pr_1_trip) * (1-no_bk_relay_tripped) * p_pr0_select)
    
    # update the probability for backup lines of primary relay pr_1
    for bp_relay_1_i in pr_relay_1.bp_relay:
        bp_line_idx = bp_relay_1_i.Located_EdgeIdx
        bp_line = myGraph.es[bp_line_idx] 
        bp_line["p_line_already_tripped"] = bp_line["p_line_already_tripped"] + (1-bp_line["p_line_already_tripped"]) *\
                    (P_two_pr_trip + P_pr_1_trip * (1-P_pr_0_trip) * (1-no_bk_relay_tripped) * p_pr1_select)



def update_p_line_already_tripped_new(myGraph, area_idx, K, relay_atk = []):
    pr_line = myGraph.es[area_idx]
    p_pr_tripped_old = pr_line["p_line_already_tripped"]
    
    pr_relay_0 = pr_line["prRelaySet"][0]
    pr_relay_1 = pr_line["prRelaySet"][1]
    
    P_pr_0_trip = pr_relay_0.p_cmp
    P_pr_1_trip = pr_relay_1.p_cmp
    
    P_two_pr_trip = P_pr_0_trip * P_pr_1_trip
    P_no_pr_trip = (1-P_pr_0_trip) * (1-P_pr_1_trip)
    
    pr_line["p_line_already_tripped"] = p_pr_tripped_old + ((1 - p_pr_tripped_old) * (1 - P_no_pr_trip)) 
    
    no_bk_relay_tripped = 1
    for bp_relay_0_i in pr_relay_0.bp_relay:
        if bp_relay_0_i.relayIdx in relay_atk:
            no_bk_relay_tripped *= (1-bp_relay_0_i.p_cmp) 
    
    for bp_relay_1_i in pr_relay_1.bp_relay:
        if bp_relay_1_i.relayIdx in relay_atk:
            no_bk_relay_tripped *= (1-bp_relay_1_i.p_cmp)
    
    # update the probability for backup lines of primary relay pr_0
    for bp_relay_0_i in pr_relay_0.bp_relay:
        bp_line_idx = bp_relay_0_i.Located_EdgeIdx
        bp_line = myGraph.es[bp_line_idx] 
        bp_line["p_line_already_tripped"] = bp_line["p_line_already_tripped"] + (1-bp_line["p_line_already_tripped"]) *\
                    (P_two_pr_trip + P_pr_0_trip * (1-P_pr_1_trip) * (1-no_bk_relay_tripped))
    
    # update the probability for backup lines of primary relay pr_1
    for bp_relay_1_i in pr_relay_1.bp_relay:
        bp_line_idx = bp_relay_1_i.Located_EdgeIdx
        bp_line = myGraph.es[bp_line_idx] 
        bp_line["p_line_already_tripped"] = bp_line["p_line_already_tripped"] + (1-bp_line["p_line_already_tripped"]) *\
                    (P_two_pr_trip + P_pr_1_trip * (1-P_pr_0_trip) * (1-no_bk_relay_tripped))



def area_or_attack_model_compute(myGraph, atk_rsc, mtd, area_idx_selected, relay_idx_selected, cascading_idx, K1, K2):
    single_relay_atk_area = deepcopy(cascading_idx)
    all_dict = sort_area_for_onetime_model(myGraph)
    
    K_p = K1 if K2 <= 0 else K2
    
    # start area-attack, based on the resources, first identify all lines to be attacked
    while atk_rsc > 0:
        max_value_idx = max(all_dict, key=lambda i: all_dict[i])
        assert max_value_idx not in area_idx_selected
        
        assert myGraph.es[max_value_idx]["prRelaySet"] != []
        relay_0 = myGraph.es[max_value_idx]["prRelaySet"][0]
        relay_1 = myGraph.es[max_value_idx]["prRelaySet"][1] 
        two_relay_rsc = (K_p * (1-relay_0.p_cmp)) + (K_p * (1-relay_1.p_cmp))
        one_relay_rsc = (K_p * (1-relay_0.p_cmp)) + (K_p * (1-relay_1.p_cmp))/2
        
        if atk_rsc >= (two_relay_rsc):
            # record the primary relays' index of the selected area
            for relay_i in myGraph.es[max_value_idx]["prRelaySet"]:
                relay_idx_selected.append(relay_i.relayIdx)
                pf = relay_i.defense_strength
                relay_i.p_cmp += (1-relay_i.p_cmp)*(1-pf**K_p)
            atk_rsc -= two_relay_rsc
            area_idx_selected.append(max_value_idx)
            if max_value_idx in single_relay_atk_area:
                single_relay_atk_area.remove(max_value_idx)
            all_dict.pop(max_value_idx, None)
        elif atk_rsc >= one_relay_rsc and atk_rsc < two_relay_rsc:
            # resources only enough to attack one relay
            relay_selected = myGraph.es[max_value_idx]["prRelaySet"][0]
            relay_idx_selected.append(relay_selected.relayIdx)
            pf = relay_selected.defense_strength
            relay_selected.p_cmp += (1-relay_selected.p_cmp)*(1-pf**K_p)
            
            atk_rsc -= one_relay_rsc
            #area_idx_selected.append(max_value_idx)
            single_relay_atk_area.append(max_value_idx)
            all_dict.pop(max_value_idx, None)
            break
        else:
            break
    
#     for line_idx in cascading_idx:
#         if line_idx not in area_idx_selected:
#             area_idx_selected.append(line_idx)
    
    print("start double area-attack",area_idx_selected,"single area-attack",single_relay_atk_area)
    for area_idx in area_idx_selected:
        update_p_line_already_tripped(myGraph, area_idx, K_p, relay_idx_selected, "two")
        #update_p_line_already_tripped_new(myGraph, area_idx, K_p, relay_idx_selected)
    
    for area_idx in single_relay_atk_area:
        update_p_line_already_tripped(myGraph, area_idx, K_p, relay_idx_selected, "one")


def area_attack_model_compute(myGraph, atk_rsc, mtd, area_idx_selected, relay_idx_selected, K1, K2):
    if mtd == "cascading" or mtd == "area_or":
        # for one-round selection, areas can be selected before the area attack (not in the loop)
        all_dict = sort_area_for_onetime_model(myGraph)
    
    while atk_rsc > 0: # start area-attack
        # compute the "effective line trip" for all areas
        # assume there are enough resources, just check the potential number for each area
        if mtd == "area_it":
            if K2 <= 0:
                all_dict = expected_tripped_line_num_each_area_model(myGraph, K1, relay_idx_selected, area_idx_selected)
            else:
                all_dict = expected_tripped_line_num_each_area_model(myGraph, K2, relay_idx_selected, area_idx_selected)
        #elif mtd == "area_onetime":
            #all_dict = sort_area_for_onetime_model(myGraph)
            
        area_idx_ok = False
        
        # if all areas have been selected, then stop the selection
        if len(area_idx_selected) >= int(myGraph["total_relay_no"]/2):
                break
        
        while area_idx_ok != True:
            # select the area index with the MAX "effective line trip"
            max_value_idx = max(all_dict, key=lambda i: all_dict[i])
            
            # if the area is already selected, delete it from the candidate set
            if max_value_idx in area_idx_selected:
                all_dict.pop(max_value_idx, None)
            else:
                area_idx_selected.append(max_value_idx)
                area_idx_ok = True
        
        E_relay_cost = K1 if K2 <= 0 else K2
        K_p = K1 if K2 <= 0 else K2
        #E_relay_cost = avg_rsc_for_relay(myGraph.es[0]["prRelaySet"][0].defense_strength, K)
        #E_relay_cost = min(K, 1/(1-myGraph.es[0]["prRelaySet"][0].defense_strength))
        #print("Rsc:",atk_rsc,"K =",K,E_relay_cost)
        assert myGraph.es[max_value_idx]["prRelaySet"] != []
        relay_0 = myGraph.es[max_value_idx]["prRelaySet"][0]
        relay_1 = myGraph.es[max_value_idx]["prRelaySet"][1] 
        two_relay_rsc = (K_p * (1-relay_0.p_cmp)) + (K_p * (1-relay_1.p_cmp))
        one_relay_rsc = (K_p * (1-relay_0.p_cmp)) + (K_p * (1-relay_1.p_cmp))/2
        #if atk_rsc >= (2*E_relay_cost): # resources are enough to attack two relays
        if atk_rsc >= (two_relay_rsc):
            # record the primary relays' index of the selected area
            for relay_i in myGraph.es[max_value_idx]["prRelaySet"]:
                relay_idx_selected.append(relay_i.relayIdx)
            #atk_rsc -= (2*E_relay_cost)
            atk_rsc -= two_relay_rsc
            #print("Area-or attack model:    ","K =", K1, "Remaining rsc:", atk_rsc)
        #elif atk_rsc >= E_relay_cost and atk_rsc < (2*E_relay_cost):
        elif atk_rsc >= one_relay_rsc and atk_rsc < two_relay_rsc:
            # resources only enough to attack one relay
            update_p_line_already_tripped(myGraph, max_value_idx, K_p, relay_idx_selected, "one")
            relay_idx_selected.append(myGraph.es[max_value_idx]["prRelaySet"][0].relayIdx)
            #atk_rsc -= E_relay_cost
            atk_rsc -= one_relay_rsc
            #print("Area-or attack model:    ","K =", K1, "Remaining rsc:", atk_rsc)
            break
        else:
            # the remaining resources are not enough to complete the K-attack on a relay, skip
            break
        
        # if resources are enough to attack two primary relays,
        # update the probability of line already tripped in an area
        update_p_line_already_tripped(myGraph, max_value_idx, K_p, relay_idx_selected)


def select_areas_effective_trip_model(myGraph, rsc, K1, mtd, K2=0):
    """Used for analysis modeling
    Select the area to attack based on the ``effective lines`` that can be tripped.
    When planning the attack, may assume each relay is assigned with N/2K resources. This is the minimum
    number of areas attacked.
    
    Process of analysis modeling:
    (1). Select area based on E(effective line trip #)
    (2). When an area is selected, update P(line_i is tripped) for each affected line_i
    (3). Go to step (1) if there are remaining resources
    
    For the area-attack after the cascading-attack, the selection can be one-round, or can be iteration
    
    After all resources are used (plan completed), each line_i will have P(line_i is tripped).
    E(total trip) = \sum_i P(line_i is tripped) 
    
    Args:
        myGraph: the graph class variable
        rsc (int): the total resource available for attack
        K1 (int): the value for total attack number for cascading attack or pure area-attack
        mtd (string): whether use cascading-attack
        K2 (int): the value of repeat number for area-attack following cascading attack
    """
    relay_idx_selected = []
    area_idx_selected = []
    cascading_area_idx = []
    atk_rsc = rsc
    old_p_trigger_cas = 0
    
    # the attacker can distribute all resources to cascading initial lines
    # if there are remaining resources, the attacker can continue with the area-attack
    # or if the resources are not enough to trigger a cascading, the attacker uses area-attack
    if mtd == "cascading":
        no_rsc = False
        for i in range(len(myGraph["cascading_initial"])):
            p_trigger_cas = 1
            for line_idx in myGraph["cascading_initial"][i]:
                # select the relay to attack, each assign K resources
                cas_relay_selected = simple_cascading_relay_select(myGraph, line_idx)
                E_relay_cost = K1
                
                if atk_rsc >= E_relay_cost:
                    relay_idx_selected.append(cas_relay_selected.relayIdx)
                    pf_relay = cas_relay_selected.defense_strength
                    cas_relay_selected.p_cmp += ((1-cas_relay_selected.p_cmp)*(1 - pf_relay**K1))
                    atk_rsc -= (E_relay_cost)
                else:
                    # resources not enough
                    no_rsc = True
                    break
                #print("Cascading attack model:    ","K =", K1, "Remaining rsc:", atk_rsc)
                # update the P(line already tripped) for the selected initial line
                # in the cascading attack step, P(line already tripped) = P(attack to relay success)
                # if a line appears in multiple initial set, then its trip probability is also additive
                pf_relay = cas_relay_selected.defense_strength
                myGraph.es[line_idx]["p_line_already_tripped"] += (1 - pf_relay**K1)
                p_trigger_cas *= (1 - pf_relay**K1)
                cascading_area_idx.append(line_idx)
            
            if no_rsc == True:
                break
            else:
                # there are enough resources to attack a complete initial set, update the P(line already tripped)
                # for all extra lines of the cascading_i, which is P(cascading_i is triggered)
                old_p_trigger_cas = p_trigger_cas
                for line_idx in myGraph["cascading_sequence"][i]:
                    myGraph.es[line_idx]["p_line_already_tripped"] += p_trigger_cas
        
        # after cascading-attack, perform agent-attack
#         for line_idx in cascading_area_idx:
#             update_p_line_already_tripped(myGraph, line_idx, K1, relay_idx_selected, "one")
                    
    
    # after the cascading-attack, if there are resources remaining, start area-attack 
    #area_attack_model_compute(myGraph, atk_rsc, mtd, area_idx_selected, relay_idx_selected, K1, K2)
    if mtd == "cascading":
        area_or_attack_model_compute(myGraph, atk_rsc, mtd, area_idx_selected, relay_idx_selected, \
                                     cascading_area_idx, K1, K2)
    
    # In the modeling, if the attack is computed, update the probability that the cascading will
    # be triggered. For the cascading-attack, if in the cascading stage, cascading is not triggered,
    # it may be triggered in the later area-attack
    #if mtd != "cascading":
    p_trigger_cas = 1
    for i in range(len(myGraph["cascading_initial"])):
        for line_idx in myGraph["cascading_initial"][i]:
            p_trigger_cas *= myGraph.es[line_idx]["p_line_already_tripped"]
        
        if p_trigger_cas > old_p_trigger_cas:
            for extra_line_idx in myGraph["cascading_sequence"][i]:
                myGraph.es[extra_line_idx]["p_line_already_tripped"] += (p_trigger_cas - old_p_trigger_cas)
    
    #debug_print(True, "Attacked relay:", relay_idx_selected)
    debug_print(False, "Selected area:", area_idx_selected)
    e_total_trip = 0
    for es_i in myGraph.es:
        e_total_trip += es_i["p_line_already_tripped"]
    
    e_total_trip = min(e_total_trip, len(myGraph.es))
    return e_total_trip

