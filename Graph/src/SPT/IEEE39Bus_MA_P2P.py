'''
Created on May 28, 2015

@author: Zhang Jiapeng
'''

import SPT.IEEE39Bus_data as sibd
from SPT import calSPT
from SPT import calSPT2
from copy import deepcopy
from SPT.IEEE39Bus_data import print_all_p2p_paths, print_all_p2p_per_hop_delay,\
    print_all_p2p_bp_paths, print_all_p2p_per_hop_delay_bp,\
    print_p2p_relay_backup_peer_partial, compute_data_statistics,\
    check_non_zero_data_list, form_new_list


# General initialization, find the MA in the graph and calculate the delay requirement for each relay.
# The division of the delay would be performed later in other function.
def graph_initialization(graph):
    graph.es["weight"] = 1
    calSPT.setRelayNo(graph,34)
    sibd.set_system_probability(graph)
#     IS_39.set_link_reliability(graph)
#     IS_39.set_link_reliability_random(graph)
           
    calSPT.findBackupRelayGraph(graph,34)
    calSPT.find_Zone3_relay_Same_bus(graph)
    calSPT.update_min_relay_req_at_a_bus(graph)
    
#     testFunction(graph)
    print()



# Setup primary path for the MA-based scheme
def set_primary_path(graph,primary_type='normal'):
    SPDistance = graph.shortest_paths_dijkstra(weights=graph.es["weight"])
    optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance) #This is the best MA location
    
    idealPos = 0
    print("Optimal locations:")
    for index in optimalNodeIndex:
        idealPos = index
        print("Bus",graph.vs[index]["name"])
    
    if primary_type == 'normal':
        calSPT.findPrimaryPath_All_Nodes(graph,optimalNodeIndex,'lw_equal',plotEN=False)
    elif primary_type == 'max_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    elif primary_type == 'equal_div_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    elif primary_type == 'sp_reliability':
        calSPT.findPrimaryPath_All_Nodes_Max_Reliability(graph, optimalNodeIndex[0], primary_type)
    
    # Caution: VERY IMPORTANT! In MA-based scheme, we need to decide the "division" of the "1-sec" delay
    calSPT.calDelayReq_PR_Graph(graph,34)



# Setup delay requirement for MA primary path and reservation resources
def MA_staticRSV_PR(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1):
    print("Use delayType",'\"'+delayType+'\"')
    calSPT.do_Perhop_DelayReq_Simple_PR(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_PR(graph,pktSize) #Reserve CA on primary path
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    calSPT.showRSV_PR(graph)
    print()
    


# This is a complete test of MA-based scheme, from initializing the graph to build primary path
# to reservation
def MA_scheme_test(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,rtnList1=[],rtnList2=[]):
    sibd.setGraph(graph)
    graph_initialization(graph)
    # sibd.testFunction_v2(sibd.myGraph)
    set_primary_path(graph)
    MA_staticRSV_PR(graph, maxEdgeIdx, pktSize, bpType, delayType, dstSft, powft)
    
    rsv_result = calSPT.print_rsv_overall_in_list(graph,True)
#     result = calSPT.compute_each_relay_false_trip_pf_only_pr(graph)
    result = calSPT.compute_each_relay_false_trip_pf_only_pr_complex(graph)
    print(len(result),result)
    pf_only_pr = calSPT.compute_system_pf_only_pr(graph)
    print("Failure probability due to primary path:",pf_only_pr)
#     pf_only_pr = calSPT.compute_system_pf_p2p(graph)
#     print("Failure probability due to primary path:",pf_only_pr)
    rtnList1.append(rsv_result)
    rtnList2.append(result)
    print()


def MA_scheme_test_bp(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,rtnList1=[],rtnList2=[]):
    sibd.setGraph(graph)
    graph_initialization(graph)
    set_primary_path(graph)
    MA_staticRSV_PR(graph, maxEdgeIdx, pktSize, bpType, delayType, dstSft, powft)
    calSPT.copyPR_RSV_to_Temp(graph) #Copy the primary reservation data to a temporary variable
    
    if bpType == 'dist':
        calSPT.findBpPath_PR_Path_NonOL_Dist(graph,"16",maxEdgeIdx,delayType,dstSft,powft)
    
    #Delay division on each hop for backup path
    calSPT.do_Perhop_DelayReq_Simple_BP(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_BP(graph, pktSize)
    calSPT.showRSV_Overall(graph)
    
    rsv_result = calSPT.print_rsv_overall_in_list(graph,True)
    result = calSPT.compute_each_relay_false_trip_pf_with_bp(graph)
    print(len(result),result)
    pf_with_bp = calSPT.compute_system_pf_with_bp(graph)
    print("Failure probability with backup path:",pf_with_bp)
    rtnList1.append(rsv_result)
    rtnList2.append(result)
    
    calSPT.hop_count_info_MA(graph)
    print()


# This is a complete test of P2P-based scheme, from initializing the graph to build p2p paths to reservation
def P2P_scheme_test(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,rtnList1=[],rtnList2=[]):
    sibd.setGraph(graph)
    graph_initialization(graph)
#     calSPT.print_remote_lines(graph)

    calSPT.find_P2P_Path_All_Relays(graph, 'lw_equal')
    calSPT.calDelayReq_P2P(graph, maxEdgeIdx)
    calSPT.do_Perhop_DelayReq_Simple_P2P(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_P2P_PR(graph, pktSize)
    calSPT.showRSV_P2P(graph)
    calSPT.showRSV_Overall(graph)
    
    rsv_result = calSPT.print_rsv_overall_in_list(graph, sortEnable=True)
    result = calSPT.compute_each_relay_false_trip_pf_p2p(graph)
#     print(rsv_result)
    print(len(result),result)
    pf_p2p = calSPT.compute_system_pf_p2p(graph)
    print("Failure probability with p2p scheme:",pf_p2p)
#     print_all_p2p_per_hop_delay(graph)
#     calSPT.print_protection_area(graph)
#     print_all_p2p_paths(graph)
    rtnList1.append(rsv_result)
    rtnList2.append(result)


# This is a complete test of P2P-based scheme, from initializing the graph to build p2p paths to reservation
# This function also consider backup path for each of the peer nodes
def P2P_scheme_test_bp(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                       enableOL=False,rtnList1=[],rtnList2=[]):
    sibd.setGraph(graph)
    graph_initialization(graph)
#     calSPT.print_remote_lines(graph)

    calSPT.find_P2P_Path_All_Relays(graph, 'lw_equal')
    calSPT.calDelayReq_P2P(graph, maxEdgeIdx)
    calSPT.do_Perhop_DelayReq_Simple_P2P(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_P2P_PR(graph, pktSize)
#     calSPT.showRSV_P2P(graph)
    
#     calSPT.find_backup_p2p_simple(graph, enOL=enableOL)
    # This is the method from the "partial backup scheme". By setting a large "req_bp_no", it is equal to
    # the "full backup path". Be careful: the two methods (full and partial) may use different paths for backup
    # though the hop count of the paths are equal.
    calSPT.find_backup_p2p_partial(graph, req_bp_no=9, enOL=enableOL, ln=-1, ridx=-1)
    calSPT.do_Perhop_DelayReq_Simple_P2P_BP(graph, maxEdgeIdx, delayType, dstSft, powft)
#     print_all_p2p_bp_paths(graph)
#     print_all_p2p_per_hop_delay_bp(graph)
    calSPT.staticRSV_P2P_BP(graph, pktSize)
#     calSPT.showRSV_Overall(graph)
     
    rsv_result = calSPT.print_rsv_overall_in_list(graph, sortEnable=True)
    result = calSPT.compute_each_relay_false_trip_pf_p2p_full_bp(graph)
    rtnList1.append(rsv_result)
    rtnList2.append(result)
    pf_p2p = calSPT.compute_system_pf_p2p(graph)
    print("Failure probability with p2p scheme:",pf_p2p)
    
    calSPT.hop_count_info_P2P(graph)



def P2P_scheme_test_bp_partial(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1,\
                       reqBpNum=1,enableOL=False,rtnList1=[],rtnList2=[]):
    sibd.setGraph(graph)
    graph_initialization(graph)
#     calSPT.print_remote_lines(graph)

    calSPT.find_P2P_Path_All_Relays(graph, 'lw_equal')
    calSPT.calDelayReq_P2P(graph, maxEdgeIdx)
    calSPT.do_Perhop_DelayReq_Simple_P2P(graph, maxEdgeIdx, delayType, dstSft, powft)
    calSPT.staticRSV_P2P_PR(graph, pktSize)
    calSPT.showRSV_P2P(graph)
    
    calSPT.find_backup_p2p_partial(graph, req_bp_no=reqBpNum, enOL=enableOL, ln=-1, ridx=-1)
#     print_all_p2p_bp_paths(graph)
#     print_p2p_relay_backup_peer_partial(graph)
    calSPT.do_Perhop_DelayReq_Partial_P2P_BP(graph, maxEdgeIdx, delayType, dstSft, powft)
#     print_all_p2p_per_hop_delay_bp(graph)
    calSPT.staticRSV_P2P_Partial_BP(graph, pktSize)
#     calSPT.showRSV_Overall(graph)
    
    rsv_result = calSPT.print_rsv_overall_in_list(graph, sortEnable=True)
    if reqBpNum > 2:
        req_reply_num = 2
    else:
        req_reply_num = reqBpNum
    result = calSPT.compute_each_relay_false_trip_pf_p2p_partial_bp(graph,req_reply_num)
    rtnList1.append(rsv_result)
    rtnList2.append(result)
    pf_p2p = calSPT.compute_system_pf_p2p(graph)
    print("Failure probability with p2p scheme:",pf_p2p)
    
    calSPT.hop_count_info_P2P(graph)
    


def compare_P2P_OL_NonOL(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1):
    copy_graph1 = deepcopy(graph)
    copy_graph2 = deepcopy(graph)
    copy_graph3 = deepcopy(graph)
    rsv_list = []
    hf_each_relay_list = []
    P2P_scheme_test_bp(graph, 34, 80, 'dist', 'equal', 0, 1, False, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp(copy_graph1, 34, 80, 'dist', 'equal', 0, 1, True, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp_partial(copy_graph2, 34, 80, 'dist', 'equal', 0, 1, 2, False, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp_partial(copy_graph3, 34, 80, 'dist', 'equal', 0, 1, 2, True, rsv_list, hf_each_relay_list)
    
    print()
    print("Statistics of RSV data:")
    compute_data_statistics(rsv_list)
    print("Statistics of HF data:")
    compute_data_statistics(hf_each_relay_list)
    
    # Plot figures
    rsv_label = ["P2P Full Non-Overlap Backup","P2P Full Overlap Backup","P2P Partial Non-Overlap Backup", \
                 "P2P Partial Overlap Backup"]
    rsv_title = "Reservation for different P2P backup schemes"
    hf_title = "Probability of false trip for each relay"
    new_rsv_list = form_new_list(rsv_list,37)
    calSPT.plotLinkUtil_From_List(new_rsv_list, rsv_label, rsv_title, 'Sorted Link', 'Capacity(Kbps)', \
                                  enableX=False, ymax=1000, ymin=0)
    calSPT.plotLinkUtil_From_List(hf_each_relay_list, rsv_label, hf_title, 'Relay Index', 'Probability', \
                                  enableX=True, enableSci=True, ymax=1e-4, ymin=0)
    

def compare_P2P_OL_num(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1):
    copy_graph1 = deepcopy(graph)
    copy_graph2 = deepcopy(graph)
    copy_graph3 = deepcopy(graph)
    rsv_list = []
    hf_each_relay_list = []
    
    P2P_scheme_test_bp_partial(graph, 34, 80, 'dist', 'equal', 0, 1, 5, True, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp_partial(copy_graph1, 34, 80, 'dist', 'equal', 0, 1, 4, True, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp_partial(copy_graph2, 34, 80, 'dist', 'equal', 0, 1, 3, True, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp_partial(copy_graph3, 34, 80, 'dist', 'equal', 0, 1, 2, True, rsv_list, hf_each_relay_list)
    
    print()
    print("Statistics of RSV data:")
    compute_data_statistics(rsv_list)
    print("Statistics of HF data:")
    compute_data_statistics(hf_each_relay_list)
    print()
    check_non_zero_data_list(hf_each_relay_list)
    
    rsv_label = ["P2P 5-Overlap Backup","P2P 4-Overlap Backup","P2P 3-Overlap Backup","P2P 2-Overlap Backup"]
    rsv_title = "Reservation for different P2P backup schemes"
    hf_title = "Probability of false trip for each relay"
    new_rsv_list = form_new_list(rsv_list,37)
    calSPT.plotLinkUtil_From_List(new_rsv_list, rsv_label, rsv_title, 'Sorted Link', 'Capacity(Kbps)', \
                                  enableX=False, ymax=1000, ymin=0)
    calSPT.plotLinkUtil_From_List(hf_each_relay_list, rsv_label, hf_title, 'Relay Index', 'Probability', \
                                  enableX=True, enableSci=True, ymax=1e-4, ymin=0)
    
    
def compare_MA_P2P(graph,maxEdgeIdx,pktSize,bpType='dist',delayType='equal',dstSft=0,powft=1):
    copy_graph1 = deepcopy(graph)
    copy_graph2 = deepcopy(graph)
    copy_graph3 = deepcopy(graph)
    rsv_list = []
    hf_each_relay_list = []
    MA_scheme_test_bp(copy_graph1, 34, 80, 'dist', 'equal', 0, 1, rsv_list, hf_each_relay_list)
    P2P_scheme_test_bp(copy_graph3, 34, 80, 'dist', 'equal', 0, 1, False, rsv_list, hf_each_relay_list)
    MA_scheme_test(graph, 34, 80, 'dist', 'equal', 0, 1, rsv_list, hf_each_relay_list)
    P2P_scheme_test(copy_graph2, 34, 80, 'dist', 'equal', 0, 1, rsv_list, hf_each_relay_list)
    
    # Plot figures
    rsv_label = ["MA backup","P2P backup","MA no-backup","P2P no-backup"]
    rsv_title = "Reservation for MA and P2P"
    hf_title = "Probability of false trip for each relay"
    new_rsv_list = form_new_list(rsv_list,37)
    calSPT.plotLinkUtil_From_List(new_rsv_list, rsv_label, rsv_title, 'Sorted Link', 'Capacity(Kbps)', \
                                  enableX=False, ymax=1000, ymin=0)
    calSPT.plotLinkUtil_From_List(hf_each_relay_list, rsv_label, hf_title, 'Relay Index', 'Probability', \
                                  enableX=True, enableSci=True, ymax=1e-4, ymin=0)
# Script starts here
# MA_scheme_test(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)
# MA_scheme_test_bp(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)
# P2P_scheme_test(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)
# P2P_scheme_test_bp(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1, enableOL=False)
# P2P_scheme_test_bp_partial(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1, 5, True)

# compare_MA_P2P(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)
# compare_P2P_OL_NonOL(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)
compare_P2P_OL_num(sibd.myGraph_39, 34, 80, 'dist', 'equal', 0, 1)