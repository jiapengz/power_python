'''
Created on May 28, 2015

@author: Zhang Jiapeng
'''

# This is the network data for IEEE 39-bus power system

import igraph as ig
from numpy import mean

# Start the initialization of the graph
myGraph_39 = ig.Graph()
def setGraph(myGraph):
    
    myGraph.add_vertices(39)

    for v in myGraph.vs:
        v["name"] = str(v.index + 1) #here the name is type "string"

    myGraph.add_edge("1", "2")
    myGraph.add_edge("1", "39")
    myGraph.add_edge("2", "3")
    myGraph.add_edge("2", "25")
    myGraph.add_edge("3", "4")
    myGraph.add_edge("3", "18")
    myGraph.add_edge("4", "5")
    myGraph.add_edge("4", "14")
    myGraph.add_edge("5", "6")
    myGraph.add_edge("5", "8")
    myGraph.add_edge("6", "7")
    myGraph.add_edge("6", "11")
    myGraph.add_edge("7", "8")
    myGraph.add_edge("8", "9")
    myGraph.add_edge("9", "39")
    myGraph.add_edge("10", "11")
    myGraph.add_edge("10", "13")
    myGraph.add_edge("13", "14")
    myGraph.add_edge("14", "15")
    myGraph.add_edge("15", "16")
    myGraph.add_edge("16", "17")
    myGraph.add_edge("16", "19")
    myGraph.add_edge("16", "21")
    myGraph.add_edge("16", "24")
    myGraph.add_edge("17", "18")
    myGraph.add_edge("17", "27")
    myGraph.add_edge("21", "22")
    myGraph.add_edge("22", "23")
    myGraph.add_edge("23", "24")
    myGraph.add_edge("25", "26")
    myGraph.add_edge("26", "27")
    myGraph.add_edge("26", "28")
    myGraph.add_edge("26", "29")
    myGraph.add_edge("28", "29")
    
    #non-transmission line
    myGraph.add_edge("6", "31")
    myGraph.add_edge("10", "32")
    myGraph.add_edge("11", "12")
    myGraph.add_edge("12", "13")
    myGraph.add_edge("19", "20")
    myGraph.add_edge("19", "33")
    myGraph.add_edge("20", "34")
    myGraph.add_edge("23", "36")
    myGraph.add_edge("29", "38")
    myGraph.add_edge("30", "2")
    myGraph.add_edge("35", "22")
    myGraph.add_edge("37", "25")
    
    #Added backup link
    myGraph.add_edge("19","21")
    
    for es in myGraph.es:
        es["name"] = str(es.index + 1) #here the name for a line is of type "string"

# Priority order for bus/line/relay
# Caution: the index in graph is "real index - 1" (start from 0)
# Here we use the "real index"
line_ip = [22,27,3,31,14,15,21,5,6,7,9,12,19,23,34]
bus_ip = [19,16,21,22,2,3,26,27,8,9,39,17,4,18,5,6,11,14,15,28,29]
relay_ip = [44,43,54,53,6,5,62,61,28,27,30,29,42,41,10,9,12,11,14,13,18,17,24,23,38,37,46,45,68,67]
#line_s_pf_dict = {22: 0.042, 27: 0.0115, 3: 0.0115, 31: 0.007, 14: 0.0024, 15: 0.0024, 21: 0.0016, \
#                  5: 0.0013, 6: 0.0013, 7: 0.0013, 9: 0.0013, 12: 0.0013, 19: 0.0013, 23: 0.0013, 34: 0.0013}
line_s_pf_dict = {22: 0.042, 27: 0.0115, 3: 0.0115, 31: 0.007, 14: 0.0017, 15: 0.0017, 21: 0.0017, \
                  5: 0.0008, 6: 0.0008, 7: 0.0008, 9: 0.0008, 12: 0.0008, 19: 0.0008, 23: 0.0008, 34: 0.0008}
reverse_relay_ip = [1, 3, 2, 7, 15, 19, 21, 22, 25, 20, 26, 31, 33, 32, 34, 35, 16, 36, 39, 40, 47, 49, 51, \
                    50, 55, 56, 57, 48, 58, 8, 59, 60, 63, 65, 52, 64, 66, 4, 67, 68, 45, 46, 37, 38, 23, 24, 17, \
                    18, 13, 14, 11, 12, 9, 10, 41, 42, 29, 30, 27, 28, 61, 62, 5, 6, 53, 54, 43, 44]


# A test function
def testFunction_v1(graph):
    global bus_ip, line_ip, relay_ip 
    for es in graph.es:
        print("Line "+str(es.index)+':',"name is",es["name"])
        #print("Line "+str(es.index)+':',"name is",es["name"]+',',"line failure P:",es["line_s_pf"])
        #for relay in es["prRelaySet"]:
            #print("relay ID:",str(relay.relayIdx)+',','averaged line pf:',relay.avg_line_pf_req,\
                  #'line_s_pf:',relay.pr_line_s_pf,'    ',end='')
        #print()
    
    print(bus_ip)
    print(set(bus_ip))
    

def testFunction_v2(graph):
    global bus_ip, line_ip, relay_ip 
    for es in graph.es:
        print("Line "+str(es.index)+':',"name is",es["name"]+',',"line failure P:",es["line_s_pf"])
        for relay in es["prRelaySet"]:
            print("relay ID:",str(relay.relayIdx)+',','averaged line pf:',relay.avg_line_pf_req,\
                  'line_s_pf:',relay.pr_line_s_pf,'    ',end='')
        print()
    
    print(bus_ip)
    print(set(bus_ip))
    

# Set up the failure probability and requirement for bus/line/relay
def set_system_probability(graph):
    global line_s_pf_dict
    graph["s_req"] = 0.000001
    graph["pf_hf"] = 1
    for keyIdx in line_s_pf_dict:
        graph.es[keyIdx-1]["line_s_pf"] = line_s_pf_dict[keyIdx]
    
    for es in graph.es:
        if es["prRelaySet"] != []:
            for relay in es["prRelaySet"]:
                relay.pr_line_s_pf = es["line_s_pf"]
                if relay.pr_line_s_pf != 0:
#                 effective_relay_no = graph["total_relay_no"]
                    effective_relay_no = 30
                    relay.avg_line_pf_req = round(graph["s_req"]/effective_relay_no,12)
                    

# Print out all p2p paths for check
def print_all_p2p_paths(graph):
    for vs_i in graph.vs:
        print("Bus "+vs_i["name"]+":")
        for peerIdx in vs_i["P2P_RVS_ES_path"]:
            print("To Bus"+str(peerIdx+1)+", ES_Path",vs_i["P2P_RVS_ES_path"][peerIdx],\
                  "    VS_Path",vs_i["P2P_RVS_VS_path"][peerIdx])


# Print out all backup paths in p2p
def print_all_p2p_bp_paths(graph):
    for vs_i in graph.vs:
        print("Bus "+vs_i["name"]+":")
        for peerIdx in vs_i["P2P_BP_RVS_ES_path"]:
            print("To Bus"+str(peerIdx+1)+", ES_Path",vs_i["P2P_BP_RVS_ES_path"][peerIdx],\
                  "    VS_Path",vs_i["P2P_BP_RVS_VS_path"][peerIdx])



# Print out all relay-to-peer path per-hop delay
def print_all_p2p_per_hop_delay(graph):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            for peer_relay_idx in relay.p2p_per_hop_dr_PR:
                print("R"+str(relay.relayIdx)+" to R"+str(peer_relay_idx)+" Direction: ",end="")
                for linkIdx in relay.p2p_per_hop_dr_PR[peer_relay_idx]:
                    for direction in relay.p2p_per_hop_dr_PR[peer_relay_idx][linkIdx]:
                        print(direction+":",relay.p2p_per_hop_dr_PR[peer_relay_idx][linkIdx][direction],end="")
                        print("   ",end="")
                    print(" ---> ",end="")
                print()



# Print out all relay-to-peer path per-hop delay for the backup path 
def print_all_p2p_per_hop_delay_bp(graph):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            for peer_relay_idx in relay.p2p_per_hop_dr_BP:
                print("R"+str(relay.relayIdx)+" to R"+str(peer_relay_idx)+" Direction: ",end="")
                for linkIdx in relay.p2p_per_hop_dr_BP[peer_relay_idx]:
                    for direction in relay.p2p_per_hop_dr_BP[peer_relay_idx][linkIdx]:
                        print(direction+":",relay.p2p_per_hop_dr_BP[peer_relay_idx][linkIdx][direction],end="")
                        print("   ",end="")
                    print(" ---> ",end="")
                print()



# Print the backup peer relay in the partial p2p backup scheme
def print_p2p_relay_backup_peer_partial(graph):
    for edge_i in graph.es:
        for relay in edge_i["prRelaySet"]:
            print("R"+str(relay.relayIdx)+" backup peers: ",end='')
            for lineIdx in relay.line_relay_bp_peers:
                print("Line",lineIdx+1," ---> ",relay.line_relay_bp_peers[lineIdx])



# Statistics of the data list (e.g. max/min/avg of the data)
# The parameter is a "list of data_list"
def compute_data_statistics(data_list):
    sum_list = []
    avg_list = []
    for data in data_list:
        sum_list.append(sum(data))
        avg_list.append(mean(data))
    
    print("Sum of data:",sum_list)
    print("Average of data:",avg_list)


# Check whether a data list has non-zero elements
def check_non_zero_data_list(data_list):
    non_zero_list = []
    for data in data_list:
        cnt = 0
        for elmnt in data:
            if elmnt > 0:
                cnt += 1
        
        non_zero_list.append(cnt)
    print("Non-zero element number:",non_zero_list)


# For each element of a "list of list", take a set of data to form a new "list of list" 
def form_new_list(list_of_list,num):
    result = []
    for my_list in list_of_list:
        temp = []
        temp = my_list[0:num]
        result.append(temp)
    
    return result