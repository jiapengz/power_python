'''
Created on Nov 5, 2014

@author: Zhang Jiapeng
'''

from SPT import calSPT
import igraph as ig
from SPT.calSPT import setRelayNo, findBackupRelayLine,\
    showRelaySet_v2, findBackupRelayGraph, calDelayReq_PR_Graph, showRSV_PR, staticRSV_PR,\
    smartRSV_PR,showAvgBPRelay,showSubtreeEdge_PR,smartRSV_2RDF_PR,findBackupPath_simple,\
    showSubtreeEdge_BP,showEmptyBus,calDelayReq_BP_Graph,staticRSV_BP,showRSV_BP,\
    smartRSV_BP,do_Perhop_DelayReq_Simple_BP, showLineErrorPro,do_Perhop_DelayReq_Simple_PR,\
    find_Zone3_relay_Same_bus

myGraph = ig.Graph()
myGraph.add_vertices(13)
# myGraph.vs["name"] = ["650","646","645","632","633","634","611","684","671",\
#                       "692","675","652","680"]
for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"

# myGraph.add_edge("650", "632")
# myGraph.add_edge("646", "645")
# myGraph.add_edge("645", "632")
# myGraph.add_edge("632", "633")
# myGraph.add_edge("633", "634")
# myGraph.add_edge("611", "684")
# myGraph.add_edge("652", "684")
# myGraph.add_edge("684", "671")
# myGraph.add_edge("680", "671")
# myGraph.add_edge("671", "692")
# myGraph.add_edge("671", "632")
# myGraph.add_edge("692", "675")

myGraph.add_edge("1", "2")
myGraph.add_edge("2", "6")
myGraph.add_edge("3", "6")
myGraph.add_edge("4", "6")
myGraph.add_edge("6", "9")
myGraph.add_edge("7", "8")
myGraph.add_edge("8", "9")
myGraph.add_edge("9", "10")
myGraph.add_edge("10", "11")
myGraph.add_edge("8", "12")
myGraph.add_edge("9", "13")

# Non-transmission Line
myGraph.add_edge("4", "5")


myGraph.es["weight"] = 1
SPDistance = myGraph.shortest_paths_dijkstra(weights=myGraph.es["weight"])
setRelayNo(myGraph,11)

optimalNodeIndex = calSPT.optimalMaxPathLength(SPDistance)

print("Optimal locations:")
for index in optimalNodeIndex:
    print("Bus",myGraph.vs[index]["name"])

# calSPT.plotSPT(myGraph, optimalNodeIndex)
#calSPT.findPrimaryBackupPath(myGraph,optimalNodeIndex,plotEN=False)

findBackupRelayGraph(myGraph,11)
find_Zone3_relay_Same_bus(myGraph)
calDelayReq_PR_Graph(myGraph,11)
do_Perhop_DelayReq_Simple_PR(myGraph, 11, 'normal', 0, 1)
# showRelaySet_v2(myGraph)
showLineErrorPro(myGraph)

staticRSV_PR(myGraph,80)
# smartRSV(myGraph,80,11)
# smartRSV_2RDF_PR(myGraph, 11, 80)
showRSV_PR(myGraph)
showAvgBPRelay(myGraph,11)
# showSubtreeEdge_PR(myGraph)
