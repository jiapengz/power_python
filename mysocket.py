import socket
import struct
import time

print('start')
srvsock = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
srvsock.bind( ('', 3000) )
srvsock.listen( 5 )

try:
	clisock, (remhost, remport) = srvsock.accept()
	str1 = clisock.recv(100)
	a, = struct.unpack('d',str1)
	print(a)
	print(str1)
	if a == 1:
		print("Done!")

	str1 = clisock.recv(100)
	b, = struct.unpack('!14s',str1)
	print(str1)
	print(b)
	c=b.decode("utf-8")
	print(c)

	data = b'12345'
	if data == b'12345':
		print('Same')
	a=len(data)
	b=int(a)
	print('Next')
	fmt='!'+str(b)+'s'
	print(fmt)
	send_data = struct.pack(fmt,data)
	clisock.send(send_data)
	
	time.sleep(5)
	print('wake up')
	data1 = 1.2
	send_data2 = struct.pack('d',data1)
	clisock.send(send_data2)

	clisock.close()
except KeyboardInterrupt:
	print('User press Ctrl+C')
	srvsock.close()