import pickle 

shoplistfile = 'shoplist.data' 
# the name of the file where we will store the object 

shoplist = ['apple', 'mango', 'carrot'] 

# Write to the file 
f = open(shoplistfile, 'wb') 
#help(f) 
pickle.dump(shoplist, f) # dump the object to a file 
f.close() 

del shoplist # remove the shoplist 

# Read back from the storage 
f = open(shoplistfile, 'rb') 
storedlist = pickle.load(f) 
print(storedlist)

