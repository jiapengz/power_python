#Create .csv file to be used in OpenDSS

print('Start')

#create a file
fileHandle = open('SolarRamp.csv','w')

counter = 0

while counter < 1000:
	if counter < 300:
		fileHandle.write(str(1)+'\n')
		
	elif counter >= 300 and counter < 400:
		temp = (400 - counter) / 100
		if temp < 0.5:
			temp = 0.5
		fileHandle.write(str(1)+'\n')
		
	elif counter >= 400 and counter < 800:
		fileHandle.write(str(1)+'\n')
		
	else:
		fileHandle.write(str(1)+'\n')
	
	counter = counter + 1

fileHandle.close()
