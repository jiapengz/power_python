'''
Created on Feb 11, 2015

@author: Zhang Jiapeng
'''

import csv
import numeric_analysis.plotFunction as napf
from copy import deepcopy


folderName = "F:\\Protection_Relay_Result\\Result_BP_Path\\2015_02_11\\"
fileName = "39_Bus_System_line_load_normal.csv"
filePath = folderName+fileName
filePfx = "39_Bus_System_line_load_F"

VSI = 17
LMN = 18
exemptLine = 2

f = open(filePath)
csv_f = csv.reader(f)

def openCSVFile(caseNum):
    global folderName, filePfx, VSI, LMN
    
    completePath = folderName + filePfx + str(caseNum) + ".csv"
    fp = open(completePath)
    
    return fp


def checkCSVFile(fp):
    global exemptLine
    counter = 0
    csv_file = csv.reader(fp)
    for row in csv_file:
        counter += 1
        if counter <= exemptLine:
            print(row)
        else:
            print(row,row[VSI],row[LMN])
            
    fp.seek(0)
            
            
def checkMaxLMN(fp):
    global exemptLine
    max_lmn = 0
    counter = 0
    csv_file = csv.reader(fp)
    for row in csv_file:
        counter += 1
        if counter <= exemptLine:
            continue
        else:
            local_lmn = float(row[LMN])
            if local_lmn > max_lmn:
                max_lmn = local_lmn
    
    fp.seek(0)
    return max_lmn


def compare_LMN(caseNum):
    normal_lmn = []
    case_lmn = []
    counter = 0
    for row in csv_f:
        counter += 1
        if counter > 2:
            normal_lmn.append(float(row[LMN]))
        
    counter = 0
    fp = openCSVFile(caseNum)
    csv_file = csv.reader(fp)
    for row in csv_file:
        counter += 1
        if counter > 2:
            case_lmn.append(float(row[LMN]))
        
    print(normal_lmn)
    print(case_lmn)
    fp.close()
    increase = 0
    delta_list = []
    delta_ratio_list = []
    for i in range(len(normal_lmn)):
        normal_val = normal_lmn[i]
        case_val = case_lmn[i]
        if normal_val < case_val:
            increase += 1
            delta_rise = case_val - normal_val
            delta_list.append(delta_rise)
            delta_ratio_list.append(delta_rise/normal_val*100)
        else:
            delta_list.append(0)
            delta_ratio_list.append(0)
            
    print(sum(i>=1 for i in case_lmn),increase,delta_list)
    print(delta_ratio_list)
    xData = []
    numRange = len(normal_lmn)
    for k in range(numRange):
        xData.append(k+1)
    
    napf.myPlotFig(xData, delta_ratio_list, 'VSI Increasing Ratio', 'Line ID', 'Ratio',yMax='',yMin='')


def retrive_All_MAX_LMN(numRange):
    result = []
    for i in range(1,numRange+1):
        fp = openCSVFile(i)
        max_lmn = checkMaxLMN(fp)
        result.append(max_lmn)
        fp.close()
    
    max_vsi = max(result)
    print('MAX VSI is',max_vsi,'at open line',result.index(max_vsi)+1)
    print(result)
    
    
def retrive_ALL_LMN(numRange):
    caseDict = {}
    for lineIdx in range(1,numRange+1):
        counter = 0
        case_lmn = []
        fp = openCSVFile(lineIdx)
        csv_file = csv.reader(fp)
        for row in csv_file:
            counter += 1
            if counter > 2:
                case_lmn.append(float(row[LMN]))

        if lineIdx not in caseDict:
            caseDict[lineIdx] = deepcopy(case_lmn)
        fp.close()
#     print(len(case_lmn),case_lmn[numRange-1],case_lmn)
#     for key in caseDict:
#         print(key,caseDict[key])
    return caseDict


def compare_ALL_oneLMN_avg_ratio(numRange,en_plot=False):
    normal_lmn = []
    normalized_lmn = []
    counter = 0
    for row in csv_f:
        counter += 1
        if counter > 2:
            normal_lmn.append(float(row[LMN]))
            normalized_lmn.append(float(row[LMN])/100)
    print(normal_lmn)
    lmnDict = retrive_ALL_LMN(numRange)
    
    avg_ratio_list = []
    for i in range(numRange):
        normal_val = normal_lmn[i]
        total_result = 0
        for dictKey in lmnDict:
            case_val = lmnDict[dictKey][i]
            if case_val <= normal_val:
                continue
            else:
                temp = (case_val - normal_val)/normal_val*100
                total_result += temp
        avg_ratio_list.append(total_result/numRange)
        
    print(avg_ratio_list)
    if en_plot == True:
        xData = []
        for k in range(numRange):
            xData.append(k+1)
        napf.myPlotFig(xData, avg_ratio_list, 'VSI Average Increasing Ratio For Each Line', 'Line ID', \
                       'Average Increasing Percentage (%)',figLabel='Avg VSI Increment',yMax='',yMin='')
#         napf.myPlotFig(xData, normalized_lmn, 'VSI for Each Line (Normal Case)', 'Line ID', 'VSI')


def compare_ALL_affected_LMN(numRange,en_plot=False):
    normal_lmn = []
    counter = 0
    for row in csv_f:
        counter += 1
        if counter > 2:
            normal_lmn.append(float(row[LMN]))
    
    lmnDict = retrive_ALL_LMN(numRange)
    print(normal_lmn)
    print(lmnDict[1])
    avg_ratio_list = []
    avg_value_list = []
    for dictKey in lmnDict:
        total_value = 0
        total_ratio = 0
        for i in range(numRange):
            normal_val = normal_lmn[i]
            case_val = lmnDict[dictKey][i]
            if case_val <= normal_val:
                continue
            else:
                temp_value = case_val - normal_val
                temp_ratio = (case_val - normal_val)/normal_val*100
                total_value += temp_value
                total_ratio += temp_ratio
        avg_value_list.append(total_value/numRange/100)
        avg_ratio_list.append(total_ratio/numRange)
    
    if en_plot == True:
        xData = []
        for k in range(numRange):
            xData.append(k+1)
        napf.myPlotFig(xData, avg_value_list, 'VSI Average Increasing For Certain Line Tripped', 'Tripped Line ID', \
                       'Average Increasing Value',figLabel='Avg VSI Increment',yMax='',yMin='')
        napf.myPlotFig(xData, avg_ratio_list, 'VSI Average Increasing Ratio For Certain Line Tripped', \
            'Tripped Line ID', 'Average Increasing Percentage (%)',figLabel='Avg VSI Ratio Increment',yMax='',yMin='')
    


def compare_ALL_LMN(numRange):
    normal_lmn = []
    counter = 0
    incmt_num_list = []
    larger_than_one_list = []
    
    for row in csv_f:
        counter += 1
        if counter > 2:
            normal_lmn.append(float(row[LMN]))
            
    for lineIdx in range(1,numRange+1):
        counter = 0
        case_lmn = []
        fp = openCSVFile(lineIdx)
        csv_file = csv.reader(fp)
        for row in csv_file:
            counter += 1
            if counter > 2:
                case_lmn.append(float(row[LMN]))
                        
        fp.close()
        increase = 0
        for j in range(len(normal_lmn)):
            normal_val = normal_lmn[j]
            case_val = case_lmn[j]
            if normal_val < case_val:
                increase += 1
        
        incmt_num_list.append(increase)
        larger_than_one_list.append(sum(i>=1 for i in case_lmn))
    print(incmt_num_list)
    xData = []
    for k in range(numRange):
        xData.append(k+1)
    
    napf.myPlotFig(xData, incmt_num_list, 'Number of Lines with VSI Increasing Under Different Tripping', \
                   'Line ID', 'Increasing Number', yMax=25, yMin=0)
    napf.myPlotFig(xData, larger_than_one_list, 'Number of VSI Larger than 1 Under Different Tripping', 'Line ID', \
                   'Number with VSI > 1', yMax=34, yMin=0)



csv_fp = openCSVFile(34)
print("Max LMN is:",checkMaxLMN(csv_fp))
# checkCSVFile(csv_fp)
# for i in range(1,5):
#     print(i)
retrive_All_MAX_LMN(34)
# compare_LMN(5)
# f.seek(0)
# compare_ALL_LMN(34)
# retrive_ALL_LMN(34)
# compare_ALL_oneLMN_avg_ratio(34,True)
compare_ALL_affected_LMN(34,True)