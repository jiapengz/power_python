'''
Created on Mar 9, 2015

@author: Zhang Jiapeng
'''
import numeric_analysis.plotFunction as napf
import math
import random
from scipy.special import comb
from decimal import Decimal
from builtins import range
from itertools import product

B16_T_AVG_21 = [0.0405, 0.0584, 0.0731, 0.091, 0.1121, 0.1396, 0.1679]
B16_T_MAX_21 = [0.2917, 0.3295, 0.3552, 0.383, 0.5246, 0.7493, 0.9887]
B16_T_AVG_19 = [0.0443, 0.0577, 0.0724, 0.0889, 0.1089, 0.1332, 0.172, 0.3069]
B16_T_MAX_19 = [0.2797, 0.292, 0.3051, 0.3192, 0.3846, 0.4816, 0.6258, 1.5683]
B16_WT_AVG_21 = [0.0418, 0.0533, 0.067, 0.083, 0.1016, 0.124, 0.1498]
B16_WT_MAX_21 = [0.2985, 0.3239, 0.3504, 0.3784, 0.408, 0.4399, 0.4748]
B16_WT_AVG_19 = [0.0418, 0.0533, 0.067, 0.083, 0.1016, 0.124, 0.1498, 0.1724]
B16_WT_MAX_19 = [0.2985, 0.3239, 0.3504, 0.3784, 0.408, 0.4399, 0.4748, 0.5319]

B21_T_AVG = [0.0447, 0.0564, 0.074, 0.1525, 0.2042]
B21_T_MAX = [0.2898, 0.2951, 0.4963, 1.5345, 2.8001]
B21_WT_AVG = [0.0405, 0.0515, 0.0648, 0.0812, 0.0946]
B21_WT_MAX = [0.2897, 0.3079, 0.3276, 0.4026, 0.532]

B24_T_AVG = [0.0505, 0.0649, 0.0842, 0.1093, 0.1442, 0.1977, 0.2672]
B24_T_MAX = [0.3151, 0.3447, 0.3772, 0.4144, 0.4949, 0.8702, 1.5601]
B24_WT_AVG = [0.0468, 0.0612, 0.078, 0.0987, 0.1249, 0.1617, 0.2146]
B24_WT_MAX = [0.3069, 0.3312, 0.3575, 0.3865, 0.4718, 0.6358, 0.7646]

B28_T_AVG = [0.0443, 0.0495, 0.0552, 0.0616, 0.0686, 0.0768]
B28_T_MAX = [0.2932, 0.2959, 0.3658, 0.4791, 0.6151, 0.783]
B28_WT_AVG = [0.0395, 0.0435, 0.0488, 0.0549, 0.0616, 0.0691]
B28_WT_MAX = [0.2893, 0.2921, 0.295, 0.3076, 0.4103, 0.5333]

B29_T_AVG = [0.0467, 0.0497, 0.0531, 0.0566, 0.0603, 0.0643]
B29_T_MAX = [0.2979, 0.3558, 0.4189, 0.488, 0.5639, 0.6481]
B29_WT_AVG = [0.0378, 0.0407, 0.0438, 0.0471, 0.051, 0.055]
B29_WT_MAX = [0.2879, 0.2897, 0.2916, 0.2935, 0.2955, 0.2975]

B3_AVG = [0.0405, 0.0427, 0.0454, 0.0482, 0.0514, 0.0562, 0.0621, 0.0749, 0.0818, 0.097, \
          0.1053, 0.1351, 0.1468, 0.1751,0.2051]
B3_MAX = [0.3096, 0.3225, 0.3355, 0.3488, 0.3624, 0.3763, 0.3904, 0.4197, 0.435, 0.4669, \
          0.4875, 0.6998, 0.7928, 1.0448, 1.1503]

B4_AVG = [0.0378, 0.0389, 0.0409, 0.0431, 0.0468, 0.0509, 0.0558, 0.0668, 0.0727, 0.0855, \
          0.0929, 0.1116, 0.1211, 0.143, 0.1716]
B4_MAX = [0.2889, 0.315, 0.3414, 0.3681, 0.3952, 0.4226, 0.4505, 0.5076, 0.5369, 0.5976, \
          0.6291, 0.9441, 0.9777, 1.05, 1.1342]

B7_AVG = [0.0386, 0.0402, 0.0424, 0.0456, 0.0495, 0.0546, 0.0599, 0.0714, 0.0726, 0.0852, \
          0.093, 0.1212, 0.1333, 0.1676, 0.2025]
B7_MAX = [0.3269, 0.3637, 0.4009, 0.4385, 0.4766, 0.5153, 0.5546, 0.6353, 0.8923, 0.9724, \
          1.0141, 1.1494, 1.2003, 1.3172, 1.408]

B8_AVG = [0.038, 0.039, 0.041, 0.0433, 0.0466, 0.0508, 0.0558, 0.0674, 0.074, 0.0799, 0.0871, \
          0.1126, 0.1228, 0.149, 0.1834]
B8_MAX = [0.2931, 0.3281, 0.3634, 0.3992, 0.4354, 0.4721, 0.5093, 0.5857, 0.625, 0.9199, 0.9591, \
          1.0847, 1.1306, 1.2349, 1.3414]

B12_AVG = [0.0425, 0.0475, 0.0543, 0.0622, 0.0717, 0.0833, 0.0983, 0.1242, 0.1341, 0.1465, 0.1505, \
           0.1605, 0.1627, 0.1665, 0.1829]
B12_MAX = [0.3166, 0.3469, 0.3792, 0.4336, 0.5333, 0.6423, 0.7671, 0.9442, 0.9838, 1.0008, 1.0136, \
           1.0715, 1.0855, 1.1083, 1.2845]

B18_AVG = [0.0419, 0.0454, 0.0493, 0.0537, 0.0585, 0.0634, 0.0692, 0.085, 0.0939, 0.1137, 0.125, 0.1609, \
           0.1793, 0.2163, 0.2367]
B18_MAX = [0.3023, 0.3129, 0.3237, 0.3348, 0.3463, 0.358, 0.3701, 0.3956, 0.4092, 0.4963, 0.547, 0.572, \
           0.6068, 0.7216, 0.8076]

NC_AVG = [B3_AVG,B4_AVG,B7_AVG,B8_AVG,B12_AVG,B18_AVG]
NC_MAX = [B3_MAX,B4_MAX,B7_MAX,B8_MAX,B12_MAX,B18_MAX]

p_z0 = {1:0.4, 2:0.3, 3:0.2, 4:0.1}

def B28_figure(reqIdxList):
    avgList = []
    maxList = []
    avgList.append(B28_T_AVG)
    avgList.append(B28_WT_AVG)
    maxList.append(B28_T_MAX)
    maxList.append(B28_WT_MAX)
    
    for myavglist in NC_AVG:
        temp = []
        for idx in reqIdxList:
            temp.append(myavglist[idx-1])
        avgList.append(temp)
        
    for mymaxlist in NC_MAX:
        temp = []
        for idx in reqIdxList:
            temp.append(mymaxlist[idx-1])
        maxList.append(temp)
        
    xData = ["200","300","400","500","600","700"]
    labelList = ["Bus28-trip","Bus28-non-trip","Bus3","Bus4","Bus7","Bus8","Bus12","Bus18"]
    napf.myPlotFig2_List(xData, avgList, "Average VSI Trend", "Load (MVar)", "Number", labelList)
    napf.myPlotFig2_List(xData, maxList, "Max VSI Trend", "Load (MVar)", "Number", labelList)
    

def B21_figure(reqIdxList):
    avgList = []
    maxList = []
    avgList.append(B21_T_AVG)
    avgList.append(B21_WT_AVG)
    maxList.append(B21_T_MAX)
    maxList.append(B21_WT_MAX)
    
    for myavglist in NC_AVG:
        temp = []
        for idx in reqIdxList:
            temp.append(myavglist[idx-1])
        avgList.append(temp)
        
    for mymaxlist in NC_MAX:
        temp = []
        for idx in reqIdxList:
            temp.append(mymaxlist[idx-1])
        maxList.append(temp)
        
    xData = ["200","500","800","1100","1300"]
    labelList = ["Bus21-trip","Bus21-non-trip","Bus3","Bus4","Bus7","Bus8","Bus12","Bus18"]
    napf.myPlotFig2_List(xData, avgList, "Average VSI Trend", "Load (MVar)", "Number", labelList)
    napf.myPlotFig2_List(xData, maxList, "Max VSI Trend", "Load (MVar)", "Number", labelList)
    
    
def B24_figure(reqIdxList):
    avgList = []
    maxList = []
    avgList.append(B24_T_AVG)
    avgList.append(B24_WT_AVG)
    maxList.append(B24_T_MAX)
    maxList.append(B24_WT_MAX)
    
    for myavglist in NC_AVG:
        temp = []
        for idx in reqIdxList:
            temp.append(myavglist[idx-1])
        avgList.append(temp)
        
    for mymaxlist in NC_MAX:
        temp = []
        for idx in reqIdxList:
            temp.append(mymaxlist[idx-1])
        maxList.append(temp)
        
    xData = ["200","500","800","1100","1400","1700","2000"]
    labelList = ["Bus24-trip","Bus24-non-trip","Bus3","Bus4","Bus7","Bus8","Bus12","Bus18"]
    napf.myPlotFig2_List(xData, avgList, "Average VSI Trend", "Load (MVar)", "Number", labelList)
    napf.myPlotFig2_List(xData, maxList, "Max VSI Trend", "Load (MVar)", "Number", labelList)
    
    
def B16_L19_figure(reqIdxList):
    avgList = []
    maxList = []
    avgList.append(B16_T_AVG_19)
    avgList.append(B16_WT_AVG_19)
    maxList.append(B16_T_MAX_19)
    maxList.append(B16_WT_MAX_19)
    
    for myavglist in NC_AVG:
        temp = []
        for idx in reqIdxList:
            temp.append(myavglist[idx-1])
        avgList.append(temp)
        
    for mymaxlist in NC_MAX:
        temp = []
        for idx in reqIdxList:
            temp.append(mymaxlist[idx-1])
        maxList.append(temp)
        
    xData = ["200","500","800","1100","1400","1700","2000","2200"]
    labelList = ["Bus16-trip-19","Bus16-non-trip-19","Bus3","Bus4","Bus7","Bus8","Bus12","Bus18"]
    napf.myPlotFig2_List(xData, avgList, "Average VSI Trend", "Load (MVar)", "Number", labelList)
    napf.myPlotFig2_List(xData, maxList, "Max VSI Trend", "Load (MVar)", "Number", labelList)


def compare_probability_combination(relay_num, atk_rsc):
    if atk_rsc > relay_num:
        print("N should be smaller than R")
        return
    
    pf = 0.0
    lower_b = min(math.ceil(atk_rsc/2), int(relay_num/2))
    upper_b = min(atk_rsc, int(relay_num/2))
    # Probability approach
    temp = 0
    for i in range(lower_b, upper_b+1):
        a1 = upper_b - lower_b + 1
        temp += ((atk_rsc-i) * (1 - math.pow(pf, 2)) + (2*i - atk_rsc) * (1 - pf)) / a1
    
    # Combination approach
    temp_1 = 0
    for i in range(lower_b, upper_b+1):
        v1 = comb(int(relay_num/2), (2*i - atk_rsc))
        v2 = math.pow(2, (2*i - atk_rsc))
        v3 = comb((int(relay_num/2)-(2*i - atk_rsc)), (atk_rsc - i))
        v4 = comb(relay_num, atk_rsc)
        
        temp_2 = (v1 * v2 * v3) / v4 # probability of i lines tripped
        temp_1 += temp_2 * ((atk_rsc-i) * (1 - math.pow(pf, 2)) + (2*i - atk_rsc) * (1 - pf)) 
        #print(v1, v2, v3, temp_2)
    
    print("Probability approach:",temp,"   Combination approach:",temp_1, atk_rsc/relay_num)
    
    return temp, temp_1


# compute the probability of "i" lines are attacked
# with the resource N, each specific "i" has a fixed attack style
# e.g., "i1" lines with one relay compromised, "i2" lines with two relays compromised
# then compute the expected number of line trips when "i" lines are attacked
def random_i_lines_atk_expect_num(R, N, df):
    if N > R:
        print("N should be smaller than R")
        return
    
    pf = df
    lower_b = min(math.ceil(N/2), int(R/2))
    upper_b = min(N, int(R/2))
    
    temp_1 = 0
    for i in range(lower_b, upper_b+1):
        v1 = comb(int(R/2), (2*i - N))
        v2 = math.pow(2, (2*i - N))
        v3 = comb((int(R/2)-(2*i - N)), (N - i))
        v4 = comb(R, N)
        
        p_i = (v1 * v2 * v3) / v4 # probability of i lines tripped
        exp_trip_i_line_atk = ((N-i) * (1 - math.pow(pf, 2)) + (2*i - N) * (1 - pf)) 
        temp_1 += p_i * exp_trip_i_line_atk
        #print(v1, v2, v3, temp_2)
    
    print("Expected number with combination approach:",temp_1, N/R)
    
    return temp_1


def random_i_line_atk_prob(R, N, i):
    if N > R:
        print("N should be smaller than R")
        p_i = -1
    
    else:
        v1 = comb(int(R/2), (2*i - N))
        v2 = math.pow(2, (2*i - N))
        v3 = comb((int(R/2)-(2*i - N)), (N - i))
        v4 = comb(R, N)
        
        p_i = (v1 * v2 * v3) / v4 # probability of i lines tripped
    
    return p_i


# E(number of tripped lines with i lines attacked)
def random_i_line_atk_exp_trip_num(N, i, df):
    pf = df
    
    exp_trip_i_line_atk = ((N-i) * (1 - math.pow(pf, 2)) + (2*i - N) * (1 - pf)) 
    
    return exp_trip_i_line_atk


# R: total relay number
# N: total attack resource
# df: defense strength (failure probability of the attack)
# L_f: length of the initial cascading line set
# E(number of local line tripped with N_A  resources)
def random_cascading_prob(R, N, df, L_f):
    if N > R:
        print("N should be smaller than R")
        return
    
    pf = df
    lower_b = min(math.ceil(N/2), int(R/2))
    upper_b = min(N, int(R/2))
    
    temp = 0
    for i in range(lower_b, upper_b+1):
        E_trip_i_atk = int(random_i_line_atk_exp_trip_num(N, i, pf))
        P_i_line_atk = random_i_line_atk_prob(R, N, i)
        
        temp1 = comb((int(R/2) - L_f), (E_trip_i_atk - L_f))
        temp2 = comb(int(R/2), E_trip_i_atk)
        P_cascading_trigger_i_line_atk = temp1 / temp2
#         temp += E_trip_i_atk * P_i_line_atk
        temp += P_i_line_atk * P_cascading_trigger_i_line_atk
    
    return temp


# compute the average tripped backup lines of a relay
def random_bp_trip_num_avg(R, N, deg_avg, df):
    if N > R:
        print("N should be smaller than R")
        return
    
    pf = df
    upper_b = min(N, (2 * deg_avg - 1))
    
    P_no_relay_in_area = 0
    for j in range(0, upper_b+1):
        v1 = comb((2 * deg_avg - 1), j)
        v2 = comb((R - 2 * deg_avg), (N - j - 1))
        v3 = comb((R-1), (N-1))
        
        if v3 == 0: # if N=1
            P_j_relay_selected = 0
        else:
            P_j_relay_selected = v1 * v2 / v3
        P_j_attack_failed = math.pow(pf, j)
        
        P_no_relay_in_area += P_j_attack_failed * P_j_relay_selected
    
    E_bp_trip_num = (deg_avg - 1) * P_no_relay_in_area


def area_attack_e_selected_bp_line(R, NA, d):
    if NA > R:
        print("NA should be smaller than R")
        return
    
    attacked_line = math.floor(NA/2)
    max_bp_num = min(attacked_line, 2*(d-1))
    
    e_line_select = 0
    for nb in range(1, max_bp_num+1):
        v1 = comb(2*(d-1), nb)
        v2 = comb(int(R/2-2*(d-1)), math.floor(NA/2)-nb)
        v3 = comb(int(R/2), math.floor(NA/2))
        
        prob = v1 * v2 / v3
        e_line_select += prob * nb
    
    print("Averaged selected line number:",e_line_select)


def total_cas_no_bt_distribution(r, ld):
    global p_z0
    total_p = 0
    
    for z0 in range(1, r+1):
#         if z0 in p_z0:
#             p = p_z0[z0]
#         else:
#             p = 0
        if z0 <= 10:
            p = 0.1
        else:
            p = 0
        
        total_p = p * z0 * ld * math.pow(r*ld, r-z0-1) * math.exp(-r * ld)/math.factorial(r-z0)
        #print("z0 =",z0,"   r =",r,"   p =",p)
    print("r =",r,"   P(r) =",total_p)
    
    return total_p


def test():
    temp_list1 = []
    temp_list2 = []
    result_list = []
    x_list = []
    R = 24
    
    for i in range(2,25,1):
        pro_app, comb_app = compare_probability_combination(R, i)
        temp_list1.append(pro_app)
        temp_list2.append(comb_app)
        x_list.append(round(i/R*100, 3))
    
    result_list.append(temp_list1)
    result_list.append(temp_list2)
    
    napf.myPlotFig_List_WithX(x_list, result_list, "Compare of two computings", \
                         "Ratio N/R (%)", "E(tripped local line number)", ["compute1","compute2"], ymax=13, ymin=0)


def random_attack_line_trip(na, pf, total_line, rsc_ini, rsc_step):
    temp_list1 = []
    result_list = []
    x_list = []
    R = total_line * 2
    
    for i in range(rsc_ini, na+3, rsc_step):
        pro_app = random_i_lines_atk_expect_num(R, i, pf)
        pro_app = Decimal("%.2f" % pro_app)
        pro_app = float(pro_app)
        temp_list1.append(round(pro_app, 2))
        x_list.append(round(i/R*100, 3))
    
    print(temp_list1)
    result_list.append(temp_list1)
    
    napf.myPlotFig_List_WithX(x_list, result_list, "Expected number of tripped local line", \
                         "Ratio N/R (%)", "E(tripped local line number)", ["compute1"], ymax=total_line, ymin=0)


def test3():
    temp_list1 = []
    result_list = []
    x_list = []
    R = 24
    
    for i in range(2,25,1):
        pro_cascading = random_cascading_prob(R, i, 0.0, 3)
        temp_list1.append(pro_cascading)
        x_list.append(round(i/R*100, 3))
    
    result_list.append(temp_list1)
    
    napf.myPlotFig_List_WithX(x_list, result_list, "Expected number of tripped local line", \
                         "Ratio N/R (%)", "E(tripped local line number)", ["compute1"], ymax=1, ymin=0)


def test_expect_bt_distribution(max_r, ld):
    rt = 0
    for i in range(max_r):
        rt += i * total_cas_no_bt_distribution(i, ld)
    
    print("Expectation of cascading number in bt distribution:",rt)


def area_attack_total_trip(na, k, d_avg, pf, total_line):
    total_attack_line = math.floor(na/2/k)
    avg_neighbor = 2*(d_avg-1)
    print("total_attack_line",total_attack_line)
    
    cnt = 0
    E_x = 0
    # for a line, there will be other (total_attack_line-1) lines to be placed
    # for all neighbor lines to be placed, there should be at least (2*d_avg - 1) lines to be attacked
    for x in range(1, avg_neighbor+1):
        if x >= total_attack_line:
            continue
        
        cnt += 1
        C_neighbor = comb(avg_neighbor, x) * comb(total_line-avg_neighbor-1, total_attack_line-x-1)
        C_total = comb(total_line-1, total_attack_line-1)
        
        if C_total == 0:
            p_x = 0
        else:
            p_x = C_neighbor/C_total
        print("x=",x,C_neighbor,C_total,p_x)
        E_x += p_x * x
    
    E_area_trip = 2 * pf * (1-pf) + (2 * d_avg - 1) * math.pow(1-pf, 2)
    E_ol_trip = E_x * (1 - math.pow(pf**k, 2))
    print(E_area_trip,E_ol_trip)
    E_effective_area_trip = (E_area_trip - E_ol_trip/max(cnt,1)) * total_attack_line
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    elif E_effective_area_trip < 0:
        E_effective_area_trip = 0
    
    return E_effective_area_trip


def area_attack_total_trip_v2(na, k, d_avg, pf, total_line):
    total_attack_line = math.floor(na/2/k)
    avg_neighbor = 2*(d_avg-1)
    #print("total_attack_line",total_attack_line)
    
    cnt = 0
    E_x = 0
    # for a line, there will be other (total_attack_line-1) lines to be placed
    # for all neighbor lines to be placed, there should be at least (2*d_avg - 1) lines to be attacked
    for x in range(1, avg_neighbor+1):
        if x >= total_attack_line:
            continue
        
        cnt += 1
        C_neighbor = comb(avg_neighbor, x) * comb(total_line-avg_neighbor-1, total_attack_line-x-1)
        C_total = comb(total_line-1, total_attack_line-1)
        
        if C_total == 0:
            p_x = 0
        else:
            p_x = C_neighbor/C_total
        #print("x=",x,C_neighbor,C_total,p_x)
        E_x += p_x * x
    print(E_x)
    E_both_pr_trip = (2 * d_avg - 1) * math.pow(1-pf**k, 2)
    E_ol_trip = E_x * (1 - math.pow(pf**k, 2))
    E_effective_both_pr_trip = (E_both_pr_trip - E_ol_trip)
    
    E_no_pr_trip = (pf**k)**2 * E_x * (1 - math.pow(pf**k, 2))
    
    E_one_pr_trip = 0
    temp_list = []
    for i in range(total_attack_line+1):
        j = total_attack_line - i
        if i <= d_avg-1 and j <= d_avg-1:
            temp_list.append([i,j])
    
    temp_p = 0 if temp_list == [] else 1/len(temp_list)
    E_effective_one_pr_trip = 0
    for pair_i in temp_list:
        p_at_least_one_bp = 1-(pf**k)**total_attack_line
        E_one_pr_trip = (d_avg-1) * (1-pf) * pf * p_at_least_one_bp
        E_ol_left_side_trip = pair_i[0] * (1 - math.pow(pf**k, 2))
        E_effective_one_pr_trip += max((E_one_pr_trip - E_ol_left_side_trip), 0)
    
    #E_effective_one_pr_trip *= (2*temp_p)
    
    E_effective_area_trip = (E_effective_both_pr_trip + E_no_pr_trip + E_effective_one_pr_trip) * total_attack_line
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    elif E_effective_area_trip < 0:
        E_effective_area_trip = 0
    
    return E_effective_area_trip


def area_attack_total_trip_v3(na, k, d_avg, pf, total_line):
    total_attack_line = math.floor(na/2/k)
    avg_neighbor = 2*(d_avg-1)
    #print("total_attack_line",total_attack_line)
    
    cnt = 0
    E_x = 0
    # for a line, there will be other (total_attack_line-1) lines to be placed
    # for all neighbor lines to be placed, there should be at least (2*d_avg - 1) lines to be attacked
    for x in range(1, avg_neighbor+1):
        if x >= total_attack_line:
            continue
        
        cnt += 1
        C_neighbor = comb(avg_neighbor, x) * comb(total_line-avg_neighbor-1, total_attack_line-x-1)
        C_total = comb(total_line-1, total_attack_line-1)
        
        if C_total == 0:
            p_x = 0
        else:
            p_x = C_neighbor/C_total
        #print("x=",x,C_neighbor,C_total,p_x)
        E_x += p_x * x
    
    E_local_trip = total_attack_line * (1 - math.pow(pf**k, 2))
    
    E_neighbor_trip = (2 * (d_avg-1)) * math.pow(1-pf, 2)
    E_ol_trip = E_x * (1 - math.pow(pf**k, 2)) * math.pow(1-pf,2)
    #print(E_neighbor_trip,E_ol_trip)
    E_effective_neighbor_trip = max((E_neighbor_trip - E_ol_trip), 0)
    print(E_x, "E_neighbor:",E_neighbor_trip,"Effective neighbor:",E_effective_neighbor_trip)
    
    E_effective_area_trip = E_local_trip + E_effective_neighbor_trip * total_attack_line
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    elif E_effective_area_trip < 0:
        E_effective_area_trip = 0
    
    return E_effective_area_trip


def area_attack_total_trip_v4(na, k, d_avg, pf, total_line, rtn_result):
    total_attack_line = math.floor(na/2/k)
    avg_neighbor = 2*(d_avg-1)
    avg_two_pr_case = total_attack_line * math.pow((1-pf**k), 2)
    avg_one_pr_case = total_attack_line * (1-pf**k) * (pf**k) * 2
    
    print("total_attack_line:",total_attack_line,"rsc:",na,"two-pr areas:",avg_two_pr_case,"one-pr areas:",avg_one_pr_case)
#     avg_two_pr_case = total_attack_line
#     avg_one_pr_case = total_attack_line
    
    cnt = 0
    E_x = 0
    total_p_nb = 0
    p_nb_gt_one = 0
    # for a line, there will be other (total_attack_line-1) lines to be placed
    # for all neighbor lines to be placed, there should be at least (2*d_avg - 1) lines to be attacked
    temp_dict = {}
    for x in range(1, avg_neighbor+1):
        if x >= total_attack_line:
            continue
        
        cnt += 1
        C_neighbor = comb(avg_neighbor, x) * comb(total_line-avg_neighbor-1, total_attack_line-x-1)
        C_total = comb(total_line-1, total_attack_line-1)
        
        if C_total == 0:
            p_x = 0
        else:
            p_x = C_neighbor/C_total
        #print("x=",x,C_neighbor,C_total,p_x)
        total_p_nb += p_x
        E_x += p_x * x
        temp_dict[x] = p_x
        
        if x > 1:
            p_nb_gt_one += p_x
    
    E_local_trip = total_attack_line * (1 - math.pow(pf**k, 2))
    
#     E_effective_two_pr_neighbor_trip = 0
#     E_effective_neighbor_trip = 0
#     for key in temp_dict:
#         E_neighbor_trip = (2 * (d_avg-1)) * math.pow(1-pf, 2)
#         E_ol_trip = key * (1 - math.pow(pf**k, 2)) * math.pow(1-pf,2)
#         
#         E_effective_neighbor_trip = max((E_neighbor_trip - E_ol_trip), 0)
#         E_effective_two_pr_neighbor_trip += (E_effective_neighbor_trip * temp_dict[key])
    
    E_neighbor_trip = (2 * (d_avg-1)) * math.pow(1-pf**k, 2)
    E_ol_trip = E_x * (1 - math.pow(pf**k, 2)) * math.pow(1-pf**k,2)
    #print(E_neighbor_trip,E_ol_trip)
    E_effective_neighbor_trip = max((E_neighbor_trip - E_ol_trip), 0)
    E_effective_two_pr_neighbor_trip = E_effective_neighbor_trip
    print("E(nb):", E_x, "P(neighbor) =", total_p_nb, "P(x>1) =", p_nb_gt_one)
    print("All nb probability:", temp_dict)
    print("Two-PR-relay E_neighbor:",E_neighbor_trip,"Effective neighbor:",E_effective_neighbor_trip)
    print("Key:",[temp_dict[key] for key in temp_dict])
    
    
    E_effective_one_pr_neighbor_trip = 0
    for key in temp_dict:
        temp_list = []
        
        # find the potential cases of how many neighbor lines in left and right side (nl, nr)
        for i in range(key+1):
            j = key - i
            if i <= d_avg-1 and j <= d_avg-1 and (total_attack_line>1):
                temp_list.append([i,j])
        
        print("Key =",key,"Neighbor line combinations:", temp_list)
        # temp_p is the probability for a single (nl, nr) case
        temp_p = 0 if temp_list == [] else 1/len(temp_list)
        #print("Totally", len(temp_list), "cases, each has probability of", temp_p)
        
        # for each of the (nl, nr) cases, compute the expected area neighbor trip when only one
        # primary relay is compromised
        E_effective_neighbor_trip_for_x = 0
        for pair_i in temp_list:
	    # probability that at least one backup relay in the attacked backup lines to be tripped
            p_at_least_one_bp = 1-(pf**k)**(pair_i[0]+pair_i[1])
            
            E_right_side_trip = (1 - math.pow(pf**k, 2)) * pair_i[1]
            E_left_side_trip = (d_avg-1) * p_at_least_one_bp
            
            E_ol_left_side_trip = pair_i[0] * (1 - math.pow(pf**k, 2))
            E_effective_left_side_trip = max((E_left_side_trip - E_ol_left_side_trip), 0)
            E_effective_neighbor_trip_for_x += ((E_effective_left_side_trip + E_right_side_trip) * temp_p)
        
        E_effective_neighbor_trip_for_x *= ((1-pf**k) * (pf**k) * 2 * temp_dict[key])
        E_effective_one_pr_neighbor_trip += E_effective_neighbor_trip_for_x
    
    E_neighbor_trip_two_pr = round(E_effective_two_pr_neighbor_trip * total_attack_line, 3)
    E_neighbor_trip_one_pr = round(E_effective_one_pr_neighbor_trip * total_attack_line, 3)
    
    E_neighbor_trip_two_pr = min(E_neighbor_trip_two_pr, avg_two_pr_case * 2 * (d_avg-1))
    E_neighbor_trip_one_pr = min(E_neighbor_trip_one_pr, avg_one_pr_case * (d_avg-1))
    
    E_effective_area_trip = E_local_trip + E_neighbor_trip_two_pr + E_neighbor_trip_one_pr
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    elif E_effective_area_trip < 0:
        E_effective_area_trip = 0
    
    rtn_result["neighbor_trip_two_pr"].append(E_neighbor_trip_two_pr)
    rtn_result["neighbor_trip_single_pr"].append(E_neighbor_trip_one_pr)
    rtn_result["E_neighbor"].append(E_x)
    return E_effective_area_trip


def area_attack_total_trip_v5(na, k, d_avg, pf, total_line, rtn_result):
    total_attack_line = math.floor(na/2/k)
    avg_neighbor = 2*(d_avg-1)
    avg_two_pr_case = total_attack_line * math.pow((1-pf**k), 2)
    avg_one_pr_case = total_attack_line * (1-pf**k) * (pf**k) * 2
    
    print("total_attack_line:",total_attack_line,"rsc:",na,"two-pr areas:",avg_two_pr_case,"one-pr areas:",avg_one_pr_case)
#     avg_two_pr_case = total_attack_line
#     avg_one_pr_case = total_attack_line
    
    cnt = 0
    E_x = 0
    total_p_nb = 0
    p_nb_gt_one = 0
    
    # for a line, there will be other (total_attack_line-1) lines to be placed
    # for all neighbor lines to be placed, there should be at least (2*d_avg - 1) lines to be attacked
    temp_dict = {}
    for x in range(1, avg_neighbor+1):
        if x >= total_attack_line:
            continue
        
        cnt += 1
        C_neighbor = comb(avg_neighbor, x) * comb(total_line-avg_neighbor-1, total_attack_line-x-1)
        C_total = comb(total_line-1, total_attack_line-1)
        
        if C_total == 0:
            p_x = 0
        else:
            p_x = C_neighbor/C_total
        #print("x=",x,C_neighbor,C_total,p_x)
        total_p_nb += p_x
        E_x += p_x * x
        temp_dict[x] = p_x
        
        if x > 1:
            p_nb_gt_one += p_x
    
    
    # average local line trip - the lower bound of the area attack
    E_local_trip = total_attack_line * (1 - math.pow(pf**k, 2))
    
    #E_neighbor_trip = (2 * (d_avg-1)) * math.pow(1-pf**k, 2)
    E_neighbor_trip = 2 * (d_avg-1)
    #E_ol_trip = E_x * (1 - math.pow(pf**k, 2)) * math.pow(1-pf**k,2)
    E_ol_trip = E_x * (1 - math.pow(pf**k, 2))
    #print(E_neighbor_trip,E_ol_trip)
    E_effective_neighbor_trip = max((E_neighbor_trip - E_ol_trip), 0)
    E_effective_two_pr_neighbor_trip = E_effective_neighbor_trip
    print("E(nb):", E_x, "P(neighbor) =", total_p_nb, "P(x>1) =", p_nb_gt_one)
    print("All nb probability:", temp_dict)
    print("Two-PR-relay E_neighbor:",E_neighbor_trip,"Effective neighbor:",E_effective_neighbor_trip)
    print("Key:",[temp_dict[key] for key in temp_dict])
    
    #E_effective_two_pr_neighbor_trip = 0
    E_effective_one_pr_neighbor_trip = 0
    
    
    E_neighbor_trip_two_pr = round(E_effective_two_pr_neighbor_trip * avg_two_pr_case, 3)
    E_neighbor_trip_one_pr = round(E_effective_one_pr_neighbor_trip * avg_one_pr_case, 3)
    
    E_effective_area_trip = E_local_trip + E_neighbor_trip_two_pr + E_neighbor_trip_one_pr
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    elif E_effective_area_trip < 0:
        E_effective_area_trip = 0
    
    rtn_result["neighbor_trip_two_pr"].append(E_neighbor_trip_two_pr)
    rtn_result["neighbor_trip_single_pr"].append(E_neighbor_trip_one_pr)
    rtn_result["local_trip"].append(E_local_trip)
    rtn_result["E_neighbor"].append(E_x)
    return E_effective_area_trip


def test_area_total_trip(max_rsc, k, d_avg, pf, total_line, rsc_step=10, enplot=False, test_bound=False):
    result_list = []
    result_list_it = []
    local_list = []
    x_axis = []
    x_axis_success_pcnt = []
    rtn_data = {}
    
    rtn_data["neighbor_trip_two_pr"] = []
    rtn_data["neighbor_trip_single_pr"] = []
    rtn_data["E_neighbor"] = []
    rtn_data["local_trip"] = []
    
    for rsc in range(2, max_rsc+3, rsc_step):
        #area_trip = area_attack_total_trip(rsc, k, d_avg, pf, total_line)
        #area_trip = area_attack_total_trip_v2(rsc, k, d_avg, pf, total_line)
        #area_trip = area_attack_total_trip_v3(rsc, k, d_avg, pf, total_line)
        #area_trip = area_attack_total_trip_v4(rsc, k, d_avg, pf, total_line, rtn_data)
        area_trip = area_attack_total_trip_v5(rsc, k, d_avg, pf, total_line, rtn_data)
        result_list.append(round(area_trip,3))
        
        area_trip_it = area_attack_total_trip_it(rsc, d_avg, pf, total_line, rtn_data)
        result_list_it.append(area_trip_it)
        
        area_local_trip = area_attack_local_trip(rsc, pf, k, total_line)
        local_list.append((area_local_trip))
        
        x_axis.append(rsc)
        x_axis_success_pcnt.append(round((1-(pf**k)**2) * (rsc/(2*k)) / (total_line) * 100, 3))
    
    print("Area trip:", result_list)
    print("Two-pr-relay neighbor trip:", rtn_data["neighbor_trip_two_pr"])
    print("Single-pr-relay neighbor trip:", rtn_data["neighbor_trip_single_pr"])
    print("Local trip:", rtn_data["local_trip"])
    print("E(neighbor):",rtn_data["E_neighbor"])
    print("Percentage of successful trip:", x_axis_success_pcnt)
    
    if enplot == True:
        temp = []
        temp.append(result_list)
        
        if test_bound == True:
            temp_add = []
            computed_nb_list = []
            weight_list = []
            max_area_line = 2*d_avg-2
            for i in range(len(result_list)):
                computed_nb_list.append(result_list_it[i] - local_list[i])
            
            #for i in range(len(computed_nb_list)):
            #   if i > 0 and computed_nb_list[i] < computed_nb_list[i-1]:
            #        computed_nb_list[i] = computed_nb_list[i-1]
            
            for i in range(len(result_list)):
                neighbor_weight = 1/((rtn_data["E_neighbor"][i])+1)
                #neighbor_weight = math.ceil(rtn_data["E_neighbor"][i])/(2*d_avg-2)
                neighbor_weight = (rtn_data["E_neighbor"][i])/(2*d_avg-2)
                #neighbor_weight = 0.3
                #neighbor_weight = rtn_data["E_neighbor"][i]/(2*d_avg-2)*0.25
                #local_weight = max(neighbor_weight,1/max_area_line)
                #local_weight = 1/((rtn_data["E_neighbor"][i])+1)
                #local_weight *= 1-(math.pow(pf**k, 2))
                weight_list.append(round(neighbor_weight,5))
                #temp_bound = (1-local_weight) * result_list_it[i] + (local_weight) * local_list[i]
                neighbor_weight *= (1-(math.pow(pf**k, 2)))
                temp_bound = (1-neighbor_weight) * computed_nb_list[i] + 1 * local_list[i]
                #temp_bound = computed_nb_list[i] / ((rtn_data["E_neighbor"][i])+1) + 1 * local_list[i]
                temp_add.append(temp_bound)
            
            print("Neighbor weight:",weight_list)
            print("Average neighbor weight:",sum(weight_list)/len(weight_list))
            print("Computed one-round bound:",temp_add)
            temp.append(temp_add)
            napf.myPlotFig_List_WithX(x_axis, temp, "Area attack trip", "Resource", "E(line trip)", \
                                  ["Area attack","Test Area Bound"], ymax=total_line, ymin=0)
        else:
            napf.myPlotFig_List_WithX(x_axis, temp, "Area attack trip", "Resource", "E(line trip)", \
                                  ["Area attack"], ymax=total_line, ymin=0)


def area_attack_local_trip(na, pf, k, total_line):
    total_attack_line = math.floor(na/2/k)
    
    E_local_trip = (1-(pf**k)**2) * total_attack_line
    
    if E_local_trip > total_line:
        E_local_trip = total_line
    
    return E_local_trip


def area_attack_total_trip_it(na, d_avg, pf, total_line, rtn_result):
    average_attack_to_relay = 1/(1-pf)
    average_attack_to_line = average_attack_to_relay * 2 
    total_attack_line = (na/average_attack_to_line)
    avg_neighbor = 2*(d_avg-1)
    print("total_attack_line iterative:",total_attack_line,"rsc:",na)
    
    E_local_trip = total_attack_line
    E_neighbor_trip = total_attack_line * avg_neighbor
    E_effective_area_trip = E_local_trip + E_neighbor_trip
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    
    return E_effective_area_trip


def area_attack_total_trip_it_majority(na, d_avg, pf, total_line, rtn_result):
    average_attack_to_relay = 1/(1-pf)
    average_attack_to_line = average_attack_to_relay * (2+d_avg-1)
    total_attack_line = (na/average_attack_to_line)
    avg_neighbor = 2*(d_avg-1)
    print("total_attack_line:",total_attack_line,"rsc:",na)
    
    E_local_trip = total_attack_line
    E_neighbor_trip = total_attack_line * avg_neighbor
    E_effective_area_trip = E_local_trip + E_neighbor_trip
    
    if E_effective_area_trip > total_line:
        E_effective_area_trip = total_line
    
    return E_effective_area_trip


def test_area_total_trip_it(max_rsc, d_avg, pf, total_line, rsc_ini=2, rsc_step=10, mode="basic", enplot=False):
    result_list = []
    local_list = []
    x_axis = []
    x_axis_success_pcnt = []
    rtn_data = {}
    
    rtn_data["neighbor_trip_two_pr"] = []
    rtn_data["neighbor_trip_single_pr"] = []
    rtn_data["E_neighbor"] = []
    
    for rsc in range(rsc_ini, max_rsc+3, rsc_step):
        if mode == "basic":
            area_trip = area_attack_total_trip_it(rsc, d_avg, pf, total_line, rtn_data)
            area_local_trip = area_attack_local_trip(rsc, pf, 1, total_line)
        else:
            area_trip = area_attack_total_trip_it_majority(rsc, d_avg, pf, total_line, rtn_data)
            area_local_trip = area_attack_local_trip(rsc, pf, 1, total_line)
        
        result_list.append(round(area_trip,3))
        local_list.append(round(area_local_trip,3))
        
        x_axis.append(rsc)
        #x_axis_success_pcnt.append(round((1-(pf**k)**2) / (2*k) * (rsc/max_rsc) * 100, 3))
    
    print("Area trip:", result_list)
    print("Area local trip:", local_list)
    print("Rsc:", x_axis)
    #print("Two-pr-relay neighbor trip:", rtn_data["neighbor_trip_two_pr"])
    #print("Single-pr-relay neighbor trip:", rtn_data["neighbor_trip_single_pr"])
    #print("Percentage of successful trip:", x_axis_success_pcnt)
    
    if enplot == True:
        temp = []
        computed_nb_list = [(x - y) for x, y in zip(result_list, local_list)]
        temp.append(result_list)
        if len(result_list) == len(local_list):
            #temp_add = [(0.65*x + 0.35*y) for x, y in zip(result_list, local_list)]
            temp_add = [(0.75*x + 1*y) for x, y in zip(computed_nb_list, local_list)]
            temp.append(temp_add)
            print(temp_add)
        napf.myPlotFig_List_WithX(x_axis, temp, "Area attack trip", "Resource", "E(line trip)", \
                                  ["Area attack","Test"], ymax=total_line, ymin=0)


def test_prob_cascading():
    p_f = 0.5
    n_l = 3
    
    result_list = []
    temp = []
    
    for k in range(1, 10):
        prob = p_trigger_cascading(p_f, n_l, k)
        result_list.append(prob)
    
    temp.append(result_list)
    napf.myPlotFig_List(temp, ["labelNameList"], "graphTitle", "x_label", "y_label", ymax=1, ymin=0)


# efficiency of the per-unit resource attack
# assume each initial line has one relay attacked
# the expected number of line trips due to cascading is computed
def test_e_trip_due_to_cascading(pf, nk, cas_no, cal_mode="total", y_max=10, enplot=False):
    p_f = pf
    n_k = nk
    cas_num = cas_no
    repeat_no = 10
    
    result_list = []
    
    for k in range(1, repeat_no+1):
        prob = p_trigger_cascading(p_f, n_k, k)
        
        # in the extreme case, each successful attack needs (K) times
        if cal_mode == "total":
            atk_efficient = ((1-p_f**k)*n_k +  prob * cas_num) / (k * n_k)
        elif cal_mode == "cas":
            atk_efficient = prob * cas_num / (k * n_k)
        result_list.append(round(atk_efficient,2))
    
    print("Efficiency for different K:",result_list)
    
    if enplot == True:
        temp_x = [i for i in range(1, repeat_no+1)]
        temp = []
        temp.append(result_list)
        napf.myPlotFig_List_WithX(temp_x, temp, ["labelNameList"], "graphTitle", "x_label", "y_label", ymax=y_max, ymin=0)
    
    return result_list


# test multiple parameters for the number of line trips due to cascading
def multiple_test_e_trip_due_to_cascading():
    temp = []
    label_list = []
    
    for i in range(10):
        result = test_e_trip_due_to_cascading(i/10, 3, 10)
        temp.append(result)
        label_list.append("P_f="+str(i/10))
    
    napf.myPlotFig_List(temp, label_list, "Attack resource efficiency", "Repeat number", \
                        "Average trip per unit resource", ymax=4, ymin=0)


def p_trigger_cascading(p_f, n_l, k):
    prob_line_i = 1-(p_f)**(k)
    prob = prob_line_i ** n_l
    
    return prob


def P_trigger_cascading_reputation(pf_relay, pf_router, k_relay, k_router, n_k):
    prob_one_relay = (1-pf_relay**(k_relay))**2 * (1-pf_router**(k_router))
    prob = prob_one_relay ** n_k
    
    return prob


def P_trigger_cascading_reputation_persist(total_rsc, pf_relay, pf_router, n_k):
    
    # the (total_rsc) may not be completed used in the attack, while the successful attack must
    # use no larger than (total_rsc) resources
    assert total_rsc >= n_k * 3
    
    prob = 0
    relay_num = n_k * 2
    router_num = n_k
    # there are totally (3 * n_k) success in a successful attack, assume there are N attacks,
    # there are (2 * n_k) relay success and (n_k) router success
    # then there are (N - (3 * n_k)) failures distributed to relays (n_i) and routers (n_j)
    for atk_rsc in range(n_k*3, total_rsc+1):
        total_fail = atk_rsc - n_k * 3
        
        for n_i in range(0, total_fail+1):
            n_j = total_fail - n_i
            comb_relay = comb(n_i+2*n_k-1, n_i)
            comb_router = comb(n_j+n_k-1, n_j)
            success_prob = (pf_relay**n_i) * comb_relay * (pf_router**n_j) * comb_router *\
                                ((1-pf_relay)**relay_num) * ((1-pf_router)**router_num)
            prob += success_prob
    
    return prob


# there are totally N1=(relay_num + router_num) success in a successful attack, assume there are N attacks,
# then there are (N - N1) failures distributed to relays (n_i) and routers (n_j)
# in iterative attack, if the attack does not compromise all target relay, the resource must be used up
# if (relay_num) relays are successfully compromised, there may be at most (relay_num + 1) relays attacked
def P_i_relay_j_router(rsc, pf_relay, pf_router, relay_num, router_num):
    total_fail = rsc - relay_num - router_num
    
    prob = 0
    for n_i in range(0, total_fail+1):
        n_j = total_fail - n_i
        comb_relay = comb(n_i+relay_num-1, n_i)
        comb_router = comb(n_j+router_num-1, n_j)
        success_prob = (pf_relay**n_i) * comb_relay * (pf_router**n_j) * comb_router *\
                            ((1-pf_relay)**relay_num) * ((1-pf_router)**router_num)
        prob += success_prob
    
    return prob


# Iterative attack procedure in an area: first relay, then router
# if a relay is not compromised, the router will not be attacked
def P_incomplete_attack_N_area_trip(total_rsc, n_k, pf_relay, pf_router):
    result_dict = {}
    
    # probability that (ini_line) lines are tripped in an incomplete attack
    for ini_line in range(0, n_k):
        temp_prob = 0
        temp_prob += P_i_relay_j_router(total_rsc, pf_relay, pf_router, (2 * ini_line), ini_line)
        temp_prob += P_i_relay_j_router(total_rsc, pf_relay, pf_router, (2 * ini_line + 1), ini_line)
        temp_prob += P_i_relay_j_router(total_rsc, pf_relay, pf_router, (2 * ini_line + 2), ini_line)
        
        if ini_line not in result_dict:
            result_dict[ini_line] = temp_prob
    
    return result_dict


def P_trigger_cascading_area(p_f, n_l, k, n_majority):
    prob_line_i = (1-(p_f)**k)**n_majority
    prob = prob_line_i ** n_l
    
    return prob


def P_trigger_cascading_hf(pf_relay, k_relay, n_k, p_hf):
    #prob = 2 * (p_hf ** n_k) * ((1 - pf_relay**(k_relay))**(n_k))
    prob = ((1-(1-p_hf)**2) ** n_k) * ((1 - pf_relay**(k_relay))**(n_k))
    
    return prob


def compare_cascading_prob(rsc, n_k, pfc, pfrelay, pfnode, k_router, result_prs, en_plot=False, set_log=False):
    # pfr: relay defense
    # pfn: router defense
    original_list = []
    reputation_list = []
    reputation_p_list = []
    hf_list = []
    x_axis = []
    result_list = []
    label_list = ["original", "reputation", "reputation-persist", "area (majority)"]
    area_cas_list = []
    
    for initial_line in range(1, n_k+1):
        K = math.floor(rsc/initial_line)
        prob = p_trigger_cascading(pfc, initial_line, K)
        original_list.append(round(prob,result_prs))
        
        # try to equally divide the k_relay and k_router
        k_router = max(int(rsc/initial_line/3), 1)
        k_relay = math.floor((K-k_router)/2)
        assert k_relay >= 0
        
        # reputation attack
        prob_r = P_trigger_cascading_reputation(pfrelay, pfnode, k_relay, k_router, initial_line)
        reputation_list.append(round(prob_r,result_prs))
        
        # reputation-persist attack
        prob_r_persist = P_trigger_cascading_reputation_persist(rsc, pfrelay, pfnode, initial_line)
        reputation_p_list.append(round(prob_r_persist,result_prs))
        
        # hidden failure attack
        hf_k_relay = K
        prob_hf = P_trigger_cascading_hf(pfrelay, hf_k_relay, initial_line, 0.001)
        hf_list.append(prob_hf)
        x_axis.append(initial_line)
        
        # area attack
        n_majority = 5
        k_area = max((rsc / initial_line / n_majority), 1) 
        prob_area = P_trigger_cascading_area(pfrelay, initial_line, k_area, n_majority)
        area_cas_list.append(prob_area)
        
        print("ini =",initial_line,"rsc =",rsc,"K =",K,"k_relay =",k_relay,"k_router =",k_router,"k_area =",k_area)
    
    result_list.append(original_list)
    result_list.append(reputation_list)
    result_list.append(reputation_p_list)
    result_list.append(area_cas_list)
    
    print("Original cascading attack:",original_list)
    print("Reputation cascading attack:",reputation_list)
    print("Reputation-persist cascading attack:",reputation_p_list)
    print("HF cascading probability:",hf_list)
    print("Area cascading attack:",area_cas_list)
    
    if en_plot == True:
        if set_log == True:
            yMax = "none"
            yMin = "none"
        else:
            yMax = 1
            yMin = 0
        napf.myPlotFig_List_WithX(x_axis, result_list, "Probability of triggering cascading", \
                         "Initial line ($n_k$)", "P(triggering cascading)", label_list, \
                         ymax=yMax, ymin=yMin, en_log=set_log)



# in this analysis, we set the resources so that given the resource, each relay in the reputation
# scheme has at least one resource to be assigned
def compare_cascading_prob_rsc(rsc, n_k, k_r, pf1, pfr, pfn, en_plot=False, set_log=False):
    original_list = []
    reputation_list = []
    x_axis = []
    result_list = []
    label_list = ["original", "reputation", "area (majority)"]
    area_cas_list = []
    
    # the minimum resource make sure that at least each router can be attacked
    # while the relays in reputation attack may be assigned 0 resource
    for rsc_i in range(n_k*k_r, rsc+1, 10):
        K = math.floor(rsc_i/n_k)
        prob = p_trigger_cascading(pf1, n_k, K)
        original_list.append(prob)
        
        k_router = k_r
        k_relay = math.floor((K-k_router)/2)
        
        k_router = max(math.floor(rsc_i/n_k/3),1)
        k_relay = max(math.floor(rsc_i/n_k/3),1)
        
        k_relay = max((rsc_i/n_k/3),1)
        k_router = (K-2*k_relay)
        
        k_router = (rsc_i/n_k/3)
        k_relay = (rsc_i/n_k/3)
        assert k_relay >= 0
        
        prob_r = P_trigger_cascading_reputation(pfr, pfn, k_relay, k_router, n_k)
        reputation_list.append(prob_r)
        
        n_majority = 5
        k_area = max((rsc_i / n_k / n_majority), 0) 
        prob_area = P_trigger_cascading_area(pfr, n_k, k_area, n_majority)
        area_cas_list.append(prob_area)
        
        
        x_axis.append(rsc_i)
        
        print("rsc =",rsc,"rsc_i =",rsc_i,"K =",K,"k_relay =",k_relay,"k_router =",k_router,"k_area =",k_area)
    
    result_list.append(original_list)
    result_list.append(reputation_list)
    result_list.append(area_cas_list)
    
    print("Original cascading attack:",original_list)
    print("Reputation cascading attack:",reputation_list)
    print("Area cascading attack:",area_cas_list)
    
    if en_plot == True:
        yMax = "none" if set_log == True else 1
        yMin = "none" if set_log == True else 0
        napf.myPlotFig_List_WithX(x_axis, result_list, "Probability of triggering cascading (Given resource)", \
                         "Attack resource ($N_A$)", "P(triggering cascading)", label_list, \
                         ymax=yMax, ymin=yMin, en_log=set_log)



def compare_cascading_expect_trip(rsc, n_k, n_m, pf1, pfr, pfn, total_line, dig_num=10, en_plot=False):
    original_list = []
    reputation_list = []
    reputation_p_list = []
    x_axis = []
    result_list = []
    label_list = ["original", "reputation", "reputation-persist", "area (majority)"]
    area_cas_list = []
    
    temp_dict = P_incomplete_attack_N_area_trip(rsc, n_k, pfr, pfn)
    
    for initial_line in range(1, n_k+1):
        # direct attack
        K = math.floor(rsc/initial_line)
        prob_o = p_trigger_cascading(pf1, initial_line, K)
        prob_local_line = 1-(pf1)**(K)
        total_original = prob_local_line * initial_line + prob_o * n_m
        original_list.append(round(total_original,dig_num))
        
        # reputation attack
        # try to equally divide the k_relay and k_router
        k_router = max(int(rsc/initial_line/3), 1)
        #k_router = 1
        k_relay = math.floor((K-k_router)/2)
        prob_r = P_trigger_cascading_reputation(pfr, pfn, k_relay, k_router, initial_line)
        prob_target_relay = (1-pfr**(k_relay))**2 * (1-pfn**(k_router))
        prob_local_line_r = 1-(pfr)**(2 * k_relay)
        #total_reputation = prob_local_line_r * initial_line + prob_target_relay * initial_line + prob_r * n_m
        total_reputation = prob_target_relay * initial_line + prob_r * n_m
        reputation_list.append(round(total_reputation,dig_num))
        
        # reputation-persist attack
        # total trip is only an approximation
        prob_r_p = P_trigger_cascading_reputation_persist(rsc, pfr, pfn, initial_line)
        #total_reputation_p = prob_r_p * n_m + prob_r_p * initial_line
        incomplete_trip = 0
        for idx in range(initial_line):
            incomplete_trip += temp_dict[idx] * idx
        total_reputation_p = min(prob_r_p * n_m + prob_r_p * initial_line + incomplete_trip, initial_line+n_m)
        reputation_p_list.append(round(total_reputation_p,dig_num))
        
        # area attack
        n_majority = 5
        k_area = max((rsc / initial_line / n_majority), 1) 
        prob_area = P_trigger_cascading_area(pfr, initial_line, k_area, n_majority)
        prob_target_relay_area = (1-(pfr)**k_area)**n_majority
        total_area = prob_target_relay_area * initial_line + prob_area * n_m
        #total_area = prob_area * n_m
        area_cas_list.append(round(total_area,dig_num))
        
        x_axis.append(initial_line)
        print("rsc =",rsc,"K =",K,"k_relay =",k_relay,"k_router =",k_router,"k_area =",k_area)
    
    result_list.append(original_list)
    result_list.append(reputation_list)
    result_list.append(reputation_p_list)
    result_list.append(area_cas_list)
    
    print("Original cascading attack:",original_list)
    print("Reputation cascading attack:",reputation_list)
    print("Reputation-persist cascading attack:",reputation_p_list)
    print("Area cascading attack:",area_cas_list)
    
    if en_plot == True:
        napf.myPlotFig_List_WithX(x_axis, result_list, "Expected line trip pure cascading", \
                         "Initial line ($n_k$)", "E(line trip in pure cascading)", label_list, ymax=total_line, ymin=0)


def compare_cascading_expect_trip_rsc(rsc, n_k, n_m, k_r, pf1, pfr, pfn, total_line, r_step, en_plot=False):
    original_list = []
    reputation_list = []
    x_axis = []
    result_list = []
    label_list = ["original", "reputation", "area (majority)"]
    area_cas_list = []
    
    for rsc_i in range(n_k*k_r, rsc+1, r_step):
        K = math.floor(rsc_i/n_k)
        prob_o = p_trigger_cascading(pf1, n_k, K)
        prob_local_line = 1-(pf1)**(K)
        total_original = prob_local_line * n_k + prob_o * n_m
        original_list.append(round(total_original,3))
        
        k_router = k_r
        k_relay = math.floor((K-k_router)/2)
        
        k_router = max((rsc_i/n_k/3),1)
        k_relay = max((rsc_i/n_k - 2*k_router),0)
        
        prob_r = P_trigger_cascading_reputation(pfr, pfn, k_relay, k_router, n_k)
        prob_target_relay = (1-pfr**(k_relay))**2 * (1-pfn**(k_router))
        prob_local_line_r = 1-(pfr)**(2 * k_relay)
        #total_reputation = prob_local_line_r * n_k + prob_target_relay * n_k + prob_r * n_m
        total_reputation = prob_target_relay * n_k + prob_r * n_m
        reputation_list.append(round(total_reputation,3))
        
        n_majority = 5
        k_area = max((rsc_i / n_k / n_majority), 0) 
        prob_area = P_trigger_cascading_area(pfr, n_k, k_area, n_majority)
        prob_target_relay_area = (1-(pfr)**k_area)**n_majority
        total_area = prob_target_relay_area * n_k + prob_area * n_m
        #total_area = prob_area * n_m
        area_cas_list.append(round(total_area,3))
        
        x_axis.append(rsc_i)
        print("rsc =",rsc,"rsc_i =",rsc_i,"K =",K,"k_relay =",k_relay,"k_router =",k_router,"k_area =",k_area)
    
    result_list.append(original_list)
    result_list.append(reputation_list)
    result_list.append(area_cas_list)
    
    print("Original cascading attack:",original_list)
    print("Reputation cascading attack:",reputation_list)
    print("Area cascading attack:",area_cas_list)
    
    if en_plot == True:
        napf.myPlotFig_List_WithX(x_axis, result_list, "Expected line trip pure cascading (Given resource)", \
                         "Attack resource", "E(line trip in pure cascading)", label_list, ymax=total_line, ymin=0)


# the expectation of trip in the i_th selection with K-attack
# two primary relays of the selected area will be attacked
def test_ith_K_Persistent(R, NA, pf, d, K, i):
    if NA > R:
        print("NA should be smaller than R")
        return
    
    if K < 1:
        print("K should be no smaller than 1")
        return
    
    if i < 1:
        print("i should be larger than 1")
        return
    
    # on average, the number of areas can be attacked
    #selected_area_num = math.ceil(NA/(2*K)) 
    #K = min(K, math.ceil(1/(1-pf)))
#     temp = 0
#     temp1 = 0
#     for j in range(1, K+1):
#         temp += (1-pf) * (pf**(j-1)) * j
#         temp1 += (1-pf) * (pf**(j-1))
#     temp += (1-temp1) * K
#     if K != 1:
#         K = int(temp)
    
    n = math.floor((1 - pf**K)* 2 * (i-1)) # average compromised relay number
    
    e_two_pr_result = (1 - pf**K)**2 * (2*d - 1)
    
    v1 = comb((R - 2*d), n)
    v2 = comb((R - 2), n)
    e_one_pr_result = 2 * (pf**K) * (1 - pf**K) * (d - 1) * (1 - v1/v2)
    
    e_ith_trip = e_two_pr_result + e_one_pr_result
    
    return e_ith_trip


# compute the expectation for all possible value of "i"
# compute the total expectation
def whole_iteration_K_Persistent(R, NA, pf, d, K):
    if NA > R:
        raise ValueError('NA should be no larger than R')
    elif K < 1:
        raise ValueError('K should be no smaller than 1')
    elif pf < 0 or pf > 1:
        raise ValueError('pf incorrect')
    
    e_total_trip = 0
    
    if pf == 0:
        area_atk_num = int(NA/2)
    elif pf == 1:
        area_atk_num = 0
    else:
        #area_atk_num = math.floor(NA/(2*K))
        area_atk_num = math.floor(max(NA/(2*K), NA*(1-pf)/2))
    
    result = []
    # compute the expected trip number for each iternation
    for i in range(area_atk_num):
        e_ith_trip = test_ith_K_Persistent(R, NA, pf, d, K, i+1)
        e_total_trip += e_ith_trip
        
        result.append(e_ith_trip)
    
    #print("pf=",pf,result)
    
    # the max tripped line number is R/2
    if e_total_trip > int(R/2):
        e_total_trip = int(R/2)
    
    return e_total_trip


def multiple_parameter_K_attack(test_type):
    pf = 0.1
    NA = 30
    R = 120
    d = 4
    K = 5
    
    x_data = []
    
    if test_type == "pf":
        result = []
        # test for different defense strength
        for p_f in range(1, 10):
            e_total_trip = whole_iteration_K_Persistent(R, NA, p_f/10, d, K)
            result.append(e_total_trip)
            x_data.append(round(p_f/10, 3))
        
        napf.myPlotFig(x_data, result, "title", "xLabel", "yLabel", yMax=R/2, yMin=0)
        
    elif test_type == "K":
        # test for different K values
        total_result = []
        label_list = []
        
        for p_f in range(1, 10):
            result = []
            for k_p in range(10):
                e_total_trip = whole_iteration_K_Persistent(R, NA, p_f/10, d, k_p+1)
                result.append(e_total_trip)
            
            total_result.append(result)
            label_list.append("$P_f=$"+str(round(p_f/10, 1)))
        
        for i in range(10):
            x_data.append(i+1)
        
        g_title = "Tripped line number for different K"
        x_label = "Value of K"
        y_label = "Expected tripped line number"
        napf.myPlotFig_List_WithX(x_data, total_result, g_title, x_label, y_label, label_list, \
                                  ymax=R/2, ymin=0, legend_p="outside")


def test_list():
    a = [1,2,3]
    b = [4,5,6]
    c = ['d','e','f']
    d = {1:'abc'}
    e = {1:100, 2:200, 3:300, 4:400, 5:500}
    
    temp = []
    temp.append(a)
    temp.append(b)
    temp.append(c)
    temp.append(d)
    temp.append(e)
    
    print("Length of list:",len(temp))
    idx = max(e, key=lambda i: e[i])
    print("Selected idx:", idx, "value is",e[idx])
    print("Total dictionary value:",sum(e.values()))
    
    print(e)
    e.pop(1, None)
    e.pop(2, None)
    print(e)
    
    temp_list = ["".join(seq) for seq in product("01", repeat=6)]
    print(temp_list)
    
    for elmnt in temp_list:
        temp_list2 = [int(elmnt[i]) for i in range(len(elmnt))]
        for i in range(len(temp_list2)):
            print(temp_list2[i],' ',end='')
            if i == len(temp_list2)-1:
                print()


def delay_adjust(sc_mode, du, dd, eta_ratio):
    d0 = 60
    cms_local = 20
    cms_delta = 20
    cms_eta = cms_delta * eta_ratio
    last_p = 0
    min_delay = 4
    rtn = 0
    
    if sc_mode == "cms":
        if du > cms_local + cms_eta:
            rtn = last_p + cms_delta - cms_eta
        elif du < cms_local - cms_eta:
            rtn = last_p + cms_delta + cms_eta
        else:
            rtn = last_p + cms_delta - (du - cms_local)
    elif sc_mode == "sms":
        if d0 <= du + dd + min_delay:
            rtn = min_delay
        else:
            rtn = d0 - du - dd
    
    return rtn


def test_cms_numerical():
    plus_effect = 0
    neg_effect = 0
    balanced = 0
    cost = 0
    cnt = 0
    d0 = 60
    old_priority = 20
    old_delay = 20
    
    random.seed(1)
    
    for i in range(200):
        for j in range(100):
            if j >= 80:
                d_up = random.uniform(30, 40)
            else:
                d_up = 20
            
            if j >= 80:
                d_down = 18
            else: 
                d_down = 20
            
            new_priority = delay_adjust("sms", d_up, d_down, 0.4)
            
            if j == 1:
                d_current = new_priority / old_priority * old_delay
                #d_current = 4
            else:
                d_current = new_priority / old_priority * old_delay
            
            print("priority:", new_priority, "   delay:", d_current)
            delta_delay = d_current - old_delay
            
            if delta_delay > 0:
                plus_effect += 1
            elif delta_delay < 0:
                neg_effect += 1
            else:
                balanced += 1
            
            cost += delta_delay
            
            if d0 < d_up + d_down + d_current:
                cnt += 1
            
            #old_priority = new_priority
            #old_delay = d_current
    
    total_pkt_num = (i+1) * (j+1)
    print("Total cost:", cost, "     Average cost:", cost/total_pkt_num)
    print("plus:", plus_effect, "    negative:", neg_effect, "     balanced:", balanced)
    print(total_pkt_num, "packets,", cnt, "violations,", "ratio =", cnt/total_pkt_num)


# B28_figure([1,2,3,4,5,6])
# B21_figure([1,4,7,9,10])
# B24_figure([1,4,7,9,11,12,14])
# B16_L19_figure([1,4,7,9,11,12,14,15])

# compare_probability_combination(24, 9)
# test()
# random_i_lines_atk_probability(24, 12)
# random_attack_line_trip(240, 0.2, 120, 12, 12)
# test3()

# test_list()
# test_prob_cascading()
# test_e_trip_due_to_cascading(0.5, 6, 60, "total", 5, True)
# multiple_test_e_trip_due_to_cascading()
# test_cms_numerical()


# area_attack_e_selected_bp_line(120, 30, 4)
# multiple_parameter_K_attack("K")
# test_expect_bt_distribution(11, 0.25)

# test_area_total_trip(110, 1, 4, 0.2, 120, 10, True, True)
# test_area_total_trip_it(240, 4, 0.2, 120, 12, 12, "majority", True)

# compare_cascading_prob(50, 10, 0.9, 0.2, 0.2, 1, 50, True, False)
compare_cascading_expect_trip(50, 10, 60, 0.9, 0.2, 0.2, 120, 10, True)
# compare_cascading_expect_trip(50, 10, 60, 0.9, 0.2, 0.2, 240, 10, True)

# compare_cascading_prob_rsc(170, 5, 2, 0.9, 0.2, 0.2, True, False)
# compare_cascading_expect_trip_rsc(170, 5, 60, 2, 0.9, 0.2, 0.2, 120, 10, True)
# compare_cascading_expect_trip_rsc(170, 5, 60, 2, 0.9, 0.2, 0.2, 240, 10, True)
