'''
Created on Sep 13, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
import random
import numeric_analysis.plotFunction as napf
import numpy as np
from copy import deepcopy
import math

# igraph uses the python RNG generator
random.seed(12345)

def plotGraph(graph,fileName=None,graphLayout=None):
    if graphLayout != None:
        myLayout = ig.Layout(graphLayout)
    else:
        myLayout = graph.layout_kamada_kawai()
    graph.vs["label"] = graph.vs["name"]
    ig.plot(graph, layout = myLayout, bbox = (800,800), margin = 50)
    if fileName != None:
        ig.plot(graph, fileName, layout = myLayout, bbox = (800,800), margin = 50)


def debug_print(enablePrint, *args):
    if enablePrint == True:
        for i in range(len(args)):
            if i == len(args)-1:
                print(args[i])
            else:
                print(args[i],"",end='')


def cal_avg_degree(graph):
    m_deg = ig.mean(graph.degree())
    print("Average degree =",m_deg)


def show_line_load(graph):
    for es_i in graph.es:
        print("Line index:",es_i.index,"   load:",es_i["load"],"   tripped:",es_i["tripped"])
    
    print()


def show_line_load_change(graph,rnd,ob_num):
    print("Round",rnd,"   ->  ",graph.es[ob_num]["load"],graph.es[ob_num]["initial_load"]*graph.es[ob_num]["tolerance"],\
              graph.es[ob_num]["accu_ol"],graph.es[ob_num]["ol_th"])


def show_graph_basic(graph):
    print("Number of edges =",len(myGraph.es))
    cal_avg_degree(graph)
#     edge_list = graph.incident(5)
#     print(edge_list,graph.vs[5].degree())


def show_final_result(graph):
    cnt = 0
    for es_i in graph.es:
        if es_i["tripped"] == True:
            cnt += 1
    
    print("Total tripped lines:",cnt) 


def show_neighbor_line_num(graph):
    num_list = []
    for es_i in graph.es:
        src_node_idx = es_i.source
        dst_node_idx = es_i.target
        
        src_edge_list = graph.incident(src_node_idx)
        dst_edge_list = graph.incident(dst_node_idx)
        
        result = len(src_edge_list) + len(dst_edge_list) - 2
        num_list.append(result)
    
    print("Average number of neighbor lines:",np.mean(num_list))
    napf.myPlotFig([], num_list, "Number of Neighbor Lines", "Line Index", "# of line",yMax=14,yMin=0)


def show_overload_at_certain_rnd(graph):
    ol_cnt = 0
    total_ol = 0
    total_ol_pct = 0
    total_load = 0
    for es_i in graph.es:
        if es_i["tripped"] == True:
            continue
        
        if es_i["load"] > es_i["initial_load"] * es_i["tolerance"]:
            ol_cnt += 1
            total_ol += (es_i["load"] - es_i["initial_load"] * es_i["tolerance"])
            total_ol_pct += (es_i["load"] - es_i["initial_load"] * es_i["tolerance"])/(es_i["initial_load"] * es_i["tolerance"])
        total_load += es_i["load"]
    
    print("# of oveloaded lines:",ol_cnt,"    Percentage",str(int(ol_cnt/graph["remain_line"]*100))+"%")
    print("Average overload:",total_ol/ol_cnt,"    Average overload percentage:",total_ol_pct/ol_cnt,\
          "    Average load:",total_load/graph["remain_line"])


def show_trip_percentage(graph):
    result = 0
    for es_i in graph.es:
        if es_i["tripped"] == True:
            result += 1
    
    print("Percentage of tripped lines:",1-graph["remain_line"]/graph["total_line"],"   another value:",\
          result/graph["total_line"])


def graph_initialization(graph):
    graph["total_line"] = len(graph.es)
    graph["remain_line"] = len(graph.es)
    graph["ctrl_th"] = 0
    graph["total_load"] = 0
    graph["shed_ratio_th"] = 1
    
    for es_i in graph.es:
        es_i["initial_load"] = random.normalvariate(1,0.05)
        es_i["load"] = es_i["initial_load"]
        es_i["ol_start_time"] = 0
        es_i["ctrl_start_time"] = 0
        es_i["overloaded"] = False
        es_i["trip_time"] = 0
        es_i["last_round_normal"] = True
        es_i["tripped"] = False
        es_i["tolerance"] = 1.2
        es_i["capacity"] = es_i["initial_load"] * es_i["tolerance"]
        es_i["max_delay"] = 1.0
        es_i["ol_th"] =  es_i["max_delay"] * 0.5 * es_i["capacity"]
        es_i["accu_ol"] = 0
        es_i["shed_ratio"] = 1
        es_i["response_time"] = 5
        es_i["start_shedding"] = False
        es_i["last_rnd_load"] = 0
        
        graph["total_load"] += es_i["initial_load"]
        
    for vs_i in graph.vs:
        vs_i["remain_deg"] = graph.degree(vs_i.index)


def set_max_delay(graph,max_delay):
    for es_i in graph.es:
        es_i["max_delay"] = max_delay
        es_i["capacity"] = es_i["initial_load"] * es_i["tolerance"]
        es_i["ol_th"] =  es_i["max_delay"] * 0.5 * es_i["capacity"]


def set_tolerance(graph,tolerance):
    for es_i in graph.es:
        es_i["tolerance"] = tolerance
        es_i["capacity"] = es_i["initial_load"] * es_i["tolerance"]
        es_i["ol_th"] =  es_i["max_delay"] * 0.5 * es_i["capacity"]


def set_shed_ratio(graph,shed_ratio):
    for es_i in graph.es:
        es_i["shed_ratio"] = shed_ratio


def set_response_time(graph,rt):
    for es_i in graph.es:
        es_i["response_time"] = rt


def set_ctrl_th(graph,ctrl_th):
    graph["ctrl_th"] = ctrl_th


def initial_event(graph,initial_idx=-1):
    if initial_idx >= 0:
        trip_num = random.randint(0,len(graph.es)-1)
        # 6 23 24 29 38 42 46 56 59
        trip_num = initial_idx
    else:
        trip_num = 0
    
    print("Initial tripped line index #",trip_num)
    src_node_idx = graph.es[trip_num].source
    dst_node_idx = graph.es[trip_num].target
    print("Src node index:",src_node_idx,"  Dst node index:",dst_node_idx)
    
    src_edge_list = graph.incident(src_node_idx)
    dst_edge_list = graph.incident(dst_node_idx)
    print("Src node:",src_edge_list,graph.vs[src_node_idx].degree())
    print("Dst node:",dst_edge_list,graph.vs[dst_node_idx].degree())
    print()
    
    trip_line(graph, trip_num)
    

def trip_line(graph,trip_index):
    redistribute_power(graph, trip_index)
    graph.es[trip_index]["tripped"] = True
    graph.es[trip_index]["load"] = 0
    graph["remain_line"] -= 1
    

def redistribute_power(graph,trip_index,show_redistribution=False):
    trip_edge = graph.es[trip_index]
    if trip_edge["tripped"] == True: #The edge is already tripped
        raise NameError("Line index "+str(trip_index)+" is already tripped.")
    
    src_idx = trip_edge.source
    dst_idx = trip_edge.target
    
    src_node = graph.vs[src_idx]
    dst_node = graph.vs[dst_idx]
    
    if src_node["remain_deg"] == 0 and dst_node["remain_deg"] == 0:
        return
    elif src_node["remain_deg"] == 1 and dst_node["remain_deg"] == 1:
        return
    elif trip_edge["load"] == 0:
        return
    else:
        if src_node["remain_deg"] >= 1:
            src_node["remain_deg"] -= 1
        if dst_node["remain_deg"] >= 1:
            dst_node["remain_deg"] -= 1
        
        power_share = trip_edge["load"] / (src_node["remain_deg"] + dst_node["remain_deg"])
        if show_redistribution == True:
            print("Redistribute line",trip_index,"to",src_node["remain_deg"] + dst_node["remain_deg"],\
              "lines, tripped load",trip_edge["load"],"power share",power_share)
        
        for edge_idx in graph.incident(src_idx):
            if edge_idx != trip_index and graph.es[edge_idx]["tripped"] != True:
                graph.es[edge_idx]["load"] += power_share
        
        for edge_idx in graph.incident(dst_idx):
            if edge_idx != trip_index and graph.es[edge_idx]["tripped"] != True:
                graph.es[edge_idx]["load"] += power_share


def adjust_ctrl_start_time(graph,current_rnd):
    for es_i in graph.es:
        if es_i["overloaded"] == True:
            if es_i["ctrl_start_time"] < current_rnd:
                es_i["ctrl_start_time"] = current_rnd


def cal_remaining_load(graph):
    total_load = 0
    for es_i in graph.es:
        if es_i["tripped"] != True:
            total_load += es_i["load"]
    
    final_pct = total_load/graph["total_load"]
    print("Remaining load:",str(int(final_pct*100))+"%")


def show_line_and_neighbor_line(graph,line_idx):
    cap_list = []
    ol_list = []
    cnt = 0
    line_i = graph.es[line_idx]
    print("Line index",line_idx,"  -->  load:",line_i["load"])
    
    src_vs_idx = line_i.source
    dst_vs_idx = line_i.target
    
    src_edge_list = graph.incident(src_vs_idx)
    dst_edge_list = graph.incident(dst_vs_idx)
    
    for es_i_idx in src_edge_list:
        if es_i_idx == line_idx:
            continue
        elif graph.es[es_i_idx]["tripped"] == True:
            continue
        cnt += 1
    
    for es_i_idx in dst_edge_list:
        if es_i_idx == line_idx:
            continue
        elif graph.es[es_i_idx]["tripped"] == True:
            continue
        cnt += 1
    
    if cnt != 0:
        power_share = graph.es[line_idx]["load"]/cnt
    else:
        power_share = 100000
    
    
    for es_i_idx in src_edge_list:
        if es_i_idx == line_idx:
            continue
        elif graph.es[es_i_idx]["tripped"] == True:
            continue
        es_i = graph.es[es_i_idx]
        capacity = es_i["initial_load"] * es_i["tolerance"]
        cap_list.append(capacity)
        ol_list.append(es_i["ol_th"])
        print("Neighbor line index",es_i_idx," --> ","capacity:",round(capacity,5),"  OL-threshold:",\
              round(es_i["ol_th"],5),"  extra-percentage to be added:",round(power_share/capacity,5))
    
    for es_i_idx in dst_edge_list:
        if es_i_idx == line_idx:
            continue
        elif graph.es[es_i_idx]["tripped"] == True:
            continue
        es_i = graph.es[es_i_idx]
        capacity = es_i["initial_load"] * es_i["tolerance"]
        cap_list.append(capacity)
        ol_list.append(es_i["ol_th"])
        print("Neighbor line index",es_i_idx," --> ","capacity:",round(capacity,5),"  OL-threshold:",\
              round(es_i["ol_th"],5),"  extra-percentage to be added:",round(power_share/capacity,5))
        
    print("Average capacity:",np.mean(cap_list),"   Average OL-threshold:",np.mean(ol_list))
    print("Average share:",power_share,"   Initial utilization:",1/graph.es[line_idx]["tolerance"],"\n")


def show_overload_round(graph,plot_enable=True):
    temp_list = []
    ol_start_list = []
    for es_i in graph.es:
        ol_round = es_i["trip_time"] - es_i["ol_start_time"]
        temp_list.append(ol_round)
        ol_start_list.append(es_i["ol_start_time"])
    
    print("Average rounds to trip a line:",np.mean(temp_list))
    print("---------------------------------------------------------------------------","\n")
    
    if plot_enable == True:
        napf.myPlotFig([], temp_list, "Line Overload Rounds", "Line Index", "Unit of Round", yMax=200, yMin=0)
        napf.myPlotFig([], ol_start_list, "Overload Start Rounds", "Line Index", "Round No.", yMax=200, yMin=0)


def cascading_process(graph,sys_ctrl=False,ctrl_type="local_normal",plot_enable=False,exempt_list=[],\
                      plot_list=[],plot_list2=[]):
    remain_line_list = []
    percentage_list = []
    percentage_rnd_list = []
    
    rnd = 0
    current_percentage = 0
    ctrl_start = False
    cascade_start = True
    while(cascade_start == True):
        cascade_start = False
        for es_i in graph.es:
            if es_i["tripped"] == True:
                continue
            elif es_i["load"] <= es_i["capacity"]:
                if es_i["accu_ol"] != 0:
                    es_i["accu_ol"] = 0
                es_i["last_round_normal"] = True
                es_i["ol_start_time"] = 0
                es_i["overloaded"] = False
                if es_i["start_shedding"] == True:
                    es_i["start_shedding"] = False
                continue
            else:
                es_i["overloaded"] = True
                if es_i["accu_ol"] < es_i["ol_th"]:
                    es_i["accu_ol"] += (es_i["load"] - es_i["capacity"])
                    cascade_start = True
#                     print("Line index",es_i.index,"has threshold",es_i["ol_th"],"current accu_ol",es_i["accu_ol"])
                    
                    if es_i["last_round_normal"] == True:
                        es_i["last_round_normal"] = False
                        es_i["ol_start_time"] = rnd
                        es_i["ctrl_start_time"] = rnd
                else:
                    print("Link index",es_i.index," tripped at round",rnd," load",es_i["load"])
                    trip_line(graph, es_i.index)
                    es_i["trip_time"] = rnd
                    trip_rate = 1 - graph["remain_line"]/graph["total_line"]
                    if trip_rate >= current_percentage:
                        percentage_list.append(rnd)
                        current_percentage += 0.1
                    
                    cascade_start = True
#                     break
                
        if sys_ctrl == True:
            trip_rate = 1 - graph["remain_line"]/graph["total_line"]
            if trip_rate >= graph["ctrl_th"] and ctrl_start == False:
                ctrl_start = True
                print("Ctrl starting at round",rnd)
                show_overload_at_certain_rnd(graph)
                show_trip_percentage(graph)
                adjust_ctrl_start_time(graph, rnd)
            
            if ctrl_start == True:
                for es_i in graph.es:
                    
                    if es_i["tripped"] == True:
                        continue
                    elif es_i["overloaded"] == False:
                        continue
                    elif es_i["ctrl_start_time"] + es_i["response_time"] > rnd:
                        print("Line index",es_i.index,"has ol_start_time",es_i["ol_start_time"],\
                              "    ctrl should be performed at rnd",\
                              es_i["ctrl_start_time"] + es_i["response_time"],"    current round",rnd)
                        continue
                    else: # Start control strategy
                        if es_i.index in exempt_list:
                            continue
                        if es_i["shed_ratio"] >= 1:
                            es_i["shed_ratio"] = 0.99
                        
                        
                        if ctrl_type == "local_normal":
                            es_i["load"] = es_i["load"] * (1 - es_i["shed_ratio"])
                        
                        elif ctrl_type == "local_1step":
                            if es_i["start_shedding"] == False:
                                each_rnd_accu = es_i["load"] - es_i["capacity"]
                                remain_ol = es_i["ol_th"] - es_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                es_i["shed_ratio"] = 1 - math.pow(es_i["capacity"]/es_i["load"], 1/rnd_to_shed)
                                es_i["start_shedding"] = True
                                print("Link index",es_i.index,"   capacity",es_i["capacity"],"   load",es_i["load"]\
                                      ,"   round remaining for control",rnd_to_shed,"   shed ratio",es_i["shed_ratio"],\
                                      "   accu",es_i["accu_ol"],"   accu_th",es_i["ol_th"])
                                if es_i["shed_ratio"] > graph["shed_ratio_th"]:
                                    es_i["shed_ratio"] = graph["shed_ratio_th"]
                                    print("Link index",es_i.index,"   adjust shed ratio to",graph["shed_ratio_th"])
                            
                            es_i["load"] = es_i["load"] * (1 - es_i["shed_ratio"])
                            if es_i["load"] <= es_i["capacity"]:
                                print("Link index",es_i.index,"successfully stops tripping.")
                                es_i["start_shedding"] = False
                        
                        elif ctrl_type == "local_adjust":
                            if es_i["start_shedding"] == False: # start load shedding
                                each_rnd_accu = es_i["load"] - es_i["capacity"]
                                remain_ol = es_i["ol_th"] - es_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                es_i["shed_ratio"] = 1 - math.pow(es_i["capacity"]/es_i["load"], 1/rnd_to_shed)
                                es_i["start_shedding"] = True
                                es_i["last_rnd_load"] = es_i["load"]
                                
                            if es_i["last_rnd_load"] < es_i["load"]: # check if additional events happen
                                print("Link index "+str(es_i.index),"needs adjust   ->   capacity",es_i["capacity"],\
                                      "   load",es_i["load"],"   last round load",es_i["last_rnd_load"])
                                each_rnd_accu = es_i["load"] - es_i["capacity"]
                                remain_ol = es_i["ol_th"] - es_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                es_i["shed_ratio"] = 1 - math.pow(es_i["capacity"]/es_i["load"], 1/rnd_to_shed)
                            
                            if es_i["shed_ratio"] > graph["shed_ratio_th"]:
                                es_i["shed_ratio"] = graph["shed_ratio_th"]
                                print("Link index",es_i.index,"   adjust shed ratio to",graph["shed_ratio_th"])
                            
                            es_i["load"] = es_i["load"] * (1 - es_i["shed_ratio"])
                            es_i["last_rnd_load"] = es_i["load"]
                            if es_i["load"] <= es_i["capacity"]:
                                print("Link index",es_i.index,"successfully stops tripping at round",rnd)
                        
                        else:
                            continue
#         ob_num = 36
#         show_line_load_change(graph,rnd,ob_num)
        
        remain_line_list.append(graph["remain_line"])
        rnd += 1
#         break
#     show_line_load(graph)
    show_final_result(graph)
    print("Critical rounds at:",percentage_list)
    for i in range(1,len(percentage_list)):
        rnd_rum = percentage_list[i] - percentage_list[i-1]
        percentage_rnd_list.append(rnd_rum)
    
    if plot_enable == True:
        napf.myPlotFig([], remain_line_list, "Remaining Line Number", "Round", "Unit of Line", yMax=100, yMin=0)
    plot_list.append(remain_line_list)
    plot_list2.append(percentage_rnd_list)
#     plotGraph(myGraph)


def cascading_test(graph,enable_ctrl,stg_type,exempt_list,mode,value_list,show_detail=False):
    graph_initialization(graph)
    
    show_graph_basic(graph)
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.09)
#     show_line_load(graph)
    if show_detail == True:
        show_line_and_neighbor_line(graph, 12)
    
    plot_list = []
    plot_list2 = []
    for value in value_list: # Start multiple loop cascading test
        copy_graph = deepcopy(graph)
        if mode == "max_delay":
            set_max_delay(copy_graph, value)
        elif mode == "tolerance":
            set_tolerance(copy_graph, value)
        elif mode == "load_shed":
            set_shed_ratio(copy_graph, value)
        elif mode == "response_time":
            set_response_time(copy_graph, value)
        elif mode == "ctrl_th":
            set_ctrl_th(copy_graph, value)
        
        print("<-----",mode+":",value,"----->")
        initial_event(copy_graph,12)
        cascading_process(copy_graph,enable_ctrl,stg_type,False,exempt_list,plot_list,plot_list2)
        cal_remaining_load(copy_graph)
        show_overload_round(copy_graph,show_detail)
    
    if mode == "max_delay":
        label_list = []
        for value in value_list:
            temp_text = "D="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different D"
    elif mode == "tolerance":
        label_list = []
        for value in value_list:
            temp_text = "T="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different T"
    elif mode == "load_shed":
        label_list = []
        for value in value_list:
            temp_text = "Shed="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different load shedding"
    elif mode == "response_time":
        label_list = []
        for value in value_list:
            temp_text = "$T_R$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different response time ($T_R$)"
    elif mode == "ctrl_th":
        label_list = []
        for value in  value_list:
            if value >= 0 and value < 1:
                temp_text = "Start control at "+str(int(value*100))+"% trip"
            elif value >= 1:
                temp_text = "Shed after round "+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of shedding moment"
    
    x_label2 = "percentage of line trip "+"("+r"$\times 0.1$)"
    y_label2 = "required number of rounds"
    napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining lines", ymax=120, ymin=0)
    napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


myGraph = ig.Graph.Watts_Strogatz(1, 30, 2, 0.15)
# show_neighbor_line_num(myGraph)
#myGraph = ig.Graph.Tree(15,2)
for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"


# cascading_test(myGraph,False,"local_normal",[38],"max_delay",[3,2.5,2,1.5,1,0.5])
# cascading_test(myGraph,False,"local_normal",[],"tolerance",[1.2,1.19,1.18,1.17,1.16,1.15])
# cascading_test(myGraph, True, "local_normal", [],"load_shed", [0.3,0.25,0.2,0.15,0.1])
cascading_test(myGraph, True, "local_adjust", [], "response_time", [4,3,2,1,0])
# cascading_test(myGraph, True, "local_1step", [], "ctrl_th", [0.3,0.2,0.1,0.0])

# cascading_test(myGraph,True,[],"max_delay",[2],True)
# show_overload_round(myGraph,False)

