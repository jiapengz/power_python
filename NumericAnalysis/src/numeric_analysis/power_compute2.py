'''
Created on Feb 26, 2015

@author: Zhang Jiapeng
'''

import csv
import numeric_analysis.plotFunction as napf
from copy import deepcopy
import numpy
import os
from math import ceil
from scipy.stats import norm
from numeric_analysis.plotFunction import myPlotFig_List

folderName = "F:\\Protection_Relay_Result\\Result_BP_Path\\2015_02_26\\"
folderName2 = "F:\\Protection_Relay_Result\\Result_BP_Path\\2015_03_08\\BC_T\\"
folderName3 = "F:\\Protection_Relay_Result\\Result_BP_Path\\2015_03_08\\BC_WT\\"
folderName4 = "F:\\Protection_Relay_Result\\Result_BP_Path\\2015_03_09\\"
folderName5 = "F:\Powerworld Sample\IEEE_39_Bus\Add_ALL_MVAR_LT_Test1\Power_after\\"
normalFileName = "39_Bus_System_line_load_normal.csv"
normalFilePath = folderName + normalFileName
filePfx = "39_Bus_Load_Boundary_WT_B"
trip_max_pfx = "39_Bus_Load_Boundary_WT_TripMAX_B"


# Actually VSI and LMN are in col. 18 and 19, but the "csv" index starts from "0"
PW = 8
VSI = 17
LMN = 18
exemptLine = 2
load_bus_39 = [3,4,7,8,12,15,16,18,20,21,23,24,25,26,27,28,29]
load_boundary_39 = [1970,1850,1350,1500,1150,2700,2760,3300,1580,1610,1606,2153,1950,1595,2200,910,964]
load_q = [2.4,184,84,176,88,153,32.3,30,103,115,84.6,-92,47.2,17,75.5,27.6,26.9]

def openCSVFile(caseNum, myFilePfx):
    global folderName, VSI, LMN
    
    completePath = folderName + myFilePfx + str(caseNum) + ".csv"
    fp = open(completePath)
    
    return fp

def openCSVFile_v2(fileName):
    fp = open(fileName)
    return fp

def plot_load_boundary_39():
    bus_data = []
    for busIdx in load_bus_39:
        bus_label = "B"+str(busIdx)
        bus_data.append(bus_label)
        
    napf.myPlotFig2(bus_data, load_boundary_39, "39-bus System Boundary Load", "Load Bus","Load (MVar)")


def cnt_threshold_num_39(TH=0, myFilePfx=''):
    rslt_list = []
    if myFilePfx != '':
        fPfx = myFilePfx
    else:
        fPfx = filePfx
    for busIdx in load_bus_39:
        th_cnt = 0
        counter = 0
        fp = openCSVFile(busIdx,fPfx)
        csv_file = csv.reader(fp)
        for row in csv_file:
            counter += 1
            if counter > exemptLine:
                my_vsi = float(row[VSI])
                if(my_vsi > TH):
                    th_cnt += 1
        rslt_list.append(th_cnt)
        fp.close()
        
    print(rslt_list)
    return rslt_list


def testFunc_39():
    bus_data = []
    for busIdx in load_bus_39:
        bus_label = "B"+str(busIdx)
        bus_data.append(bus_label)
    list1 = cnt_threshold_num_39(0.9, filePfx)
    list2 = cnt_threshold_num_39(0.9, trip_max_pfx)
    yDataList = []
    yDataList.append(list1)
    yDataList.append(list2)
    labelList = ["Before Tripping MAX","After Tripping MAX"]
    napf.myPlotFig2_List(bus_data, yDataList, "Number of VSIs above threshold", "Load Bus", "Number", labelList,5,0)


def compute_vsi_change(original_list,changed_list):
    result = 0
    for i in range(len(original_list)):
        if changed_list[i] > original_list[i]:
            result += (changed_list[i]-original_list[i])/original_list[i]
    result = result/len(original_list)
    return result


def compare_vsi_39(load_bus_idx,failIdxList,mode='max',TH=0):
    normal_vsi = []
    trip_vsi_list = []
    load_fp = openCSVFile(load_bus_idx, filePfx)
    load_csv_f = csv.reader(load_fp)
    counter = 0
    for row in load_csv_f:
        counter += 1
        if counter > 2:
            normal_vsi.append(float(row[VSI]))
            
#     print(normal_vsi)
    for idx in failIdxList:
        tempList = []
        fileName = folderName+filePfx+str(load_bus_idx)+'_F'+str(idx)+'.csv'
        trip_fp = openCSVFile_v2(fileName)
        load_trip_csv_f = csv.reader(trip_fp)
        counter = 0
        for row in load_trip_csv_f:
            counter += 1
            if counter > 2:
                tempList.append(float(row[VSI]))
        
        trip_vsi_list.append(tempList)
#     print(trip_vsi_list)
    if mode == 'max':
        tempList = []
        for vsi_list in trip_vsi_list:
            tempList.append(max(vsi_list))
        print("MAXes are:",tempList)
    elif mode == 'avg':
        tempList = []
        for vsi_list in trip_vsi_list:
            tempList.append(numpy.mean(vsi_list))
        print("Averages are:",tempList)
    elif mode == 'change':
        tempList = []
        for vsi_list in trip_vsi_list:
            value = compute_vsi_change(normal_vsi, vsi_list)
            tempList.append(value)
        print("Change ratios are:",tempList)
    elif mode == 'max_cnt':
        tempList = []
        for vsi_list in trip_vsi_list:
            th_cnt = 0
            for vsi_i in vsi_list:
                if vsi_i > TH:
                    th_cnt += 1
            tempList.append(th_cnt)
        print(tempList)
    None


def cal_avg_from_folder(folder_name,deduction=0,outputAvgList=[],outputMaxList=[]):
    for file_name in os.listdir(folder_name):
        complete_name = folder_name + file_name
        fp = openCSVFile_v2(complete_name)
        csv_file = csv.reader(fp)
        counter = 0
        total_vsi = 0
        max_vsi = 0
        
        for row in csv_file:
            counter += 1
            if counter > exemptLine:
                my_vsi = float(row[VSI])
                total_vsi += my_vsi
                max_vsi = max(max_vsi,my_vsi)
        
        avg_vsi = total_vsi/(counter-exemptLine-deduction)
        outputAvgList.append(round(avg_vsi,4))
        outputMaxList.append(round(max_vsi,4))
        print(file_name+':',"max VSI =",max_vsi,"average VSI =",avg_vsi)
        fp.close()
    print()


def cal_avg_increase_pcnt():
    avgList1 = []
    avgList2 = []
    cal_avg_from_folder(folderName2,1,avgList1)
    cal_avg_from_folder(folderName3,0,avgList2)
    
    for idx in range(len(avgList1)):
        icrs = avgList1[idx] - avgList2[idx]
        pcnt = icrs / avgList2[idx]
        print("Increase percentage:",pcnt)


def print_req_fields():
    for dr_name in os.listdir(folderName4):
        avglist = []
        maxlist = []
        if os.path.isdir(folderName4+dr_name+'\\'):
            cal_avg_from_folder(folderName4+dr_name+'\\', 0, avglist, maxlist)
            print(dr_name+" avg:",(avglist))
            print(dr_name+" max:",(maxlist))
        print()


def cal_reactive_load():
    print("Number of load bus:",len(load_q))
    for i in range(1,16):
        result = []
        for load in load_q:
            result.append(ceil(load+abs(load)*(2+i/10)))
        print(str(2+i/10)+':',result)


def cal_avg_transmitted_power(idxList=[]):
    tempList = []
    avgList = []
    for file_name in os.listdir(folderName5):
        result = []
        complete_file_name = folderName5 + file_name
        fp = openCSVFile_v2(complete_file_name)
        csv_file = csv.reader(fp)
        counter = 0
        
        for row in csv_file:
            counter += 1
            if counter > exemptLine:
                result.append(row[PW])
        
        print(file_name+':',result)
        tempList.append(result)
    
    for i in range(len(tempList[0])):
        total = 0
        for j in range(len(tempList)):
            total += float(tempList[j][i])
        avgList.append(round(total/len(tempList),1))
        
    print("Averaged power:",avgList)
    
    if idxList != []:
        sum_result = 0
        
        fail_list = []
        non_fail_list = []
        
        for idx in idxList:
            fail_list.append(abs(avgList[idx-1]))
        sum_result = sum(fail_list)
        print("Required average value:",sum_result/len(fail_list))
        print("Max in required values:",max(fail_list),"   min:",min(fail_list),"   std:",numpy.std(fail_list))
        
        for elemt in avgList:
            new_elemt = abs(elemt)
            if new_elemt not in fail_list:
                non_fail_list.append(new_elemt)
        sum_result = sum(non_fail_list)
        print("Remaining average value:",sum_result/len(non_fail_list))
        print("Max in remaining values:",max(non_fail_list),"   min",min(non_fail_list),"   std:",numpy.std(non_fail_list))
        print(fail_list)
        print(non_fail_list)


def cal_CCDF(mean,std,end):
    result = 1-norm(mean,std).cdf(end)
    print(result)



def plot_two_util():
    OL = [241.536, 209.28, 345.856, 147.2, 473.707, 533.077, 296.064, 700.365, 71.68, 214.144, 29.44, 50.688, 29.44, \
             209.28, 214.4, 111.744, 100.352, 146.944, 725.669, 690.536, 557.717, 16.64, 65.28, 32.0, 514.517, 116.16, \
             51.2, 56.32, 51.2, 141.76, 150.72, 46.08, 46.08, 51.2, 0.0, 0.0, 61.056, 61.056, \
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 20.48]
    
    Non_OL = [436.224, 452.352, 699.669, 306.325, 698.005, 731.904, 546.816, 778.304, 287.36, 299.136, 113.92, 215.68, \
              128.64, 452.352, 457.472, 125.248, 102.464, 146.944, 910.648, 865.515, 829.589, 56.32, 16.64, 76.8, \
              713.344, 167.445, 107.52, 112.64, 102.4, 295.445, 202.005, 66.56, 66.56, 92.16, 0.0, 0.0, 79.68, 79.68, \
              0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 71.68]
    
    OL_m5 = [103.936, 71.68, 237.376, 90.56, 521.899, 620.373, 191.232, 599.381, 71.68, 109.312, 29.44, 50.688, 29.44, \
             30.72, 35.84, 111.744, 100.352, 146.944, 625.685, 591.552, 573.013, 32.0, 65.28, 16.64, 601.813, 144.96, \
             56.32, 40.96, 35.84, 85.12, 104.64, 46.08, 46.08, 51.2, 0.0, 0.0, 61.056, 61.056, 0.0, 0.0, 0.0, 0.0, 0.0, \
             0.0, 0.0, 0.0, 35.84]
    
    Sorted_OL = [725.669, 700.365, 690.536, 557.717, 533.077, 514.517, 473.707, 345.856, 296.064, 241.536, 214.4, \
                 214.144, 209.28, 209.28, 150.72, 147.2, 146.944, 141.76, 116.16, 111.744, 100.352, 71.68, 65.28, \
                 61.056, 61.056, 56.32, 51.2, 51.2, 51.2, 50.688, 46.08, 46.08, 32.0, 29.44, 29.44, 20.48, 16.64]#, \
                 #0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    Sorted_Non_OL = [910.648, 865.515, 829.589, 778.304, 731.904, 713.344, 699.669, 698.005, 546.816, 457.472, 452.352, \
                     452.352, 436.224, 306.325, 299.136, 295.445, 287.36, 215.68, 202.005, 167.445, 146.944, 128.64, \
                     125.248, 113.92, 112.64, 107.52, 102.464, 102.4, 92.16, 79.68, 79.68, 76.8, 71.68, 66.56, 66.56, \
                     56.32, 16.64, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    Sorted_Non_OL_2 = [875.381, 851.248, 823.717, 803.221, 767.061, 748.501, 638.144, 627.371, 467.136, 457.472, \
                       452.352, 452.352, 436.224, 299.136, 244.8, 253.92, 237.68, 160.48, 146.0, 128.64, 125.248, \
                       113.92, 105.92, 102.464, 92.16, 77.264, 72.56, 66.56, 65.28, 56.32, 51.2, 51.2, 32.0, 20.48, \
                       16.64, 0.0, 0.0]#, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    
    Sorted_OL_m5 = [625.685, 620.373, 601.813, 599.381, 591.552, 573.013, 521.899, 237.376, 191.232, 146.944, \
                        144.96, 111.744, 109.312, 104.64, 103.936, 100.352, 90.56, 85.12, 71.68, 71.68, 65.28, 61.056, \
                        61.056, 56.32, 51.2, 50.688, 46.08, 46.08, 40.96, 35.84, 35.84, 35.84, 32.0, 30.72, 29.44, \
                        29.44, 16.64, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    Sorted_Single = [336.768, 311.488, 301.888, 237.056, 220.8, 217.856, 184.384, 149.803, 131.2, 115.2, 114.56, 111.68, \
                     90.88, 72.704, 66.176, 62.72, 58.368, 55.808, 55.808, 51.2, 48.256, 46.08, 46.08, 42.496, 39.68, \
                     31.36, 31.36, 30.72, 29.44, 23.04, 23.04, 20.48, 19.2, 16.896, 15.36, 15.36, 8.96]#, 0.0, 0.0, 0.0, \
                     #0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]


    #0.99999994391196660883129567580276
    cnt = 0
    for i in range(len(Sorted_OL)):
        cnt += (Sorted_Non_OL[i] - Sorted_OL[i])
    print("Saved CA:",cnt,"Kbps")
    
    cnt = 0
    for i in range(len(Sorted_OL)):
        cnt += (Sorted_Non_OL[i] - Sorted_OL_m5[i])
    print("Saved CA m5:",cnt,"Kbps")
    
    cnt = 0
    rlt = 0
    temp = 0
    for i in range(len(Sorted_OL)):
        if Sorted_Non_OL_2[i] != 0 and Sorted_OL[i] != 0:
            rlt += (Sorted_Non_OL_2[i] - Sorted_OL[i])/Sorted_Non_OL_2[i]
            temp += 1
    print("Non-OL & OL Average saving:",rlt/temp)
    print("Non-OL & OL Average saving, new:",rlt/len(Sorted_OL))

    cnt = 0
    rlt = 0
    temp = 0
    for i in range(len(Sorted_OL)):
        if Sorted_OL[i] != 0 and Sorted_Single[i] != 0:
            rlt += (Sorted_OL[i] - Sorted_Single[i])/Sorted_OL[i]
            temp += 1
    print("Average saving:",rlt/temp)
    print("Average saving, new:",rlt/len(Sorted_OL))
    
    dataList = [OL,Non_OL,OL_m5]
    sorted_dataList = [Sorted_OL,Sorted_Non_OL_2]
    sorted_dataList_2 = [Sorted_OL, Sorted_Single]
    sorted_dataList_3 = [Sorted_Non_OL_2, Sorted_OL, Sorted_Single]
    labelList = ["Power-aware Backup Selection","Topology-based Backup Selection","Overlap M5"]
    labelList_2 = ["Full Reservation","Improved Reservation"]
    labelList_3 = ["Topology-based Backup Selection", "Power-aware Backup Selection", "Improved Reservation"]
    myPlotFig_List(dataList,labelList,"Sorted Link Utilization","Sorted Link",'Reservation (Kbps)',ymax=1000,ymin=0)
    myPlotFig_List(sorted_dataList,labelList,"Sorted Link Reservation","Network Link",'Reservation (Kbps)',ymax=1000,ymin=0)
    myPlotFig_List(sorted_dataList_2,labelList_2,"Sorted Link Reservation","Network Link",'Reservation (Kbps)',ymax=1000,ymin=0)
    myPlotFig_List(sorted_dataList_3,labelList_3,"Sorted Link Reservation","Sorted Link",'Reservation (Kbps)',ymax=1000,ymin=0)



# plot_load_boundary_39()
# cnt_threshold_num_39(0.9, filePfx)
# cnt_threshold_num_39(0.9, trip_max_pfx)
# testFunc_39()
# myMode = 'avg'
# my_TH = 0.8
# compare_vsi_39(3, [5,3,13], myMode, my_TH)
# compare_vsi_39(24, [24,29,13], myMode, my_TH)
# compare_vsi_39(27, [26,31,13], myMode, my_TH)
# compare_vsi_39(20, [22,19,34], myMode, my_TH)

# cal_avg_increase_pcnt()
# for file in os.listdir(folderName4):
#     if os.path.isdir(folderName4+file):
#         print(folderName4+file)
#     elif os.path.isfile(folderName4+file):
#         print(folderName4+file)

# cal_avg_from_folder(folderName4+'B24\\')


# print_req_fields()
# cal_reactive_load()
# cal_avg_transmitted_power([3,5,6,7,9,12,14,15,19,21,22,23,27,31,34])
cal_CCDF(1.65, 0.55, 3.35)
print(0.0000000013)
plot_two_util()