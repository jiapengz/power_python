'''
Created on Oct 16, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
import random
import numeric_analysis.plotFunction as napf
import numpy as np
from copy import deepcopy
import math

# igraph uses the python RNG generator
random.seed(12345)

def plotGraph(graph,fileName=None,graphLayout=None):
    if graphLayout != None:
        myLayout = ig.Layout(graphLayout)
    else:
        myLayout = graph.layout_kamada_kawai()
    graph.vs["label"] = graph.vs["name"]
    ig.plot(graph, layout = myLayout, bbox = (800,800), margin = 50)
    if fileName != None:
        ig.plot(graph, fileName, layout = myLayout, bbox = (800,800), margin = 50)


def debug_print(enablePrint, *args):
    if enablePrint == True:
        for i in range(len(args)):
            if i == len(args)-1:
                print(args[i])
            else:
                print(args[i],"",end='')


def show_graph_basic(graph):
    print("Number of nodes =",len(myGraph.vs),"       Number of edges =",len(myGraph.es))
    cal_avg_degree(graph)
    print("---------------------------------------------------------------------------","\n")


def cal_avg_degree(graph):
    m_deg = ig.mean(graph.degree())
    print("Average degree =",m_deg)


def graph_initialization(graph):
    graph["total_node"] = len(graph.vs)
    graph["remain_node"] = len(graph.vs)
    graph["ctrl_th"] = 0
    graph["total_load"] = 0
    graph["shed_ratio_th"] = 1
    graph["shut_same_rnd"] = 0
    graph["new_event_interference"] = 0
    graph["new_event_if"] = 0
    graph["new_event_if_vs"] = 0
    graph["global_response_time"] = 5
    graph["global_ctrl_start_time"] = 0
    graph["current_failure_percentage"] = 0
    graph["cascading_final_stop_time"] = 0
    
    for vs_i in graph.vs:
        vs_i["remain_deg"] = graph.degree(vs_i.index)
        vs_i["initial_load"] = random.normalvariate(1,0.05)
        vs_i["load"] = vs_i["initial_load"]
        vs_i["tolerance"] = 1.2
        vs_i["max_delay"] = 1.0
        vs_i["capacity"] = vs_i["initial_load"] * vs_i["tolerance"]
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]
        vs_i["accu_ol"] = 0
        
        vs_i["overloaded"] = False
        vs_i["ol_start_time"] = 0
        vs_i["shutdown"] = False
        vs_i["shutdown_time"] = 0
        
        vs_i["ctrl_start_time"] = 0
        vs_i["shed_ratio"] = 1
        vs_i["response_time"] = 5
        vs_i["start_shedding"] = False
        vs_i["last_rnd_load"] = 0
        vs_i["last_rnd_normal"] = True
        vs_i["plan_ctrl_rnd"] = 0
        
        vs_i["new_event_affect"] = False
        
        vs_i["finally_will_fail"] = False
        vs_i["stop_shedding_time"] = 0
        vs_i["v_failure_time"] = 0
        vs_i["event_dict"] = {}
        vs_i["in_queue"] = False
        vs_i["global_start_load"] = 0
        vs_i["global_start_accu"] = 0
        vs_i["load_at_failure"] = 0
        
        vs_i["shed_in_ctrl"] = False
        vs_i["total_shed_load"] = 0
        vs_i["global_shed_in_ctrl"] = False
        vs_i["global_total_shed_load"] = 0
        
        graph["total_load"] += vs_i["initial_load"]


def set_max_delay(graph,max_delay):
    for vs_i in graph.vs:
        vs_i["max_delay"] = max_delay
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]


def set_tolerance(graph,tolerance):
    for vs_i in graph.vs:
        vs_i["tolerance"] = tolerance
        vs_i["capacity"] = vs_i["initial_load"] * vs_i["tolerance"]
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]


def set_shed_ratio(graph,shed_ratio):
    for vs_i in graph.vs:
        vs_i["shed_ratio"] = shed_ratio


def set_response_time(graph,rt):
    for vs_i in graph.vs:
        vs_i["response_time"] = rt


def set_global_response_time(graph,grt):
    graph["global_response_time"] = grt


def set_ctrl_th(graph,ctrl_th):
    graph["ctrl_th"] = ctrl_th


def set_max_shed_ratio(graph,max_shed_ratio):
    graph["shed_ratio_th"] = max_shed_ratio

def shutdown_node(graph,shutdown_index):
    redistribute_power(graph, shutdown_index)
    graph.vs[shutdown_index]["shutdown"] = True
    graph.vs[shutdown_index]["load"] = 0
    graph["remain_node"] -= 1


def redistribute_power(graph,shutdown_index,show_redistribution=False):
    down_vs = graph.vs[shutdown_index]
    if down_vs["shutdown"] == True: #The node is already shut down
        raise NameError("Node index "+str(shutdown_index)+" is already shut down.")
    
    neighbor_idx_list = graph.neighbors(shutdown_index)
    counter = 0
    for vs_idx in neighbor_idx_list:
        if graph.vs[vs_idx]["shutdown"] != True:
            counter += 1
    
    if counter != 0:
        power_share = down_vs["load"]/counter
        for vs_idx in neighbor_idx_list:
            if graph.vs[vs_idx]["shutdown"] != True:
                graph.vs[vs_idx]["load"] += power_share
                
                if graph.vs[vs_idx]["start_shedding"] == True:
                    graph["new_event_interference"] += 1


def initial_node_event(graph,initial_idx=-1,debug_enable=True):
    if initial_idx >= 0:
        shutdown_idx = random.randint(0,len(graph.vs)-1)
        shutdown_idx = initial_idx
    else:
        shutdown_idx = 0
    
    debug_print(debug_enable,"Initial shut down node index #",shutdown_idx,"   neighbors:",graph.neighbors(shutdown_idx))
    shutdown_node(graph, shutdown_idx)


def update_node_overload_state(graph,current_rnd):
    for vs_i in graph.vs:
        if vs_i["load"] > vs_i["capacity"]:
            if vs_i["overloaded"] == False:
                vs_i["overloaded"] = True
                vs_i["ol_start_time"] = current_rnd
                
            if vs_i["last_rnd_normal"] == True:
                vs_i["last_rnd_normal"] = False
                vs_i["ctrl_start_time"] = current_rnd
#                 print("Node",vs_i.index,"updates to overload at time", current_rnd)


def adjust_ctrl_start_time(graph,current_rnd):
    for vs_i in graph.vs:
        if vs_i["overloaded"] == True:
            if vs_i["ctrl_start_time"] < current_rnd:
                vs_i["ctrl_start_time"] = current_rnd


def cal_remaining_load(graph):
    total_load = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] != True:
            total_load += vs_i["load"]
    
    final_pct = total_load/graph["total_load"]
    print("Remaining load:",str(int(final_pct*100))+"%")
    
    return final_pct


def cal_remaining_node(graph):
    total_node = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] != True:
            total_node += 1
    
    final_pct = total_node/graph["total_node"]
    print("Remaining node:",total_node,"   Percentage:",str(int(final_pct*100))+"%")
    
    return total_node


def display_seperate_line():
    print("---------------------------------------------------------------------------","\n")


def show_overload_round(graph,plot_enable=True):
    temp_list = []
    ol_start_list = []
    for vs_i in graph.vs:
        if vs_i["shutdown"] == True:
            ol_round = vs_i["shutdown_time"] - vs_i["ol_start_time"]
            temp_list.append(ol_round)
        ol_start_list.append(vs_i["ol_start_time"])
    
    print("Average rounds to trip a line:",np.mean(temp_list))
    
    if plot_enable == True:
        napf.myPlotFig([], temp_list, "Nodes Overload Rounds", "Node Index", "Unit of Round", yMax=200, yMin=0)
        napf.myPlotFig([], ol_start_list, "Overload Start Rounds", "Node Index", "Round No.", yMax=200, yMin=0)


def show_overload_at_certain_rnd(graph,debug_enable):
    ol_cnt = 0
    total_ol = 0
    total_ol_pct = 0
    total_load = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] == True:
            continue
        
        if vs_i["load"] > vs_i["capacity"]:
            ol_cnt += 1
            total_ol += (vs_i["load"] - vs_i["capacity"])
            total_ol_pct += (vs_i["load"] - vs_i["capacity"])/(vs_i["capacity"])
        total_load += vs_i["load"]
    
    debug_print(debug_enable,"# of oveloaded nodes:",ol_cnt,"    Percentage",str(int(ol_cnt/graph["remain_node"]*100))+"%")
    debug_print(debug_enable,"Average overload:",total_ol/ol_cnt,"    Average overload percentage:",total_ol_pct/ol_cnt,\
          "    Average load:",total_load/graph["remain_node"])


def show_shutdown_percentage(graph,debug_enable):
    debug_print(debug_enable,"Percentage of shutdown nodes:",1-graph["remain_node"]/graph["total_node"])


def show_shutdown_same_rnd_pcnt(graph):
    print("Number of same rnd ctrl shutdown:",graph["shut_same_rnd"],"   percentage:",\
          graph["shut_same_rnd"]/graph["total_node"])


def show_node_neighbors(graph,vs_idx):
    node_idx_list = graph.neighbors(vs_idx)
    print("Node "+str(vs_idx)+" has neighbors:",node_idx_list)


def show_new_event_interference(graph):
    print(graph["new_event_interference"],"new events occur during the control","  --->  ",graph["new_event_if"],
          "  --->  ","Affected number of nodes:",graph["new_event_if_vs"])


def show_system_state(graph):
    for vs_i in graph.vs:
        if vs_i["shutdown"] == True:
            print("Node index",vs_i.index,"   shut down")
        else:
            print("Node index",vs_i.index,"   load",vs_i["load"],"   capacity",vs_i["capacity"])


def cascading_process(graph,sys_ctrl=False,ctrl_type="local_normal",debug_enable=True,plot_enable=False,exempt_list=[],\
                      plot_list=[],plot_list2=[]):
    remain_node_list = []
    percentage_list = []
    percentage_rnd_list = []
    
    rnd = 0
    current_percentage = 0
    ctrl_start = False
    cascade_start = True
    
    # Order of the loop:
    # (1). Update the state of nodes, check if they are overload, and compute the accumulation
    # (2). Check if there are nodes shutdown in the round
    # (3). After the shutdown of nodes in previous loop, update the state of nodes (e.g. whether overload)
    # (4). Apply control strategy
    #
    # In this way, when a node fails, its load is re-distributed and neighbors states are updated. However, since in
    # step (2) we do not update the state, there will not be the case of "continuous shutdown" in the loop. Then we
    # update the state of a node so that the control can be applied on the "just-overloaded node"
    
    while(cascade_start == True):
        cascade_start = False
        for vs_i in graph.vs:
            if vs_i["shutdown"] == True:
                continue
            elif vs_i["load"] <= vs_i["capacity"]:
                if vs_i["accu_ol"] != 0:
                    vs_i["accu_ol"] = 0
                vs_i["last_rnd_normal"] = True
#                 vs_i["ol_start_time"] = 0
                vs_i["overloaded"] = False
                if vs_i["start_shedding"] == True:
                    vs_i["start_shedding"] = False
                continue
            else:
                vs_i["overloaded"] = True
                if vs_i["accu_ol"] < vs_i["accu_th"]:
                    vs_i["accu_ol"] += (vs_i["load"] - vs_i["capacity"])
                    cascade_start = True
#                     print("Node index",vs_i.index,"has threshold",vs_i["accu_th"],"current accu_ol",vs_i["accu_ol"])
                    
                    if vs_i["last_rnd_normal"] == True:
                        vs_i["last_rnd_normal"] = False
                        vs_i["ol_start_time"] = rnd
                        vs_i["ctrl_start_time"] = rnd
#                         print("Node index",vs_i.index,"set ol_start_time to",rnd)
        
        for vs_i in graph.vs:
            if vs_i["shutdown"] == False and vs_i["overloaded"] == True and vs_i["accu_ol"] > vs_i["accu_th"]:
                debug_print(debug_enable,"Node index",vs_i.index," shut down at round",rnd," load",vs_i["load"])
                shutdown_node(graph, vs_i.index)
                vs_i["shutdown_time"] = rnd
                vs_i["start_shedding"] = False
                
                if vs_i["plan_ctrl_rnd"] == rnd:
                    graph["shut_same_rnd"] += 1
                    debug_print(debug_enable,"---Node index",vs_i.index,"has the same shut-ctrl rnd---")
                
                shutdown_rate = 1 - graph["remain_node"]/graph["total_node"]
                if shutdown_rate >= current_percentage:
                    percentage_list.append(rnd)
                    current_percentage += 0.1
                
                cascade_start = True
        
        update_node_overload_state(graph,rnd)
        
        if sys_ctrl == True: # Perform control to stop cascading
            trip_rate = 1 - graph["remain_node"]/graph["total_node"]
            if trip_rate >= graph["ctrl_th"] and ctrl_start == False:
                ctrl_start = True
                debug_print(debug_enable,"Ctrl starting at round",rnd)
                show_overload_at_certain_rnd(graph,debug_enable)
                show_shutdown_percentage(graph,debug_enable)
                adjust_ctrl_start_time(graph, rnd)
                
            if ctrl_start == True:
                for vs_i in graph.vs:
                    
                    if vs_i["shutdown"] == True:
                        continue
                    elif vs_i["overloaded"] == False:
                        continue
                    elif vs_i["ctrl_start_time"] + vs_i["response_time"] > rnd:
                        debug_print(debug_enable,"Node index",vs_i.index,"has ol_start_time",vs_i["ol_start_time"],\
                              "    control should be performed at rnd",\
                              vs_i["ctrl_start_time"] + vs_i["response_time"],"    current round",rnd)
                        vs_i["plan_ctrl_rnd"] = vs_i["ctrl_start_time"] + vs_i["response_time"]
                        continue
                    else: # Start control strategy
                        if vs_i.index in exempt_list:
                            continue
                        if vs_i["shed_ratio"] >= 1:
                            vs_i["shed_ratio"] = 0.99
                        
                        
                        if ctrl_type == "local_fixed_rate" or ctrl_start == "local_fixed_rate_v2":
                            if vs_i["start_shedding"] == False:
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
                            
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
#                             debug_print(debug_enable,"Node index",vs_i.index,"starts local fixed ctrl at",rnd)
                            load_after_shed = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            if load_after_shed <= vs_i["capacity"]:
                                if ctrl_type == "local_fixed_rate":
                                    vs_i["load"] = load_after_shed
                                elif ctrl_type == "local_fixed_rate_v2":
                                    vs_i["load"] = vs_i["capacity"]
#                                 debug_print(debug_enable,"Node index",vs_i.index,"is successful at round",rnd)
                            else:
                                vs_i["load"] = load_after_shed 
                            
#                             if vs_i["load"] <= vs_i["capacity"]:
#                                 debug_print(debug_enable,"Node index",vs_i.index,"is successful at round",rnd)
                            vs_i["last_rnd_load"] = vs_i["load"]
                        
                        elif ctrl_type == "local_non_adjust":
                            if vs_i["start_shedding"] == False:
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
#                                 print("Node index",vs_i.index,"   capacity",vs_i["capacity"],"   load",vs_i["load"]\
#                                       ,"   round remaining for control",rnd_to_shed,"   shed ratio",vs_i["shed_ratio"],\
#                                       "   accu",vs_i["accu_ol"],"   accu_th",vs_i["accu_th"])
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            load_after_shed = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            if load_after_shed < vs_i["capacity"]:
                                vs_i["load"] = vs_i["capacity"]
                            else:
                                vs_i["load"] = load_after_shed 
                            vs_i["last_rnd_load"] = vs_i["load"]
                            if vs_i["load"] <= vs_i["capacity"]:
                                debug_print(debug_enable,"Node index",vs_i.index,\
                                            "successfully stops overloading at round",rnd)
                                vs_i["start_shedding"] = False
                        
                        elif ctrl_type == "local_adjust":
                            if vs_i["start_shedding"] == False: # start load shedding
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
                            
                            # check if additional events happen
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                                debug_print(debug_enable,"Node index "+str(vs_i.index),"needs adjust   ->   capacity",\
                                             vs_i["capacity"],"   load",vs_i["load"],"   last round load",\
                                             vs_i["last_rnd_load"],"   accu",vs_i["accu_ol"],"   accu_th",vs_i["accu_th"])
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                debug_print(debug_enable,"Node index "+str(vs_i.index),"has",rnd_to_shed,"rnds left",\
                                            "new ratio",vs_i["shed_ratio"])
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            load_after_shed = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            if load_after_shed < vs_i["capacity"]:
                                vs_i["load"] = vs_i["capacity"]
                            else:
                                vs_i["load"] = load_after_shed 
#                             print("Current load:",vs_i["load"])
                            vs_i["last_rnd_load"] = vs_i["load"]
                            if vs_i["load"] <= vs_i["capacity"]:
                                debug_print(debug_enable,"Node index",vs_i.index,"successfully stops overloading at round"\
                                            ,rnd,"   current load",vs_i["load"])
                        
                        else:
                            continue
#         if rnd == 27:
#             print("At round",rnd)
#             show_system_state(graph)
#             break
        remain_node_list.append(graph["remain_node"])
        rnd += 1
    
    graph["cascading_final_stop_time"] = rnd
    debug_print(debug_enable,"Critical rounds at:",percentage_list)
    for i in range(1,len(percentage_list)):
        rnd_rum = percentage_list[i] - percentage_list[i-1]
        percentage_rnd_list.append(rnd_rum)
    
    if plot_enable == True:
        napf.myPlotFig([], remain_node_list, "Remaining Node Number", "Round", "Unit of Node", yMax=100, yMin=0)
    plot_list.append(remain_node_list)
    plot_list2.append(percentage_rnd_list)


def cascading_process_global(graph,sys_ctrl=False,ctrl_type="local_normal",debug_enable=True,plot_enable=False,\
                             exempt_list=[],plot_list=[],plot_list2=[]):
    remain_node_list = []
    percentage_list = []
    percentage_rnd_list = []
    
    rnd = 0
    current_percentage = 0
    ctrl_start = False
    cascade_start = True
    
    while(cascade_start == True):
        cascade_start = False
        for vs_i in graph.vs:
            if vs_i["shutdown"] == True:
                continue
            elif vs_i["load"] <= vs_i["capacity"]:
                if vs_i["accu_ol"] != 0:
                    vs_i["accu_ol"] = 0
                vs_i["last_rnd_normal"] = True
                vs_i["ol_start_time"] = 0
                vs_i["overloaded"] = False
                if vs_i["start_shedding"] == True:
                    vs_i["start_shedding"] = False
                continue
            else:
                vs_i["overloaded"] = True
                if vs_i["accu_ol"] < vs_i["accu_th"]:
                    vs_i["accu_ol"] += (vs_i["load"] - vs_i["capacity"])
                    cascade_start = True
#                     print("Node index",vs_i.index,"has threshold",vs_i["accu_th"],"current accu_ol",vs_i["accu_ol"])
                    
                if vs_i["last_rnd_normal"] == True:
                    vs_i["last_rnd_normal"] = False
                    vs_i["ol_start_time"] = rnd
                    vs_i["ctrl_start_time"] = rnd
#                     print("Node index",vs_i.index,"set ol_start_time to",rnd)
        
        for vs_i in graph.vs:
            if vs_i["shutdown"] == False and vs_i["overloaded"] == True and vs_i["accu_ol"] > vs_i["accu_th"]:
                debug_print(debug_enable,"Node index",vs_i.index," shut down at round",rnd," load",vs_i["load"])
                shutdown_node(graph, vs_i.index)
                vs_i["shutdown_time"] = rnd
                vs_i["start_shedding"] = False
                vs_i["v_failure_time"] = rnd
                vs_i["finally_will_fail"] = True
                
                if vs_i["plan_ctrl_rnd"] == rnd:
                    graph["shut_same_rnd"] += 1
                    debug_print(debug_enable,"---Node index",vs_i.index,"has the same shut-ctrl rnd---")
                
                shutdown_rate = 1 - graph["remain_node"]/graph["total_node"]
                if shutdown_rate >= current_percentage:
                    percentage_list.append(rnd)
                    current_percentage += 0.1
                
                cascade_start = True
            
        update_node_overload_state(graph,rnd)
        
        if sys_ctrl == True: # Perform control to stop cascading
            trip_rate = 1 - graph["remain_node"]/graph["total_node"]
            if trip_rate >= graph["ctrl_th"] and ctrl_start == False:
                ctrl_start = True
                debug_print(debug_enable,"Ctrl starting point is at round",rnd)
                show_overload_at_certain_rnd(graph,debug_enable)
                show_shutdown_percentage(graph,debug_enable)
                adjust_ctrl_start_time(graph, rnd) # adjust the local part of the control
                graph["global_ctrl_start_rnd"] = rnd
                
            if ctrl_start == True:
                # global control starts from the "t + response_time"
                global_start_time = graph["global_ctrl_start_rnd"] + graph["global_response_time"]
                if global_start_time == rnd:
                    update_node_overload_state(graph,rnd)
                    debug_print(debug_enable,"Global Control starts at round",rnd)
#                     show_system_state(graph)
                    graph["global_ctrl_start_time"] = rnd
                    graph["current_failure_percentage"] = current_percentage
                    global_ctrl_schedule(graph,remain_node_list,percentage_list)
                    break
                    
                
                for vs_i in graph.vs:
                    if vs_i["shutdown"] == True:
                        continue
                    elif vs_i["overloaded"] == False:
                        continue
                    elif vs_i["ctrl_start_time"] + vs_i["response_time"] > rnd:
                        debug_print(debug_enable,"Node index",vs_i.index,"has ol_start_time",vs_i["ol_start_time"],\
                              "    LOCAL CONTROL should start at rnd",\
                              vs_i["ctrl_start_time"] + vs_i["response_time"],"    current round",rnd)
                        vs_i["plan_ctrl_rnd"] = vs_i["ctrl_start_time"] + vs_i["response_time"]
                        continue
                    elif vs_i["ctrl_start_time"] + vs_i["response_time"] <= rnd and global_start_time > rnd:
#                         debug_print(debug_enable,"Node index",vs_i.index,"starts LOCAL CONTROL at",rnd)
                        vs_i["shed_in_ctrl"] = True
                        node_shed_ratio = graph["shed_ratio_th"]
                        load_after_shed = vs_i["load"] * (1 - node_shed_ratio)
                        current_old_load = vs_i["load"]
                        if load_after_shed <= vs_i["capacity"]:
                            vs_i["load"] = vs_i["capacity"]
#                             debug_print(debug_enable,"Node index",vs_i.index,"is successful at round",rnd)
                        else:
                            vs_i["load"] = load_after_shed
                        vs_i["total_shed_load"] += (current_old_load - vs_i["load"]) 
                        continue
                    
#         if rnd == 27:
#             print("At round",rnd)
#             show_system_state(graph)
#             break
        remain_node_list.append(graph["remain_node"])
        rnd += 1
    
    debug_print(debug_enable,"Critical rounds at:",percentage_list)
    for i in range(1,len(percentage_list)):
        rnd_rum = percentage_list[i] - percentage_list[i-1]
        percentage_rnd_list.append(rnd_rum)
    
    if plot_enable == True:
        napf.myPlotFig([], remain_node_list, "Remaining Node Number", "Round", "Unit of Node", yMax=100, yMin=0)
    plot_list.append(remain_node_list)
    plot_list2.append(percentage_rnd_list)


def global_ctrl_schedule(graph, remain_node_list, percentage_list):
    v_ol_idx_set = []
    print()
    cal_remaining_node(graph)
    print("Current time:",graph["global_ctrl_start_time"],"  Check the already overloaded nodes")
    for vs_i in graph.vs:
        if vs_i["shutdown"] == True:
            continue
        
        vs_i["global_start_load"] = vs_i["load"]
        vs_i["global_start_accu"] = vs_i["accu_ol"]
        vs_i["global_shed_in_ctrl"] = vs_i["shed_in_ctrl"]
        vs_i["global_total_shed_load"] = vs_i["total_shed_load"]
        
        # First identify the "overloaded" nodes at the start of control, then find out those will be
        # shut down later. For those nodes with successful control, we do not enqueue them.
        if vs_i["overloaded"] == True:
            update_virtual_failure_state(graph, vs_i.index, graph["global_ctrl_start_time"])
            if vs_i["finally_will_fail"] == True:
                temp_set = []
                temp_set.append(vs_i.index)
                temp_set.append(vs_i["v_failure_time"])
                v_ol_idx_set.append(temp_set)
                vs_i["in_queue"] = True
    
    v_ol_idx_set = sorted(v_ol_idx_set, key=lambda myset: myset[1])
    last_event_time = graph["global_ctrl_start_time"]
    
    print("*****Start artificial queue*****")
    while v_ol_idx_set != []:
        print("Queue:",v_ol_idx_set)
        first_event = v_ol_idx_set.pop(0)
        current_event_time = first_event[1]
        print("At time",current_event_time,"before shutdown, the system node state is:")
        cal_remaining_node(graph)
        
        if last_event_time < current_event_time:
            for i in range(last_event_time, current_event_time):
                remain_node_list.append(graph["remain_node"])
                print("Time",i," --> ",graph["remain_node"],"nodes")
            last_event_time = current_event_time
            print("Node",first_event[0],"fails, update remain_node_list","   remaining nodes:",graph["remain_node"])
            cal_remaining_node(graph)
        elif last_event_time == current_event_time:
            None
        else:
            raise NameError("Current time cannot be smaller than the last time")
        
        global_ctrl_shutdown_node(graph, first_event[0], current_event_time, v_ol_idx_set)
        
        shutdown_rate = 1 - graph["remain_node"]/graph["total_node"]
        if shutdown_rate >= graph["current_failure_percentage"]:
            percentage_list.append(current_event_time)
            graph["current_failure_percentage"] += 0.1
        
        if v_ol_idx_set != []:
            v_ol_idx_set = sorted(v_ol_idx_set, key=lambda myset: myset[1])
    
    current_event_time = show_global_lasting_time(graph)
    for i in range(last_event_time, current_event_time + 1):
        remain_node_list.append(graph["remain_node"])
        print("Time",i," --> ",graph["remain_node"],"nodes")
    
    verify_global_node_state(graph)
    graph["cascading_final_stop_time"] = current_event_time


# "global_ctrl_shutdown_node" is used in global control. It shuts down a node, redistribute its load
# to the "still-alive" neighbors. Then each of the "affected" neighbor will compute the "virtual state",
# check if it will "finally shut down" in the future.
def global_ctrl_shutdown_node(graph, shutdown_index, current_time, v_ol_queue):
    print("Shutdown node index",shutdown_index,"at time",current_time)
    global_ctrl_redistribute_power(graph, shutdown_index, current_time, v_ol_queue)
    graph.vs[shutdown_index]["shutdown"] = True
    graph.vs[shutdown_index]["load"] = 0
    graph.vs[shutdown_index]["overloaded"] = False
    graph.vs[shutdown_index]["in_queue"] = False
    graph["remain_node"] -= 1


def global_ctrl_redistribute_power(graph, shutdown_index, current_time, v_ol_queue):
    down_vs = graph.vs[shutdown_index]
    if down_vs["shutdown"] == True: #The node is already shut down
        raise NameError("Node index "+str(shutdown_index)+" is already shut down.")
    
    neighbor_idx_list = graph.neighbors(shutdown_index)
    counter = 0
    for vs_idx in neighbor_idx_list:
        if graph.vs[vs_idx]["shutdown"] != True:
            counter += 1
    
    print("Affected nodes by node",shutdown_index," -> ",end='')
    for vs_idx in neighbor_idx_list:
        if graph.vs[vs_idx]["shutdown"] != True:
            print(vs_idx,"  ",end='')
    print()
    
    if counter != 0:
        power_share = down_vs["load"]/counter
        print("power_share =",power_share)
        for vs_idx in neighbor_idx_list:
            if graph.vs[vs_idx]["shutdown"] != True:
#                 graph.vs[vs_idx]["load"] += power_share
                
                if current_time not in graph.vs[vs_idx]["event_dict"]:
                    graph.vs[vs_idx]["event_dict"][current_time] = 0
                    graph.vs[vs_idx]["event_dict"][current_time] += power_share
                else:
                    graph.vs[vs_idx]["event_dict"][current_time] += power_share
                
                update_virtual_failure_state(graph, vs_idx, current_time)
                
                if graph.vs[vs_idx]["finally_will_fail"] == True:
                    if graph.vs[vs_idx]["in_queue"] == False:
                        print("Add new record for node",vs_idx)
                        temp_set = []
                        temp_set.append(vs_idx)
                        temp_set.append(graph.vs[vs_idx]["v_failure_time"])
                        v_ol_queue.append(temp_set)
                        graph.vs[vs_idx]["in_queue"] = True
                    else:
                        print("Already have the record for node",vs_idx," Update it")
                        for elmt_set in v_ol_queue:
                            if elmt_set[0] == vs_idx:
                                elmt_set[1] = graph.vs[vs_idx]["v_failure_time"]
                                break


def update_virtual_failure_state(graph, idx, current_rnd):
    vs_i = graph.vs[idx]
    if vs_i["shutdown"] == True: #The node is already shut down
        raise NameError("Update virtual state failed, Node index "+str(idx)+" is already shut down.")
    print("At node",vs_i.index,"   current time",current_rnd,"   ini_load="+str(vs_i["global_start_load"]),\
          "   C0="+str(vs_i["capacity"]))
    
    virtual_time = graph["global_ctrl_start_time"]
    vs_i["load"] = vs_i["global_start_load"]
    vs_i["accu_ol"] = vs_i["global_start_accu"]
    vs_i["shed_in_ctrl"] = vs_i["global_shed_in_ctrl"]
    vs_i["total_shed_load"] = vs_i["global_total_shed_load"]
    
    total_extra_load = 0
    for time_key in vs_i["event_dict"]:
        total_extra_load += vs_i["event_dict"][time_key]
    total_overload = max(vs_i["global_start_load"] + total_extra_load - vs_i["capacity"], 0)
    already_shed_load = 0
    
    if total_overload > 0 and vs_i["shed_in_ctrl"] == False:
        vs_i["shed_in_ctrl"] = True
    
    if len(vs_i["event_dict"]) != 0:
        latest_time = max(vs_i["event_dict"].keys(), key=int)
    else:
        latest_time = current_rnd
    
#     if vs_i.index == 2:
    print("latest_time =",latest_time)
    for time_key in vs_i["event_dict"]:
        print("--- LE at round,",time_key,"  total extra load",vs_i["event_dict"][time_key],"---")
    print("Node",vs_i.index,"starts virtual calculation, initial virtual time",virtual_time,"    total load to shed:",\
          total_overload)
    while(True):
        # redistribute load
        if virtual_time in vs_i["event_dict"]:
            vs_i["load"] += vs_i["event_dict"][virtual_time]
            
        # shed load
        node_shed_ratio = graph["shed_ratio_th"]
        max_load_shed = vs_i["load"] * node_shed_ratio
        
        remain_load_to_shed = max(total_overload - already_shed_load, 0)
        if max_load_shed >= remain_load_to_shed:
            vs_i["load"] -= remain_load_to_shed
            vs_i["finally_will_fail"] = False
            vs_i["total_shed_load"] += remain_load_to_shed
        else:
            vs_i["load"] -= max_load_shed
            already_shed_load += max_load_shed
            vs_i["total_shed_load"] += max_load_shed
        
        if virtual_time >= latest_time and vs_i["load"] <= vs_i["capacity"]:
            break
        
        # advance the virtual time
        virtual_time += 1
        
        # update node state
        if vs_i["load"] > vs_i["capacity"]:
            vs_i["accu_ol"] += (vs_i["load"] - vs_i["capacity"])
            if vs_i["accu_ol"] >= vs_i["accu_th"]:
                vs_i["v_failure_time"] = virtual_time
                vs_i["finally_will_fail"] = True
                vs_i["load_at_failure"] = vs_i["load"]
                print("Node",vs_i.index,"is going to shut down at time",virtual_time,"   with load",vs_i["load"])
                break
    
    if vs_i["finally_will_fail"] == False:
        load_diff = vs_i["capacity"] - vs_i["load"]
        vs_i["load"] = vs_i["capacity"]
        vs_i["stop_shedding_time"] = virtual_time
        print("Node",vs_i.index,"will stop overload at time",virtual_time)


def verify_global_node_state(graph):
    counter = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] != True:
            if vs_i["load"] > vs_i["capacity"]:
#                 raise NameError("Node index "+str(vs_i.index)+" should have load no larger than its capacity")
                print("Node index",vs_i.index,"has load",vs_i["load"],"   ---> capacity",vs_i["capacity"])
                counter += 1
    
    if counter == 0:
        print("All active nodes fulfill the load requirement")


def show_global_lasting_time(graph):
    max_time = 0
    for vs_i in graph.vs:
        if vs_i["finally_will_fail"] == True:
            if vs_i["v_failure_time"] > max_time:
                max_time = vs_i["v_failure_time"]
        else:
            if vs_i["stop_shedding_time"] > max_time:
                max_time = vs_i["stop_shedding_time"]
                
    print("Overload lasts until round",max_time)
    
    return max_time


def show_global_greedy_ctrl_remaining_node_state(graph):
    total_remaining_load = 0
    total_remaining_shed_load = 0
    total_capacity_difference = 0
    counter = 0
    
    for vs_i in graph.vs:
        if vs_i["shutdown"] != True:
            if vs_i["shed_in_ctrl"] == True:
                counter += 1
                total_remaining_load += vs_i["load"]
                total_remaining_shed_load += vs_i["total_shed_load"]
                total_capacity_difference += (vs_i["capacity"] - vs_i["load"])
    
    print("Total remaining load of affected nodes:",total_remaining_load,"   Affected nodes:",counter)
    print("Total shed load:",total_remaining_shed_load,"    Total capacity difference:",total_capacity_difference)


def cascading_test(graph,enable_ctrl,stg,ini_idx,exempt_list,mode,value_list,show_detail=False):
    graph_initialization(graph)
    show_graph_basic(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_max_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.1)
    set_global_response_time(graph, 4)
    
    plot_list = []
    plot_list2 = []
    
    for value in value_list: # Start multiple loop cascading test
        copy_graph = deepcopy(graph)
        if mode == "max_delay":
            set_max_delay(copy_graph, value)
        elif mode == "tolerance":
            set_tolerance(copy_graph, value)
        elif mode == "load_shed":
            set_shed_ratio(copy_graph, value)
        elif mode == "response_time":
            set_response_time(copy_graph, value)
        elif mode == "global_response_time":
            set_global_response_time(graph, value)
        elif mode == "ctrl_th":
            set_ctrl_th(copy_graph, value)
        else:
            raise NameError(mode+" is not a valid mode")
        
        print("<-----",mode+":",value,"----->")
        initial_node_event(copy_graph,ini_idx) #17
        if stg == "global":
            cascading_process_global(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
        else: 
            cascading_process(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
            show_shutdown_same_rnd_pcnt(copy_graph)
            show_overload_round(copy_graph,show_detail)
        
        cal_remaining_load(copy_graph)
        cal_remaining_node(copy_graph)
        display_seperate_line()
        
    if mode == "max_delay":
        label_list = []
        for value in value_list:
            temp_text = "$D_0$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different permitted time-delay"
    elif mode == "tolerance":
        label_list = []
        for value in value_list:
            temp_text = "R="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different Tolerance Ratio"
    elif mode == "load_shed":
        label_list = []
        for value in value_list:
            temp_text = "S="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different load shedding amount"
    elif mode == "response_time":
        label_list = []
        for value in value_list:
            temp_text = "$T_R$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different response time ($T_R$)"
    elif mode == "global_response_time":
        label_list = []
        for value in value_list:
            temp_text = "$T_R$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different global response time ($T_R$)"
    elif mode == "ctrl_th":
        label_list = []
        for value in  value_list:
            if value >= 0 and value < 1:
                temp_text = "Start control at "+str(int(value*100))+"% cascading"
            elif value >= 1:
                temp_text = "Shed after round "+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of shedding moment"
    
    x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
    y_label2 = "required number of rounds"
    napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
    napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_strategy_test(graph,enable_ctrl,stg_list,ini_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    show_graph_basic(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.6)
    set_ctrl_th(graph, 0.1)
    set_max_shed_ratio(graph, 0.6)
    
    plot_list = []
    plot_list2 = []
    label_list = []
    
    for stg in stg_list:
        label_list.append(stg)
        copy_graph = deepcopy(graph)
        print("<-----","strategy:",stg,"----->")
        initial_node_event(copy_graph,ini_idx)
        cascading_process(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
        cal_remaining_load(copy_graph)
        show_shutdown_same_rnd_pcnt(copy_graph)
        show_overload_round(copy_graph,show_detail)
        show_new_event_interference(copy_graph)
        display_seperate_line()
    
    if show_graph == True:
        graph_title = "Effect of different strategies"
        x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
        y_label2 = "required number of rounds"
        napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
        napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_newevent_test(graph,enable_ctrl,stg,top_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.1)
    
    result_list = []
    
    for i in range(0,top_idx):
        copy_graph = deepcopy(graph)
        initial_node_event(copy_graph,i)
        cascading_process(copy_graph,enable_ctrl,stg,False,False)
        result_list.append(copy_graph["new_event_if"])
    
    result = 0
    for value in result_list:
        if value != 0:
            result += 1
    
    print(top_idx,"cases tested. Strategy:",stg," -> ",result,"cases have new events observed.")


def cascading_global_strategy_test(graph,enable_ctrl,stg_type,ini_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_max_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.2)
    set_global_response_time(graph, 3)
    
    plot_list = []
    plot_list2 = []
    
    copy_graph = deepcopy(graph)
    initial_node_event(copy_graph,ini_idx) #17
    cascading_process_global(copy_graph,enable_ctrl,stg_type,True,False,exempt_list,plot_list,plot_list2)
    cal_remaining_load(copy_graph)
    show_global_greedy_ctrl_remaining_node_state(copy_graph)
    display_seperate_line()
    
    label_list = ["Global Strategy"]
    graph_title = "Global Strategy Test"
    x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
    y_label2 = "required number of rounds"
    napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
    napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_global_local_compare(graph,enable_ctrl,stg_list,ini_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_max_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.2)
    set_global_response_time(graph, 4)
    
    plot_list = []
    plot_list2 = []
    label_list = []
    
    for stg in stg_list:
        label_list.append(stg)
        copy_graph = deepcopy(graph)
        print("<-----","strategy:",stg,"----->")
        initial_node_event(copy_graph,ini_idx)
        if stg == "global":
            cascading_process_global(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
        else:
            cascading_process(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
        cal_remaining_node(copy_graph)
        cal_remaining_load(copy_graph)
        display_seperate_line()
    
    if show_graph == True:
        graph_title = "Effect of different strategies"
        x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
        y_label2 = "required number of rounds"
        napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
        napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_GL_compare_multiple(graph,enable_ctrl,stg,top_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_max_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.1)
    set_global_response_time(graph, 4)
    
    result_list_load = []
    result_list_node = []
    result_list_time = []
    
    for i in range(0,top_idx):
        copy_graph = deepcopy(graph)
        initial_node_event(copy_graph,i)
        if stg == "global":
            cascading_process_global(copy_graph,enable_ctrl,stg,False,False,exempt_list)
        else:
            cascading_process(copy_graph,enable_ctrl,stg,False,False,exempt_list)
        
        result_list_load.append(cal_remaining_load(copy_graph))
        result_list_node.append(cal_remaining_node(copy_graph))
        result_list_time.append(copy_graph["cascading_final_stop_time"])
    
    print("Average node remaining:",np.mean(result_list_node))
    print("Average load remaining:",str(int(np.mean(result_list_load)*100))+"%")
    print("Average time to complete:",np.mean(result_list_time))


myGraph = ig.Graph.Watts_Strogatz(1, 60, 2, 0.15)
for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"

strategy_list = ["local_fixed_rate","local_non_adjust","local_adjust"]
stg_list_GL = ["global","local_fixed_rate"]
# show_node_neighbors(myGraph, 9)
# plotGraph(myGraph)
# plotGraph(myGraph, "System_60_nodes.png")

# cascading_test(myGraph,True,"global",17,[],"max_delay",[5,4,3,2,1])
# cascading_test(myGraph,False,"global",17,[],"tolerance",[1.3,1.25,1.2,1.15,1.1,1.05,1.0])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [],"load_shed", [0.4,0.3,0.24,0.2,0.1])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [], "response_time", [4,3,2,1,0])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [], "ctrl_th", [0.3,0.2,0.1,0.0])
# cascading_test(myGraph, True, "global", 17, [], "global_response_time", [5,4,3,2,1])

# cascading_strategy_test(myGraph, True, strategy_list, 22, show_graph=True)
# cascading_newevent_test(myGraph, True, "local_fixed_rate", 60, False)

cascading_global_strategy_test(myGraph, True, "global", 17, show_graph=True)
# cascading_global_local_compare(myGraph, True, stg_list_GL, 59, show_graph=True)
# cascading_GL_compare_multiple(myGraph, True, "global", 60, show_graph=False)