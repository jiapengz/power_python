'''
Created on Mar 23, 2016

@author: Zhang Jiapeng
'''

import numeric_analysis.plotFunction as npf
import os.path
import re
from _overlapped import NULL


input1_1 = [2, 5, 8, 10, 15, 20, 25]
input1_2 = [16.6, 26.6, 29.1, 30, 31.1, 31.6, 32]
title1 = "Change of load on line3."
xlabel1 = "load(bus2):load(bus3)"
ylabel1 = "load(line3):load(bus2)(%)"


input2_1 = [2, 3, 4, 5, 6, 7, 8, 12, 16, 20]
input2_2 = [25, 40, 50, 57, 62, 66, 70, 78, 83, 86.44]
title2 = "Change of load on line3 with different impedance of line1."
xlabel2 = "impedance1:impednace2"
ylabel2 = "load(line3):load(bus2)(%)"


input3_1 = [1, 2, 3, 4, 5, 6, 7]
input3_2 = [4, 3, 2.5, 2.5, 2.4, 2.35, 2.35]
title3 = "Different impedance of line1 when half load across line3."
xlabel3 = "load(bus2):load(bus3)"
ylabel3 = "impedance1:impednace2"


input4_1 = [300, 800, 1300, 1800, 2300]
input4_2 = [41.73, 31.37, 29.04, 28.04, 27.51]
title4 = "With the same difference of load, the percentage of load across line3."
xlabel4 = "load(bus2)(MW)"
ylabel4 = "load(line3):load(bus2)(%)"
flabel4 = "Difference=200MW"


input5_1 = [2, 4, 6, 8, 10, 15, 20, 25, 30]
input5_2 = [-5, 10, 16.2, 20, 22.3, 25.6, 27.3, 28.5, 29]
input5_3 = [11.76, 20.67, 24.35, 26.37, 27.63, 29.39, 30.3, 30.8, 31.2]
title5 = "The percentage of load across line3 and line5."
xlabel5 = "load(bus4):load(bus3)"
ylabel5 = "load(line3):load(line5)(%)"
flabel5_1 = "line2 impedance doubles"
flabel5_2 = "line5 impedance doubles"
# list_input5_1 = []
list_input5_2 = []
# list_input5_1.append(input5_1)
# list_input5_1.append(input5_1)
list_input5_2.append(input5_2)
list_input5_2.append(input5_3)
flabel5_list = []
flabel5_list.append(flabel5_1)
flabel5_list.append(flabel5_2)
# ------------------------------------------------------------------------------------------------------
# ------------------------------------------------------------------------------------------------------


def plotfunc(in1, in2, plottl, xlbl, ylbl, fl='', yMax_v=100, yMin_v=0):
    npf.myPlotFig(in1, in2, plottl, xlbl, ylbl, figLabel=fl, yMax=yMax_v, yMin=yMin_v)
    
    
def plotfunc_list(in1, in2, plottl, xlbl, ylbl, fl='', yMax_v=100, yMin_v=0):
    npf.myPlotFig_List_WithX(in1, in2, plottl, xlbl, ylbl, labelNameList=fl, ymax=yMax_v, ymin=yMin_v)
    


def compute_total_from_list(ydata_list, label_list):
    temp_list = []
    if len(ydata_list) < 2:
        return
    
    len1 = len(ydata_list)
    for i in range(len(ydata_list[0])):
        temp = 0
        for j in range(len1):
            temp += ydata_list[j][i]
        temp_list.append(temp)
    
    ydata_list.append(temp_list)
    label_list.append("Sum")

def plot_from_folder(filename, yMax_v=100, yMin_v=0, mode="all", enable_log=False, enable_sum=False):
    if os.path.isfile(filename) == False:
        print("File does not exist!")
        return
    else:
        fp = open(filename)
        xdata = []
        yDataList = []
        line_num = 0
        for myLine in fp:
            line_num += 1
            if line_num == 1: # Title
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                plottl = splitLine[1]
                continue
            elif line_num == 2: # x-axis label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                xlbl = splitLine[1]
                continue
            elif line_num == 3: # y-axis label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                ylbl = splitLine[1]
                continue
            elif line_num == 4: # figure label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                fl = []
                for i in range(len(splitLine)):
                    if i == 0:
                        continue
                    else:
                        fl.append(splitLine[i])
                continue
            
            if myLine == '\n': # skip empty line
                continue
            
            myLine = myLine.strip() # get rid the '\n' at each line
            #splitLine = myLine.split(' ')
            splitLine = re.split(' |, ', myLine)
            
            if xdata == []:
                for x_var in splitLine:
                    xdata.append(float(x_var))
            else:
                temp = []
                for y_var in splitLine:
                    temp.append(float(y_var))
                yDataList.append(temp)
        
        if enable_sum == True:
            compute_total_from_list(yDataList, fl)
        
    if mode == "all":
        if len(yDataList) == 1:
            npf.myPlotFig(xdata, yDataList[0], plottl, xlbl, ylbl, figLabel=fl[0], yMax=yMax_v, yMin=yMin_v)
        elif len(yDataList) > 1:
            npf.myPlotFig_List_WithX(xdata, yDataList, plottl, xlbl, ylbl, labelNameList=fl, ymax=yMax_v, ymin=yMin_v,\
                                     en_log = enable_log)
        else:
            print("No data to plot!")
    elif mode == "diff" or mode == "diff_ratio":
        y_diff = []
        for i in range(0, len(yDataList), 2):
            temp = []
            for j in range(len(yDataList[i])):
                if mode == "diff":
                    diff_result = yDataList[i+1][j] - yDataList[i][j]
                elif mode == "diff_ratio":
                    if yDataList[i][j] != 0:
                        diff_result = (yDataList[i+1][j] - yDataList[i][j]) / yDataList[i][j]
                    else:
                        diff_result = 0
                temp.append(round(diff_result,5))
            
            print(temp)
            y_diff.append(temp)
        
        plottl = "Difference of analysis and simulation for iterative area attack"
        #plottl = "Compare analysis/simulation for iterative area attack (majority rule)"
        #plottl = "Difference of cascading attack line trip (critical relays $P_f=0.9$)"
        #ylbl = "Extra reputation cascading trip ratio "+r"$(\frac{reputation-original}{origianl})$"
        xlbl = "Attack resource"
        #xlbl = "Initial line ($n_k$)"
        #fl = ["Extra damage ratio"]
        #plottl = "Difference of analysis/simulation for one-round area attack ($L=120$ $K=5$)"
        ylbl = "Percentage of difference "+r"$(\frac{simulation\ -\ analysis}{analysis})$"
        #xlbl = "Percentage of compromised line (%)"
        fl = [r"$P_f=0.2$", r"$P_f=0.5$", r"$P_f=0.8$"]
        
        if len(y_diff) == 1:
            npf.myPlotFig(xdata, y_diff[0], plottl, xlbl, ylbl, figLabel=fl[0], yMax=yMax_v, yMin=yMin_v)
        elif len(yDataList) > 1:
            npf.myPlotFig_List_WithX(xdata, y_diff, plottl, xlbl, ylbl, labelNameList=fl, ymax=yMax_v, ymin=yMin_v)
        else:
            print("No data to plot!")



def plot_from_folder_imp_index(filename, yMax_v=100, yMin_v=0):
    if os.path.isfile(filename) == False:
        print("File does not exist!")
        return
    else:
        fp = open(filename)
        xdata = []
        yDataList = []
        line_num = 0
        for myLine in fp:
            line_num += 1
            if line_num == 1: # Title
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                plottl = splitLine[1]
                continue
            elif line_num == 2: # x-axis label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                xlbl = splitLine[1]
                continue
            elif line_num == 3: # y-axis label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                ylbl = splitLine[1]
                continue
            elif line_num == 4: # figure label
                myLine = myLine.strip()
                splitLine = myLine.split(',')
                fl = []
                for i in range(len(splitLine)):
                    if i == 0:
                        continue
                    else:
                        fl.append(splitLine[i])
                continue
            
            myLine = myLine.strip() # get rid the '\n' at each line
            splitLine = myLine.split(', ')
            
            if xdata == []:
                for x_var in splitLine:
                    xdata.append(float(x_var))
            else:
                temp = []
                for y_var in splitLine:
                    temp.append(float(y_var))
#                 yDataList.append(temp)
                
                temp2 = []
                for elmnt in temp:
                    temp2.append(round((100-elmnt)/elmnt,2))
                print(temp2)
                yDataList.append(temp2)
                
        if len(yDataList) == 1:
            npf.myPlotFig(xdata, yDataList[0], plottl, xlbl, ylbl, figLabel=fl[0], yMax=yMax_v, yMin=yMin_v)
        elif len(yDataList) > 1:
            npf.myPlotFig_List_WithX(xdata, yDataList, plottl, xlbl, ylbl, labelNameList=fl, ymax=yMax_v, ymin=yMin_v)
        else:
            print("No data to plot!")


def compute_avg_of_list(filename, yMax_v, yMin_v):
    if os.path.isfile(filename) == False:
        print("File does not exist!")
        return
    else:
        fp = open(filename)
        
        xdata = []
        yDataList = []
        line_num = 0
        
        for myLine in fp:
            line_num += 1
            
            myLine = myLine.strip() # get rid the '\n' at each line
            splitLine = myLine.split(', ')
            
            if xdata == []:
                for x_var in splitLine:
                    xdata.append(float(x_var))
            else:
                if yDataList == []:
                    for y_var in splitLine:
                        yDataList.append(float(y_var))
                else:
                    for i in range(len(splitLine)):
                        yDataList[i] += float(splitLine[i])
        
        result_list = [x/line_num for x in yDataList]
        
        print(result_list)


# plotfunc(input1_1, input1_2, title1, xlabel1, ylabel1)
# plotfunc(input2_1, input2_2, title2, xlabel2, ylabel2)
# plotfunc(input3_1, input3_2, title3, xlabel3, ylabel3, yMax_v=5, yMin_v=0)
# plotfunc(input4_1, input4_2, title4, xlabel4, ylabel4, flabel4)

# plotfunc_list(input5_1, list_input5_2, title5, xlabel5, ylabel5, flabel5_list)


folderName = "F:\\power_note\\note_02_21_17\\figures5\\cas_new\\"
fileName = "data_cas_reputation_cmp2.txt"
totalPath = folderName + fileName

# compute_avg_of_list(totalPath, yMax_v=120, yMin_v=0)
# plot_from_folder(totalPath,yMax_v=120, yMin_v=0, mode="all", enable_log=False)
plot_from_folder(totalPath,yMax_v=1, yMin_v=0, mode="all", enable_log=True)
# plot_from_folder(totalPath,yMax_v=0.8, yMin_v=-0.8, mode="diff_ratio")
# plot_from_folder_imp_index(totalPath,yMax_v=40, yMin_v=0)