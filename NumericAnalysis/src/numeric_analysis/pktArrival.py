'''
Created on Dec 12, 2014

@author: Zhang Jiapeng
'''

import matplotlib.pyplot as plt
import math as mt
import operator
import numpy as np
from functools import reduce
from copy import deepcopy

marker = ['.', #point marker 
',', #pixel marker 
'o', #circle marker 
'v', #triangle_down marker 
'^', #triangle_up marker 
'<', #triangle_left marker 
'>', #triangle_right marker 
'1', #tri_down marker 
'2', #tri_up marker 
'3', #tri_left marker 
'4', #tri_right marker 
's', #square marker 
'p', #pentagon marker 
'*', #star marker 
'h', #hexagon1 marker 
'H', #hexagon2 marker 
'+', #plus marker 
'x', #x marker 
'D', #diamond marker 
'd', #thin_diamond marker 
'|', #vline marker 
'_', #hline marker 
]

color = ['b', #blue 
'g', #green 
'r', #red 
'c', #cyan 
'm', #magenta 
'y', #yellow 
'k', #black 
'w', #white 
]

def c(n,k):  
    return  reduce(operator.mul, range(n - k + 1, n + 1)) /reduce(operator.mul, range(1, k +1))  

def fac(n):
    return reduce(operator.mul, range(1,n+1))  


def myPlotFig(xData,yData,title,xLabel,yLabel,lty='s-',figLabel=''):
    plt.plot(xData,yData,lty,label=figLabel,markersize=5)
#     minNum = min(yData)
    for i in range(len(xData)): # Add annotation
        xyStr = '%.1e' % (round(yData[i],8))
        plt.annotate(xyStr, xy = (xData[i], yData[i]), xytext = (xData[i], yData[i]))
    plt.grid()
    plt.legend(loc="best")
    plt.title(title)
    plt.xlabel(xLabel)
    plt.ylabel(yLabel)
#     plt.show()

# "givenLoad" is an index in [0,1] to indicate the amount of reservation 
def effectOfBinNo(givenLoad=0):
    global marker
    BN = [2,6,10,14,18,22,26,30]
    flow_No_Set = [10,20,30]
    P_AR = []
    AVG_Load = []
    Ca_to_rsv = []
    result = 0
    resultSet = []
    
    for flow_no in flow_No_Set:
        del P_AR[:]
        del AVG_Load[:]
        del Ca_to_rsv[:]
        del resultSet[:]
        
        for i in BN:
            P_AR.append(1/i)
            if givenLoad == 0: # This is average load
                meanLoad = (flow_no/i)
            elif givenLoad > 0 and givenLoad <= 1:
                meanLoad = (flow_no*givenLoad)
            else:
                meanLoad = (givenLoad)
            
            AVG_Load.append(meanLoad)
            if meanLoad < 1:
                meanLoad = 1
            
            Ca_to_rsv.append(meanLoad)
            
        print(P_AR)
        print(AVG_Load)
        print(Ca_to_rsv)
            
        for i in range(len(BN)):
            result = 0
            N_Prime = mt.floor(Ca_to_rsv[i])
            for j in range(N_Prime+1,flow_no+1):
                #print(i,j)
                result = result + pow(P_AR[i],j)*pow((1-P_AR[i]),flow_no-j)*(j-N_Prime)/j
            
            resultSet.append(result*BN[i])
        
        myLabel = "flow="+str(flow_no)
        myMarker = marker[flow_No_Set.index(flow_no)]+'-'
        myPlotFig(BN, resultSet, "Violation Probability", "Number of Bin", "P(v)",\
                  lty=myMarker,figLabel=myLabel)
    
    plt.show()


def plotBar():
    data1=[44.199,79.360,148.000]
    data2=[28.800,25.600,39.019]
    barIndex=np.arange(3)
    bar_width = 0.35
#     ["13Bus","24Bus","39Bus"]
    plt.bar(barIndex, data1, bar_width, color='b', label='Common RSV') #facecolor between (0,1)
    plt.bar(barIndex+bar_width, data2, bar_width, color='r', label='Smart RSV')
    
    plt.xlabel('System Type')
    plt.ylabel('Capacity Reserved (kbps)')
    plt.title('Reservation Comparison')
    plt.xticks(barIndex+bar_width, ("13Bus","24Bus","39Bus")) 
    plt.xlim(-bar_width,len(barIndex)) 
    plt.legend(loc='best')
    plt.tight_layout()
    plt.show()
    
    
def plotImprove():
    data1=[45,167,293,399,1591]
    data2=[29,56,78,70,172]
    dataDiff=[]
    dataImp=[]
#     xIndex=["13Bus","24Bus","39Bus"]
    
    for i in range(len(data1)):
        dataDiff.append(data1[i]-data2[i])
        dataImp.append(dataDiff[i]/data1[i])
    
    barIndex=np.arange(5)
    bar_width = 0.35
    

    plt.plot(dataImp,'bs-',markersize=10)

    plt.grid()
    plt.xlabel('System Type')
    plt.ylabel('Capacity Saved (%)')
    plt.title('Reservation Improvement')
    plt.xticks(barIndex, ("13-Bus","24-Bus","39-Bus","57-Bus","118-Bus")) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width) 
#     plt.legend(loc='best')
    plt.tight_layout()
    plt.ylim(0,1)
    plt.show()
    
# print(pow(0.5,30)*16)
# s = '%.2E' % (round(0.0000001,7))
# print(s)
# print(fac(5),c(5,2))
# RT = 0
# for i in range(15,30):
#     RT = RT + c(29,i)
# print(RT*pow(0.5,30))

def plotCurve():
    data1=[45,167,293,399,1591]
    data2=[29,56,78,70,172]
    data3=[52,100,145,125,275]
    
    barIndex=np.arange(5)
    bar_width = 0.35
    markerSize = 10
    
    plt.plot(data1,'bs-',label="Static Reservation",markersize=markerSize)
    plt.plot(data2,'r*--',label="Smart Reservation",markersize=markerSize)
    plt.plot(data3,'k^:',label="Two HF Reservation",markersize=markerSize)
    
    plt.grid()
    plt.legend(loc="best")
    plt.xlabel('System Type')
    plt.ylabel('Capacity Reserved (kbps)')
    plt.title('Reservation Comparison')
    plt.xticks(barIndex, ("13-Bus","24-Bus","39-Bus","57-Bus","118-Bus")) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width) 
    plt.ylim(0,1650)
    plt.show()
    

def plotTrend(hop_cnt,dstSft,pw=1):
    totalList = []
    weightList = []
    trendList = []
    temp = []
    for sft in range(dstSft+1):
        temp.clear()
        for j in range(pw):
            temp.clear()
            for i in range(hop_cnt):
                temp.append(pow(i+1+sft,j+1))
            totalList.append(deepcopy(temp))
        
    print(totalList)
    #print(sum(totalList[2]))
    
    for oneList in totalList:
        temp.clear()
        listSum = sum(oneList)
        for elemt in oneList:
            temp.append(round(listSum/elemt,3))
        weightList.append(deepcopy(temp))
    
    print(weightList)
    
    for oneList in weightList:
        temp.clear()
        listSum = sum(oneList)
        for elemt in oneList:
            temp.append(round(elemt/listSum,3))
        trendList.append(deepcopy(temp))
    
    print(trendList)
    newList = []
    for i in range(hop_cnt):
        newList.append(round(1/hop_cnt,3))
    
    trendList.append(deepcopy(newList))
    print(trendList)
    
    # Plot the trend curve
    barIndex=np.arange(hop_cnt)
    bar_width = 0.35
    markerSize = 10
    for subList in trendList:
        global marker
        global color
        idx = trendList.index(subList)
        markerLen = len(marker)
        colorLen = len(color)
        ls = marker[idx % markerLen] + color[idx % colorLen] + '-'
        
        if idx == len(trendList)-1:
            labelName = "Equally"
        else:
            labelName = 'Shift='+str(idx)
        
        plt.plot(subList,ls,label=labelName,markersize=markerSize)
    
    plt.grid()
    plt.legend(loc="best")
    plt.xlabel('Hop Count')
    plt.ylabel('Delay Proportion')
    plt.xticks(barIndex, ("1","2","3","4","5","6")) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width) 
    plt.show()
    
    
def plotTrend_v2(hop_cnt,pw):
    totalList = []
    weightList = []
    trendList = []
    temp = []
    for sft in range(pw+1):
        temp.clear()
        for i in range(hop_cnt):
            temp.append(pow(i+1,sft+1))
        totalList.append(deepcopy(temp))
        
    print(totalList)
    #print(sum(totalList[2]))
    
    for oneList in totalList:
        temp.clear()
        listSum = sum(oneList)
        for elemt in oneList:
            temp.append(round(listSum/elemt,3))
        weightList.append(deepcopy(temp))
    
    print(weightList)
    
    for oneList in weightList:
        temp.clear()
        listSum = sum(oneList)
        for elemt in oneList:
            temp.append(round(elemt/listSum,3))
        trendList.append(deepcopy(temp))
    
    print(trendList)
    newList = []
    for i in range(hop_cnt):
        newList.append(round(1/hop_cnt,3))
    
    trendList.append(deepcopy(newList))
    print(trendList)
    
    # Plot the trend curve
    barIndex=np.arange(hop_cnt)
    bar_width = 0.35
    markerSize = 10
    for subList in trendList:
        global marker
        global color
        idx = trendList.index(subList)
        markerLen = len(marker)
        colorLen = len(color)
        ls = marker[idx % markerLen] + color[idx % colorLen] + '-'
        
        if idx == len(trendList)-1:
            labelName = "Equally"
        else:
            labelName = 'Shift='+str(idx)
        
        plt.plot(subList,ls,label=labelName,markersize=markerSize)
    
    plt.grid()
    plt.legend(loc="best")
    plt.xlabel('Hop Count')
    plt.ylabel('Delay Proportion')
    plt.xticks(barIndex, ("1","2","3","4","5","6")) 
    plt.xlim(-bar_width,len(barIndex)-2*bar_width) 
    plt.show()

# plotBar()
plotCurve()
# plotImprove()

# plotTrend(6, 2, 2)
# plotTrend_v2(6, 3)
