'''
Created on Oct 16, 2015

@author: Zhang Jiapeng
'''

import igraph as ig
import random
import numeric_analysis.plotFunction as napf
import numpy as np
from copy import deepcopy
import math

# igraph uses the python RNG generator
random.seed(12345)

def plotGraph(graph,fileName=None,graphLayout=None):
    if graphLayout != None:
        myLayout = ig.Layout(graphLayout)
    else:
        myLayout = graph.layout_kamada_kawai()
    graph.vs["label"] = graph.vs["name"]
    ig.plot(graph, layout = myLayout, bbox = (800,800), margin = 50)
    if fileName != None:
        ig.plot(graph, fileName, layout = myLayout, bbox = (800,800), margin = 50)


def debug_print(enablePrint, *args):
    if enablePrint == True:
        for i in range(len(args)):
            if i == len(args)-1:
                print(args[i])
            else:
                print(args[i],"",end='')


def show_graph_basic(graph):
    print("Number of nodes =",len(myGraph.vs),"       Number of edges =",len(myGraph.es))
    cal_avg_degree(graph)
    print("---------------------------------------------------------------------------","\n")


def cal_avg_degree(graph):
    m_deg = ig.mean(graph.degree())
    print("Average degree =",m_deg)


def graph_initialization(graph):
    graph["total_node"] = len(graph.vs)
    graph["remain_node"] = len(graph.vs)
    graph["ctrl_th"] = 0
    graph["total_load"] = 0
    graph["shed_ratio_th"] = 1
    graph["shut_same_rnd"] = 0
    graph["new_event_interference"] = 0
    graph["new_event_if"] = 0
    graph["new_event_if_vs"] = 0
    
    for vs_i in graph.vs:
        vs_i["remain_deg"] = graph.degree(vs_i.index)
        vs_i["initial_load"] = random.normalvariate(1,0.05)
        vs_i["load"] = vs_i["initial_load"]
        vs_i["tolerance"] = 1.2
        vs_i["max_delay"] = 1.0
        vs_i["capacity"] = vs_i["initial_load"] * vs_i["tolerance"]
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]
        vs_i["accu_ol"] = 0
        
        vs_i["overloaded"] = False
        vs_i["ol_start_time"] = 0
        vs_i["shutdown"] = False
        vs_i["shutdown_time"] = 0
        
        vs_i["ctrl_start_time"] = 0
        vs_i["shed_ratio"] = 1
        vs_i["response_time"] = 5
        vs_i["start_shedding"] = False
        vs_i["last_rnd_load"] = 0
        vs_i["last_rnd_normal"] = True
        vs_i["plan_ctrl_rnd"] = 0
        
        vs_i["new_event_affect"] = False
        
        graph["total_load"] += vs_i["initial_load"]


def set_max_delay(graph,max_delay):
    for vs_i in graph.vs:
        vs_i["max_delay"] = max_delay
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]


def set_tolerance(graph,tolerance):
    for vs_i in graph.vs:
        vs_i["tolerance"] = tolerance
        vs_i["capacity"] = vs_i["initial_load"] * vs_i["tolerance"]
        vs_i["accu_th"] =  vs_i["max_delay"] * 0.5 * vs_i["capacity"]


def set_shed_ratio(graph,shed_ratio):
    for vs_i in graph.vs:
        vs_i["shed_ratio"] = shed_ratio


def set_response_time(graph,rt):
    for vs_i in graph.vs:
        vs_i["response_time"] = rt


def set_ctrl_th(graph,ctrl_th):
    graph["ctrl_th"] = ctrl_th


def set_max_shed_ratio(graph,max_shed_ratio):
    graph["shed_ratio_th"] = max_shed_ratio

def shutdown_node(graph,shutdown_index):
    redistribute_power(graph, shutdown_index)
    graph.vs[shutdown_index]["shutdown"] = True
    graph.vs[shutdown_index]["load"] = 0
    graph["remain_node"] -= 1


def redistribute_power(graph,shutdown_index,show_redistribution=False):
    down_vs = graph.vs[shutdown_index]
    if down_vs["shutdown"] == True: #The node is already shut down
        raise NameError("Node index "+str(shutdown_index)+" is already shut down.")
    
    neighbor_idx_list = graph.neighbors(shutdown_index)
    counter = 0
    for vs_idx in neighbor_idx_list:
        if graph.vs[vs_idx]["shutdown"] != True:
            counter += 1
    
    if counter != 0:
        power_share = down_vs["load"]/counter
        for vs_idx in neighbor_idx_list:
            if graph.vs[vs_idx]["shutdown"] != True:
                graph.vs[vs_idx]["load"] += power_share
                
                if graph.vs[vs_idx]["start_shedding"] == True:
                    graph["new_event_interference"] += 1


def initial_node_event(graph,initial_idx=-1,debug_enable=True):
    if initial_idx >= 0:
        shutdown_idx = random.randint(0,len(graph.vs)-1)
        shutdown_idx = initial_idx
    else:
        shutdown_idx = 0
    
    debug_print(debug_enable,"Initial shut down node index #",shutdown_idx,"   neighbors:",graph.neighbors(shutdown_idx))
    shutdown_node(graph, shutdown_idx)


def adjust_ctrl_start_time(graph,current_rnd):
    for vs_i in graph.vs:
        if vs_i["overloaded"] == True:
            if vs_i["ctrl_start_time"] < current_rnd:
                vs_i["ctrl_start_time"] = current_rnd


def cal_remaining_load(graph):
    total_load = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] != True:
            total_load += vs_i["load"]
    
    final_pct = total_load/graph["total_load"]
    print("Remaining load:",str(int(final_pct*100))+"%")


def display_seperate_line():
    print("---------------------------------------------------------------------------","\n")


def show_overload_round(graph,plot_enable=True):
    temp_list = []
    ol_start_list = []
    for vs_i in graph.vs:
        ol_round = vs_i["shutdown_time"] - vs_i["ol_start_time"]
        temp_list.append(ol_round)
        ol_start_list.append(vs_i["ol_start_time"])
    
    print("Average rounds to trip a line:",np.mean(temp_list))
    
    if plot_enable == True:
        napf.myPlotFig([], temp_list, "Nodes Overload Rounds", "Node Index", "Unit of Round", yMax=200, yMin=0)
        napf.myPlotFig([], ol_start_list, "Overload Start Rounds", "Node Index", "Round No.", yMax=200, yMin=0)


def show_overload_at_certain_rnd(graph,debug_enable):
    ol_cnt = 0
    total_ol = 0
    total_ol_pct = 0
    total_load = 0
    for vs_i in graph.vs:
        if vs_i["shutdown"] == True:
            continue
        
        if vs_i["load"] > vs_i["capacity"]:
            ol_cnt += 1
            total_ol += (vs_i["load"] - vs_i["capacity"])
            total_ol_pct += (vs_i["load"] - vs_i["capacity"])/(vs_i["capacity"])
        total_load += vs_i["load"]
    
    debug_print(debug_enable,"# of oveloaded nodes:",ol_cnt,"    Percentage",str(int(ol_cnt/graph["remain_node"]*100))+"%")
    debug_print(debug_enable,"Average overload:",total_ol/ol_cnt,"    Average overload percentage:",total_ol_pct/ol_cnt,\
          "    Average load:",total_load/graph["remain_node"])


def show_shutdown_percentage(graph,debug_enable):
    debug_print(debug_enable,"Percentage of shutdown nodes:",1-graph["remain_node"]/graph["total_node"])


def show_shutdown_same_rnd_pcnt(graph):
    print("Number of same rnd ctrl shutdown:",graph["shut_same_rnd"],"   percentage:",\
          graph["shut_same_rnd"]/graph["total_node"])


def show_node_neighbors(graph,vs_idx):
    node_idx_list = graph.neighbors(vs_idx)
    print("Node "+str(vs_idx)+" has neighbors:",node_idx_list)


def show_new_event_interference(graph):
    print(graph["new_event_interference"],"new events occur during the control","  --->  ",graph["new_event_if"],
          "  --->  ","Affected number of nodes:",graph["new_event_if_vs"])


def cascading_process(graph,sys_ctrl=False,ctrl_type="local_normal",debug_enable=True,plot_enable=False,exempt_list=[],\
                      plot_list=[],plot_list2=[]):
    remain_node_list = []
    percentage_list = []
    percentage_rnd_list = []
    
    rnd = 0
    current_percentage = 0
    ctrl_start = False
    cascade_start = True
    
    while(cascade_start == True):
        cascade_start = False
        for vs_i in graph.vs:
            if vs_i["shutdown"] == True:
                continue
            elif vs_i["load"] <= vs_i["capacity"]:
                if vs_i["accu_ol"] != 0:
                    vs_i["accu_ol"] = 0
                vs_i["last_rnd_normal"] = True
                vs_i["ol_start_time"] = 0
                vs_i["overloaded"] = False
                if vs_i["start_shedding"] == True:
                    vs_i["start_shedding"] = False
                continue
            else:
                vs_i["overloaded"] = True
                if vs_i["accu_ol"] < vs_i["accu_th"]:
                    vs_i["accu_ol"] += (vs_i["load"] - vs_i["capacity"])
                    cascade_start = True
#                     print("Node index",vs_i.index,"has threshold",vs_i["accu_th"],"current accu_ol",vs_i["accu_ol"])
                    
                    if vs_i["last_rnd_normal"] == True:
                        vs_i["last_rnd_normal"] = False
                        vs_i["ol_start_time"] = rnd
                        vs_i["ctrl_start_time"] = rnd
                else:
                    debug_print(debug_enable,"Node index",vs_i.index," shut down at round",rnd," load",vs_i["load"])
                    shutdown_node(graph, vs_i.index)
                    vs_i["shutdown_time"] = rnd
                    vs_i["start_shedding"] = False
                    
                    if vs_i["plan_ctrl_rnd"] == rnd:
                        graph["shut_same_rnd"] += 1
                        debug_print(debug_enable,"---Node index",vs_i.index,"has the same shut-ctrl rnd---")
                    
                    shutdown_rate = 1 - graph["remain_node"]/graph["total_node"]
                    if shutdown_rate >= current_percentage:
                        percentage_list.append(rnd)
                        current_percentage += 0.1
                    
                    cascade_start = True
        
        if sys_ctrl == True: # Perform control to stop cascading
            trip_rate = 1 - graph["remain_node"]/graph["total_node"]
            if trip_rate >= graph["ctrl_th"] and ctrl_start == False:
                ctrl_start = True
                debug_print(debug_enable,"Ctrl starting at round",rnd)
                show_overload_at_certain_rnd(graph,debug_enable)
                show_shutdown_percentage(graph,debug_enable)
                adjust_ctrl_start_time(graph, rnd)
                
            if ctrl_start == True:
                for vs_i in graph.vs:
                    
                    if vs_i["shutdown"] == True:
                        continue
                    elif vs_i["overloaded"] == False:
                        continue
                    elif vs_i["ctrl_start_time"] + vs_i["response_time"] > rnd:
                        debug_print(debug_enable,"Node index",vs_i.index,"has ol_start_time",vs_i["ol_start_time"],\
                              "    control should be performed at rnd",\
                              vs_i["ctrl_start_time"] + vs_i["response_time"],"    current round",rnd)
                        vs_i["plan_ctrl_rnd"] = vs_i["ctrl_start_time"] + vs_i["response_time"]
                        continue
                    else: # Start control strategy
                        if vs_i.index in exempt_list:
                            continue
                        if vs_i["shed_ratio"] >= 1:
                            vs_i["shed_ratio"] = 0.99
                        
                        
                        if ctrl_type == "local_fixed_rate":
                            if vs_i["start_shedding"] == False:
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
                            
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            vs_i["load"] = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            vs_i["last_rnd_load"] = vs_i["load"]
                        
                        elif ctrl_type == "local_non_adjust":
                            if vs_i["start_shedding"] == False:
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
#                                 print("Node index",vs_i.index,"   capacity",vs_i["capacity"],"   load",vs_i["load"]\
#                                       ,"   round remaining for control",rnd_to_shed,"   shed ratio",vs_i["shed_ratio"],\
#                                       "   accu",vs_i["accu_ol"],"   accu_th",vs_i["accu_th"])
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            load_after_shed = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            if load_after_shed < vs_i["capacity"]:
                                vs_i["load"] = vs_i["capacity"]
                            else:
                                vs_i["load"] = load_after_shed 
                            vs_i["last_rnd_load"] = vs_i["load"]
                            if vs_i["load"] <= vs_i["capacity"]:
                                debug_print(debug_enable,"Node index",vs_i.index,\
                                            "successfully stops overloading at round",rnd)
                                vs_i["start_shedding"] = False
                        
                        elif ctrl_type == "local_adjust":
                            if vs_i["start_shedding"] == False: # start load shedding
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                vs_i["start_shedding"] = True
                                vs_i["last_rnd_load"] = vs_i["load"]
                            
                            # check if additional events happen
                            if vs_i["last_rnd_load"] < vs_i["load"] and vs_i["start_shedding"] == True:
                                graph["new_event_if"] += 1
                                if vs_i["new_event_affect"] == False:
                                    vs_i["new_event_affect"] = True
                                    graph["new_event_if_vs"] += 1
                                debug_print(debug_enable,"Node index "+str(vs_i.index),"needs adjust   ->   capacity",\
                                             vs_i["capacity"],"   load",vs_i["load"],"   last round load",\
                                             vs_i["last_rnd_load"],"   accu",vs_i["accu_ol"],"   accu_th",vs_i["accu_th"])
                                each_rnd_accu = vs_i["load"] - vs_i["capacity"]
                                remain_ol = vs_i["accu_th"] - vs_i["accu_ol"]
                                rnd_to_shed = int(remain_ol/each_rnd_accu) + 1
                                vs_i["shed_ratio"] = 1 - math.pow(vs_i["capacity"]/vs_i["load"], 1/rnd_to_shed)
                                debug_print(debug_enable,"Node index "+str(vs_i.index),"has",rnd_to_shed,"rnds left",\
                                            "new ratio",vs_i["shed_ratio"])
                            
                            if vs_i["shed_ratio"] > graph["shed_ratio_th"]:
                                vs_i["shed_ratio"] = graph["shed_ratio_th"]
                                debug_print(debug_enable,"Node index",vs_i.index,"   adjust shed ratio to",\
                                            graph["shed_ratio_th"])
                            
                            load_after_shed = vs_i["load"] * (1 - vs_i["shed_ratio"])
                            if load_after_shed < vs_i["capacity"]:
                                vs_i["load"] = vs_i["capacity"]
                            else:
                                vs_i["load"] = load_after_shed 
#                             print("Current load:",vs_i["load"])
                            vs_i["last_rnd_load"] = vs_i["load"]
                            if vs_i["load"] <= vs_i["capacity"]:
                                debug_print(debug_enable,"Node index",vs_i.index,"successfully stops overloading at round"\
                                            ,rnd,"   current load",vs_i["load"])
                        
                        else:
                            continue
        
        remain_node_list.append(graph["remain_node"])
        rnd += 1
    
    debug_print(debug_enable,"Critical rounds at:",percentage_list)
    for i in range(1,len(percentage_list)):
        rnd_rum = percentage_list[i] - percentage_list[i-1]
        percentage_rnd_list.append(rnd_rum)
    
    if plot_enable == True:
        napf.myPlotFig([], remain_node_list, "Remaining Node Number", "Round", "Unit of Node", yMax=100, yMin=0)
    plot_list.append(remain_node_list)
    plot_list2.append(percentage_rnd_list)


def cascading_test(graph,enable_ctrl,stg_type,ini_idx,exempt_list,mode,value_list,show_detail=False):
    graph_initialization(graph)
    show_graph_basic(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.1)
    
    plot_list = []
    plot_list2 = []
    
    for value in value_list: # Start multiple loop cascading test
        copy_graph = deepcopy(graph)
        if mode == "max_delay":
            set_max_delay(copy_graph, value)
        elif mode == "tolerance":
            set_tolerance(copy_graph, value)
        elif mode == "load_shed":
            set_shed_ratio(copy_graph, value)
        elif mode == "response_time":
            set_response_time(copy_graph, value)
        elif mode == "ctrl_th":
            set_ctrl_th(copy_graph, value)
        
        print("<-----",mode+":",value,"----->")
        initial_node_event(copy_graph,ini_idx) #17
        cascading_process(copy_graph,enable_ctrl,stg_type,True,False,exempt_list,plot_list,plot_list2)
        cal_remaining_load(copy_graph)
        show_shutdown_same_rnd_pcnt(copy_graph)
        show_overload_round(copy_graph,show_detail)
        display_seperate_line()
        
    if mode == "max_delay":
        label_list = []
        for value in value_list:
            temp_text = "$D_0$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different permitted time-delay"
    elif mode == "tolerance":
        label_list = []
        for value in value_list:
            temp_text = "R="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different Tolerance Ratio"
    elif mode == "load_shed":
        label_list = []
        for value in value_list:
            temp_text = "S="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different load shedding amount"
    elif mode == "response_time":
        label_list = []
        for value in value_list:
            temp_text = "$T_R$="+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of different response time ($T_R$)"
    elif mode == "ctrl_th":
        label_list = []
        for value in  value_list:
            if value >= 0 and value < 1:
                temp_text = "Start control at "+str(int(value*100))+"% cascading"
            elif value >= 1:
                temp_text = "Shed after round "+str(value)
            label_list.append(temp_text)
        graph_title = "Effect of shedding moment"
    
    x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
    y_label2 = "required number of rounds"
    napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
    napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_strategy_test(graph,enable_ctrl,stg_list,ini_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    show_graph_basic(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.2)
    set_max_shed_ratio(graph, 0.3)
    
    plot_list = []
    plot_list2 = []
    label_list = []
    
    for stg in stg_list:
        label_list.append(stg)
        copy_graph = deepcopy(graph)
        print("<-----","strategy:",stg,"----->")
        initial_node_event(copy_graph,ini_idx)
        cascading_process(copy_graph,enable_ctrl,stg,True,False,exempt_list,plot_list,plot_list2)
        cal_remaining_load(copy_graph)
        show_shutdown_same_rnd_pcnt(copy_graph)
        show_overload_round(copy_graph,show_detail)
        show_new_event_interference(copy_graph)
        display_seperate_line()
    
    if show_graph == True:
        graph_title = "Effect of different strategies"
        x_label2 = "percentage of node shutdown "+"("+r"$\times 0.1$)"
        y_label2 = "required number of rounds"
        napf.myPlotFig_List(plot_list, label_list, graph_title, "round", "number of remaining nodes", ymax=120, ymin=0)
        napf.myPlotFig_List(plot_list2, label_list, graph_title, x_label2, y_label2, True, ymax=20, ymin=0)


def cascading_newevent_test(graph,enable_ctrl,stg,top_idx,show_graph,exempt_list=[],show_detail=False):
    graph_initialization(graph)
    
    set_max_delay(graph, 2.5)
    set_tolerance(graph, 1.2)
    set_response_time(graph, 1)
    set_shed_ratio(graph, 0.3)
    set_ctrl_th(graph, 0.1)
    
    result_list = []
    
    for i in range(0,top_idx):
        copy_graph = deepcopy(graph)
        initial_node_event(copy_graph,i)
        cascading_process(copy_graph,enable_ctrl,stg,False,False)
        result_list.append(copy_graph["new_event_if"])
    
    result = 0
    for value in result_list:
        if value != 0:
            result += 1
    
    print(top_idx,"cases tested. Strategy:",stg," -> ",result,"cases have new events observed.")

    
myGraph = ig.Graph.Watts_Strogatz(1, 60, 2, 0.15)
for v in myGraph.vs:
    v["name"] = str(v.index + 1) #here the name is type "string"

strategy_list = ["local_fixed_rate","local_non_adjust","local_adjust"]
# show_node_neighbors(myGraph, 9)
# plotGraph(myGraph)
# plotGraph(myGraph, "System_60_nodes.png")

# cascading_test(myGraph,False,"local_fixed_rate",17,[],"max_delay",[5,4,3,2,1])
# cascading_test(myGraph,False,"local_fixed_rate",17,[],"tolerance",[1.3,1.25,1.2,1.15,1.1,1.05,1.0])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [],"load_shed", [0.4,0.3,0.24,0.2,0.1])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [], "response_time", [4,3,2,1,0])
# cascading_test(myGraph, True, "local_fixed_rate", 17, [], "ctrl_th", [0.3,0.2,0.1,0.0])

cascading_strategy_test(myGraph, True, strategy_list, 22, show_graph=True)
# cascading_newevent_test(myGraph, True, "local_fixed_rate", 60, False)